﻿Imports BEC

Public Class clsTranslate

    Public Shared Function TranslateCategoryFunctions(sCategory As String, oFunctions As List(Of String)) As clsCategoryFunctions
        Dim oCat As New clsCategory(0, sCategory, Nothing)
        Dim oFunctionList As New List(Of clsFunction)

        For Each s As String In oFunctions
            Dim oFunction As New clsFunction(0, s, Nothing)
            oFunctionList.Add(oFunction)
        Next

        Return New clsCategoryFunctions()
    End Function
    Public Shared Function TranslateCategoriesFunctions(oCategories As List(Of String), oFunctions As List(Of List(Of String))) As List(Of clsCategoryFunctions)

        Dim oCatsFus As New List(Of clsCategoryFunctions)
        For i As Integer = 0 To oCategories.Count - 1
            Dim sCategory As String = oCategories(i)
            Dim oCat As New clsCategory(0, sCategory, Nothing)
            Dim oFunctionList As New List(Of clsFunction)
            For Each s As String In oFunctions(i)
                Dim oFunction As New clsFunction(0, s, Nothing)
                oFunctionList.Add(oFunction)
            Next
            Dim oCatfus As New clsCategoryFunctions()
            oCatsFus.Add(oCatfus)
        Next
        Return oCatsFus
    End Function

    Public Shared Function TranslateCategoryFunctions(oCatFus As clsCategoryFunctions) As List(Of Object)


        Dim oFunctions As New List(Of String)
        For Each f As clsFunction In oCatFus.Functions
            oFunctions.Add(f.Name)
        Next

        Dim o As List(Of Object)

        o.Add(oFunctions)
        Return o

    End Function
    Public Shared Function TranslateCategoriesFunctions(oCatsFus As List(Of clsCategoryFunctions)) As List(Of Object)
        Dim oCategories As New List(Of String)
        Dim oFunctions As New List(Of List(Of String))
        For Each a As clsCategoryFunctions In oCatsFus

            Dim oFunctieList As New List(Of String)
            For Each b As clsFunction In a.Functions
                oFunctieList.Add(b.Name)
            Next
            oFunctions.Add(oFunctieList)
        Next

        Dim o As List(Of Object)

        o.Add(oFunctions)
        Return o

    End Function

End Class
