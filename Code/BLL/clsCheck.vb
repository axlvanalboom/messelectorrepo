﻿Imports BEC

Public Class clsCheck

    'DOUBLES
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oList"></param>
    ''' <returns>true if the list contains a double</returns>
    Public Shared Function Check_Dubbels(oList As List(Of Object)) As Boolean
        For i As Integer = 0 To oList.Count - 2
            For j As Integer = i + 1 To oList.Count - 1
                If oList(i) = oList(j) Then
                    Return True
                End If
            Next
        Next
        Return False
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oList"></param>
    ''' <returns>true if the list contains a double</returns>
    Public Shared Function Check_Dubbels(oList As List(Of String)) As Boolean
        For i As Integer = 0 To oList.Count - 2
            For j As Integer = i + 1 To oList.Count - 1
                If oList(i) = oList(j) Then
                    Return True
                End If
            Next
        Next
        Return False
    End Function

    'CategoryFunction
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sCategory"></param>
    ''' <param name="oFunctions"></param>
    ''' <returns>true if everything is okay</returns>
    Public Shared Function Check_CategoryFunctions(sCategory As String, oFunctions As List(Of String)) As Boolean

        If sCategory Is Nothing OrElse sCategory = "" Then Return False
        If oFunctions Is Nothing OrElse oFunctions.Count = 0 Then Return False

        For Each s As String In oFunctions
            If s Is Nothing OrElse s = "" Then Return False
        Next

        If (Check_Dubbels(oFunctions)) Then Return False

        Return True

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oCatFus"></param>
    ''' <returns>true if everything is okay</returns>
    Public Shared Function Check_CategoryFunctions(oCatFus As clsCategoryFunctions) As Boolean
        If oCatFus Is Nothing Then Return False
        Dim o As List(Of Object) = clsTranslate.TranslateCategoryFunctions(oCatFus)
        Return Check_CategoryFunctions(o(0), o(1))
    End Function

    'CategoriesFunctions
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oCategories"></param>
    ''' <param name="oFunctions"></param>
    ''' <returns>true if everything is okay</returns>
    Public Shared Function Check_CategoriesFunctions(oCategories As List(Of String), oFunctions As List(Of List(Of String))) As Boolean

        If oCategories Is Nothing OrElse oCategories.Count = 0 Then Return False
        For Each s As String In oCategories
            If s Is Nothing OrElse s = "" Then Return False
        Next

        If (clsCheck.Check_Dubbels(oCategories)) Then Return False

        If oFunctions Is Nothing OrElse oFunctions.Count = 0 Then Return False
        For Each o As List(Of String) In oFunctions
            If o Is Nothing OrElse o.Count = 0 Then Return False
            For Each s As String In o
                If s Is Nothing OrElse s = "" Then Return False
            Next
            If (clsCheck.Check_Dubbels(o)) Then Return False
        Next

        If Not (oCategories.Count = oFunctions.Count) Then Return False

        Return True

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oCategories"></param>
    ''' <param name="oFunctions"></param>
    ''' <returns>true if everything is okay</returns>
    Public Shared Function Check_CategoriesFunctions(oCatsFus As List(Of clsCategoryFunctions)) As Boolean
        If oCatsFus Is Nothing OrElse oCatsFus.Count = 0 Then Return False
        Dim o As List(Of Object) = clsTranslate.TranslateCategoriesFunctions(oCatsFus)
        Return Check_CategoriesFunctions(o(0), o(1))
    End Function

End Class
