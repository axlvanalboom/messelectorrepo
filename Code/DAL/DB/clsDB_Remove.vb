﻿Imports System.Data.SqlClient

Partial Public Class clsDB

    Private Shared Sub DeleteAllDataFromDB()
        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = 
                "
                delete from CasCatFuLink;
                delete from MeCatFuLink;
                delete from AspNetUserRoles;
                delete from ComMeCasSeAcLink;  
                delete from Content;
                delete from Company;
                delete from MesSoftware;
                delete from Activity;
                delete from Category;
                delete from Functions;
                delete from Cases;
                delete from Sector;          
                "

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try
    End Sub


    Public Shared Sub RemoveSupplier(sSupplier As String)

        If sSupplier Is Nothing OrElse sSupplier = "" Then Exit Sub

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "delete from Supplier where suName = @suName"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@suName", sSupplier))
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

End Class
