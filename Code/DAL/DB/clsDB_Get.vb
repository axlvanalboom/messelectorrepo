﻿Imports System.Data.SqlClient

Partial Public Class clsDb

    ''' <summary>
    ''' Get all the possible suppliers
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetAllSuppliersFull()
        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "select suName,suDescription, suWebsite, suPdf from Supplier"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDT As New DataTable
            oDA.Fill(oDT)

            Return oDT

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Get all the possible suppliers names
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetAllSuppliersNames()
        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "select suName from Supplier"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDT As New DataTable
            oDA.Fill(oDT)

            Return oDT

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Get all the possible category names
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetAllCategoryNames()
        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "select catName from Category"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDT As New DataTable
            oDA.Fill(oDT)

            Return oDT

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    Public Shared Function GetAllMesSoftwareNames() As DataTable
        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "select meName from MesSoftware;"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDT As New DataTable
            oDA.Fill(oDT)

            Return oDT

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <returns>suName,meDescription,meWebsite,mePdf</returns>
    Public Shared Function GetMesSoftwareInfo(sMes As String) As List(Of Object)

        If (sMes Is Nothing) Or sMes = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)
        Dim oData As New List(Of Object)
        Try

            oCnn.Open()

            Dim sSQL As String = "
                select meName, suName, meDescription, meWebsite, mePdf from MesSoftware
                join Supplier on suId=mesuSupplier
                where meName = @meName;"

            sSQL += "                    
                select catName, fuName from MeCatFu_link
                join MesSoftware on meId=mcflMes
                join Category on catId=mcflCategory
                join Functions on fuId = mcflFunction
                where meName = @meName;"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            Dim oDT As DataTable = oDS.Tables.Item(0)

            If oDT.Rows.Count = 0 Then Return Nothing
            For i As Integer = 0 To oDT.Columns.Count - 1
                If Not IsDBNull(oDT.Rows(0).Item(i)) Then
                    oData.Add(oDT.Rows(0).Item(i))
                Else
                    oData.Add(Nothing)
                End If
            Next

            oDT = oDS.Tables.Item(1)
            'Dim oCatFus As clsCatFus
            'Dim sPrevCat As String
            'For Each r As DataRow In oDT.Rows
            '    Dim sCat As String = r.Item(0)
            '    If Not (sCat = sPrevCat) Then
            '        If sPrevCat IsNot Nothing Then oData.Add(oCatFus)
            '        oCatFus = New clsCatFus(sCat)
            '        sPrevCat = sCat
            '    End If
            '    oCatFus.AddFunction(r.Item(1))
            'Next
            'oData.Add(oCatFus) 'laatsten ook toevoegen
            Return oData

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try
    End Function

End Class
