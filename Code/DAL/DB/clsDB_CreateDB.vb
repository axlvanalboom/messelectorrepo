﻿Imports System.Data.SqlClient

Partial Public Class clsDB

    'creating is without the links between the tables!!!

    Public Shared Sub CreateFullDb()
        CreateCompany()
        CreateActivity()
        CreateCategory()
        CreateSector()
        CreateFunction()
        CreateMesSoftware()
        CreateCase()
        CreateContent()
        CreateMeCatFuLink()
        CreateCasCatFuLink()
        CreateComMeCasSeAcLink()
    End Sub

    Public Shared Sub CreateCompany()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Company (
                comId int primary key identity(1,1),
				comName varchar(MAX) not null,
				comDescription varchar(MAX),
				comWebsite varchar(MAX),
				comContactEmail varchar(MAX),
				comContactTelefoon varchar(MAX),
                comContent nvarchar(MAX),
                comPublic bit
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateActivity()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Activity (
                acId int primary key identity(1,1),
				acName varchar(MAX) not null,
                acDescription varchar(MAX)
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateCategory()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Category (
                catId int primary key identity(1,1),
				catName varchar(MAX) not null,
                catDescription varchar(MAX)
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateSector()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Sector (
                seId int primary key identity(1,1),
				seName varchar(MAX) not null,   
                seDescription varchar(MAX)
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateFunction()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Functions (
                fuId int primary key identity(1,1),
				fuName varchar(MAX) not null,
                fuDescription varchar(MAX)
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateMesSoftware()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table MesSoftware (
                meId int primary key identity(1,1),
				meName varchar(MAX) not null,
				meDescription varchar(MAX),
				meWebsite varchar(MAX),
                meContent nvarchar(MAX),
                mePublic bit
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateCase()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Cases (
                casId int primary key identity(1,1),
				casName varchar(MAX) not null,
				casDescription varchar(MAX),
				casWebsite varchar(MAX),
                casContent nvarchar(MAX),
                casPublic bit
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateContent()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table Content (
                conId int primary key identity(1,1),
				conContentPath varchar(MAX) not null,
				conPublic bit not null
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateMeCatFuLink()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table MeCatFuLink(
                mcflId int primary key identity(1,1),
				mcflMes int not null,
				mcflCategory int not null,
				mcflFunction int not null
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateCasCatFuLink()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table CasCatFuLink (
                ccflId int primary key identity(1,1),
				ccflCase int not null,
				ccflCategory int not null,
				ccflFunction int not null
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub
    Public Shared Sub CreateComMeCasSeAcLink()

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim sSQL As String = "create table ComMeCasSeAcLink(
                cmcsalId int primary key identity(1,1),
				cmcsalCompany int not null,
				cmcsalActivity int not null,
				cmcsalMesSoftware int not null,
				cmcsalSector int not null,
				cmcsalCase int not null
                );"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub


End Class
