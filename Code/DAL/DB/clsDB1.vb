﻿Imports System.Data.SqlClient
Imports System.Data
Imports BEC
Imports clsCheck

Partial Class clsDB

#Region "Algemene dingen"

    ''' <summary>
    ''' Add an extra company to the DB
    ''' </summary>
    ''' <param name="sCompany">Name of the new company</param>
    ''' <param name="sDescription"></param>
    ''' <param name="sWebsite"></param>
    ''' <param name="sContactEmail"></param>
    ''' <param name="sContactTelephone"></param>
    ''' <returns>return true if the company is added to the DB</returns>
    Public Shared Function AddCompany(sCompany As String, Optional sDescription As String = Nothing,
                                          Optional sWebsite As String = Nothing,
                                          Optional sContactEmail As String = Nothing,
                                          Optional sContactTelephone As String = Nothing) As Boolean?

        If sCompany Is Nothing OrElse sCompany = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xCompany As Boolean? = Check_Company(sCompany, oCnn)
            If (xCompany Is Nothing) Then Return Nothing
            If (xCompany = True) Then Return False

            Dim sSQL As String = "insert into Company (comName,comDescription,comWebsite,comContactEmail,comContactTelefoon) 
                values (@comName,@comDescription,@comWebsite,@comContactEmail,@comContactTelefoon);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
            oCmd.Parameters.Add(New SqlParameter("@comDescription", If(sDescription, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@comWebsite", If(sWebsite, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@comContactEmail", If(sContactEmail, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@comContactTelefoon", If(sContactTelephone, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Add a new Mes-software to the DB
    ''' </summary>
    ''' <param name="sMes">Name of the software</param>
    ''' <param name="sCompany">Name of the company</param>
    ''' <param name="sDescription">Short description of the software</param>
    ''' <param name="sWebsite">Websitelink</param>
    ''' <returns>return true if the software is added to the DB</returns>
    Public Shared Function AddMesSoftware(sMes As String, Optional sDescription As String = Nothing,
                                         Optional sWebsite As String = Nothing) As Boolean?

        If sMes Is Nothing OrElse sMes = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xMes As Boolean? = Check_MesSoftware(sMes, oCnn)
            If (xMes Is Nothing) Then Return Nothing
            If (xMes = True) Then Return False

            Dim sSQL As String = "insert into MesSoftware (meName, meDescription, meWebsite) 
                        values (@meName,@meDescription,@meWebsite);"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@meDescription", If(sDescription, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@meWebsite", If(sWebsite, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Add a new category to the DB
    ''' </summary>
    ''' <param name="sCategory">name of the new category</param>
    ''' <param name="sDescription">description of the Category</param>
    ''' ''' <returns>return true if the category is added to the DB</returns>
    Public Shared Function AddCategory(sCategory As String, Optional sDescription As String = Nothing) As Boolean?

        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xCategory As Boolean? = Check_Category(sCategory, oCnn)
            If (xCategory Is Nothing) Then Return Nothing
            If (xCategory = True) Then Return False

            Dim sSQL As String = "insert into Category (catName,catDescription) values (@catName,@catDescription);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
            oCmd.Parameters.Add(New SqlParameter("@catDescription", If(sDescription, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Add a new function to the DB
    ''' </summary>
    ''' <param name="sFunction">name of the new function</param>
    ''' <param name="sDescription">description of the function</param>
    ''' <returns>return true if the function is added to the DB</returns>
    Public Shared Function AddFunction(sFunction As String, Optional sDescription As String = Nothing) As Boolean?

        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xFunction As Boolean? = Check_Function(sFunction, oCnn)
            If (xFunction Is Nothing) Then Return Nothing
            If (xFunction = True) Then Return False

            Dim sSQL As String = "insert into Functions (fuName, fuDescription) values (@fuName,@fuName);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))
            oCmd.Parameters.Add(New SqlParameter("@fuDescription", If(sDescription, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Add a new sector to the DB
    ''' </summary>
    ''' <param name="sSector">name of the new sector</param>
    ''' <param name="sDescription">description of the sector</param>
    ''' <returns>return true if the sector is added to the DB</returns>
    Public Shared Function AddSector(sSector As String, Optional sDescription As String = Nothing) As Boolean?

        If sSector Is Nothing OrElse sSector = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xSector As Boolean? = Check_Sector(sSector, oCnn)
            If (xSector Is Nothing) Then Return Nothing
            If (xSector = True) Then Return False

            Dim sSQL As String = "insert into Sector(seName,seDescription) values (@seName, @seDescription);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@seDescription", If(sDescription, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' add a new case to the Db
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sDescription">description of the case</param>
    ''' <param name="sWebsite">website-link of the case</param>
    ''' <returns>return true if the case is added to the DB</returns>
    Public Shared Function AddCase(sCase As String, Optional sDescription As String = Nothing,
                                  Optional sWebsite As String = Nothing) As Boolean?

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xCase As Boolean? = Check_Case(sCase, oCnn)
            If (xCase Is Nothing) Then Return Nothing
            If (xCase = True) Then Return False

            Dim sSQL As String = "insert into Cases(casName,casDescription,casWebsite) 
                    values (@casName,@casDescription,@casWebsite);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            oCmd.Parameters.Add(New SqlParameter("@casDescription", If(sDescription, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@casWebsite", If(sWebsite, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' add a new activity to the Db
    ''' </summary>
    ''' <param name="sActivity">activity name</param>
    ''' <param name="sDescription">description of the activity</param>
    ''' <returns>return true if the activity is added to the DB</returns>
    Public Shared Function AddActivity(sActivity As String, Optional sDescription As String = Nothing,
                                  Optional sWebsite As String = Nothing) As Boolean?

        If sActivity Is Nothing OrElse sActivity = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xActivity As Boolean? = Check_Activity(sActivity, oCnn)
            If (xActivity Is Nothing) Then Return Nothing
            If (xActivity = True) Then Return False

            Dim sSQL As String = "insert into Activity(acName,acDescription) 
                    values (@acName,@acDescription);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))
            oCmd.Parameters.Add(New SqlParameter("@acDescription", If(sDescription, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' add a new role to the Db
    ''' </summary>
    ''' <param name="sContent">role name</param>
    ''' <param name="sPublic">description of the roles</param>
    ''' <returns>return true if the roles is added to the DB</returns>
    Public Shared Function AddRole(sRole As String, Optional sDescription As String = Nothing) As Boolean?

        If sRole Is Nothing OrElse sRole = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xRole As Boolean? = Check_Role(sRole, oCnn)
            If (xRole Is Nothing) Then Return Nothing
            If (xRole = True) Then Return False

            Dim sSQL As String = "insert into Roles(roName,roDescription) 
                    values (@roName,@roDescription);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@roName", sRole))
            oCmd.Parameters.Add(New SqlParameter("@roDescription", If(sDescription, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' add a new contentpath to the Db
    ''' </summary>
    ''' <param name="sContentPath">contentpath</param>
    ''' <param name="sPublic">description of the roles</param>
    ''' <returns>return true if the contentpath is added to the DB</returns>
    Public Shared Function AddContentPath(sContentPath As String, Optional sPublic As Boolean = False) As Boolean?

        If sContentPath Is Nothing OrElse sContentPath = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xContent As Boolean? = Check_ContentPath(sContentPath, oCnn)
            If (xContent Is Nothing) Then Return Nothing
            If (xContent = True) Then Return False

            Dim sSQL As String = "insert into Content(conContentPath,conPublic) 
                    values (@conContentPath,@conPublic);"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@conContentPath", sContentPath))
            oCmd.Parameters.Add(New SqlParameter("@conPublic", sPublic))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

#End Region


#Region "MeCatFu"

    ''' <summary>
    ''' Add the link between a mes software, a category and a function
    ''' </summary>
    ''' <param name="sMes">mes name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    Public Shared Function AddMesCategoryFunctionLink(sMes As String, sCategory As String, sFunction As String) As Boolean?

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing
        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            Dim xCheck As Boolean? = Check_MesCategoryFunctionExist(sMes, sCategory, sFunction, oCnn)
            If (xCheck Is Nothing) Then Return Nothing
            If (xCheck = False) Then Return False

            ' de code hierna wordt enkel uitgevoerd als de mes, de category en de functie bestaan!

            'control of de link er nog niet in zit!
            xCheck = Check_MesCategoryFunctionLink(sMes, sCategory, sFunction, oCnn)
            If (xCheck Is Nothing) Then Return Nothing
            If (xCheck = True) Then Return False

            'toevoegen
            Dim sSQL As String = "              
                insert into MeCatFuLink (mcflMes, mcflCategory, mcflFunction)
                values (
                (select meId from MesSoftware where meName = @meName),
                (select catId from Category where catName = @catName),
                (select fuId from Functions where fuName = @fuName));"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
            oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    Public Shared Function AddMesCategoryFunctionsLink(sMes As String, oCatFus As clsCategoryFunctions) As Boolean
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(oCatFus)) Then Return False
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoryFunctions(oCatFus)
        Return AddMesCategoryFunctionsLink(sMes, oList(0), oList(1))
    End Function
    ''' <summary>
    ''' Add the link between a mes software, a category and a couple functions
    ''' </summary>
    ''' <param name="sMes">mes name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">list containing all the function names</param>
    ''' <returns>true if the link is added to the DB</returns>
    Public Shared Function AddMesCategoryFunctionsLink(sMes As String, sCategory As String, oFunctions As List(Of String)) As Boolean

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            If (Not Check_MesCategoryFunctionsExist(sMes, sCategory, oFunctions, oCnn)) Then Return False 'bestaat de mes de category en de functies
            Dim xCheck As Boolean? = Check_MesCategoryFunctionsLink(sMes, sCategory, oFunctions, oCnn) 'bestaat de link nog niet?
            If (xCheck Is Nothing OrElse xCheck) Then Return False
            'toevoegen

            Dim sSQL As String = ""
            For i As Integer = 0 To oFunctions.Count - 1
                sSQL += "
                    insert into MeCatFuLink (mcflMes, mcflCategory, mcflFunction)
                    values (
                    (select meId from MesSoftware where meName = @meName),
                    (select catId from Category where catName = @catName),
                    (select fuId from Functions where fuName = @fuName" + i.ToString + "));"
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

            For i As Integer = 0 To oFunctions.Count - 1
                Dim sParaName As String = "@fuName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
            Next

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="oCatsFus"></param>
    ''' <returns>true if all the links are added to the db</returns>
    Public Shared Function AddMesCategoriesFunctionsLink(sMes As String, oCatsFus As List(Of clsCategoryFunctions)) As Boolean
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCatsFus)) Then Return False
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoriesFunctions(oCatsFus)
        Return AddMesCategoriesFunctionsLink(sMes, oList(0), oList(1))
    End Function
    ''' <summary>
    ''' Add the link between a mes software, a category and a couple functions
    ''' </summary>
    ''' <param name="sMes">mes name</param>
    ''' <param name="oCategories">list of the category names</param>
    ''' <param name="oFunctions">list containing all the function names</param>
    ''' <returns>true if all the links are added to the db</returns>
    Public Shared Function AddMesCategoriesFunctionsLink(sMes As String, oCategories As List(Of String), oFunctions As List(Of List(Of String))) As Boolean

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            If (Not Check_MesCategoriesFunctionsExist(sMes, oCategories, oFunctions, oCnn)) Then Return False
            Dim xCheck As Boolean? = Check_MesCategoriesFunctionsLink(sMes, oCategories, oFunctions, oCnn) 'bestaat de link nog niet?
            If (xCheck Is Nothing OrElse xCheck) Then Return False

            Dim sSQL As String = ""
            For i As Integer = 0 To oCategories.Count - 1
                For j As Integer = 0 To oFunctions(i).Count - 1
                    sSQL += "
                    insert into MeCatFuLink (mcflMes, mcflCategory, mcflFunction)
                    values (
                    (select meId from MesSoftware where meName = @meName),
                    (select catId from Category where catName = @catName" + i.ToString + "),
                    (select fuId from Functions where fuName = @fuName" + i.ToString + "_" + j.ToString + "));"
                Next
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oCategories.Count - 1
                Dim sParaName As String = "@catName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
            Next
            For i As Integer = 0 To oFunctions.Count - 1
                For j As Integer = 0 To oFunctions(i).Count - 1
                    Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
                Next
            Next

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

#End Region

#Region "CasCatFu"

    '---------------------------------------------------------------------------------------------------------------------------------------------------

    ''' <summary>
    ''' Add a link between a case, a category and a function
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    ''' <returns>true if the link is added to the link</returns>
    Public Shared Function AddCaseCategoryFunctionLink(sCase As String, sCategory As String, sFunction As String) As Boolean

        'primary check
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If sCategory Is Nothing OrElse sCategory = "" Then Return False
        If sFunction Is Nothing OrElse sFunction = "" Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            If (Not Check_CaseCategoryFunctionExist(sCase, sCategory, sFunction, oCnn)) Then Return False 'bestaat de case, de category en de functie?

            'extra controle:
            ' een case behoort altijd bij een Mes!
            ' dus de aangegeven category-functies moeten ook bij deze MES aanwezig zijn!
            ' --> extra check
            If (Not Check_CaseCategoryFunctionBelongToMes(sCase, sCategory, sFunction, oCnn)) Then Return False

            Dim xCheck As Boolean? = Check_CaseCategoryFunctionLink(sCase, sCategory, sFunction, oCnn)
            If (xCheck OrElse xCheck Is Nothing) Then Return False ' zit de link er nog niet in?

            'toevoegen van de link
            Dim sSQL As String = "              
                insert into CasCatFuLink (ccflCase, ccflCategory, ccflFunction)
                values (
                (select casId from Cases where casName = @casName),
                (select catId from Category where catName = @catName),
                (select fuId from Functions where fuName = @fuName));"
            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
            oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Add the link between a case software, a category and a couple functions
    ''' </summary>
    ''' <param name="sCase">mes name</param>
    ''' <param name="oCatFus">containing the category and the associated functions</param>
    ''' <returns>true if the link is added to the DB</returns>
    Public Shared Function AddCaseCategoryFunctionsLink(sCase As String, oCatFus As clsCategoryFunctions) As Boolean
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(oCatFus)) Then Return False
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoryFunctions(oCatFus)
        Return AddCaseCategoryFunctionsLink(sCase, oList(0), oList(1))
    End Function
    ''' <summary>
    ''' Add a link between a case, a category and a couple functions
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">list containing all the function names</param>
    ''' <returns>true if the link is added to the link</returns>
    Public Shared Function AddCaseCategoryFunctionsLink(sCase As String, sCategory As String, oFunctions As List(Of String)) As Boolean

        'primary check
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            If (Not Check_CaseCategoryFunctionsExist(sCase, sCategory, oFunctions, oCnn)) Then Return False 'bestaat de case, de category en de functie?
            If (Not Check_CaseCategoryFunctionsBelongToMes(sCase, sCategory, oFunctions, oCnn)) Then Return False

            Dim xCheck As Boolean? = Check_CaseCategoryFunctionsLink(sCase, sCategory, oFunctions, oCnn) ' zit de link er nog niet in?
            If (xCheck OrElse xCheck Is Nothing) Then Return False

            'effectief toevoegen
            Dim sSQL As String = ""
            For i As Integer = 0 To oFunctions.Count - 1
                sSQL += "
                    insert into CasCatFuLink (ccflCase, ccflCategory, ccflFunction)
                    values (
                    (select casId from Cases where casName = @casName),
                    (select catId from Category where catName = @catName),
                    (select fuId from Functions where fuName = @fuName" + i.ToString + "));"
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

            For i As Integer = 0 To oFunctions.Count - 1
                Dim sParaName As String = "@fuName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
            Next

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Add the link between a case software, a couple categories and a associated functions
    ''' </summary>
    ''' <param name="sCase"></param>
    ''' <param name="oCatsFus"></param>
    ''' <returns>true if all the links are added to the DB</returns>
    Public Shared Function AddCaseCategoriesFunctionsLink(sCase As String, oCatsFus As List(Of clsCategoryFunctions)) As Boolean
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCatsFus)) Then Return False
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoriesFunctions(oCatsFus)
        Return AddCaseCategoriesFunctionsLink(sCase, oList(0), oList(1))
    End Function
    ''' <summary>
    ''' Add a link between a case, a category and a couple functions
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">list containing all the function names</param>
    ''' <returns>true if the link is added to the link</returns>
    Public Shared Function AddCaseCategoriesFunctionsLink(sCase As String, oCategories As List(Of String), oFunctions As List(Of List(Of String))) As Boolean

        'primary check
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            If (Not Check_CaseCategoriesFunctionsExist(sCase, oCategories, oFunctions, oCnn)) Then Return False 'bestaat de case, de category en de functie?
            If (Not Check_CaseCategoriesFunctionsBelongToMes(sCase, oCategories, oFunctions, oCnn)) Then Return False

            Dim xCheck As Boolean? = Check_CaseCategoriesFunctionsLink(sCase, oCategories, oFunctions, oCnn) ' zit de link er nog niet in?
            If (xCheck OrElse xCheck Is Nothing) Then Return False

            Dim sSQL As String = ""
            For i As Integer = 0 To oFunctions.Count - 1
                sSQL += "
                    
                    (select catId from Category where catName = @catName),
                    (select fuId from Functions where fuName = @fuName" + i.ToString + "));"
            Next
            For i As Integer = 0 To oCategories.Count - 1
                For j As Integer = 0 To oFunctions(i).Count - 1
                    sSQL += "
                    insert into CasCatFuLink (ccflCase, ccflCategory, ccflFunction)
                    values (
                    (select casId from Cases where casName = @casName),
                    (select catId from Category where catName = @catName" + i.ToString + "),
                    (select fuId from Functions where fuName = @fuName" + i.ToString + "_" + j.ToString + "));"
                Next
            Next


            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            For i As Integer = 0 To oCategories.Count - 1
                Dim sParaName As String = "@catName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
            Next
            For i As Integer = 0 To oFunctions.Count - 1
                For j As Integer = 0 To oFunctions(i).Count - 1
                    Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
                Next
            Next

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try
    End Function


#End Region

#Region "CompanyMesCaseSectorActivity"

    Public Shared Function AddComMeAcLink(sCompany As String, sMes As String, sActivity As String) As Boolean

        If sCompany Is Nothing OrElse sCompany = "" Then Return False
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If sActivity Is Nothing OrElse sActivity = "" Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            If (Not Check_ComMeCasSeAcExist(sCompany, sMes, Nothing, Nothing, sActivity, oCnn)) Then Return False
            If (Not Check_ComMeCasSeAc_NotLinked(sCompany, sMes, Nothing, Nothing, sActivity, oCnn)) Then Return False

            Dim sSQL As String = "
                    insert into ComMeCasSeAcLink(cmcsalCompany, cmcsalMesSoftware,cmcsalActivity) 
                    values (
                    (select comId from Company where comName = @comName),
                    (select meId from MesSoftware where meName = @meName)
                    (select acId from Activity where acName = @acName)
                    )"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

    Public Shared Function AddComMeSeAcLink(sCompany As String, sMes As String, sSector As String, sActivity As String) As Boolean

        If sCompany Is Nothing OrElse sCompany = "" Then Return False
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If sSector Is Nothing OrElse sSector = "" Then Return False
        If sActivity Is Nothing OrElse sActivity = "" Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            'If (Not Check_ComMeCasSeAcExist(sCompany, sMes, Nothing, sSector, sActivity, oCnn)) Then Return False
            'If (Not Check_ComMeCasSeAc_NotLinked(sCompany, sMes, Nothing, sSector, sActivity, oCnn)) Then Return False

            Dim sSQL As String = "
                    insert into ComMeCasSeAcLink(cmcsalCompany, cmcsalMesSoftware,cmcsalSector,cmcsalActivity) 
                    values (
                    (select comId from Company where comName = @comName),
                    (select meId from MesSoftware where meName = @meName),
                    (select seId from Sector where seName = @seName),
                    (select acId from Activity where acName = @acName)
                    )"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function
    Public Shared Function AddComMeCasSeAcLink(sCompany As String, sMes As String, sCase As String, sSector As String, sActivity As String) As Boolean

        If sCompany Is Nothing OrElse sCompany = "" Then Return False
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If sSector Is Nothing OrElse sSector = "" Then Return False
        If sActivity Is Nothing OrElse sActivity = "" Then Return False

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            'If (Not Check_ComMeCasSeAcExist(sCompany, sMes, sCase, sSector, sActivity, oCnn)) Then Return False
            'If (Not Check_Case_NotLinked(sCase, oCnn)) Then Return False
            'If (Not Check_ComMeCasSeAc_NotLinked(sCompany, sMes, sCase, sSector, sActivity, oCnn)) Then Return False

            Dim sSQL As String = "
                    insert into ComMeCasSeAcLink(cmcsalCompany, cmcsalMesSoftware,cmcsalSector,cmcsalActivity,cmcsalCase) 
                    values (
                    (select comId from Company where comName = @comName),
                    (select meId from MesSoftware where meName = @meName),
                    (select seId from Sector where seName = @seName),
                    (select acId from Activity where acName = @acName),
                    (select casId from Cases where casName = @casName)
                    )"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))

            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function



    ''' <summary>
    ''' Add the link between a mes software and a sector 
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="sSector">sector name</param>
    Public Shared Sub AddMesSectorLink(sMes As String, sSector As String)

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Exit Sub
        If sSector Is Nothing OrElse sSector = "" Then Exit Sub

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()




            ' de code hierna wordt enkel uitgevoerd als de mes en de sector bestaan 

            'controle of het er niet al in zit de link?
            If (Check_MesSectorLink(sMes, sSector, oCnn)) Then Exit Sub

            'toevoegen van de link
            Dim sSQL As String = "
                    insert into MeCasSe_link(mcslMes, mcslSector) 
                    values (
                    (select meId from MesSoftware where meName = @meName),       
                    (select seId from Sector where seName = @seName)
                    )"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Add the link between a mes software, a sector and a case
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="sSector">sector name</param>
    ''' <param name="sCase">case name </param>
    Public Shared Sub AddMesSectorCaseLink(sMes As String, sSector As String, sCase As String)

        'primary check
        If sMes Is Nothing OrElse sMes = "" OrElse sSector Is Nothing OrElse sSector = "" Then Exit Sub

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            ' checks:
            ' bestaat de mes en de sector wel?
            ' bestaat de case als die meegegeven is

            Dim sSQL As String = " 
                select meId from MesSoftware where meName = @meName;
                select seId from Sector where seName = @seName;
                select casId from Cases where casName = @casName;"
            Dim oCmd As SqlCommand
            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                    Exit Sub
                End If
            Next

            ' de code hierna wordt enkel uitgevoerd als de mes, de sector en de case bestaan

            ' controle: een case kan maar tot 1 mes software en sector horen!
            'If (Check_CaseLinked(sCase, oCnn)) Then Exit Sub

            'controle of het er niet al in zit de link?
            If (Check_MesSectorCaseLink(sMes, sSector, sCase, oCnn)) Then Exit Sub

            'toevoegen van de link

            sSQL = "
                    insert into MeCasSe_link(mcslMes, mcslCase,mcslSector) 
                    values (
                    (select meId from MesSoftware where meName = @meName),
                    (select casId from Cases where casName = @casName),
                    (select seId from Sector where seName = @seName)
                    )"
            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Add the link between a mes software, a sector and a couple cases
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="oCases">List containing all the case names </param>
    ''' <param name="sSector">sector name</param>
    Public Shared Sub AddMesSectorCasesLink(sMes As String, sSector As String, oCases As List(Of String))

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Exit Sub
        If sSector Is Nothing OrElse sSector = "" Then Exit Sub
        If oCases Is Nothing OrElse oCases.Count = 0 Then Exit Sub

        ' controle op dubbele functies
        For i As Integer = 0 To oCases.Count - 2
            For j As Integer = i + 1 To oCases.Count - 1
                If oCases(i) = oCases(j) Then
                    Exit Sub
                End If
            Next
        Next

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()


            Dim sSQL As String
            Dim oCmd As SqlCommand

            sSQL = " 
                select meId from MesSoftware where meName = @meName;
                select seId from Sector where seName = @seName;"

            For i As Integer = 0 To oCases.Count - 1
                sSQL = sSQL + "select casId from Cases where casName = @casName" + i.ToString + ";"
            Next

            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            For i As Integer = 0 To oCases.Count - 1
                Dim sParaName As String = "@casName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)))
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                    Exit Sub
                End If
            Next

            ' de code hierna wordt enkel uitgevoerd als de mes, de sector en alle cases bestaan 

            ' controle: een case kan maar tot 1 mes software en sector horen!
            If (Check_CasesLinked(oCases, oCnn)) Then Exit Sub ' elke case mag nog niet gelinkt zijn met een ander mes!

            'controle of het er niet al in zit de link?
            If (Check_MesSectorCasesLink(sMes, sSector, oCases, oCnn)) Then Exit Sub ' als er al een link is --> Stop

            'toevoegen van de link

            For i As Integer = 0 To oCases.Count - 1
                sSQL += "
                    insert into MeCasSe_link(mcslMes, mcslCase,mcslSector) 
                    values (
                    (select meId from MesSoftware where meName = @meName),
                    (select casId from Cases where casName = @casName" + i.ToString + "),
                    (select seId from Sector where seName = @seName)
                    )"
            Next

            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))

            For i As Integer = 0 To oCases.Count - 1
                Dim sParaName As String = "@casName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)))
            Next

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Add the link between a mes software, a couple sectors and a couple cases per sector
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="oSectors">List containing all the case names </param>
    ''' <param name="oCases">sector name</param>
    Public Shared Sub AddMesSectorsCasesLink(sMes As String, oSectors As List(Of String), oCases As List(Of List(Of String)))

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Exit Sub
        If oSectors Is Nothing OrElse oSectors.Count = 0 Then Exit Sub
        If oCases Is Nothing OrElse oCases.Count = 0 Then Exit Sub
        For Each oCase As List(Of String) In oCases
            If oCase Is Nothing OrElse oCase.Count = 0 Then Exit Sub
        Next
        If Not oSectors.Count = oCases.Count Then Exit Sub

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            Dim sSQL As String
            Dim oCmd As SqlCommand

            sSQL = "select meId from MesSoftware where meName = @meName;"

            For i As Integer = 0 To oSectors.Count - 1
                sSQL += "Select seId from Sector where seName = @seName" + i.ToString + ";"
            Next

            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    sSQL += "select casId from Cases where casName = @casName" + i.ToString + "_" + j.ToString + ";"
                Next
            Next

            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSectors.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSectors(i)))
            Next
            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    Dim sParaName As String = "@casName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)(j)))
                Next
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                    Exit Sub
                End If
            Next

            ' de code hierna wordt enkel uitgevoerd als de mes, de sector en alle cases bestaan 

            '' controle: een case kan maar tot 1 mes software en sector horen!
            If (Check_MultiCasesLinked(oCases, oCnn)) Then Exit Sub ' elke case mag nog niet gelinkt zijn met een ander mes!

            'controle of het er niet al in zit de link?
            If (Check_MesSectorsCasesLink(sMes, oSectors, oCases, oCnn)) Then Exit Sub ' als er al een link is --> Stop

            'toevoegen van de link

            For i As Integer = 0 To oSectors.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    sSQL += "
                    insert into MeCasSe_link(mcslMes, mcslCase,mcslSector) 
                    values (
                    (select meId from MesSoftware where meName = @meName),
                    (select casId from Cases where casName = @casName" + i.ToString + "_" + j.ToString + "),
                    (select seId from Sector where seName = @seName" + i.ToString + "))"
                Next
            Next

            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSectors.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSectors(i)))
            Next
            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    Dim sParaName As String = "@casName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)(j)))
                Next
            Next

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Add the link between a mes software and a sector 
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="oSectors">list containing all the sector name</param>
    Public Shared Sub AddMesSectorsLink(sMes As String, oSectors As List(Of String))

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Exit Sub
        If oSectors Is Nothing OrElse oSectors.Count = 0 Then Exit Sub

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try
            oCnn.Open()

            ' checks:

            Dim sSQL As String = "select meId from MesSoftware where meName = @meName;"

            For i As Integer = 0 To oSectors.Count - 1
                sSQL += "Select seId from Sector where seName = @seName" + i.ToString + ";"
            Next

            Dim oCmd As SqlCommand
            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSectors.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSectors(i)))
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                    Exit Sub
                End If
            Next

            ' de code hierna wordt enkel uitgevoerd als de mes en de sector bestaan 

            'controle of het er niet al in zit de link?
            If (Check_MesSectorsLink(sMes, oSectors, oCnn)) Then Exit Sub

            'toevoegen van de link
            For i As Integer = 0 To oSectors.Count - 1
                sSQL += "
                    insert into MeCasSe_link(mcslMes, mcslSector) 
                    values (
                    (select meId from MesSoftware where meName = @meName),       
                    (select seId from Sector where seName = @seName" + i.ToString + ")
                    );"
            Next

            oCmd = New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSectors.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSectors(i)))
            Next

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Sub

#End Region

#Region "Content"

    ''' <summary>
    ''' Add a new Mes-software to the DB
    ''' </summary>
    ''' <param name="sMes">Name of the software</param>
    ''' <param name="sCompany">Name of the company</param>
    ''' <param name="sDescription">Short description of the software</param>
    ''' <param name="sWebsite">Websitelink</param>
    ''' <returns>return true if the software is added to the DB</returns>
    Public Shared Function AddContent(sMes As String, Optional sDescription As String = Nothing,
                                         Optional sWebsite As String = Nothing) As Boolean?

        If sMes Is Nothing OrElse sMes = "" Then Return Nothing

        Dim oCnn As New SqlConnection(My.Settings.Connectionstring)

        Try

            oCnn.Open()

            Dim xMes As Boolean? = Check_MesSoftware(sMes, oCnn)
            If (xMes Is Nothing) Then Return Nothing
            If (xMes = True) Then Return False

            Dim sSQL As String = "insert into MesSoftware (meName, meDescription, meWebsite) 
                        values (@meName,@meDescription,@meWebsite);"

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@meDescription", If(sDescription, DBNull.Value)))
            oCmd.Parameters.Add(New SqlParameter("@meWebsite", If(sWebsite, DBNull.Value)))
            oCmd.ExecuteNonQuery()

            Return True

        Catch ex As Exception
            Throw ex
        Finally
            oCnn.Close()
        End Try

    End Function

#End Region

End Class
