Imports System.Data.SqlClient
Imports BEC

Partial Class clsDB

#Region "Algemeen"

    ''' <summary>
    ''' Check if a company is already present in the DB
    ''' </summary>
    ''' <param name="sCompany">Company name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the company is present in the DB</returns>
    Private Shared Function Check_Company(sCompany As String, oCnn As SqlConnection) As Boolean?

        If sCompany Is Nothing OrElse sCompany = "" Then Return Nothing

        Dim sSQL As String = "select comId from Company where comName = @comName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Check if a mes software is already present in the DB
    ''' </summary>
    ''' <param name="sMes">Mes name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the mes software is present in the Db</returns>
    Private Shared Function Check_MesSoftware(sMes As String, oCnn As SqlConnection) As Boolean?
        If sMes Is Nothing OrElse sMes = "" Then Return Nothing

        Dim sSQL As String = "select meId from MesSoftware where meName = @meName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Check if a category is already present in the DB
    ''' </summary>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the category is present in the Db</returns>
    Private Shared Function Check_Category(sCategory As String, oCnn As SqlConnection) As Boolean?

        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing

        Dim sSQL As String = "select catId from Category where catName = @catName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Check if a function is already present in the DB
    ''' </summary>
    ''' <param name="sFunction">function name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the function is present in the Db</returns>
    Private Shared Function Check_Function(sFunction As String, oCnn As SqlConnection) As Boolean?

        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing

        Dim sSQL As String = "select fuId from Functions where fuName = @fuName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Check if a sector is already present in the DB
    ''' </summary>
    ''' <param name="sSector">sector name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the sector is present in the Db</returns>
    Private Shared Function Check_Sector(sSector As String, oCnn As SqlConnection) As Boolean?
        If sSector Is Nothing OrElse sSector = "" Then Return Nothing

        Dim sSQL As String = "select seId from Sector where seName = @seName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Check if a Case is already present in the DB
    ''' </summary>
    ''' <param name="sActivity">case name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the case is present in the Db</returns>
    Private Shared Function Check_Case(sCase As String, oCnn As SqlConnection) As Boolean?

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing

        Dim sSQL As String = "select casId from Cases where casName = @casName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Check if a Activity is already present in the DB
    ''' </summary>
    ''' <param name="sActivity">activity name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the case is present in the Db</returns>
    Private Shared Function Check_Activity(sActivity As String, oCnn As SqlConnection) As Boolean?

        If sActivity Is Nothing OrElse sActivity = "" Then Return Nothing

        Dim sSQL As String = "select acId from Activity where acName = @acName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Check if a role is already present in the DB
    ''' </summary>
    ''' <param name="sContentPath">role name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the role is present in the Db</returns>
    Private Shared Function Check_Role(sRole As String, oCnn As SqlConnection) As Boolean?

        If sRole Is Nothing OrElse sRole = "" Then Return Nothing

        Dim sSQL As String = "SELECT roId FROM Roles where roName = @roName"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@roName", sRole))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Check if a contentpath is already present in the DB
    ''' </summary>
    ''' <param name="sContentPath">role name</param>
    ''' <param name="oCnn">SQL connection</param>
    ''' <returns>true if the content path is present in the Db</returns>
    Private Shared Function Check_ContentPath(sContentPath As String, oCnn As SqlConnection) As Boolean?
        If sContentPath Is Nothing OrElse sContentPath = "" Then Return Nothing

        Dim sSQL As String = "SELECT conId FROM Content where conContentPath = @conContentPath"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@conContentPath", sContentPath))
        Dim i As Integer = oCmd.ExecuteScalar

        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region

#Region "links"

#Region "MesCategoryFunction"

#Region "MesCategoryFunction"

    ''' <summary>
    ''' check if an mes, a category and a function exists
    ''' </summary>
    ''' <param name="sMes">Mes Name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    ''' <param name="oCnn">sql connection</param>
    ''' <returns>true if the mes, the category and the function exist</returns>
    Private Shared Function Check_MesCategoryFunctionExist(sMes As String, sCategory As String, sFunction As String, oCnn As SqlConnection) As Boolean?

        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing
        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing

        Dim sSQL As String = "select meId from MesSoftware where meName = @meName;
            select catId from Category where catName = @catName;
            select fuId from Functions where fuName = @fuName;"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
        oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))
        Dim i As Integer = oCmd.ExecuteScalar

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True
    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a category and a function
    ''' </summary>
    ''' <param name="sMes">Mes name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes, category and the function</returns>
    Private Shared Function Check_MesCategoryFunctionLink(sMes As String, sCategory As String, sFunction As String, oCnn As SqlConnection) As Boolean

        Try

            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing
            If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String = "
                    select mcflId from MeCatFuLink
                    join MesSoftware on meId=mcflMes
                    join Category on catId=mcflCategory
                    join Functions on fuId = mcflFunction
                    where meName = @meName and catName = @catName and fuName = @fuName"

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
            oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))

            Dim i As Integer = oCmd.ExecuteScalar

            If (i = 0) Then 'de link zit er nog niet in
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

#Region "MesCategoryFunctions"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="oCatFus"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>true if the mes, the category and the function exist</returns>
    Private Shared Function Check_MesCategoryFunctionsExist(sMes As String, oCatFus As clsCategoryFunctions, oCnn As SqlConnection) As Boolean
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(oCatFus)) Then Return False
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoryFunctions(oCatFus)
        Return Check_MesCategoryFunctionsExist(sMes, oList(0), oList(1), oCnn)
    End Function
    ''' <summary>
    ''' check if an mes, a category and a couple of functions exists
    ''' </summary>
    ''' <param name="sMes">Mes Name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">function name</param>
    ''' <param name="oCnn">sql connection</param>
    ''' <returns>true if the mes, the category and the function exist</returns>
    Private Shared Function Check_MesCategoryFunctionsExist(sMes As String, sCategory As String, oFunctions As List(Of String), oCnn As SqlConnection) As Boolean

        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return False

        Dim sSQL As String = " 
                select meId from MesSoftware where meName = @meName;
                select catId from Category where catName = @catName;"

        For i As Integer = 0 To oFunctions.Count - 1
            sSQL = sSQL + "select fuId from Functions where fuName = @fuName" + i.ToString + ";"
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

        For i As Integer = 0 To oFunctions.Count - 1
            Dim sParaName As String = "@fuName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel!
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a category and a couple off functions
    ''' </summary>
    ''' <param name="sMes">Mes name</param>
    ''' <param name="oCatFus"></param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes, the category and a function
    ''' returns false if their isn't a link between te mes, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_MesCategoryFunctionsLink(sMes As String, oCatFus As clsCategoryFunctions, oCnn As SqlConnection) As Boolean?
        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If (Not clsCheck.Check_CategoryFunctions(oCatFus)) Then Return Nothing
        Dim oList As List(Of Object) = clsTranslate.TranslateCategoryFunctions(oCatFus)
        Return Check_MesCategoryFunctionsLink(sMes, oList(0), oList(1), oCnn)
    End Function
    ''' <summary>
    ''' Check if there is already a link between a mes, a category and a couple off functions
    ''' </summary>
    ''' <param name="sMes">Mes name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">list of all the functions</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes, the category and a function
    ''' returns false if their isn't a link between te mes, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_MesCategoryFunctionsLink(sMes As String, sCategory As String, oFunctions As List(Of String), oCnn As SqlConnection) As Boolean?
        'primary check
        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return Nothing

        Dim sSQL As String
        For i As Integer = 0 To oFunctions.Count - 1
            sSQL += "
                    select mcflId from MeCatFuLink
                    join MesSoftware on meId=mcflMes
                    join Category on catId=mcflCategory
                    join Functions on fuId = mcflFunction
                    where meName = @meName and catName = @catName and fuName = @fuName" + i.ToString + ";"
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)

        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

        For i As Integer = 0 To oFunctions.Count - 1
            Dim sParaName As String = "@fuName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count > 0) Then
                ' het zit erin!
                ' er is een link
                Return True
            End If
        Next

        Return False

    End Function

#End Region

#Region "MesCategoriesFunctions"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="oCatsFus"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>true if all the elements exist</returns>
    Private Shared Function Check_MesCategoriesFunctionsExist(sMes As String, oCatsFus As List(Of clsCategoryFunctions), oCnn As SqlConnection) As Boolean
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCatsFus)) Then Return False
        Dim olist As List(Of Object) = clsTranslate.TranslateCategoriesFunctions(oCatsFus)
        Return Check_MesCategoriesFunctionsExist(sMes, olist(0), olist(1), oCnn)
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="oCategories"></param>
    ''' <param name="oFunctions"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>true if all the elements exist</returns>
    Private Shared Function Check_MesCategoriesFunctionsExist(sMes As String, oCategories As List(Of String), oFunctions As List(Of List(Of String)), oCnn As SqlConnection) As Boolean
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return False

        Dim sSQL As String = " select meId from MesSoftware where meName = @meName;"
        For i As Integer = 0 To oCategories.Count - 1
            sSQL += "Select catId from Category where catName = @catName" + i.ToString + ";"
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                sSQL += "select fuId from Functions where fuName = @fuName" + i.ToString + "_" + j.ToString + ";"
            Next
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        For i As Integer = 0 To oCategories.Count - 1
            Dim sParaName As String = "@catName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
            Next
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="oCatsFus"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>returns true if their is already a link between the mes, the category and a function
    ''' returns false if their isn't a link between te mes, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_MesCategoriesFunctionsLink(sMes As String, oCatsFus As List(Of clsCategoryFunctions), oCnn As SqlConnection) As Boolean?
        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If (Not clsCheck.Check_CategoriesFunctions(oCatsFus)) Then Return Nothing
        Dim olist As List(Of Object) = clsTranslate.TranslateCategoriesFunctions(oCatsFus)
        Return Check_MesCategoriesFunctionsLink(sMes, olist(0), olist(1), oCnn)
    End Function
    ''' <summary>
    '''  Check if there is already a link between a mes, a couple categories and a couple functions
    ''' </summary>
    ''' <param name="sMes">Mes name</param>
    ''' <param name="oCategories">list containing the category names</param>
    ''' <param name="oFunctions">list of list of all the functions</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes, the category and a function
    ''' returns false if their isn't a link between te mes, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_MesCategoriesFunctionsLink(sMes As String, oCategories As List(Of String), oFunctions As List(Of List(Of String)), oCnn As SqlConnection) As Boolean?

        If sMes Is Nothing OrElse sMes = "" Then Return Nothing
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return Nothing

        Dim sSQL As String

        For i As Integer = 0 To oCategories.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                sSQL += "
                    select mcflId from MeCatFuLink
                    join MesSoftware on meId=mcflMes
                    join Category on catId=mcflCategory
                    join Functions on fuId = mcflFunction
                    where meName = @meName and catName = @catName" + i.ToString + " and fuName = @fuName" + i.ToString + "_" + j.ToString + ";"
            Next
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)

        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        For i As Integer = 0 To oCategories.Count - 1
            Dim sParaName As String = "@catName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
            Next
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count > 0) Then
                ' het zit erin!
                ' er is een link
                Return True
            End If
        Next

        Return False

    End Function

#End Region

#End Region
    '---------------------------------------------------------------------------------------------------------------------------------------------------
#Region "CaseCategoryFunction"

#Region "CaseCategoryFunction"

    ''' <summary>
    ''' check if an case, a category and a function exists
    ''' </summary>
    ''' <param name="sCase">caseName</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    ''' <param name="oCnn">sql connection</param>
    ''' <returns>true if the case, the category and the function exist</returns>
    Private Shared Function Check_CaseCategoryFunctionExist(sCase As String, sCategory As String, sFunction As String, oCnn As SqlConnection) As Boolean

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing
        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing

        Dim sSQL As String = " 
                select casId from Cases where casName = @casName;
                select catId from Category where catName = @catName;
                select fuId from Functions where fuName = @fuName;"
        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
        oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if the category and function belongs to the MES-software that is used within the case
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCaseCategory">name of the category the case wants to use</param>
    ''' <param name="sCaseFunction">name of the function the case wants to use</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>true if the category and function off the case also belong to the MES software that the case uses</returns>
    Private Shared Function Check_CaseCategoryFunctionBelongToMes(sCase As String, sCaseCategory As String, sCaseFunction As String, oCnn As SqlConnection)
        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If sCaseCategory Is Nothing OrElse sCaseCategory = "" Then Return Nothing
        If sCaseFunction Is Nothing OrElse sCaseFunction = "" Then Return Nothing
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String = "
                    Select  catName, fuName from MeCatFuLink
                    Join Category on catId= mcflCategory
                    Join Functions on fuId = mcflFunction
                    where mcflMes =
                    (select top(1) cmcsalMes from ComMeCasSeAcLink join Cases on casId = cmcsalCase where casName = @casName);"

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDT As New DataTable
        oDA.Fill(oDT)

        For Each oRow As DataRow In oDT.Rows()
            If oRow.Item(0) = sCaseCategory And oRow.Item(1) = sCaseFunction Then
                ' de category en de functie die de case wilt gebruiken behoort effectief tot de MES software
                Return True
            End If
        Next

        Return False

    End Function

    ''' <summary>
    ''' Check if there is already a link between a case, a category and a function
    ''' </summary>
    ''' <param name="sCase">Case name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="sFunction">function name</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the case, the category and a function
    ''' returns false if their isn't a link between te case, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_CaseCategoryFunctionLink(sCase As String, sCategory As String, sFunction As String, oCnn As SqlConnection) As Boolean?

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If sCategory Is Nothing OrElse sCategory = "" Then Return Nothing
        If sFunction Is Nothing OrElse sFunction = "" Then Return Nothing
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String = "
                select ccflId from CasCatFuLink
                join Cases on casId=ccflCase
                join Category on catId=ccflCategory
                join Functions on fuId = ccflFunction
                where casName = @casName and catName = @catName and fuName = @fuName"

        Dim oCmd As New SqlCommand(sSQL, oCnn)

        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))
        oCmd.Parameters.Add(New SqlParameter("@fuName", sFunction))

        Dim i As Integer = oCmd.ExecuteScalar

        If (i = 0) Then 'de link zit er nog niet in
            Return False
        Else
            Return True
        End If

    End Function

#End Region

#Region "CaseCategoryFunctions"

    ''' <summary>
    ''' check if an case, a category and a couple off functions exists
    ''' </summary>
    ''' <param name="sCase">caseName</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">function name</param>
    ''' <param name="oCnn">sql connection</param>
    ''' <returns>true if the case, the category and all the functions exist</returns>
    Private Shared Function Check_CaseCategoryFunctionsExist(sCase As String, sCategory As String, oFunctions As List(Of String), oCnn As SqlConnection) As Boolean

        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return False

        Dim sSQL As String
        Dim oCmd As SqlCommand

        sSQL = " 
                select casId from Cases where casName = @casName;
                select catId from Category where catName = @catName;"

        For i As Integer = 0 To oFunctions.Count - 1
            sSQL = sSQL + "select fuId from Functions where fuName = @fuName" + i.ToString + ";"
        Next

        oCmd = New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

        For i As Integer = 0 To oFunctions.Count - 1
            Dim sParaName As String = "@fuName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if the category and functions belongs to the MES-software that is used within the case
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">name of the category the case wants to use</param>
    ''' <param name="oFunctions">list containg the names of the functions the case wants to use</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>true if the category and all the functions off the case also belong to the MES software that the case uses</returns>
    Private Shared Function Check_CaseCategoryFunctionsBelongToMes(sCase As String, sCategory As String, oFunctions As List(Of String), oCnn As SqlConnection)
        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return False
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String = "
                    Select  catName, fuName from MeCatFuLink
                    Join Category on catId= mcflCategory
                    Join Functions on fuId = mcflFunction
                    where mcflMes =
                    (select top(1) cmcsalMes from ComMeCasSeAcLink join Cases on casId = cmcsalCase where casName = @casName);"

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDT As New DataTable
        oDA.Fill(oDT)

        For Each sCaseFunction As String In oFunctions
            Dim xPresent As Boolean
            For Each oRow As DataRow In oDT.Rows()
                If oRow.Item(0) = sCategory And oRow.Item(1) = sCaseFunction Then
                    ' de category en functie behoren ook tot de mes
                    xPresent = True
                    Exit For
                End If
            Next
            If (Not xPresent) Then Return False 'als er 1 niet in zit dan is het niet goed!
        Next
        ' ze zitten er allemaal in
        Return True

    End Function

    ''' <summary>
    ''' Check if there is already a link between the case, the category and the functions
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">category name</param>
    ''' <param name="oFunctions">list of all the functions</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the case, the category and a function
    ''' returns false if their isn't a link between te case, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_CaseCategoryFunctionsLink(sCase As String, sCategory As String, oFunctions As List(Of String), oCnn As SqlConnection) As Boolean?

        'primary check
        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If (Not clsCheck.Check_CategoryFunctions(sCategory, oFunctions)) Then Return Nothing
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String

        For i As Integer = 0 To oFunctions.Count - 1
            sSQL += "
                    select ccflId from CasCatFuLink
                    join Cases on casId=ccflCase
                    join Category on catId=ccflCategory
                    join Functions on fuId = ccflFunction
                    where casName = @casName and catName = @catName and fuName = @fuName" + i.ToString + ";"
        Next


        Dim oCmd As New SqlCommand(sSQL, oCnn)

        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        oCmd.Parameters.Add(New SqlParameter("@catName", sCategory))

        For i As Integer = 0 To oFunctions.Count - 1
            Dim sParaName As String = "@fuName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)))
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count > 0) Then
                'er is een link aanwezig
                Return True
            End If
        Next

        Return False

    End Function

#End Region

#Region "CaseCategoriesFunctions"

    ''' <summary>
    ''' check if an case, a category and a couple off functions exists
    ''' </summary>
    ''' <param name="sCase">caseName</param>
    ''' <param name="oCategories">category names</param>
    ''' <param name="oFunctions">function names</param>
    ''' <param name="oCnn">sql connection</param>
    ''' <returns>true if the case, all categories and all functions exist</returns>
    Private Shared Function Check_CaseCategoriesFunctionsExist(sCase As String, oCategories As List(Of String), oFunctions As List(Of List(Of String)), oCnn As SqlConnection) As Boolean

        If sCase Is Nothing OrElse sCase = "" Then Return False
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return False

        Dim sSQL As String = " select meId from MesSoftware where meName = @meName;"
        For i As Integer = 0 To oCategories.Count - 1
            sSQL += "Select catId from Category where catName = @catName" + i.ToString + ";"
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                sSQL += "select fuId from Functions where fuName = @fuName" + i.ToString + "_" + j.ToString + ";"
            Next
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        For i As Integer = 0 To oCategories.Count - 1
            Dim sParaName As String = "@catName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
            Next
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if the category and functions belongs to the MES-software that is used within the case
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="sCategory">name of the category the case wants to use</param>
    ''' <param name="oFunctions">list containg the names of the functions the case wants to use</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>true if the category and all the functions off the case also belong to the MES software that the case uses</returns>
    Private Shared Function Check_CaseCategoriesFunctionsBelongToMes(sCase As String, oCategories As List(Of String), oFunctions As List(Of List(Of String)), oCnn As SqlConnection)

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return False
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String = "
                Select  catName, fuName from MeCatFuLink
                Join Category on catId= mcflCategory
                Join Functions on fuId = mcflFunction
                where mcflMes =
                (select top(1) cmcsalMes from ComMeCasSeAcLink join Cases on casId = cmcsalCase where casName = @casName);"

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDT As New DataTable
        oDA.Fill(oDT)

        For i As Integer = 0 To oCategories.Count() - 1
            Dim sCategory As String = oCategories(i)
            Dim oFunctionlist As List(Of String) = oFunctions(i)

            For Each sCaseFunction As String In oFunctionlist
                Dim xPresent As Boolean
                For Each oRow As DataRow In oDT.Rows()
                    If oRow.Item(0) = sCategory And oRow.Item(1) = sCaseFunction Then
                        ' de category en functie behoren ook tot de mes
                        xPresent = True
                        Exit For
                    End If
                Next
                If (Not xPresent) Then Return False 'als er 1 niet in zit dan is het niet goed!
            Next

        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if there is already a link between the case, the category and the functions
    ''' </summary>
    ''' <param name="sCase">case name</param>
    ''' <param name="oCategories">category name</param>
    ''' <param name="oFunctions">list of all the functions</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the case, the category and a function
    ''' returns false if their isn't a link between te case, the category and all the functions
    ''' return nothings if their is something wrong with the input</returns>
    Private Shared Function Check_CaseCategoriesFunctionsLink(sCase As String, oCategories As List(Of String), oFunctions As List(Of List(Of String)), oCnn As SqlConnection) As Boolean?

        If sCase Is Nothing OrElse sCase = "" Then Return Nothing
        If (Not clsCheck.Check_CategoriesFunctions(oCategories, oFunctions)) Then Return Nothing
        If oCnn Is Nothing Then Return Nothing

        Dim sSQL As String

        For i As Integer = 0 To oCategories.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                sSQL += "
                    select ccflId from CasCatFuLink
                    join Cases on casId=ccflCase
                    join Category on catId=mcflCategory
                    join Functions on fuId = mcflFunction
                    where meName = @meName and catName = @catName" + i.ToString + " and fuName = @fuName" + i.ToString + "_" + j.ToString + ";"
            Next
        Next

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        For i As Integer = 0 To oCategories.Count - 1
            Dim sParaName As String = "@catName" + i.ToString
            oCmd.Parameters.Add(New SqlParameter(sParaName, oCategories(i)))
        Next
        For i As Integer = 0 To oFunctions.Count - 1
            For j As Integer = 0 To oFunctions(i).Count - 1
                Dim sParaName As String = "@fuName" + i.ToString + "_" + j.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oFunctions(i)(j)))
            Next
        Next

        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            If (a.Rows.Count > 0) Then
                ' het zit erin!
                ' er is een link
                Return True
            End If
        Next

        Return False

    End Function

#End Region

#End Region

#Region "CompanyMesCaseSectorActivity"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sCompany"></param>
    ''' <param name="sMes"></param>
    ''' <param name="sCase"></param>
    ''' <param name="sSector"></param>
    ''' <param name="sActivity"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>true if the company, mes, case, sector, activity all exist</returns>
    Private Shared Function Check_ComMeCasSeAcExist(sCompany As String, sMes As String, sCase As String, sSector As String, sActivity As String, oCnn As SqlConnection) As Boolean
        If sCompany Is Nothing OrElse sCompany = "" Then Return False
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If sSector Is Nothing OrElse sSector = "" Then Return False
        If sActivity Is Nothing OrElse sActivity = "" Then Return False
        If oCnn Is Nothing Then Return False

        Dim sSQL As String = "              
                select comId from Company where comName = @comName;
                select meId from MesSoftware where meName = @meName;
                Select casId from Sector where casName = @casName;
                Select seId from Sector where seName = @seName;
                Select acId from Activity where acName = @acName;"

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
        oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))
        Dim oDA As New SqlDataAdapter(oCmd)
        Dim oDS As New DataSet
        oDA.Fill(oDS)

        For Each a As DataTable In oDS.Tables
            'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
            If (a.Rows.Count = 0 OrElse a.Rows(0).Item(0) Is DBNull.Value) Then
                Return False
            End If
        Next

        Return True

    End Function

    ''' <summary>
    ''' Check if the case is already linked to a mes and sector
    ''' </summary>
    ''' <param name="sCase">name off the case </param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if the case is already linked to a mes and sector </returns>
    Private Shared Function Check_Case_NotLinked(sCase As String, oCnn As SqlConnection) As Boolean

        If sCase Is Nothing OrElse sCase = "" Then Return False
        If oCnn Is Nothing Then Return False

        Dim sSQL As String = "
                        SELECT TOP (1) cmcsalId from ComMeCasSeAcLink
                        join Cases on casId=cmcsalCase
                        where casName = @casName"

        Dim oCmd As New SqlCommand(sSQL, oCnn)
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))
        Dim i As Integer = oCmd.ExecuteScalar

        If (i = 0) Then
            'de case is nog niet gelinkt met iets
            Return True
        Else
            'de case is al gelinkt
            Return False
        End If

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sCompany"></param>
    ''' <param name="sMes"></param>
    ''' <param name="sCase"></param>
    ''' <param name="sSector"></param>
    ''' <param name="sActivity"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>true if the link doesn't exist</returns>
    Private Shared Function Check_ComMeCasSeAc_NotLinked(sCompany As String, sMes As String, sCase As String, sSector As String, sActivity As String, oCnn As SqlConnection) As Boolean

        If sCompany Is Nothing OrElse sCompany = "" Then Return False
        If sMes Is Nothing OrElse sMes = "" Then Return False
        If sCase Is Nothing OrElse sCase = "" Then Return False
        If sSector Is Nothing OrElse sSector = "" Then Return False
        If sActivity Is Nothing OrElse sActivity = "" Then Return False
        If oCnn Is Nothing Then Return False

        Dim sSQL As String = "
                    select cmcsalId from ComMeCasSeAcLink
                    join Company on comId=cmcsalCompany
                    join MesSoftware on meId=cmcsalMesSoftware                                 
                    join Activity on acId=cmcsalActivity
                    left join Sector on seId=cmcsalSector
                    left join Cases on casId=cmcsalCase
                    where meName = @meName and seName = @seName and comName =@comName and acName = @acName and casName = @casName"

        Dim oCmd As New SqlCommand(sSQL, oCnn)

        oCmd.Parameters.Add(New SqlParameter("@comName", sCompany))
        oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
        oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
        oCmd.Parameters.Add(New SqlParameter("@acName", sActivity))
        oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

        Dim i As Integer = oCmd.ExecuteScalar

        If (i = 0) Then 'de link zit er nog niet in
            Return False
        Else
            Return True
        End If

    End Function




    ''' <summary>
    ''' Check if there is already a link between a mes, a sector
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="sSector">sector name</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes and the sector</returns>
    Private Shared Function Check_MesSectorLink(sMes As String, sSector As String, oCnn As SqlConnection) As Boolean

        Try

            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If sSector Is Nothing OrElse sSector = "" Then Return Nothing
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String = "
                    select mcslId from MeCasSe_link
                    join MesSoftware on meId=mcslMes
                    join Sector on seId=mcslSector
                    where meName = @meName and seName = @seName"

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))

            Dim i As Integer = oCmd.ExecuteScalar

            If (i = 0) Then 'de link zit er nog niet in
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a sector and a case
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="sCase">case name </param>
    ''' <param name="sSector">sector name</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes, sector and the case</returns>
    Private Shared Function Check_MesSectorCaseLink(sMes As String, sSector As String, sCase As String, oCnn As SqlConnection) As Boolean

        Try

            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If sSector Is Nothing OrElse sSector = "" Then Return Nothing
            If sCase Is Nothing OrElse sCase = "" Then Return Nothing
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String = "
                    select mcslId from MeCasSe_link
                    join MesSoftware on meId=mcslMes
                    join Sector on seId=mcslSector
                    left join Cases on casId = mcslCase
                    where meName = @meName and seName = @seName and casName = @casName"

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            oCmd.Parameters.Add(New SqlParameter("@casName", sCase))

            Dim i As Integer = oCmd.ExecuteScalar

            If (i = 0) Then 'de link zit er nog niet in
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a sector and the cases
    ''' </summary>
    ''' <param name="sMes"></param>
    ''' <param name="sSector"></param>
    ''' <param name="oCases"></param>
    ''' <param name="oCnn"></param>
    ''' <returns>returns true if their is already a link between the mes, the sector and one off the cases</returns>
    Private Shared Function Check_MesSectorCasesLink(sMes As String, sSector As String, oCases As List(Of String), oCnn As SqlConnection)
        Try

            'primary check
            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If sSector Is Nothing OrElse sSector = "" Then Return Nothing
            If oCases Is Nothing OrElse oCases.Count = 0 Then Return Nothing
            For i As Integer = 0 To oCases.Count - 2
                For j As Integer = i + 1 To oCases.Count - 1
                    If oCases(i) = oCases(j) Then
                        Return Nothing
                    End If
                Next
            Next
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String = ""

            For i As Integer = 0 To oCases.Count - 1
                sSQL += "
                    select mcslId from MeCasSe_link
                    join MesSoftware on meId=mcslMes
                    join Sector on seId=mcslSector
                    left join Cases on casId = mcslCase
                    where meName = @meName and seName = @seName and casName = @casName" + i.ToString + ";"
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            oCmd.Parameters.Add(New SqlParameter("@seName", sSector))
            For i As Integer = 0 To oCases.Count - 1
                Dim sParaName As String = "@casName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)))
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count > 0) Then
                    ' het zit erin!
                    ' er is een link
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a sector
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="oSectors">List containing the sector names</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes and one off the Cases</returns>
    Private Shared Function Check_MesSectorsLink(sMes As String, oSectors As List(Of String), oCnn As SqlConnection) As Boolean
        'controle of er een link is tussen een sector en de MES software
        Try
            'primary check
            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If oSectors Is Nothing OrElse oSectors.Count = 0 Then Return Nothing
            For i As Integer = 0 To oSectors.Count - 2
                For j As Integer = i + 1 To oSectors.Count - 1
                    If oSectors(i) = oSectors(j) Then
                        Return Nothing
                    End If
                Next
            Next
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String

            For i As Integer = 0 To oSectors.Count - 1
                sSQL += "
                    select mcslId from MeCasSe_link
                    join MesSoftware on meId=mcslMes
                    join Sector on seId=mcslSector
                    where meName = @meName and seName = @seName" + i.ToString + ";"
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSectors.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSectors(i)))
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count > 0) Then
                    ' het zit erin!
                    ' er is een link
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Check if there is already a link between a mes, a sector and a cases
    ''' </summary>
    ''' <param name="sMes">Mes software name</param>
    ''' <param name="oCases">List containing the secotr names</param>
    ''' <param name="oCases">List of lists that contains the case names</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if their is already a link between the mes and a sector and a case</returns>
    Private Shared Function Check_MesSectorsCasesLink(sMes As String, oSector As List(Of String), oCases As List(Of List(Of String)), oCnn As SqlConnection)
        Try

            'primary check
            If sMes Is Nothing OrElse sMes = "" Then Return Nothing
            If oCases Is Nothing OrElse oCases.Count = 0 Then Return Nothing
            For Each oCase As List(Of String) In oCases
                If oCase Is Nothing OrElse oCase.Count = 0 Then Return Nothing
            Next
            If Not oCases.Count = oCases.Count Then Return Nothing

            'dubbels
            For i As Integer = 0 To oSector.Count - 2
                For j As Integer = i + 1 To oSector.Count - 1
                    If oSector(i) = oSector(j) Then
                        Return Nothing
                    End If
                Next
            Next

            'dubbels
            For k As Integer = 0 To oCases.Count - 1
                For i As Integer = 0 To oCases(k).Count - 2
                    For j As Integer = i + 1 To oCases(k).Count - 1
                        If oCases(k)(i) = oCases(k)(j) Then
                            Return Nothing
                        End If
                    Next
                Next
            Next

            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String = ""

            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    sSQL += "
                    select mcslId from MeCasSe_link
                    join MesSoftware on meId=mcslMes
                    join Sector on seId=mcslSector
                    left join Cases on casId = mcslCase
                    where meName = @meName and seName = @seName" + i.ToString + " and casName = @casName" + i.ToString + "_" + j.ToString + ";"
                Next
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)
            oCmd.Parameters.Add(New SqlParameter("@meName", sMes))
            For i As Integer = 0 To oSector.Count - 1
                Dim sParaName As String = "@seName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oSector(i)))
            Next
            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    Dim sParaName As String = "@casName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)(j)))
                Next
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count > 0) Then
                    ' het zit erin!
                    ' er is een link
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#End Region

#Region "Extra"


    ''' <summary>
    ''' Check if one off the case is already linked to a mes and sector
    ''' </summary>
    ''' <param name="oCases">List containing the case names</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns>returns true if one of the cases is already linked to a mes and sector </returns>
    Private Shared Function Check_CasesLinked(oCases As List(Of String), oCnn As SqlConnection) As Boolean
        Try
            'primary check
            If oCases Is Nothing OrElse oCases.Count = 0 Then Return Nothing
            'dubbels
            For i As Integer = 0 To oCases.Count - 2
                For j As Integer = i + 1 To oCases.Count - 1
                    If oCases(i) = oCases(j) Then
                        Return Nothing
                    End If
                Next
            Next
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String

            For i As Integer = 0 To oCases.Count - 1
                sSQL += "
                        SELECT TOP (1) mcslId 
                        from MeCasSe_link
                        join Cases on casId = mcslCase
                        where casName = @casName" + i.ToString + ";"
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            For i As Integer = 0 To oCases.Count - 1
                Dim sParaName As String = "@casName" + i.ToString
                oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)))
            Next
            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count > 0) Then
                    ' het zit erin!
                    ' er is een link
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Check if one off the cases is already linked to a mes and sector
    ''' </summary>
    ''' <param name="oCases">List of lists that contains the case names</param>
    ''' <param name="oCnn">openstaande DB connectie</param>
    ''' <returns></returns>
    Private Shared Function Check_MultiCasesLinked(oCases As List(Of List(Of String)), oCnn As SqlConnection) As Boolean
        Try
            'primary check
            If oCases Is Nothing OrElse oCases.Count = 0 Then Return Nothing
            For Each oCase As List(Of String) In oCases
                If oCase Is Nothing OrElse oCase.Count = 0 Then Return Nothing
            Next
            If oCnn Is Nothing Then Return Nothing

            Dim sSQL As String

            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    sSQL += "
                        SELECT TOP (1) mcslId 
                        from MeCasSe_link
                        join Cases on casId = mcslCase
                        where casName = @casName" + i.ToString + "_" + j.ToString + ";"
                Next
            Next

            Dim oCmd As New SqlCommand(sSQL, oCnn)

            For i As Integer = 0 To oCases.Count - 1
                For j As Integer = 0 To oCases(i).Count - 1
                    Dim sParaName As String = "@casName" + i.ToString + "_" + j.ToString
                    oCmd.Parameters.Add(New SqlParameter(sParaName, oCases(i)(j)))
                Next
            Next

            Dim oDA As New SqlDataAdapter(oCmd)
            Dim oDS As New DataSet
            oDA.Fill(oDS)

            For Each a As DataTable In oDS.Tables
                'als er geen rijen zijn of er zit null in de row dan zit het niet in de tabel en moet de functie niet uitgevoerd worden!
                If (a.Rows.Count > 0) Then
                    ' het zit erin!
                    ' er is een link
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class

