﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {
        // Lock object
        private static readonly object oLock = new object();

        // Validation of given list with categories and functions
        public static bool ValidateCatFu(List<clsCategoryFunctions> lstCatFu)
        {
            // Check input parameter
            if (lstCatFu == null)
                return false;

            // Check existing categories and functions
            if (clsStatic.Categories == null)
                clsStatic.Categories = clsDB.Get_Categories();

            if (clsStatic.Functions == null)
                clsStatic.Functions = clsDB.Get_Functions();

            // Check given categories and functions
            foreach (clsCategoryFunctions oCatFu in lstCatFu)
            {
                if (oCatFu.Id != 0 && (oCatFu.Functions == null || oCatFu.Functions.Count == 0))               
                    throw new Exception("Input parameters are wrong! A category can't be added without functions!");               

                if (clsStatic.Categories.Find(x => x.Id == oCatFu.Id) == null)
                    throw new Exception($"Could not find category {oCatFu.Name}.");

                foreach (clsFunction oFu in oCatFu.Functions)
                    if (clsStatic.Functions.Find(x => x.Id == oFu.Id) == null)
                        throw new Exception($"Could not find function {oFu.Name}.");
            
            }

            


            return true;
        }

        // Add an object
        private static clsReturn addObject<T>(clsBaseModel oObject) where T : clsBaseModel
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    clsBaseModel oTemp = getObject<T>(oObject.Name, false);
                    if (oTemp != null)
                        throw new Exception($"This {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} already exists.");
                    else
                    {
                        int iResult = 0;
                        if (oObject.GetType() == typeof(clsSector))
                        {
                            iResult = clsDB.Add_Sector(oObject);
                            clsStatic.Sectors = null;
                        }
                        else if (oObject.GetType() == typeof(clsFunction))
                        {
                            iResult = clsDB.Add_Function(oObject);
                            clsStatic.Functions = null;
                        }
                        else if (oObject.GetType() == typeof(clsCategory))
                        {
                            iResult = clsDB.Add_Category(oObject);
                            clsStatic.Categories = null;
                        }
                        else if (oObject.GetType() == typeof(clsActivity))
                        {
                            iResult = clsDB.Add_Activity(oObject);
                            clsStatic.Activities = null;
                        }
                        else if (oObject.GetType() == typeof(clsCompany))
                        {
                            iResult = clsDB.Add_Company((clsCompany)oObject);
                            clsStatic.Companies = null;
                        }
                        else if (oObject.GetType() == typeof(clsSoftware))
                        {
                            iResult = clsDB.Add_Mes((clsSoftware)oObject);
                            clsStatic.Softwares = null;
                        }
                        else if (oObject.GetType() == typeof(clsCase))
                        {
                            iResult = clsDB.Add_Case((clsCase)oObject);
                            clsStatic.Cases = null;
                        }
                        else if (oObject.GetType() == typeof(clsAnswerMC))
                        {
                            iResult = clsDB.Add_QuestionAnswer((T)oObject);
                            clsStatic.QuestionAnswers = null;
                        }
                        else if (oObject.GetType() == typeof(clsQuestionCategory))
                        {
                            iResult = clsDB.Add_QuestionCategory((T)oObject);
                            clsStatic.QuestionCategories = null;
                        }
                        
                        if (iResult == 0)
                            throw new Exception($"Could not add {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'");

                        oReturn.Data = iResult;
                        oReturn.Message = $"Successfully added {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'";
                        oReturn.Status = 1;
                    }
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit an object
        private static clsReturn editObject<T>(clsBaseModel oObject, byte bUserLevel = 1, string sUserId = "") where T : clsBaseModel
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    clsBaseModel oTemp = getObject<T>(oObject.Id);
                    if (oTemp == null)
                        throw new Exception($"This {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} doesn't exist.");
                    else
                    {
                        int iResult = 0;
                        if (oObject.GetType() == typeof(clsSector))
                        {
                            iResult = clsDB.Edit_Sector((clsSector)oObject);
                            clsStatic.Sectors = null;
                        }
                        else if (oObject.GetType() == typeof(clsFunction))
                        {
                            iResult = clsDB.Edit_Function((clsFunction)oObject);
                            clsStatic.Functions = null;
                        }
                        else if (oObject.GetType() == typeof(clsCategory))
                        {
                            iResult = clsDB.Edit_Category((clsCategory)oObject);
                            clsStatic.Categories = null;
                        }
                        else if (oObject.GetType() == typeof(clsActivity))
                        {
                            iResult = clsDB.Edit_Activity((clsActivity)oObject);
                            clsStatic.Activities = null;
                        }
                        else if (oObject.GetType() == typeof(clsCompany))
                        {
                            oObject = filterObject<clsCompany>((clsCompany)oObject, bUserLevel, sUserId, false);
                            iResult = clsDB.Edit_Company((clsCompany)oObject);
                            clsStatic.Companies = null;
                        }
                        else if (oObject.GetType() == typeof(clsSoftware))
                        {
                            oObject = filterObject<clsSoftware>((clsSoftware)oObject, bUserLevel, sUserId, false);
                            iResult = clsDB.Edit_Mes((clsSoftware)oObject);
                            clsStatic.Softwares = null;
                        }
                        else if (oObject.GetType() == typeof(clsCase))
                        {
                            oObject = filterObject<clsCase>((clsCase)oObject, bUserLevel, sUserId, false);
                            iResult = clsDB.Edit_Case((clsCase)oObject);
                            clsStatic.Cases = null;
                        }
                        else if (oObject.GetType() == typeof(clsAnswerMC))
                        {
                            iResult = clsDB.Edit_QuestionAnswer((clsAnswerMC)oObject);
                            clsStatic.QuestionAnswers = null;
                        }
                        else if (oObject.GetType() == typeof(clsQuestionCategory))
                        {
                            iResult = clsDB.Edit_QuestionCategory((clsQuestionCategory)oObject);
                            clsStatic.QuestionCategories = null;
                        }
                        else if (oObject.GetType() == typeof(clsQuestionMC))
                        {
                            iResult = clsDB.Edit_Question(oObject);
                            clsStatic.Questions = null;
                        }
                        else if (oObject.GetType() == typeof(clsQuestionRange))
                        {
                            iResult = clsDB.Edit_Question(oObject);
                            clsStatic.Questions = null;
                        }
                        else if (oObject.GetType() == typeof(clsSoftwareAnswerMC))
                        {
                            iResult = clsDB.Edit_SoftwareAnswer((clsSoftwareAnswerMC)oObject);
                            clsStatic.SetSoftwareAnswersToNullBySoftwareAnswer(oObject.Id);
                        }
                        else if (oObject.GetType() == typeof(clsSoftwareAnswerRange))
                        {
                            iResult = clsDB.Edit_SoftwareAnswer((clsSoftwareAnswerRange)oObject);
                            clsStatic.SetSoftwareAnswersToNullBySoftwareAnswer(oObject.Id);
                        }

                        if (iResult == 0)
                            throw new Exception($"Could not edit {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'");

                        oReturn.Data = iResult;
                        oReturn.Message = $"Successfully edited {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'";
                        oReturn.Status = 1;
                    }
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Delete an object
        private static clsReturn deleteObject<T>(int iId) where T : clsBaseModel
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    clsBaseModel oObject = getObject<T>(iId);

                    int iResult = 0;
                    if (typeof(T) == typeof(clsSector))
                    {
                        iResult = clsDB.Delete_Sector(iId);
                        clsStatic.Sectors = null;
                    }
                    else if (typeof(T) == typeof(clsFunction))
                    {
                        iResult = clsDB.Delete_Function(iId);
                        clsStatic.Functions = null;
                    }
                    else if (typeof(T) == typeof(clsCategory))
                    {
                        iResult = clsDB.Delete_Category(iId);
                        clsStatic.Categories = null;
                    }
                    else if (typeof(T) == typeof(clsActivity))
                    {
                        iResult = clsDB.Delete_Activity(iId);
                        clsStatic.Activities = null;
                    }
                    else if (typeof(T) == typeof(clsCompany))
                    {
                        iResult = clsDB.Delete_Company(iId);
                        clsStatic.Companies = null;
                    }
                    else if (typeof(T) == typeof(clsSoftware))
                    {
                        iResult = clsDB.Delete_Mes(iId);
                        clsStatic.Softwares = null;
                    }
                    else if (typeof(T) == typeof(clsCase))
                    {
                        iResult = clsDB.Delete_Case(iId);
                        clsStatic.Cases = null;
                    }

                    if (iResult == 0)
                        throw new Exception($"Could not delete {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'");

                    oReturn.Data = iResult;
                    oReturn.Message = $"Successfully deleted {typeof(T).Name.Substring(3, typeof(T).Name.Length - 3).ToLower()} '{oObject.Name}'";
                    oReturn.Status = 1;

                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get objects by type and visibility wrapped in a clsReturn
        private static clsReturn getFilteredObjectsWrapped<T>(byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true) where T : clsBaseModel
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                oReturn.Data = getFilteredObjects<T>(bUserLevel, sUserId, xIncludePublic);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }

            return oReturn;
        }

        // Get object by Id type and visibility wrapped in a clsReturn
        private static clsReturn getFilteredObjectByIdWrapped<T>(int iId, byte bUserLevel = 1, string sUserId = "", bool xException = true) where T : clsBaseModel
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                oReturn.Data = getFilteredObjectById<T>(iId, bUserLevel, sUserId, xException);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }
            return oReturn;
        }

        // Get objects by type and visibility
        private static List<clsBaseModel> getFilteredObjects<T>(byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true) where T : clsBaseModel
        {
            List<clsBaseModel> lstObjects = null;
     
            // Check model type requested
            if (typeof(T).BaseType == typeof(clsAdvancedBaseModel))
            {
                // Advanced base models

                // Check user level
                checkUserLevel(bUserLevel);

                // Get object list
                List<clsAdvancedBaseModel> lstAdvObjects = null;
                if (typeof(T) == typeof(clsCompany))
                    lstAdvObjects = clsStatic.Companies.Cast<clsAdvancedBaseModel>().ToList();
                else if (typeof(T) == typeof(clsSoftware))
                    lstAdvObjects = clsStatic.Softwares.Cast<clsAdvancedBaseModel>().ToList();
                else if (typeof(T) == typeof(clsCase))
                    lstAdvObjects = clsStatic.Cases.Cast<clsAdvancedBaseModel>().ToList();
                else
                    throw new Exception("Invalid type given.");

                // Filter the objects
                lstObjects = filterObjectsList<T>(lstAdvObjects, bUserLevel, sUserId, xIncludePublic);
            }
            else
            {
                /*
                if (typeof(T) == typeof(clsActivity))
                    lstObjects = clsStatic.Activities;
                else if (typeof(T) == typeof(clsFunction))
                    lstObjects = clsStatic.Functions;
                else if (typeof(T) == typeof(clsCategory))
                    lstObjects = clsStatic.Categories;
                else if (typeof(T) == typeof(clsSector))
                    lstObjects = clsStatic.Sectors;
                else
                    throw new Exception("Invalid type given.");
                */
                lstObjects = getList<T>();
            }

            return lstObjects;
        }

        // Get object by Id type and visibility
        private static T getFilteredObjectById<T>(int iId, byte bUserLevel = 1, string sUserId = "", bool xException = true) where T : clsBaseModel
        {
            clsBaseModel oObject = null;

            // Check model type requested
            if (typeof(T).BaseType == typeof(clsAdvancedBaseModel))
            {
                // Advanced base models

                // Check user level
                checkUserLevel(bUserLevel);

                // Get object
                clsAdvancedBaseModel oAdvObject = null;

                if (typeof(T) == typeof(clsCompany))
                    oAdvObject = (clsAdvancedBaseModel)clsDB.Get_Company(iId);
                else if (typeof(T) == typeof(clsSoftware))
                    oAdvObject = (clsAdvancedBaseModel)clsDB.Get_Mes(iId);
                else if (typeof(T) == typeof(clsCase))
                    oAdvObject = (clsAdvancedBaseModel)clsDB.Get_Case(iId);
                else if (xException)
                    throw new Exception("Invalid type given.");

                // Check if this object even exists
                if (oAdvObject == null)
                {
                    if (xException)
                        throw new Exception("No object found with this id.");
                    else
                        return null;
                }

                // Filter the object
                oObject = filterObject<T>(oAdvObject, bUserLevel, sUserId);
            }
            else
            {
                if (typeof(T) == typeof(clsActivity))
                    oObject = clsDB.Get_Activity(iId);
                else if (typeof(T) == typeof(clsFunction))
                    oObject = clsDB.Get_Function(iId);
                else if (typeof(T) == typeof(clsCategory))
                    oObject = clsDB.Get_Category(iId);
                else if (typeof(T) == typeof(clsSector))
                    oObject = clsDB.Get_Sector(iId);
                else
                    throw new Exception("Invalid type given.");

                if (oObject == null)
                {
                    if (xException)
                        throw new Exception("No object found with this id.");
                    else
                        return null;
                }
            }

            return (T)oObject;
        }

        // Check if object with this id exists, throws exception unless told not to.
        private static T getObject<T>(int iId, bool xException = true) where T : clsBaseModel
        {
            var lstStatic = getList<T>();
            clsBaseModel oObject = lstStatic.Find(x => x.Id == iId);
            if (oObject != null)
                return (T)oObject.Clone();
            else if (xException)
                throw new Exception($"Could not find a {typeof(T).Name} object with this Id.");
            else return null;
        }

        // Check if object with this name exists, throws exception unless told not to.
        private static T getObject<T>(string sName, bool xException = true) where T : clsBaseModel
        {
            var lstStatic = getList<T>();

            clsBaseModel oObject = lstStatic.Find(x => x.Name == sName);
            if (oObject != null)
                return (T)oObject.Clone();
            else if (xException)
                throw new Exception($"Could not find a {typeof(T).Name} object with this name.");
            else return null;
        }

        private static List<clsBaseModel> getList<T>()
        {
            List<clsBaseModel> lstStatic = null;

            if (typeof(T) == typeof(clsActivity))
                lstStatic = clsStatic.Activities;
            else if (typeof(T) == typeof(clsFunction))
                lstStatic = clsStatic.Functions;
            else if (typeof(T) == typeof(clsCategory))
                lstStatic = clsStatic.Categories;
            else if (typeof(T) == typeof(clsSector))
                lstStatic = clsStatic.Sectors;
            else if (typeof(T) == typeof(clsCompany))
                lstStatic = clsStatic.Companies;
            else if (typeof(T) == typeof(clsSoftware))
                lstStatic = clsStatic.Softwares;
            else if (typeof(T) == typeof(clsCase))
                lstStatic = clsStatic.Cases;
            else if (typeof(T) == typeof(clsQuestionCategory))
                lstStatic = clsStatic.QuestionCategories;
            else if (typeof(T) == typeof(clsAnswerMC))
                lstStatic = clsStatic.QuestionAnswers;
            else if (typeof(T) == typeof(clsAbstractQuestions))
                lstStatic = clsStatic.Questions;
            else if (typeof(T) == typeof(clsAbstractSoftwareAnswer))
                lstStatic = clsStatic.GetSoftwareAnswers().Cast<clsBaseModel>().ToList();
            else
                throw new Exception("Invalid type given.");

            return lstStatic;
        }

        // Check user level function
        private static void checkUserLevel(byte bUserLevel)
        {
            if (bUserLevel > 3 || bUserLevel < 1)
                throw new Exception("Invalid user level given.");
        }

        // Get a list of the ids this owner can reach
        private static List<int> getOwnerIds<T>(string sUserId = "") where T : clsBaseModel
        {
            // Ids visible for owner
            List<int> lstIds = new List<int>();

            // User is owner 
            clsLinks oLinks = new clsLinks();
            oLinks.User.Add(sUserId);
            List<clsLink> lstLinks = clsDB.Get_ComRoUsLink(oLinks);

            if (typeof(T) == typeof(clsCompany))
            {
                // Companies are asked for
                // only check for linked companies
                if (lstLinks.Count > 0)
                    lstIds.Add(lstLinks.First().Company);
            }
            else
            {
                // Software or cases are asked
                // get links for linked companies
                if (lstLinks.Count > 0 && lstLinks.First().Company > 0)
                {
                    oLinks = new clsLinks();
                    oLinks.Company.Add(lstLinks.First().Company);
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Add software id to filter 
                        if (typeof(T) == typeof(clsSoftware))
                            if (!lstIds.Contains(oLink.Mes))
                                lstIds.Add(oLink.Mes);

                        // Add case id to filter
                        if (typeof(T) == typeof(clsCase))
                            lstIds.Add(oLink.Case);
                    }
                }

            }

            return lstIds;
        }

        // Filter an objects list
        private static List<clsBaseModel> filterObjectsList<T>(List<clsAdvancedBaseModel> lstObjects, byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true) where T : clsBaseModel
        {
            List<clsBaseModel> lstReturn = new List<clsBaseModel>();

            // Filter objects on user level and visibility
            if (bUserLevel == 3)
                foreach (clsAdvancedBaseModel oObject in lstObjects)
                    lstReturn.Add(oObject);
            else 
            {
                List<int> lstIds = new List<int>();

                // Ids visible for owner
                if (bUserLevel == 2)
                    lstIds = getOwnerIds<T>(sUserId);

                // Filter non public objects unless Id in lstIds   
                foreach (clsAdvancedBaseModel oObject in lstObjects)
                    if ((xIncludePublic && oObject.Public) || lstIds.Contains(oObject.Id))
                        lstReturn.Add(oObject);
            }

            return lstReturn;
        }

        // Filter object 
        private static clsBaseModel filterObject<T>(clsAdvancedBaseModel oObject, byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true) where T : clsBaseModel
        {
            // Filter objects on user level and visibility
            if (bUserLevel == 3)
                return oObject;
            else
            {
                List<int> lstIds = new List<int>();

                // Ids visible for owner
                if (bUserLevel == 2)
                    lstIds = getOwnerIds<T>(sUserId);

                // Filter non public objects unless Id in lstIds   
                if ((xIncludePublic && oObject.Public) || lstIds.Contains(oObject.Id))
                    return oObject;
                else
                    throw new Exception("Access to this object is limited to the owner and administrators.");
            }
        }

        // Get content
        private static string getContentById<T>(int iId, byte bUserLevel = 1, string sUserId = "") where T : clsBaseModel
        {
            // Check incoming type
            if (typeof(T).BaseType != typeof(clsAdvancedBaseModel))
                throw new Exception("Invalid type given.");

            // Get object
            clsBaseModel oObject = getObject<T>(iId);

            // Check if user may see it
            oObject = filterObject<T>((clsAdvancedBaseModel)oObject, bUserLevel, sUserId);

            // Get the content
            if (typeof(T) == typeof(clsCompany))
                oObject = (clsAdvancedBaseModel)clsDB.Get_CompanyContent(iId);
            else if (typeof(T) == typeof(clsSoftware))
                oObject = (clsAdvancedBaseModel)clsDB.Get_MesContent(iId);
            else if (typeof(T) == typeof(clsCase))
                oObject = (clsAdvancedBaseModel)clsDB.Get_CaseContent(iId);

            // Return the content
            return ((clsAdvancedBaseModel)oObject).Content;
        }

        // Edit content
        private static int editContentById<T>(int iId, string sContent, byte bUserLevel = 1, string sUserId = "") where T : clsBaseModel
        {
            lock (oLock)
            {


                // Check incoming type
                if (typeof(T).BaseType != typeof(clsAdvancedBaseModel))
                    throw new Exception("Invalid type given.");

                // Get object
                clsBaseModel oObject = getObject<T>(iId);

                // Check if user may see it
                oObject = filterObject<T>((clsAdvancedBaseModel)oObject, bUserLevel, sUserId);

                // Get the content
                int iReturn = 0;
                if (typeof(T) == typeof(clsCompany))
                    iReturn = clsDB.Edit_CompanyContent(iId, sContent);
                else if (typeof(T) == typeof(clsSoftware))
                    iReturn = clsDB.Edit_MesContent(iId, sContent);
                else if (typeof(T) == typeof(clsCase))
                    iReturn = clsDB.Edit_CaseContent(iId, sContent);

                // Return result
                return iReturn;
            }
        }

        // Edit content visibility
        private static int editContentVisibilityById<T>(int iId, bool xPublic) where T : clsBaseModel
        {
            lock (oLock)
            {

                // Get the content
                int iReturn = 0;
                if (typeof(T) == typeof(clsCompany))
                    iReturn = clsDB.Edit_CompanyVisibility(iId, xPublic);
                else if (typeof(T) == typeof(clsSoftware))
                    iReturn = clsDB.Edit_MesVisibility(iId, xPublic);
                else if (typeof(T) == typeof(clsCase))
                    iReturn = clsDB.Edit_CaseVisibility(iId, xPublic);

                // Return result
                return iReturn;
            }
        }

        // Search function
        private static clsFullyLinked FullyLinkedLogic(List<clsFullLink> oList, byte bUserLevel, string sUserId, clsFullLinks oLinks)
        {
          clsFullyLinked oReturn = new clsFullyLinked();

            // Filter the return values
            List<clsBaseModel> oCompanies = getFilteredObjects<clsCompany>(bUserLevel, sUserId);
            List<clsBaseModel> oSoftwares = getFilteredObjects<clsSoftware>(bUserLevel, sUserId);
            List<clsBaseModel> oCases = getFilteredObjects<clsCase>(bUserLevel, sUserId);

            // Check for null and set to lower case
            if (oLinks.String == null)
                oLinks.String = new List<string>();
            for (int i = 0; i < oLinks.String.Count; i++)
                oLinks.String[i] = oLinks.String[i].ToLower();

            // Format the results and filter with strings as well (search in name and description)
            foreach (clsFullLink o in oList)
            {
                clsBaseModel oCompany = oCompanies.Find(x => x.Id == o.Company);
                if (oCompany != null && !oReturn.Company.Contains(oCompany))
                {
                    if (oLinks.String.Count > 0)
                    {
                        if ((oCompany.Name != null && oLinks.String.Any(oCompany.Name.ToLower().Contains)) 
                            || (oCompany.Description != null && oLinks.String.Any(oCompany.Description.ToLower().Contains)))
                            oReturn.Company.Add(oCompany);
                    }
                    else
                        oReturn.Company.Add(oCompany);
                }

                clsBaseModel oMes = oSoftwares.Find(x => x.Id == o.Mes);
                if (oMes != null && !oReturn.Mes.Contains(oMes))
                {
                    if (oLinks.String.Count > 0)
                    {
                        if ((oMes.Name != null && oLinks.String.Any(oMes.Name.ToLower().Contains)) 
                            || (oMes.Description != null && oLinks.String.Any(oMes.Description.ToLower().Contains)))
                            oReturn.Mes.Add(oMes);
                    }
                    else
                        oReturn.Mes.Add(oMes);
                }

                clsBaseModel oCase = oCases.Find(x => x.Id == o.Case);
                if (oCase != null && !oReturn.Case.Contains(oCase))
                {
                    if (oLinks.String.Count > 0)
                    {
                        if ((oCase.Name != null && oLinks.String.Any(oCase.Name.ToLower().Contains)) 
                            || (oCase.Description != null && oLinks.String.Any(oCase.Description.ToLower().Contains)))
                            oReturn.Case.Add(oCase);
                    }
                    else
                        oReturn.Case.Add(oCase);
                }
            }

            // Company alone has no links and cannot be found, so need to add them manually
            if (oLinks.Company != null && oLinks.Company.Count > oReturn.Company.Count) 
            {
                foreach (clsCompany oCompany in oReturn.Company)
                    oLinks.Company.Remove(oCompany.Id);

                foreach (int iId in oLinks.Company)
                    oReturn.Company.Add(getObject<clsCompany>(iId));
            }

            return oReturn;
        }

        

    }
}
