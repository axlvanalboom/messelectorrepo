﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEC;
using DAL.DB;
using System.Collections;

namespace BLL
{
    public static partial class clsBLL//_Logging
    {
        private static Random random = new Random();
        public static string GetRandomId()
        {
            try
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, 200)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
            catch { return null; }
        }

        public static void AddLog(string sUserId, string sType)
        {
            try
            {
                string sTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
                clsDB.AddLog(sUserId, sTimeStamp, sType);
            }
            catch (Exception e)
            {
                //something went wrong --> wat hiermee doen
                // dit moet niet naar de gebruiker terug gestuurd worden!
                // log errors ook loggen?

                //throw;
            }
        }
        public static void AddLog(string sUserId, string sType, string sParameter)
        {
            try
            {
                //string sTimeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff");
                string sTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
                clsDB.AddLog(sUserId, sTimeStamp, sType, sParameter);
            }
            catch (Exception e)
            {
                //something went wrong --> wat hiermee doen
                // dit moet niet naar de gebruiker terug gestuurd worden!
                // log errors ook loggen?

                //throw;
            }
        }
        public static void AddLog(string sUserId, string sType, int iParameter)
        {
            try
            {
                //string sTimeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff");
                string sTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
                clsDB.AddLog(sUserId, sTimeStamp, sType, iParameter);
            }
            catch (Exception e)
            {
                //something went wrong --> wat hiermee doen
                // dit moet niet naar de gebruiker terug gestuurd worden!
                // log errors ook loggen?

                //throw;
            }
        }

        public static void AddLogMainSearch(string sUserId, clsFullLinks Links)
        {
            if (Links == null) {return; }
            string sTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
            string sParameter = "";
            
            if (Links.Activity != null)
                foreach(int i in Links.Activity)
                    sParameter += "act" + i.ToString() + " ";
            if (Links.Case != null)
                foreach (int i in Links.Case)
                    sParameter += "cas" + i.ToString() + " ";
            if (Links.Mes != null)
                foreach (int i in Links.Mes)
                    sParameter += "mes" + i.ToString() + " ";
            if (Links.Company != null)
                foreach (int i in Links.Company)
                    sParameter += "com" + i.ToString() + " ";
            if (Links.Sector != null)
                foreach (int i in Links.Sector)
                    sParameter += "sec" + i.ToString() + " ";
            if (Links.MesCategory != null)
                foreach (int i in Links.MesCategory)
                    sParameter += "cat" + i.ToString() + " ";
            if (Links.MesFunction != null)
                foreach (int i in Links.MesFunction)
                    sParameter += "fun" + i.ToString() + " ";

            clsDB.AddLog(sUserId, sTimeStamp, "MainSearch", sParameter);
        }

        public static List<clsLog> GetAllLogs()
        {
            try
            {
                return OverrideIds(clsDB.GetAllLogs());
            }
            catch (Exception e) { return null; }
        }

        /// <summary>
        /// Use the Time code to get the last day, week,month or year. 
        /// It's also possible to give a start time and stop time. These times needs to be BOTH epoch- or datetimeformat
        /// </summary>
        /// <param name="sTimeCode"></param>
        /// <param name="sStart"></param>
        /// <param name="sStop"></param>
        /// <returns></returns>
        public static List<clsLog> GetLogsBetweenTime(string sTimeCode, string sStart, string sStop)
        {
            try
            {
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return OverrideIds(clsDB.GetLogsByTime(sStart, sStop));
            }
            catch { return null; }
        }

        public static List<clsLog> GetLogsById(string sId)
        {
            try
            {
                return OverrideIds(clsDB.GetLogsById(sId));
            }
            catch { return null; }
        }
        public static List<clsLog> GetLogsByIdAndTime(string sId, string sTimeCode, string sStart, string sStop)
        {
            try
            {
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return OverrideIds(clsDB.GetLogsByIdAndTime(sId, sStart, sStop));
            }
            catch { return null; }
        }

        public static List<clsLog> GetLogsByType(string sType)
        {
            try
            {
                return OverrideIds(clsDB.GetLogsByType(sType));
            }
            catch { return null; }
        }
        public static List<clsLog> GetLogsByTypeAndTime(string sType, string sTimeCode, string sStart, string sStop)
        {
            try
            {
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return OverrideIds(clsDB.GetLogsByTypeAndTime(sType, sStart, sStop));
            }
            catch { return null; }
        }

        public static List<clsDayAmountVisitors> GetLogsAmountOfVisitors()
        {
            try
            {
                return clsDB.GetAmmountOfVisitors();
            }
            catch { return null; }
        }
        public static List<clsDayAmountVisitors> GetLogsAmountOfVisitorsBetweenTime(string sTimeCode, string sStart, string sStop)
        {
            try
            {
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return clsDB.GetAmmountOfVisitorsBetweenTime(sStart, sStop);
            }
            catch { return null; }
        }

        public static List<clsUserViews> GetLogsUserViews()
        {
            try
            {
                return LogCreateUserViews(OverrideIds(clsDB.GetUserViews()));
            }
            catch { return null; }
        }
        public static List<clsUserViews> GetLogsUserViewsBetweenTime(string sTimeCode, string sStart, string sStop)
        {
            try
            {
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                //return LogCreateUserViews(OverrideIds(clsDB.GetLogsByTime(sStart, sStop)));
                return LogCreateUserViews(OverrideIds(clsDB.GetUserViewsByTime(sStart, sStop)));
            }
            catch { return null; }
        }

        public static List<StringInt> GetLogsAmountDetailView(string sType)
        {
            try
            {
                switch (sType)
                {
                    case "Company":
                        sType = "Company Detail"; break;
                    case "Software":
                        sType = "Software Detail"; break;
                    case "Case":
                        sType = "Case Detail"; break;
                    default:
                        throw new Exception("Wrong Type!");
                }
                return clsDB.GetAmountDetailViews(sType);
            }
            catch { return null; }
        }
        public static List<StringInt> GetLogsAmountDetailViewBetweenTime(string sType, string sTimeCode, string sStart, string sStop)
        {
            try
            {
                switch(sType)
                {
                    case "Company":
                        sType = "Company Detail"; break;
                    case "Software":
                        sType = "Software Detail"; break;
                    case "Case":
                        sType = "Case Detail"; break;
                    default:
                        throw new Exception("Wrong Type!");
                }

                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return clsDB.GetAmountDetailViewsBetweenTime(sType,sStart,sStop);
            }
            catch { return null; }
        }

        public static List<StringInt> GetAmmountOfViewsOffTypeParameter(string sType, string sParameter)
        {
            try
            {
                switch (sType)
                {
                    case "Company":
                        sType = "Company Detail"; break;
                    case "Software":
                        sType = "Software Detail"; break;
                    case "Case":
                        sType = "Case Detail"; break;
                    default:
                        throw new Exception("Wrong Type!");
                }
                return clsDB.GetAmmountOfViewsOffTypeParameter(sType,sParameter);
            }
            catch { return null; }
        }
        public static List<StringInt> GetAmmountOfViewsOffTypeParameterBetweenTime(string sType, string sParameter, string sTimeCode, string sStart, string sStop)
        {
            try
            {
                switch (sType)
                {
                    case "Company":
                        sType = "Company Detail"; break;
                    case "Software":
                        sType = "Software Detail"; break;
                    case "Case":
                        sType = "Case Detail"; break;
                    default:
                        throw new Exception("Wrong Type!");
                }
                LogTimeChecker(ref sTimeCode, ref sStart, ref sStop);
                return clsDB.GetAmmountOfViewsOffTypeParameterBetweenTime(sType, sParameter,sStart,sStop);
            }
            catch { return null; }
        }

        private static List<clsLog> OverrideIds(List<clsLog> oLogs)
        {
            int iUser = 1;

            Hashtable oUsers = new Hashtable();

            foreach (clsLog oLog in oLogs)
            {
                if (oUsers.ContainsKey(oLog.Id))
                {
                    oLog.IdVisible = oUsers[oLog.Id].ToString();
                }
                else
                {
                    oUsers.Add(oLog.Id, iUser.ToString());
                    oLog.IdVisible = iUser.ToString();
                    iUser += 1;
                }
            }

            return oLogs;
        }
        private static void LogTimeChecker(ref string sTimeCode, ref string sStart, ref string sStop)
        {
            if (sTimeCode != null && sTimeCode != "" && sTimeCode != "Other")
            {
                sStop = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
                DateTimeOffset oStart;
                switch (sTimeCode)
                {
                    case "Last Day":
                        oStart = DateTimeOffset.Now.AddDays(-1);
                        break;
                    case "Last Week":
                        oStart = DateTimeOffset.Now.AddDays(-7);
                        break;
                    case "Last Month":
                        oStart = DateTimeOffset.Now.AddMonths(-1);
                        break;
                    case "Last Year":
                        oStart = DateTimeOffset.Now.AddYears(-1);
                        break;
                    case "All Time":
                        oStart = new DateTimeOffset();
                        oStart = oStart.AddYears(2019); // went online in 2020 (starts on year 1 --> +2019)
                        break;
                    default:
                        throw new Exception("Timecode doens't exist!");
                }
                sStart = oStart.ToUnixTimeSeconds().ToString();
            }
            else
            {
                //epoch time is represented as long
                //parsable to long? == epoch time

                long lStart = 0;
                long lStop = 0;

                if (!(long.TryParse(sStart, out lStart) && long.TryParse(sStop, out lStop)))
                {
                    //it's not epoch time
                    // right string format?

                    DateTimeOffset dDate1, dDate2;
                    if (DateTimeOffset.TryParse(sStart, out dDate1) && DateTimeOffset.TryParse(sStop, out dDate2))
                    {
                        // right format 
                        //stop time= 00:00:00 --> change to 23:59:59:99 (end of day)
                        if (dDate2.Hour == 0 && dDate2.Minute == 0 && dDate2.Second == 0 && dDate2.Millisecond == 0)
                        {
                            dDate2 = dDate2.AddHours(23);
                            dDate2 = dDate2.AddMinutes(59);
                            dDate2 = dDate2.AddSeconds(59);
                            dDate2 = dDate2.AddMilliseconds(99);
                        }

                        // make epoch time
                        sStart = dDate1.ToUnixTimeSeconds().ToString();
                        sStop = dDate2.ToUnixTimeSeconds().ToString();
                    }
                    else
                    {
                        throw new Exception("Not the right time format!");
                    }
                }

            }
        }

        private static List<clsUserViews> LogCreateUserViews(List<clsLog> oLogs)
        {
            List<clsUserViews> oUserViews = new List<clsUserViews>();

            foreach (clsLog oLog in oLogs)
            {
                int iIndex = -1;
                for (int i = 0; i < oUserViews.Count; i++)
                {
                    if (oUserViews[i].UserId == oLog.IdVisible)
                    { iIndex = i; break; }
                }

                string sTime = oLog.Time;
                long lTime = 0;
                long.TryParse(sTime, out lTime);
                sTime = DateTimeOffset.FromUnixTimeSeconds(lTime).ToLocalTime().ToString("HH:mm:ss dd/MM/yyyy");
                if (iIndex >= 0)
                { // it already is present!
                    oUserViews[iIndex].Views.Add(new clsViews(sTime, oLog.Type, oLog.Parameter));
                }
                else
                {
                    var oUser = new clsUserViews();
                    oUser.UserId = oLog.IdVisible;
                    oUser.Views = new List<clsViews>();
                    oUser.Views.Add(new clsViews(sTime, oLog.Type, oLog.Parameter));
                    oUserViews.Add(oUser);
                }

            }

            return oUserViews;
        }
    }
}
