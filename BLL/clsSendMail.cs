﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.IO;

namespace BLL
{
    public class clsSendMail
    {

        private static void Send_EMail(string sEmailSender,string sEmailReceipant, string sSubject, string sBody, string sSmtpServer = "smtp.ugent.be")
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {

                using (MailMessage message = new MailMessage())
                {
                    MailAddress fromAddress = new MailAddress(sEmailSender);

                    smtpClient.Host = sSmtpServer;
                    smtpClient.UseDefaultCredentials = false;

                    message.From = fromAddress;
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    message.Body = sBody;
                    message.To.Add(sEmailReceipant);
                    
                    smtpClient.Send(message);
                 
                }
            }
        }
        private static void Send_Emails(string sEmailSender, List<string> oEmailReceipants, List<string> oEmailCcs, string sSubject, string sBody, string sSmtpServer = "smtp.ugent.be")
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                using (MailMessage message = new MailMessage())
                {
                    MailAddress fromAddress = new MailAddress(sEmailSender);

                    smtpClient.Host = sSmtpServer;
                    smtpClient.UseDefaultCredentials = false;

                    message.From = fromAddress;
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    message.Body = sBody;

                    foreach (string s in oEmailReceipants)
                    {
                        message.To.Add(s);
                    }
                    foreach (string s in oEmailCcs)
                    {
                        message.CC.Add(s);
                    }

                    smtpClient.Send(message);
                }
            }
        }

        public static void EmailConfirmation(string sEmailReceipant, string sCallBackUrl)
        {
            string sBody = "<body style=\"margin: 0; padding: 0; \">"
                            + "<tr> <td> Please Confirm your email using next link: </td> </tr> <br>"
                            + "<tr> <td>" + sCallBackUrl + "</td> </tr> <br> <br>"
                            + "Please do not reply. This is an automatic email. For more information visit our website. <br>"
                            + "https://www.isye.be/project/mes4sme/  <br> <br>"
                            + "With kind regards <br> MES4SME team  <br>";

            Send_EMail("noreply-isye-mes4sme@ugent.be",sEmailReceipant, "Email confirmation", sBody);
        }
        public static void PasswordReset(string sEmailReceipant, string sCallBackUrl)
        {
            string sBody = "<body style=\"margin: 0; padding: 0; \">"
                            + "<tr> <td> Use next link to reset your password: </td> </tr> <br>"
                            + "<tr> <td>" + sCallBackUrl + "</td> </tr> <br> <br>"
                            + "Please do not reply. This is an automatic email. For more information visit our website. <br>"
                            + "https://www.isye.be/project/mes4sme/  <br> <br>"
                            + "With kind regards <br> MES4SME team <br>";

            Send_EMail("noreply-isye-mes4sme@ugent.be",sEmailReceipant, "Email reset", sBody);
        }
        
        private static void ContactEmailOriginal(string sName,string sSender,string sSubject, string sBody)
        {
            string sEmailSubject = "MES4SME-selector Contact: Name " + sName +"; Subject "+sSubject;

            string sBodyHtml = "<body style=\"margin: 0; padding: 0; \">"
                    + sBody.Replace("\r\n", "<br>");

            BEC.clsContacts oContacts = DAL.DB.clsDB.GetContacts();
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();
            List<string> Bcc = new List<string>();
                               
            foreach (BEC.clsContact o in oContacts.To) { To.Add(o.Email); }
            To.Add(sSender);

            foreach (BEC.clsContact o in oContacts.Cc) { Cc.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Bcc) { Bcc.Add(o.Email); }

            clsSendMail.Send_Emails(sSender, To, Cc, sEmailSubject, sBodyHtml);
        }
        public static void ContactEmail(string sName, string sSender, string sSubject, string sBody)
        {
            string sEmailSubject = "MES4SME-selector Contact";

            string sBodyHtml = "<body style=\"margin: 0; padding: 0; \">"
                    + "Name: " + sName + "<br>"
                    + "Email: " +sSender + "<br>"
                    + "Subject: " + sSubject + "<br>"
                    + "Text: <br>" 
                    + sBody.Replace("\r\n", "<br>");

            BEC.clsContacts oContacts = DAL.DB.clsDB.GetContacts();
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();
            List<string> Bcc = new List<string>();

            foreach (BEC.clsContact o in oContacts.To) { To.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Cc) { Cc.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Bcc) { Bcc.Add(o.Email); }

            clsSendMail.Send_EMail("noreply-isye-mes4sme@ugent.be", sSender,sEmailSubject,sBodyHtml);
            clsSendMail.Send_Emails("noreply-isye-mes4sme@ugent.be", To, Cc, sEmailSubject, sBodyHtml);
            
        }
    
        public static void LetAdminKnow(string sName,  string sSubject, string sBody)
        {
            string sEmailSubject = "MES4SME-selector TalkToAdmin";

            string sBodyHtml = "<body style=\"margin: 0; padding: 0; \">"
                    + "Sender: " + sName + "<br>"
                    + "Subject: " + sSubject + "<br>"
                    + "Text: <br>"
                    + sBody.Replace("\r\n", "<br>");

            BEC.clsContacts oContacts = DAL.DB.clsDB.GetContacts();
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();
            List<string> Bcc = new List<string>();

            foreach (BEC.clsContact o in oContacts.To) { To.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Cc) { Cc.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Bcc) { Bcc.Add(o.Email); }

            clsSendMail.Send_EMail("noreply-isye-mes4sme@ugent.be", sName, sEmailSubject, sBodyHtml);
            clsSendMail.Send_Emails("noreply-isye-mes4sme@ugent.be", To, Cc, sEmailSubject, sBodyHtml);

        }

        public static void NewUser(string sName, string sEmail)
        {
            string sEmailSubject = "MES4SME-selector New User";

            string sBodyHtml = "<body style=\"margin: 0; padding: 0; \">"
                    + "New User: <br>"
                    + "Name: " + sName + "<br>"
                    + "Email: " +sEmail;

            BEC.clsContacts oContacts = DAL.DB.clsDB.GetContacts();
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();
            List<string> Bcc = new List<string>();

            foreach (BEC.clsContact o in oContacts.To) { To.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Cc) { Cc.Add(o.Email); }
            foreach (BEC.clsContact o in oContacts.Bcc) { Bcc.Add(o.Email); }

            clsSendMail.Send_Emails("noreply-isye-mes4sme@ugent.be", To, Cc, sEmailSubject, sBodyHtml);

        }
    }
}