﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL.DB;
using Newtonsoft.Json;
using HtmlAgilityPack;
using System.Collections;

namespace BLL
{
    public static partial class clsBLL
    {
        #region "Sector"

        // Add a sector
        public static clsReturn addSector(clsSector oSector)
        {
            return addObject<clsSector>(oSector);
        }

        // Edit a sector
        public static clsReturn editSector(clsSector oSector)
        {
            return editObject<clsSector>(oSector);
        }

        // Delete a sector
        public static clsReturn deleteSector(int iId)
        {
            return deleteObject<clsSector>(iId);
        }

        // Get all sectors
        public static clsReturn getSectors()
        {
            return getFilteredObjectsWrapped<clsSector>();
        }

        #endregion

        #region "Activity"

        // Add a Activity
        public static clsReturn addActivity(clsActivity oActivity)
        {
            return addObject<clsActivity>(oActivity);
        }

        // Edit a Activity
        public static clsReturn editActivity(clsActivity oActivity)
        {
            return editObject<clsActivity>(oActivity);
        }

        // Delete a Activity
        public static clsReturn deleteActivity(int iId)
        {
            return deleteObject<clsActivity>(iId);
        }

        // Get all Activitys
        public static clsReturn getActivities()
        {
            return getFilteredObjectsWrapped<clsActivity>();
        }

        #endregion

        #region "Category"

        // Add a Category
        public static clsReturn addCategory(clsCategory oCategory)
        {
            return addObject<clsCategory>(oCategory);
        }

        // Edit a Category
        public static clsReturn editCategory(clsCategory oCategory)
        {
            return editObject<clsCategory>(oCategory);
        }

        // Delete a Category
        public static clsReturn deleteCategory(int iId)
        {
            return deleteObject<clsCategory>(iId);
        }

        // Get all Categorys
        public static clsReturn getCategories()
        {
            return getFilteredObjectsWrapped<clsCategory>();
        }

        #endregion

        #region "Function"

        // Add a Function
        public static clsReturn addFunction(clsFunction oFunction)
        {
            return addObject<clsFunction>(oFunction);
        }

        // Edit a Function
        public static clsReturn editFunction(clsFunction oFunction)
        {
            return editObject<clsFunction>(oFunction);
        }

        // Delete a Function
        public static clsReturn deleteFunction(int iId)
        {
            return deleteObject<clsFunction>(iId);
        }

        // Get all Functions
        public static clsReturn getFunctions()
        {
            return getFilteredObjectsWrapped<clsFunction>();
        }

        #endregion

        #region "Company"

        // Add a company
        public static clsReturn addCompany(clsCompany oCompany)
        {
            return addObject<clsCompany>(oCompany);
        }

        // Edit a company
        public static clsReturn editCompany(clsCompany oCompany, byte bUserLevel = 1, string sUserId = "")
        {
            return editObject<clsCompany>(oCompany, bUserLevel, sUserId);
        }

        // Delete a company
        public static clsReturn deleteCompany(clsCompany oCompany)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check company
                    oCompany = getObject<clsCompany>(oCompany.Id);

                    // Check links for this company
                    clsLinks oLinks = new clsLinks();
                    oLinks.Company.Add(oCompany.Id);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);

                    // If no links ok to delete the company otherwise return the message
                    if (lstLinks == null || lstLinks.Count == 0)
                    {
                        if (clsDB.Delete_Company(oCompany) > 0)
                            clsStatic.Companies = null;
                        else
                            throw new Exception("No company was deleted, please provide an existing company to delete.");
                    }
                    else
                        throw new Exception("Cannot delete a company while it is linked by a software or a case. Delete the linked item(s) and try again.");

                    // Done
                    oReturn.Status = 1;
                    oReturn.Message = $"Successfully deleted company '{oCompany.Name}'.";
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get company
        public static clsReturn getCompanyById(int iId, byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectByIdWrapped<clsCompany>(iId, bUserLevel, sUserId);
        }

        // Get companies
        public static clsReturn getCompanies(byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectsWrapped<clsCompany>(bUserLevel, sUserId);
        }

        // Get companies for config page
        public static clsReturn getConfigCompanies(byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectsWrapped<clsCompany>(bUserLevel, sUserId, false);
        }

        // Get all companies with software activities linked
        public static clsReturn getCompaniesLinked(byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            return getLinkedCompanies(0, bUserLevel, sUserId, xIncludePublic);
        }

        // Get company with software activities linked by id
        public static clsReturn getCompanyLinkedById(int iCompanyId, byte bUserLevel = 1, string sUserId = "")
        {
            return getLinkedCompanies(iCompanyId, bUserLevel, sUserId);
        }

        // Get company objects with links inner function
        private static clsReturn getLinkedCompanies(int iCompanyId = 0, byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Check if all companies are requested or just one and do the necessary
                clsLinks oLinks = null;
                List<clsBaseModel> lstCompanies = new List<clsBaseModel>();
                List<clsUser> lstUsers = new List<clsUser>();

                if (iCompanyId > 0)
                {
                    // Get only the company that is asked for
                    clsCompany oCompany = getFilteredObjectById<clsCompany>(iCompanyId, bUserLevel, sUserId);
                    oLinks = new clsLinks();
                    oLinks.Company.Add(iCompanyId);
                    lstCompanies.Add(oCompany);
                }
                else
                {
                    // Get all companies
                    lstCompanies = getFilteredObjects<clsCompany>(bUserLevel, sUserId, xIncludePublic);
                    lstUsers = clsDB.Get_Users();
                }

                // Create list of linked companies
                List<clsCompanyLinked> lstCompaniesLinked = new List<clsCompanyLinked>();

                // Get all links
                List<clsLink> lstCamscLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                List<clsLink> lstCruLinks = clsDB.Get_ComRoUsLink(oLinks);

                // Loop companies
                foreach (clsCompany oCompany in lstCompanies)
                {
                    // Create a new linked company object from base data
                    clsCompanyLinked oCompanyLinked = new clsCompanyLinked(oCompany);

                    // Link software activities
                    List<clsLink> lstLinks = lstCamscLinks.FindAll(x => x.Company == oCompany.Id && x.Mes > 0 && x.Activity > 0);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Check if software link was already made
                        clsSoftwareLink oSL = oCompanyLinked.SoftwareActivities.Find(x => x.Activity.Id == oLink.Activity && x.Software.Id == oLink.Mes);
                        if (oSL == null)
                        {
                            // Link does not exist yet
                            oCompanyLinked.SoftwareActivities.Add(new clsSoftwareLink(
                                oLink.Id,
                                getObject<clsActivity>(oLink.Activity),
                                getObject<clsSoftware>(oLink.Mes)
                            ));
                        }
                    }

                    // Link case sector
                    lstLinks = lstCamscLinks.FindAll(x => x.Company == oCompany.Id && x.Case > 0 && x.Sector > 0);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Add the case and sector link
                        oCompanyLinked.CasesSectors.Add(new clsCaseLink(
                            oLink.Id,
                            getObject<clsCase>(oLink.Case),
                            getObject<clsSector>(oLink.Sector)
                        ));
                    }

                    // Link users
                    lstLinks = lstCruLinks.FindAll(x => x.Company == oCompany.Id);
                    foreach (clsLink oLink in lstLinks)
                    {
                        clsUser oUser = null;
                        if (lstUsers.Count == 0)
                            oUser = clsDB.Get_User(oLink.User);
                        else
                            oUser = lstUsers.Find(x => x.Id == oLink.User);
                        oCompanyLinked.Contacts.Add(oUser);
                    }

                    // Add linked user to return list
                    lstCompaniesLinked.Add(oCompanyLinked);
                }

                // Return the linked companies
                if (iCompanyId > 0)
                {
                    // Add content when asked per id
                    lstCompaniesLinked.First().Content = ((clsCompany)clsDB.Get_CompanyContent(iCompanyId)).Content;
                    oReturn.Data = lstCompaniesLinked.First();
                }
                else
                    oReturn.Data = lstCompaniesLinked;
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;

            }
            finally
            {

            }

            return oReturn;
        }

        // Get company content by id
        public static clsReturn getCompanyContentById(int iId, byte bUserLevel = 1, string sUserId = "")
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getContentById<clsCompany>(iId, bUserLevel, sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Edit company content by id
        public static clsReturn editCompanyContentById(int iId, string sContent, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {


                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentById<clsCompany>(iId, sContent, bUserLevel, sUserId);
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit company content visibility by id
        public static clsReturn editCompanyContentVisibilityById(int iId, bool xPublic)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentVisibilityById<clsCompany>(iId, xPublic);
                    oReturn.Message = "Company visibility changed.";
                    oReturn.Status = 1;

                    clsStatic.Companies = null;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get ids of the owners companies
        public static clsReturn getOwnedCompanies(string sUserId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getOwnerIds<clsCompany>(sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        #endregion

        #region "Software"

        // Add a software
        public static clsReturn addSoftware(clsSoftware oSoftware, int iCompanyId, int iActivityId, byte bUserLevel = 1, string sUserId = "", List<clsCategoryFunctions> lstCategories = null)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Prerequisetes 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCompany>(sUserId).Contains(iCompanyId))
                            throw new Exception("You do not own this company, cannot add a software with this company.");
                    }

                    clsBaseModel oTemp = getObject<clsSoftware>(oSoftware.Name, false);
                    if (oTemp != null)
                        throw new Exception($"This software already exists.");

                    clsCompany oCompany = getObject<clsCompany>(iCompanyId);
                    clsActivity oActivity = getObject<clsActivity>(iActivityId);
                    ValidateCatFu(lstCategories);

                    // Add software to software table
                    int iSoftwareId = clsDB.Add_Mes(oSoftware);
                    if (iSoftwareId == 0)
                        throw new Exception("Could not add the software.");
                    clsStatic.Softwares = null;

                    // Add link to CAMSC link table
                    int iResult = clsDB.Add_ComAcMeSeCasLink(iCompanyId, iActivityId, iSoftwareId, 0, 0);
                    if (iResult == 0)
                        throw new Exception("Could not add software link, delete the software and try again.");

                    // Add links to MCF link table
                    if (lstCategories != null)
                        foreach (clsCategoryFunctions oCat in lstCategories)
                            foreach (clsFunction oFunc in oCat.Functions)
                                clsDB.Add_MeCatFuLink(iSoftwareId, oCat.Id, oFunc.Id);


                    oReturn.Data = iSoftwareId;
                    oReturn.Status = 1;
                    oReturn.Message = $"Successfully added software '{oSoftware.Name}'.";
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit a software
        public static clsReturn editSoftware(clsSoftware oSoftware, byte bUserLevel = 1, string sUserId = "")
        {
            return editObject<clsSoftware>(oSoftware, bUserLevel, sUserId);
        }

        // Delete a software
        public static clsReturn deleteSoftware(int iSoftwareId)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check and find incoming software id
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftwareId);

                    // Check if no cases are attached on the software, otherwise we may not delete it.
                    clsLinks oLinks = new clsLinks();
                    oLinks.Mes = new List<int>();
                    oLinks.Mes.Add(iSoftwareId);
                    List<clsLink> lstLink = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    foreach (clsLink oLink in lstLink)
                        if (oLink.Case > 0)
                            throw new Exception("Cannot delete this MES software while cases are attached. Remove the cases first.");

                    // If no cases delete the links first and then delete the software from the table
                    if (clsDB.Delete_ComAcMeSeCasLink(0, 0, iSoftwareId, 0, 0) == 0)
                        throw new Exception("The link with this software could not be deleted.");
                    clsDB.Delete_MeCatFuLink_WithMes(iSoftwareId);

                    // Delete the MES
                    if (clsDB.Delete_Mes(iSoftwareId) == 0)
                        throw new Exception($"The software '{oSoftware.Name}' could not be deleted.");

                    // Done
                    clsStatic.Softwares = null;
                    oReturn.Status = 1;
                    oReturn.Message = $"Successfully deleted software '{oSoftware.Name}'.";
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get software
        public static clsReturn getSoftwareById(int iId, byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectByIdWrapped<clsSoftware>(iId, bUserLevel, sUserId);
        }

        // Get softwares
        public static clsReturn getSoftwares(byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectsWrapped<clsSoftware>(bUserLevel, sUserId);
        }

        // Get softwares for config
        public static clsReturn getConfigSoftwares(byte bUserLevel = 1, string sUserId = "")
        {
            return getLinkedSoftware(0, bUserLevel, sUserId, false);
        }

        // Get softwares linked
        public static clsReturn getSoftwareLinked(byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            return getLinkedSoftware(0, bUserLevel, sUserId, xIncludePublic);
        }

        public static clsReturn getSoftwareLinkedById(int iSoftwareId, byte bUserLevel = 1, string sUserId = "")
        {
            return getLinkedSoftware(iSoftwareId, bUserLevel, sUserId);
        }

        private static clsReturn getLinkedSoftware(int iSoftwareId = 0, byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Check if all softwares are requested or just one and do the necessary
                clsLinks oLinks = null;
                List<clsBaseModel> lstSoftwares = new List<clsBaseModel>();
                if (iSoftwareId > 0)
                {
                    clsSoftware oSoftware = getFilteredObjectById<clsSoftware>(iSoftwareId, bUserLevel, sUserId);
                    oLinks = new clsLinks();
                    oLinks.Mes.Add(iSoftwareId);
                    lstSoftwares.Add(oSoftware);
                }
                else
                    lstSoftwares = getFilteredObjects<clsSoftware>(bUserLevel, sUserId, xIncludePublic);

                // Create list of linked softwares
                List<clsSoftwareLinked> lstSoftwaresLinked = new List<clsSoftwareLinked>();

                // Get all links to companys and cases
                List<clsLink> lstCamscLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                List<clsLink> lstMcfLinks = clsDB.Get_MeCatFuLink(oLinks);

                // Loop softwares
                foreach (clsSoftware oSoftware in lstSoftwares)
                {
                    // Create a new software linked object with the base data
                    clsSoftwareLinked oSoftwareLinked = new clsSoftwareLinked(oSoftware);

                    // Link company activities
                    List<clsLink> lstLinks = lstCamscLinks.FindAll(x => x.Mes == oSoftware.Id && x.Company > 0 && x.Activity > 0);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Check if company link was already made
                        clsCompanyLink oCL = oSoftwareLinked.CompanyActivities.Find(x => x.Activity.Id == oLink.Activity && x.Company.Id == oLink.Company);
                        if (oCL == null)
                        {
                            // Link does not exist yet
                            oSoftwareLinked.CompanyActivities.Add(new clsCompanyLink(
                                oLink.Id,
                                getObject<clsActivity>(oLink.Activity),
                                getObject<clsCompany>(oLink.Company)
                            ));
                        }
                    }

                    // Link case sector
                    lstLinks = lstCamscLinks.FindAll(x => x.Mes == oSoftware.Id && x.Case > 0 && x.Sector > 0);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Add the case and sector link
                        oSoftwareLinked.CasesSectors.Add(new clsCaseLink(
                            oLink.Id,
                            getObject<clsCase>(oLink.Case),
                            getObject<clsSector>(oLink.Sector)
                        ));
                    }

                    // Link categories and functions
                    lstLinks = lstMcfLinks.FindAll(x => x.Mes == oSoftware.Id);
                    foreach (clsLink oLink in lstLinks)
                    {
                        // Check if category was already made
                        clsCategoryFunctions oCF = oSoftwareLinked.CategoryFunctions.Find(x => x.Id == oLink.Category);
                        if (oCF == null)
                            oSoftwareLinked.CategoryFunctions.Add(new clsCategoryFunctions(getObject<clsCategory>(oLink.Category)));
                        oSoftwareLinked.CategoryFunctions.Find(x => x.Id == oLink.Category).Functions.Add(getObject<clsFunction>(oLink.Function));
                    }

                    // Add linked user to return list
                    lstSoftwaresLinked.Add(oSoftwareLinked);
                }

                // Return the linked companies
                if (iSoftwareId > 0)
                {
                    lstSoftwaresLinked.First().Content = ((clsSoftware)clsDB.Get_MesContent(iSoftwareId)).Content;
                    oReturn.Data = lstSoftwaresLinked.First();
                }
                else
                    oReturn.Data = lstSoftwaresLinked;
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;

            }
            finally
            { }

            return oReturn;
        }

        // Get software content by id
        public static clsReturn getSoftwareContentById(int iId, byte bUserLevel = 1, string sUserId = "")
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getContentById<clsSoftware>(iId, bUserLevel, sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Edit software content by id
        public static clsReturn editSoftwareContentById(int iId, string sContent, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentById<clsSoftware>(iId, sContent, bUserLevel, sUserId);
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit company content visibility by id
        public static clsReturn editSoftwareContentVisibilityById(int iId, bool xPublic)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentVisibilityById<clsSoftware>(iId, xPublic);
                    oReturn.Message = "Software visibility changed.";
                    oReturn.Status = 1;

                    clsStatic.Softwares = null;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get ids of the owners softwares
        public static clsReturn getOwnedSoftwares(string sUserId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getOwnerIds<clsSoftware>(sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        #endregion

        #region "Cases"

        // Add a new case
        // todo - check that the catfus are added to the software if they did not exist yet
        public static clsReturn addCase(clsCase oCase, int iCompanyId, int iSoftwareId, int iSectorId, int iActivityId, byte bUserLevel = 1, string sUserId = "", List<clsCategoryFunctions> lstCategories = null)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCompany>(sUserId).Contains(iCompanyId))
                            throw new Exception("You do not own this company, cannot add a case with this company.");

                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftwareId))
                            throw new Exception("You do not own this software, cannot add a case with this software.");
                    }

                    clsBaseModel oTemp = getObject<clsCase>(oCase.Name, false);
                    if (oTemp != null)
                        throw new Exception($"This case already exists.");

                    clsCompany oCompany = getObject<clsCompany>(iCompanyId);
                    clsSector oSector = getObject<clsSector>(iSectorId);
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftwareId);
                    clsActivity oActivity = getObject<clsActivity>(iActivityId);
                    ValidateCatFu(lstCategories);

                    // Add the case to the case table
                    int iCaseId = clsDB.Add_Case(oCase);

                    // Create or update link tables

                    // Check current state of table for this situation
                    // Only of same activity, company, software and no case attached edit the link
                    // Otherwise add a new link with the new link information
                    clsLinks oLinks = new clsLinks();
                    oLinks.Activity.Add(iActivityId);
                    oLinks.Company.Add(iCompanyId);
                    oLinks.Mes.Add(iSoftwareId);
                    oLinks.Case.Add(0);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks != null && lstLinks.Count > 0)
                        clsDB.Edit_ComAcMeSeCasLink(lstLinks[0].Id, iCompanyId, iActivityId, iSoftwareId, iSectorId, iCaseId);
                    else
                        clsDB.Add_ComAcMeSeCasLink(iCompanyId, iActivityId, iSoftwareId, iSectorId, iCaseId);

                    // Create the links to the categories and its functions within
                    if (lstCategories != null)
                        foreach (clsCategoryFunctions oCatFu in lstCategories)
                            foreach (clsFunction oFu in oCatFu.Functions)
                                clsDB.Add_CasCatFuLink(iCaseId, oCatFu.Id, oFu.Id);

                    // Done
                    clsStatic.Cases = null;
                    oReturn.Data = iCaseId;
                    oReturn.Message = $"Successfully added case '{oCase.Name}'.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit case
        public static clsReturn editCase(clsCase oCase, byte bUserLevel = 1, string sUserId = "")
        {
            return editObject<clsCase>(oCase, bUserLevel, sUserId);
        }

        // Edit the case sector, check for case and sector 
        public static clsReturn editCaseSector(int iCase, int iSector)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check and find incoming sector id
                    clsSector oSector = getObject<clsSector>(iSector);

                    // Check and find incoming case id
                    clsCase oCase = getObject<clsCase>(iCase);

                    // Find the link id
                    int iLinkId = 0;
                    clsLinks oLinks = new clsLinks();
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks != null && lstLinks.Count > 0)
                        iLinkId = lstLinks[0].Id;
                    else
                        throw new Exception("No link found for this case");

                    // Edit the sector in the link
                    clsDB.Edit_ComAcMeSeCasLink(iLinkId, 0, 0, 0, iSector, 0);

                    oReturn.Message = "Succesfully changed the sector for this case.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Delete case
        public static clsReturn deleteCaseById(int iCaseId)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // No checks to be done, if the given case id does not exist it cannot do harm
                    // If this is the only case linked to the same company, activity and software this link must remain 
                    // with DBNull set to case id and sector, otherwise we can delete it completely
                    // First we check the number of links 

                    // Check and find incoming case id
                    clsCase oCase = getObject<clsCase>(iCaseId);

                    // Check if link exists
                    clsLinks oLinks = new clsLinks();
                    oLinks.Case.Add(iCaseId);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks == null || lstLinks.Count == 0)
                        throw new Exception("This case has no link, check database consistency.");

                   
                    //// Now delete the catfu links
                    //clsDB.Delete_CasCatFuLink_WithCase(iCaseId);
                    clsLinks oCasCatFus= new clsLinks();
                    oCasCatFus.Case.Add(iCaseId);
                    List<clsLink> lstCasCatFus = clsDB.Get_CasCatFuLink(oCasCatFus);
                    foreach(clsLink oCasCatfu in lstCasCatFus)
                    {
                        deleteCaseCategoryFunctionLink(iCaseId, oCasCatfu.Category, oCasCatfu.Function, 3, "");
                    }

                    // Check links for same company activity
                    oLinks = new clsLinks();
                    oLinks.Company.Add(lstLinks[0].Company);
                    oLinks.Activity.Add(lstLinks[0].Activity);
                    oLinks.Mes.Add(lstLinks[0].Mes);
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks == null)
                        throw new Exception("No link exists for the combination of company, software and activity, check DB consistency first.");

                    // Delete or update the link
                    if (lstLinks.Count == 1)
                        clsDB.Edit_ComAcMeSeCasLink(lstLinks[0].Id, 0, 0, 0, null, null);
                    else
                        clsDB.Delete_ComAcMeSeCasLink(0, 0, 0, 0, iCaseId);


                    // And delete the case
                    if (clsDB.Delete_Case(iCaseId) == 0)
                        throw new Exception("No case found to delete");

                    // Done
                    clsStatic.Cases = null;
                    oReturn.Status = 1;
                    oReturn.Message = $"Successfully deleted case '{oCase.Name}'.";

                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }

                return oReturn;
            }
        }

        // Get case by id
        public static clsReturn getCaseById(int iCaseId, byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectByIdWrapped<clsCase>(iCaseId, bUserLevel, sUserId);
        }

        // Get all cases
        public static clsReturn getCases(byte bUserLevel = 1, string sUserId = "")
        {
            return getFilteredObjectsWrapped<clsCase>(bUserLevel, sUserId);
        }

        // Get linked case by id
        public static clsReturn getCaseLinkedById(int iCaseId, byte bUserLevel = 1, string sUserId = "")
        {
            return getLinkedCases(iCaseId, bUserLevel, sUserId);
        }

        // Get linked cases
        public static clsReturn getCasesLinked(byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            return getLinkedCases(0, bUserLevel, sUserId, xIncludePublic);
        }

        // Get linked cases inner function
        private static clsReturn getLinkedCases(int iCaseId = 0, byte bUserLevel = 1, string sUserId = "", bool xIncludePublic = true)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Check if all cases are requested or just one and do the necessary
                clsLinks oLinks = null;
                List<clsBaseModel> lstCases = new List<clsBaseModel>();
                if (iCaseId > 0)
                {
                    clsCase oCase = getFilteredObjectById<clsCase>(iCaseId, bUserLevel, sUserId);
                    oLinks = new clsLinks();
                    oLinks.Case.Add(iCaseId);
                    lstCases.Add(oCase);
                }
                else
                    lstCases = getFilteredObjects<clsCase>(bUserLevel, sUserId, xIncludePublic);

                // Create list of linked cases
                List<clsCaseLinked> lstCasesLinked = new List<clsCaseLinked>();

                // Get the links for this case
                List<clsLink> lstCamsLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                List<clsLink> lstCfLinks = clsDB.Get_CasCatFuLink(oLinks);

                // Check links
                if (lstCamsLinks == null || lstCamsLinks.Count() < lstCases.Count())
                    throw new Exception("No links or not enough links found to match all the cases.");

                // Loop cases
                foreach (clsCase oCase in lstCases)
                {
                    // Create case linked objects based on the case data
                    clsCaseLinked oCaseLinked = new clsCaseLinked(oCase);

                    // Get cams link for this case
                    clsLink oLink = lstCamsLinks.Find(x => x.Case == oCase.Id);
                    if (oLink == null)
                        throw new Exception($"No cams link found for case {oCase.Name}.");

                    // Fill the object with the linked data
                    oCaseLinked.Company = getObject<clsCompany>(oLink.Company);
                    oCaseLinked.Activity = getObject<clsActivity>(oLink.Activity);
                    oCaseLinked.Sector = getObject<clsSector>(oLink.Sector);
                    oCaseLinked.Software = getObject<clsSoftware>(oLink.Mes);

                    // Get cf link for this case
                    List<clsLink> lstLinks = lstCfLinks.FindAll(x => x.Case == oCase.Id);
                    foreach (clsLink oLnk in lstLinks)
                    {
                        // Check if category was already made
                        clsCategoryFunctions oCF = oCaseLinked.CategoryFunctions.Find(x => x.Id == oLnk.Category);
                        if (oCF == null)
                            oCaseLinked.CategoryFunctions.Add(new clsCategoryFunctions(getObject<clsCategory>(oLnk.Category)));
                        oCaseLinked.CategoryFunctions.Find(x => x.Id == oLnk.Category).Functions.Add(getObject<clsFunction>(oLnk.Function));
                    }
                    
                    // Return linked object 
                    lstCasesLinked.Add(oCaseLinked);
                }

                // Return the linked cases
                if (iCaseId > 0)
                {
                    lstCasesLinked.First().Content = ((clsCase)clsDB.Get_CaseContent(iCaseId)).Content;
                    oReturn.Data = lstCasesLinked.First();
                }
                else
                    oReturn.Data = lstCasesLinked;

                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;

            }
            finally
            {

            }

            return oReturn;
        }

        // Get case content by id
        public static clsReturn getCaseContentById(int iId, byte bUserLevel = 1, string sUserId = "")
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getContentById<clsCase>(iId, bUserLevel, sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Edit case content by id
        public static clsReturn editCaseContentById(int iId, string sContent, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentById<clsCase>(iId, sContent, bUserLevel, sUserId);
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit case content visibility by id
        public static clsReturn editCaseContentVisibilityById(int iId, bool xPublic)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Get the content
                    oReturn.Data = editContentVisibilityById<clsCase>(iId, xPublic);
                    oReturn.Message = "Case visibility changed.";
                    oReturn.Status = 1;

                    clsStatic.Cases = null;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Get ids of the owners cases
        public static clsReturn getOwnedCases(string sUserId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get the content
                oReturn.Data = getOwnerIds<clsCase>(sUserId);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        #endregion

        #region "User"

        // Get user by Id
        public static clsReturn getUserById(string sUserId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
               var oData = clsDB.Get_User(sUserId);
                if (oData !=null)
                {
                    oReturn.Data = oData;
                    oReturn.Status = 1;
                }
                else { oReturn.Message = "This user doesn't exist!"; }
                
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;

            }
            finally
            {

            }

            return oReturn;
        }
        
        public static clsReturn getUserByEmail(string sUserEmail)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                var oData = clsDB.Get_UserByEmail(sUserEmail);
                if (oData != null)
                {
                    oReturn.Data = oData;
                    oReturn.Status = 1;
                }
                else { oReturn.Message = "This user doesn't exist!"; }

            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
                oReturn.Status = 0;
                oReturn.Data = null;
            }
            finally
            {

            }

            return oReturn;
        }

        // Get all the users
        public static clsReturn getUsers()
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                oReturn.Data = clsDB.Get_Users();
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;

            }
            finally
            {

            }

            return oReturn;
        }

        // Edit user
        public static clsReturn editUser(clsUser oUser)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Edit of the regular user information
                // Check for user Id first and edit the user contents
                clsUser oOldUser = clsDB.Get_User(oUser.Id);
                if (oOldUser == null)
                    throw new Exception("This user does not exist, changes not possible.");
                    
                oReturn.Data = clsDB.Edit_User(oUser);
                oReturn.Message = $"User {oUser.UserName} updated successfully.";
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Edit users company and role
        public static clsReturn editUserComRo(string sUserId, int iCompanyId, int iRoleId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Check user

                clsUser oUser = clsDB.Get_User(sUserId);
                if (oUser == null)
                {
                    oReturn.Message = "Invalid user id.";
                    return oReturn;
                }

                // Check role
                if (iRoleId < 1 || iRoleId > 3)
                {
                    oReturn.Message = "Invalid role id.";
                    return oReturn;
                }

                // Check company id
                int? inCompanyId = iCompanyId;
                clsCompany oCompany = getObject<clsCompany>(iCompanyId, false);
                if (oCompany == null)
                    inCompanyId = null;

                // Change or update the link table row
                clsLinks oLinks = new clsLinks();
                oLinks.User.Add(oUser.Id);
                List<clsLink> lstLinks = clsDB.Get_ComRoUsLink(oLinks);
                if (lstLinks == null || lstLinks.Count() == 0)
                {
                    // No link exists, add a new link
                    clsDB.Add_ComRoUsLink(inCompanyId, iRoleId, sUserId);
                }
                else
                {
                    // Update the existing link
                    clsDB.Edit_ComRoUsLink(lstLinks[0].Id, inCompanyId, iRoleId, sUserId);
                }

                oReturn.Message = $"Role and company for user '{oUser.UserName}' successfully updated.";
                oReturn.Data = new object();
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Delete user (also delete the link)
        public static clsReturn deleteUser(string sUserId)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Check if the user exists
                clsUser oUser = clsDB.Get_User(sUserId);
                if (oUser == null)
                {
                    oReturn.Message = "Invalid user id.";
                    return oReturn;
                }

                // Check if link exitst
                clsLinks oLinks = new clsLinks();
                oLinks.User.Add(oUser.Id);
                List<clsLink> lstLinks = clsDB.Get_ComRoUsLink(oLinks);
                if (lstLinks != null && lstLinks.Count() > 0)
                {
                    // Link exists, delete the link 
                    clsDB.Delete_ComRoUsLink(lstLinks[0].Id);
                }
                
                // Delete the user
                oReturn.Data = clsDB.Delete_User(sUserId);
                oReturn.Message = $"User {oUser.UserName} deleted successfully.";
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        // Get users linked information
        public static clsReturn getUsersLinked()
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                // Get all users
                List<clsUser> lstUsers = clsDB.Get_Users();

                // Get all links
                List<clsLink> lstLinks = clsDB.Get_ComRoUsLink();

                // Get all roles
                List<clsRole> lstRoles = clsDB.Get_Roles();

                // Link the data in the objects
                List<clsUserLinked> lstUsersLinked = new List<clsUserLinked>();

                foreach (clsUser oUser in lstUsers)
                {
                    // Add user info to linked user
                    clsUserLinked oUserLinked = new clsUserLinked(oUser);

                    // Search for existing link
                    clsLink oLink = lstLinks.Find(x => x.User == oUser.Id);
                    if (oLink != null)
                    {
                        // Add company 
                        oUserLinked.Company = getObject<clsCompany>(oLink.Company, false);

                        // Add role
                        oUserLinked.Role = lstRoles.Find(x => x.Id == oLink.Role);
                    }

                    // Add linked user to response list
                    lstUsersLinked.Add(oUserLinked);
                }

                // Done
                oReturn.Data = lstUsersLinked;
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }
            finally
            {

            }

            return oReturn;
        }

        #endregion

        #region "Links"

        // Add a company activity to a software
        // Check if it is not already existing, only 1 supplier possible?
        public static clsReturn addSoftwareCompanyActivityLink(int iSoftware, int iCompany, int iActivity)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsCompany oCompany = getObject<clsCompany>(iCompany);
                    clsActivity oActivity = getObject<clsActivity>(iActivity);

                    // Check if link already exists
                    clsLinks oLinks = new clsLinks();
                    oLinks.Activity.Add(iActivity);
                    oLinks.Company.Add(iCompany);
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);

                    // If link already exists return why, otherwise add the new link
                    if (lstLinks != null && lstLinks.Count > 0)
                        throw new Exception("This type of link already exists between this software, company and activity.");

                    oReturn.Data = clsDB.Add_ComAcMeSeCasLink(iCompany, iActivity, iSoftware, 0, 0);
                    oReturn.Message = "Succesfully added company, software and activity link.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Delete a company activity to a software
        // Check if the link has no case attached, check if not the only link to this software
        public static clsReturn deleteSoftwareCompanyActivityLink(int iSoftware, int iCompany, int iActivity)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsCompany oCompany = getObject<clsCompany>(iCompany);
                    clsActivity oActivity = getObject<clsActivity>(iActivity);

                    // Check the existing links for this company and software to know if it is not the only one
                    clsLinks oLinks = new clsLinks();
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);

                    // Check for consistency
                    if (lstLinks != null)
                    {
                        // Delete all links with a case get the number deleted to know if there were
                        if (lstLinks.RemoveAll(x => x.Case > 0 || x.Company != iCompany || x.Activity != iActivity) > 0)
                        {
                            if (lstLinks.Count == 0)
                                throw new Exception("Cannot delete this activity, it has a case attached.");

                            // Delete the link(s), should only be one but if more clean it up
                            foreach (clsLink oLink in lstLinks)
                                clsDB.Delete_ComAcMeSeCasLink_WithId(oLink.Id);
                        }
                        else
                            throw new Exception("Cannot delete this activity, it is the only link to this software.");
                    }

                    oReturn.Message = "Succesfully deleted the link.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Add categorys and functions to a software
        // Check for existance before adding
        private static clsReturn addSoftwareCategoryFunctionLinkOld(int iSoftware, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftware))
                            throw new Exception("You do not own this software.");
                    }

                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and add it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                    if (lstLinks == null || lstLinks.Count() == 0)
                        clsDB.Add_MeCatFuLink(iSoftware, iCategory, iFunction);
                    else
                        throw new Exception("This function was already part of this software, no changes performed");

                    oReturn.Message = "Succesfully added function to this software.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Delete categorys and functions to a software
        // No checks, may be deleted at any time
        private static clsReturn deleteSoftwareCategoryFunctionLinkOld(int iSoftware, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftware))
                            throw new Exception("You do not own this software.");
                    }

                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and delete it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                    if (lstLinks != null && lstLinks.Count() > 0)
                        foreach (clsLink oLink in lstLinks)
                            clsDB.Delete_MeCatFuLink_WithId(oLink.Id);
                    else
                        throw new Exception("This function is not part of this software, no changes done.");

                    ////delete the catfu's, because the won't be right anymore
                    List<int> oMes = new List<int>() { iSoftware };
                    clsLinks oMesCatFus = new clsLinks();
                    oMesCatFus.Mes = oMes;
                    List<clsLink> oLinks2 = clsDB.Get_ComAcMeSeCasLink(oMesCatFus);
                    foreach (clsLink oLink in oLinks2)
                    {
                        int iCase = oLink.Case;
                        if (iCase != 0)
                        {
                            // delete the catfu's of the case, cause the software doens't offer this catfu anymore
                            List<int> oCases = new List<int>() { iCase };
                            clsLinks oCaseCatFus = new clsLinks();
                            oCaseCatFus.Case = oCases;
                            List<clsLink> oCatFus = clsDB.Get_CasCatFuLink(oCaseCatFus);
                            foreach (clsLink l in oCatFus)
                            {
                                if (l.Category == iCategory && l.Function == iFunction)
                                {
                                    deleteCaseCategoryFunctionLink(iCase, iCategory, iFunction, bUserLevel, sUserId);
                                }
                            }
                        }
                    }

                    oReturn.Message = "Succesfully deleted function from this software.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Add a function and category to a case
        // Check if existing and if this software is supporting this functions first!!
        private static clsReturn addCaseCategoryFunctionLinkOld(int iCase, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCase>(sUserId).Contains(iCase))
                            throw new Exception("You do not own this case.");
                    }

                    // Check and find incoming objects
                    clsCase oCase = getObject<clsCase>(iCase);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check if function already exists
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_CasCatFuLink(oLinks);
                    if (lstLinks == null || lstLinks.Count() == 0)
                    {
                        // Check if the software supports the requested function
                        int iSoftware = 0;
                        oLinks = new clsLinks();
                        oLinks.Case.Add(iCase);
                        lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                        if (lstLinks != null && lstLinks.Count() > 0)
                            iSoftware = lstLinks[0].Mes;
                        if (iSoftware == 0)
                            throw new Exception("No software found for this case.");

                        oLinks = new clsLinks();
                        oLinks.Mes.Add(iSoftware);
                        lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                        clsLink oLink = lstLinks.Find(
                            x => x.Category == iCategory &&
                            x.Mes == iSoftware &&
                            x.Function == iFunction);
                        if (oLink == null)
                            throw new Exception("The case software does not support this function in this category, add it before trying again.");

                        // Add the link
                        clsDB.Add_CasCatFuLink(iCase, iCategory, iFunction);
                    }
                    else
                        throw new Exception("This function was already part of this case, no changes performed");

                    oReturn.Message = "Succesfully added function to this case.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }

                return oReturn;
            }
        }

        // Delete function and category to a case
        // Check if existing first
        private static clsReturn deleteCaseCategoryFunctionLinkOld(int iCase, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCase>(sUserId).Contains(iCase))
                            throw new Exception("You do not own this case.");
                    }

                    // Check and find incoming objects
                    clsCase oCase = getObject<clsCase>(iCase);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and delete it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_CasCatFuLink(oLinks);
                    if (lstLinks != null && lstLinks.Count() > 0)
                        foreach (clsLink oLink in lstLinks)
                            clsDB.Delete_CasCatFuLink_WithId(oLink.Id);
                    else
                        throw new Exception("This function is not part of this case, no changes done.");

                    oReturn.Message = "Succesfully deleted function from this case.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Edit case company and/or software
        // Check if company already exists
        // If new combination with the same company/activity/software already exists with no case attached then attach to this case
        // If the new combination does not exist add new link
        // If old combination company/activity/software not exists
        public static clsReturn editCaseCompanySoftware(int iCase, int iCompany, int iSoftware, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCompany>(sUserId).Contains(iCompany))
                            throw new Exception("You do not own this company.");

                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftware))
                            throw new Exception("You do not own this software.");

                        if (!getOwnerIds<clsCase>(sUserId).Contains(iCase))
                            throw new Exception("You do not own this case.");
                    }

                    // No need to check if parameters exist as the link query will have no results

                    // Find the original link for this case
                    clsLinks oLinks = new clsLinks();
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks == null || lstLinks.Count != 1)
                        throw new Exception("No or too many link(s) found for these parameters, check DB consistency or parameter input.");

                    clsLink oOldLink = lstLinks[0];

                    // Check if changes are required
                    if (oOldLink.Company == iCompany && oOldLink.Mes == iSoftware)
                        throw new Exception("No changes to be perform.");

                    // Old link
                    oLinks = new clsLinks();
                    oLinks.Company.Add(oOldLink.Company);
                    oLinks.Activity.Add(oOldLink.Activity);
                    oLinks.Mes.Add(oOldLink.Mes);
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    // Delete this link
                    lstLinks.RemoveAll(x => x.Case == iCase);
                    if (lstLinks.Count > 0)
                    {
                        // This company, activity and software combination will still exist after deletion of the old link.
                        // Ok to delete the old link when changing the software and/or the company.
                        clsDB.Delete_ComAcMeSeCasLink_WithId(oOldLink.Id);
                    }
                    else
                    {
                        // This link is the only link for this company, activity and software combination.
                        // Only delete case and sector from the old link
                        clsDB.Edit_ComAcMeSeCasLink(oOldLink.Id, 0, 0, 0, null, null);
                    }

                    //Get the activity from the new company and software!
                    
                    oLinks = new clsLinks();
                    oLinks.Company.Add(iCompany);
                    oLinks.Mes.Add(iSoftware);
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    // a company, software can only have 1 activity that link them -->all the activities will be the same
                    int iNewActivity = lstLinks[0].Activity; 

                    // New link
                    oLinks = new clsLinks();
                    oLinks.Company.Add(iCompany);
                    oLinks.Mes.Add(iSoftware);
                    oLinks.Case.Add(0);
                    // is wrong! needs to be updated to the activty off the new company and software!!
                    //oLinks.Activity.Add(oOldLink.Activity); 
                    oLinks.Activity.Add(iNewActivity);
                    oReturn.Data = iNewActivity;
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks.Count > 0)
                    {
                        // This combination already exists without a case attached
                        // Attach the case and sector to this link
                        clsDB.Edit_ComAcMeSeCasLink(lstLinks[0].Id, 0, 0, 0, oOldLink.Sector, iCase);
                    }
                    else
                    {
                        // This combination does not exist yet
                        // Create a new link with this combination and this case
                        clsDB.Add_ComAcMeSeCasLink(iCompany, oOldLink.Activity, iSoftware, oOldLink.Sector, iCase);
                    }

                    //// delete the catfu's, because the won't be right anymore
                    //List<int> oCases = new List<int>() { iCase };
                    //clsLinks oCaseCatFus = new clsLinks();
                    //oCaseCatFus.Case = oCases;
                    //List<clsLink> oCatFus = clsDB.Get_CasCatFuLink(oCaseCatFus);
                    //foreach (clsLink oLink in oCatFus)
                    //{
                    //    deleteCaseCategoryFunctionLink(oLink.Case, oLink.Category, oLink.Function, bUserLevel, sUserId);
                    //}

                    //// Now delete the catfu links
                    //clsDB.Delete_CasCatFuLink_WithCase(iCaseId);
                    clsLinks oCasCatFus = new clsLinks();
                    oCasCatFus.Case.Add(iCase);
                    List<clsLink> lstCasCatFus = clsDB.Get_CasCatFuLink(oCasCatFus);
                    foreach (clsLink oCasCatfu in lstCasCatFus)
                    {
                        deleteSoftwareCategoryFunctionLink(oOldLink.Mes, oCasCatfu.Category, oCasCatfu.Function, bUserLevel, sUserId);
                        addSoftwareCategoryFunctionLink(iSoftware, oCasCatfu.Category, oCasCatfu.Function, bUserLevel, sUserId);
                    }

                    oReturn.Message = "Succesfully edited the case.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }

                return oReturn;
            }
        }

        public static clsReturn getFullyLinked(clsFullLinks oLinks = null, byte bUserLevel = 0, string sUserId = "")
        {
            clsReturn oReturn = new clsReturn();
            try
            {
                oReturn.Data = FullyLinkedLogic(clsDB.Get_FullyLinked(oLinks), bUserLevel, sUserId, oLinks);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
                oReturn.Data = null;
                oReturn.Status = 0;
            }
            return oReturn;
        }


        // Add categorys and functions to a software
        // Check for existance before adding
        private static clsReturn addSoftwareCategoryFunctionLink(int iSoftware, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftware))
                            throw new Exception("You do not own this software.");
                    }

                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and add it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                    if (lstLinks == null || lstLinks.Count() == 0)
                    {
                        clsDB.Add_MeCatFuLink(iSoftware, iCategory, iFunction);
                        oReturn.Message = "Succesfully added function to this software.";
                    }
                    else
                        oReturn.Message = "Function already presented at this software";

                    oReturn.Status = 1;

                }
                catch (Exception oException) { oReturn.Message = oException.Message; }


                return oReturn;
            }
        }

        // Add a function and category to a case
        // Check if existing and if this software is supporting this functions first!!
        public static clsReturn addCaseCategoryFunctionLink(int iCase, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCase>(sUserId).Contains(iCase))
                            throw new Exception("You do not own this case.");
                    }

                    // Check and find incoming objects
                    clsCase oCase = getObject<clsCase>(iCase);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check if function already exists
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_CasCatFuLink(oLinks);
                    if (lstLinks == null || lstLinks.Count() == 0)
                    {
                        // Check if the software supports the requested function
                        int iSoftware = 0;
                        oLinks = new clsLinks();
                        oLinks.Case.Add(iCase);
                        lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                        if (lstLinks != null && lstLinks.Count() > 0)
                            iSoftware = lstLinks[0].Mes;
                        if (iSoftware == 0)
                            throw new Exception("No software found for this case.");

                        // Add the link
                        clsDB.Add_CasCatFuLink(iCase, iCategory, iFunction);

                        // no we need to add it to the software
                        addSoftwareCategoryFunctionLink(iSoftware, iCategory, iFunction, bUserLevel, sUserId);

                        //oLinks = new clsLinks();
                        //oLinks.Mes.Add(iSoftware);
                        //lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                        //clsLink oLink = lstLinks.Find(
                        //    x => x.Category == iCategory &&
                        //    x.Mes == iSoftware &&
                        //    x.Function == iFunction);
                        //if (oLink == null)
                        //    throw new Exception("The case software does not support this function in this category, add it before trying again.");
                   
                    }
                    else
                        throw new Exception("This function was already part of this case, no changes performed");

                    oReturn.Message = "Succesfully added function to this case.";
                    oReturn.Status = 1;
                
                }
                catch (Exception oException) { oReturn.Message = oException.Message; }

                return oReturn;
            }
        }

        // Delete function and category to a case
        // Check if existing first
        public static clsReturn deleteCaseCategoryFunctionLink(int iCase, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsCase>(sUserId).Contains(iCase))
                            throw new Exception("You do not own this case.");
                    }

                    // Check and find incoming objects
                    clsCase oCase = getObject<clsCase>(iCase);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and delete it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Case.Add(iCase);
                    List<clsLink> lstLinks = clsDB.Get_CasCatFuLink(oLinks);
                    if (lstLinks != null && lstLinks.Count() > 0)
                        foreach (clsLink oLink in lstLinks)
                            clsDB.Delete_CasCatFuLink_WithId(oLink.Id);
                    else
                        throw new Exception("This function is not part of this case, no changes done.");


                    // delete the catfu from the software
                    oLinks = new clsLinks();
                    oLinks.Case.Add(iCase);
                    lstLinks = clsDB.Get_ComAcMeSeCasLink(oLinks);
                    if (lstLinks != null && lstLinks.Count() > 0)
                        deleteSoftwareCategoryFunctionLink(lstLinks[0].Mes,iCategory,iFunction,bUserLevel,sUserId);

                    oReturn.Message = "Succesfully deleted function from this case.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }

        // Delete categorys and functions to a software
        // No checks, may be deleted at any time
        private static clsReturn deleteSoftwareCategoryFunctionLink(int iSoftware, int iCategory, int iFunction, byte bUserLevel = 1, string sUserId = "")
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    // Check prerequisites 
                    if (bUserLevel < 3)
                    {
                        if (!getOwnerIds<clsSoftware>(sUserId).Contains(iSoftware))
                            throw new Exception("You do not own this software.");
                    }

                    // Check and find incoming objects
                    clsSoftware oSoftware = getObject<clsSoftware>(iSoftware);
                    clsFunction oFunction = getObject<clsFunction>(iFunction);
                    clsCategory oCategory = getObject<clsCategory>(iCategory);

                    // Check for existance of the link in the DB and delete it
                    clsLinks oLinks = new clsLinks();
                    oLinks.Category.Add(iCategory);
                    oLinks.Function.Add(iFunction);
                    oLinks.Mes.Add(iSoftware);
                    List<clsLink> lstLinks = clsDB.Get_MeCatFuLink(oLinks);
                    if (lstLinks != null && lstLinks.Count() > 0)
                    {
                        List<int> oMes = new List<int>() { iSoftware };
                        clsLinks oMesCatFus = new clsLinks();
                        oMesCatFus.Mes = oMes;
                        List<clsLink> oLinks2 = clsDB.Get_ComAcMeSeCasLink(oMesCatFus);
                        bool xContain = false;
                        foreach (clsLink oLink in oLinks2)
                        {
                            int iCase = oLink.Case;
                            if (iCase != 0)
                            {
                                // contains this case the catfu?

                                clsLinks oLinks3 = new clsLinks();
                                oLinks3.Category.Add(iCategory);
                                oLinks3.Function.Add(iFunction);
                                oLinks3.Case.Add(iCase);
                                List<clsLink> lstLinks3 = clsDB.Get_CasCatFuLink(oLinks3);
                                if (lstLinks3 != null && lstLinks3.Count() > 0)
                                {
                                    //there is a case containing this catfu --> DO NOT DELETE IT
                                    xContain = true;
                                    break;
                                }

                            }
                        }

                        if (!xContain) // de link effectief verwijderen      
                            foreach (clsLink oLink in lstLinks)
                                clsDB.Delete_MeCatFuLink_WithId(oLink.Id);
                    }
                    else
                        throw new Exception("This function is not part of this software, no changes done.");

                    

                    oReturn.Message = "Succesfully deleted function from this software.";
                    oReturn.Status = 1;
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;

                }
                finally
                {

                }

                return oReturn;
            }
        }


        #endregion

        #region "EditorJS" 

        /// <summary>
        /// Uses HtmlAgilityPack to get the meta information from a url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static clsMetaInformation GetMetaDataFromUrl(string url)
        {
            // Get the URL specified
            var webGet = new HtmlWeb();
            var document = webGet.Load(url);
            var metaTags = document.DocumentNode.SelectNodes("//meta");
            clsMetaInformation metaInfo = new clsMetaInformation(url);
            if (metaTags != null)
            {
                int matchCount = 0;
                foreach (var tag in metaTags)
                {
                    var tagName = tag.Attributes["name"];
                    var tagContent = tag.Attributes["content"];
                    var tagProperty = tag.Attributes["property"];
                    if (tagName != null && tagContent != null)
                    {
                        switch (tagName.Value.ToLower())
                        {
                            case "title":
                                metaInfo.Title = tagContent.Value;
                                matchCount++;
                                break;
                            case "description":
                                metaInfo.Description = tagContent.Value;
                                matchCount++;
                                break;
                            case "twitter:title":
                                metaInfo.Title = string.IsNullOrEmpty(metaInfo.Title) ? tagContent.Value : metaInfo.Title;
                                matchCount++;
                                break;
                            case "twitter:description":
                                metaInfo.Description = string.IsNullOrEmpty(metaInfo.Description) ? tagContent.Value : metaInfo.Description;
                                matchCount++;
                                break;
                            case "twitter:image":
                                metaInfo.ImageUrl = string.IsNullOrEmpty(metaInfo.ImageUrl) ? tagContent.Value : metaInfo.ImageUrl;
                                matchCount++;
                                break;
                        }
                    }
                    else if (tagProperty != null && tagContent != null)
                    {
                        switch (tagProperty.Value.ToLower())
                        {
                            case "og:title":
                                metaInfo.Title = string.IsNullOrEmpty(metaInfo.Title) ? tagContent.Value : metaInfo.Title;
                                matchCount++;
                                break;
                            case "og:description":
                                metaInfo.Description = string.IsNullOrEmpty(metaInfo.Description) ? tagContent.Value : metaInfo.Description;
                                matchCount++;
                                break;
                            case "og:image":
                                metaInfo.ImageUrl = string.IsNullOrEmpty(metaInfo.ImageUrl) ? tagContent.Value : metaInfo.ImageUrl;
                                matchCount++;
                                break;
                        }
                    }
                }
                metaInfo.HasData = matchCount > 0;
            }
            return metaInfo;
        }

        #endregion

        #region "Contact"

        public static void SendContactEmail(string sName,string sSender,string sSubject,string sBody)
        {
            try
            {
                clsSendMail.ContactEmail(sName, sSender, sSubject, sBody);
            }
            catch (Exception e) { }
        }

        public static clsReturn AddContactEmail(clsContact oContact)
        {
            clsReturn oReturn = new clsReturn();
            try
            {
                oReturn.Data = clsDB.AddContact(oContact);
                oReturn.Status = 1;
                return oReturn;
            }
            catch (Exception e)
            {
                oReturn.Message = e.Message;
                oReturn.Data = null;
                oReturn.Status = 0;
                return oReturn;
            }
        }
        public static clsReturn EditContactEmail(clsContact oContact)
        {
            clsReturn oReturn = new clsReturn();
            try
            {
                oReturn.Data = clsDB.EditContact(oContact);
                oReturn.Status = 1;
                return oReturn;
            }
            catch (Exception e)
            {
                oReturn.Message = e.Message;
                oReturn.Data = null;
                oReturn.Status = 0;
                return oReturn;
            }
        }
        public static clsReturn DeleteContactEmail(clsContact oContact)
        {
            clsReturn oReturn = new clsReturn();
            try
            {
                oReturn.Data = clsDB.DeleteContact(oContact);
                oReturn.Status = 1;
                return oReturn;
            }
            catch (Exception e)
            {
                oReturn.Message = e.Message;
                oReturn.Data = null;
                oReturn.Status = 0;
                return oReturn;
            }
        }
        public static clsReturn GetContactEmails()
        {
            clsReturn oReturn = new clsReturn();
            try
            {
                oReturn.Data = clsDB.GetContacts();
                oReturn.Status = 1;
                return oReturn;
            }
            catch (Exception e)
            {
                oReturn.Message = e.Message;
                oReturn.Data = null;
                oReturn.Status = 0;
                return oReturn;
            }
        }
        #endregion

        #region "Emails"

        public static void EmailConfirmation(string sEmailReceipant, string sCallBackUrl)
        {
            try
            {
                clsSendMail.EmailConfirmation(sEmailReceipant, sCallBackUrl);
            }
            catch (Exception e) { }
        }
        public static void PassWordReset(string sEmailReceipant, string sCallBackUrl)
        {
            try
            {
                clsSendMail.PasswordReset(sEmailReceipant, sCallBackUrl);
            }
            catch (Exception e) { }
        }
        public static void NewUser(string sUserId)
        {
            try
            {
                clsUser oUser = clsDB.Get_User(sUserId);
                clsSendMail.NewUser(oUser.UserName,oUser.Email);
            }
            catch (Exception e) { }
        }


        #endregion

    }
}
