﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {

        public static clsReturn AddQuestionAnswer(clsAnswerMC x){return addObject<clsAnswerMC>(x);}

        public static clsReturn EditQuestionAnswer(clsAnswerMC x) { return editObject<clsAnswerMC>(x); }

        public static clsReturn ReturnAnswers()
        {
            return getFilteredObjectsWrapped<clsAnswerMC>();
        }
        public static List<clsBaseModel> GetQuestionAnswers() { return getList<clsAnswerMC>(); }
        public static List<clsAnswerMC> getQuestionAnswers() { return getList<clsAnswerMC>().Cast<clsAnswerMC>().ToList(); }

        public static clsReturn DeleteQuestionAnswer(clsAnswerMC x) { return DeleteQuestionAnswer(x.Id); }
        public static clsReturn DeleteQuestionAnswer(int id){ return Return(id, deleteQuestionAsnwer);}
        private static clsReturn deleteQuestionAsnwer(int id, clsReturn r)
        {
            clsAnswerMC oA = getObject<clsAnswerMC>(id);

            if (oA == null)
                throw new Exception("This answer doesn't exist!");

            foreach (var q in GetQuestions())
            {
                if (q is clsQuestionMC)
                {
                    var qmc = (clsQuestionMC)q;
                    foreach(var a in qmc.PossibleAnswers)
                        if (a.Id == oA.Id)
                            throw new Exception("Cannot delete this Answer while Questions have this as a possible answer. Remove the questions first.");
                }
            }

            if (clsDB.Delete_QuestionAnswer(id) == 0)
                throw new Exception($"The answer '{oA.Name}' could not be deleted.");

            clsStatic.QuestionAnswers = null;
            r.Status = 1;
            r.Message = $"Successfully deleted answer '{oA.Name}'.";

            return r;
        }

    }
}
