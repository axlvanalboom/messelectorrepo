﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {
        public static clsReturn AddQuestion(clsAbstractQuestions x){ return Return(x, addQuestion);}
        private static clsReturn addQuestion(clsAbstractQuestions x, clsReturn r)
        {     
            clsBaseModel oTemp = GetQuestion(x.Name);
            if (oTemp != null)
                throw new Exception($"This question already exists.");

            if (x.Category == null)
                throw new Exception($"This question doesn't have a Category");
            if (getObject<clsQuestionCategory>(x.Category.Id, false) == null)
                throw new Exception($"This question has a Category that doesn't exist!");

            if (x.CategoryMinimum < 0) x.CategoryMinimum = 0;
            if (x.CategoryMinimum > 100) x.CategoryMinimum = 100;
            if (x.CategoryMaximum < 0) x.CategoryMaximum = 0;
            if (x.CategoryMaximum > 100) x.CategoryMaximum = 100;

            if (x is clsQuestionMC)
            {
                var q = (clsQuestionMC)x;
                if (q.PossibleAnswers == null || q.PossibleAnswers.Count < 2)
                    throw new Exception($"This question needs at least 2 possible answers!");
                foreach(var pa in q.PossibleAnswers)
                    if (getObject<clsAnswerMC>(pa.Id) == null)
                        throw new Exception($"This question has a possible answer that doens't exist! possible answer: {pa.Name}!");
            }
            else if (x is clsQuestionRange)
            {
                var q = (clsQuestionRange)x;
                if (q.Minimum>= q.Maximum)
                    throw new Exception($"This question has a minimum thats greater or equal that it's maximum!");
            }
            else
                throw new Exception($"Wrong question type");

            int id = clsDB.Add_Question(x);

            // Done
            clsStatic.Questions = null;
            r.Data = id;
            r.Message = $"Successfully added question '{x.Name}'.";
            r.Status = 1;

            return r;
        }

        public static clsReturn EditQuestion(clsAbstractQuestions x) { return editObject<clsAbstractQuestions>(x); }

        public static clsReturn ReturnQuestions()
        {
            return getFilteredObjectsWrapped<clsAbstractQuestions>();
        }
        public static List<clsBaseModel> GetQuestions() { return getList<clsAbstractQuestions>(); }
        public static List<clsAbstractQuestions> getQuestions() { return getList<clsAbstractQuestions>().Cast<clsAbstractQuestions>().ToList(); ; }

        public static List<clsAbstractQuestions> GetQuestions(List<clsQuestionFilter> loFilters)
        {
            return QuestionFilterAnalyzator(loFilters);
        }
        private static List<clsAbstractQuestions> QuestionFilterAnalyzator(List<clsQuestionFilter> loFilters)
        {
            var loCategories = getQuestionCategories();
            var loQuestions = getQuestions();

            var diiCategories = new Dictionary<int, int>();
            loFilters.ForEach(f =>
            {
                foreach (var c in loCategories)
                {
                    if (c.Id == f.CategoryId)
                    {
                        diiCategories.Add(c.Id, f.Value);
                        break;
                    }
                }
            });

            var result = new List<clsAbstractQuestions>();

            loQuestions.ForEach(q =>
            {
                if (diiCategories.ContainsKey(q.Category.Id))
                {
                    var value = diiCategories[q.Category.Id];
                    if (value >= q.CategoryMinimum && value <= q.CategoryMaximum)
                        result.Add(q);
                }
            });

            List<clsAbstractQuestions> SortedList = result.OrderBy(r => r.Category.Id).ToList();

            return SortedList;//.GroupBy(x => x.Category).ToDictionary(g=>g.Key, g=>g.ToList());
        }

        private static clsBaseModel GetQuestion(int id) { return getObject<clsAbstractQuestions>(id); }
        private static clsBaseModel GetQuestion(string sName) { return getObject<clsAbstractQuestions>(sName, false); }

        public static clsReturn DeleteQuestion(int id){ return Return(id, deleteQuestion);}
        private static clsReturn deleteQuestion(int id, clsReturn r)
        {
            clsAbstractQuestions oQ = getObject<clsAbstractQuestions>(id);

            if (oQ == null)
                throw new Exception("This question doesn't exist!");

            foreach (var sa in clsStatic.GetSoftwareAnswers())
            {
                if (sa is clsSoftwareAnswerMC)
                {
                    if (((clsSoftwareAnswerMC)sa).Question.Id == oQ.Id)
                        throw new Exception("Cannot delete this Question while a Software answer has an answer for this question. Remove the software answers first.");
                }
                else if (sa is clsSoftwareAnswerRange)
                {
                    if (((clsSoftwareAnswerRange)sa).Question.Id == oQ.Id)
                        throw new Exception("Cannot delete this Question while a Software answer has an answer for this question. Remove the software answers first.");
                }
            }

            if (clsDB.Delete_Question(id) == 0)
                throw new Exception($"The question '{oQ.Name}' could not be deleted.");

            clsStatic.Questions = null;
            r.Status = 1;
            r.Message = $"Successfully deleted question '{oQ.Name}'.";

            return r;
        }

    }
}
