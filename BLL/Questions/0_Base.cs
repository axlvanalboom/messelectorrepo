﻿using System;
using System.Collections.Generic;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {

        private static clsReturn Return<T>(T x, Func<T, clsReturn, clsReturn> f) where T: clsBaseModel
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    oReturn = f(x, oReturn);
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }

                return oReturn;
            }
        }
        private static clsReturn Return(int id, Func<int, clsReturn, clsReturn> f)
        {
            lock (oLock)
            {

                clsReturn oReturn = new clsReturn();

                try
                {
                    oReturn = f(id, oReturn);
                }
                catch (Exception oException)
                {
                    oReturn.Message = oException.Message;
                }

                return oReturn;
            }
        }

    }
}
