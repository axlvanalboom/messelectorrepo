﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {

        private static clsBaseModel GetSoftware(int id) { return getObject<clsSoftware>(id); } 

        public static clsReturn AddSoftwareAnswers(clsSoftwareWithAnswers x) { return Return(x, addSoftwareAnswers); }
        private static clsReturn addSoftwareAnswers(clsSoftwareWithAnswers x, clsReturn r)
        {
            clsBaseModel oTemp = GetSoftware(x.Id);
            if (oTemp == null)
                throw new Exception($"This software doesn't exists.");

            var oCurrentAnswers = clsStatic.GetSoftwareAnswers(x);
            if(oCurrentAnswers != null)      
                foreach(var ca in oCurrentAnswers)
                    if (x.Answers.Where(a=> a.QuestionId == ca.QuestionId).Any())
                        throw new Exception($"This software already has an answer for one of the questions!");

            foreach (var a in x.Answers)
                SoftwareAnswerCheck(a);

            clsDB.Add_SoftwareAnswers(x);

            // Done
            clsStatic.SetSoftwareAnswersToNull(x);
            r.Message = $"Successfully added all the answers to software '{x.Name}'.";
            r.Status = 1;

            return r;
        }

        private static void SoftwareAnswerCheck(clsAbstractSoftwareAnswer x)
        {
            if (x is clsSoftwareAnswerMC)
            {
                var mc = (clsSoftwareAnswerMC)x;
                if (!(mc.Question.PossibleAnswers.Where(pa => pa.Id == mc.Answer.Id).Any()))
                    throw new Exception($"Software answer for question {mc.Question.Name} uses an answer that isn't a possible answer for this question!");
            }
            else if (x is clsSoftwareAnswerRange)
            {
                var range = (clsSoftwareAnswerRange)x;
                if (range.Value > range.Question.Maximum || range.Value < range.Question.Minimum)
                    throw new Exception($"Software answer for question {range.Question.Name} uses an answer that's outside [min,max]!");
            }
            else
                throw new Exception($"Unknown abstract software type!");
        }
        public static clsReturn EditSoftwareAnswer(clsAbstractSoftwareAnswer x) { return Return(x, editSoftwareAnswer); }
        private static clsReturn editSoftwareAnswer(clsAbstractSoftwareAnswer x, clsReturn r)
        {
            SoftwareAnswerCheck(x);

            var iResult = clsDB.Edit_SoftwareAnswer(x);

            if (iResult == 0)
                throw new Exception($"Could not edit '{x.Name}'!");

            clsStatic.SetSoftwareAnswersToNullBySoftwareAnswer(x.Id);

            r.Data = iResult;
            r.Message = $"Successfully edited '{x.Name}'!";
            r.Status = 1;
            return r;
        }

        public static clsReturn ReturnSoftwareAnswers(clsSoftware x)
        {
            clsReturn oReturn = new clsReturn();

            try
            {
                oReturn.Data = GetSoftwareAnswer(x);
                oReturn.Status = 1;
            }
            catch (Exception oException)
            {
                oReturn.Message = oException.Message;
            }

            return oReturn;
        }
        public static List<clsAbstractSoftwareAnswer> GetSoftwareAnswer(clsSoftware x) { return clsStatic.GetSoftwareAnswers(x); ; }

        private static List<clsSoftwareWithAnswers> getSoftwaresWithAnswers()
        {
            return clsStatic.GetSoftwareWithAnswers();
        }

        public static clsReturn DeleteSoftwareAnswer(int id)
        {
            return Return(id, deleteSoftwareAnswer);
        }
        private static clsReturn deleteSoftwareAnswer(int id, clsReturn r)
        {
            clsAbstractSoftwareAnswer oSA = getObject<clsAbstractSoftwareAnswer>(id);

            if (oSA == null)
                throw new Exception("This software answer doesn't exist!");

            if (clsDB.Delete_SoftwareAnswer(id) == 0)
                throw new Exception($"The software answer '{oSA.Name}' could not be deleted.");

            clsStatic.SetSoftwareAnswersToNullBySoftwareAnswer(id);
            r.Status = 1;
            r.Message = $"Successfully deleted software answer '{oSA.Name}'.";

            return r;
        }

    }
}
