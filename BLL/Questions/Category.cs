﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEC;
using DAL.DB;

namespace BLL
{
    public static partial class clsBLL
    {

        public static clsReturn AddQuestionCategory(clsQuestionCategory x){return addObject<clsQuestionCategory>(x);}

        public static clsReturn EditQuestionCategory(clsQuestionCategory x) { return editObject<clsQuestionCategory>(x); }

        public static clsReturn ReturnQuestionCategories()
        {
            return getFilteredObjectsWrapped<clsQuestionCategory>();
        }
        public static List<clsBaseModel> GetQuestionCategories() { return getList<clsQuestionCategory>(); }
        public static List<clsQuestionCategory> getQuestionCategories() { return getList<clsQuestionCategory>().Cast<clsQuestionCategory>().ToList(); ; }

        public static clsReturn DeleteQuestionCategory(clsQuestionCategory x) {return DeleteQuestionCategory(x.Id); }
        public static clsReturn DeleteQuestionCategory(int id){return Return(id, deleteQuestionCategory);}
        private static clsReturn deleteQuestionCategory(int id, clsReturn r)
        {
            clsQuestionCategory oQC = getObject<clsQuestionCategory>(id);

            if (oQC == null)
                throw new Exception("This Question Category doesn't exist!");

            foreach (var q in GetQuestions())
                if (q.Id == oQC.Id)
                    throw new Exception("Cannot delete this Question Category while Questions have this category. Remove the questions first.");

            if (clsDB.Delete_QuestionCategory(id) == 0)
                throw new Exception($"The Question Category '{oQC.Name}' could not be deleted.");

            clsStatic.QuestionCategories = null;
            r.Status = 1;
            r.Message = $"Successfully deleted question category '{oQC.Name}'.";

            return r;
        }

    }
}
