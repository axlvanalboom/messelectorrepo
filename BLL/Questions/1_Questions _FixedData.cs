﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;

namespace BLL2
{
    internal static class clsBLL_private
    {

        private static List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> Softwares;
        private static List<clsAnswerMC> Answers;
        private static List<clsQuestionCategory> Categories;
        private static List<clsAbstractQuestions> QuestionData;
        private static Dictionary<eComparisonOperator, string> desTranslation1;
        private static Dictionary<string, eComparisonOperator> desTranslation2;

        private static bool MadeData = false;
        private static void MakeData()
        {
            if (MadeData) return;

            #region Categories

            Categories = new List<clsQuestionCategory>();

            var oQcScheduling = new clsQuestionCategory() { Id = 1, Name = "Detailed Scheduling" };
            var oQcDispatching = new clsQuestionCategory() { Id = 2, Name = "Dispatching" };
            var oQcExeMgt = new clsQuestionCategory() { Id = 3, Name = "Execution Management" };
            var oQcResMgt = new clsQuestionCategory() { Id = 4, Name = "Resource Management" };
            var oQcDefMgt = new clsQuestionCategory() { Id = 5, Name = "Definition Management" };
            var oQcDC = new clsQuestionCategory() { Id = 6, Name = "Data Collection" };
            var oQcAnalysis = new clsQuestionCategory() { Id = 7, Name = "Analysis" };
            var oQcTracking = new clsQuestionCategory() { Id = 8, Name = "Tracking" };

            Categories.Add(oQcScheduling);
            Categories.Add(oQcDispatching);
            Categories.Add(oQcExeMgt);
            Categories.Add(oQcResMgt);
            Categories.Add(oQcDefMgt);
            Categories.Add(oQcDC);
            Categories.Add(oQcAnalysis);
            Categories.Add(oQcTracking);

            #endregion

            #region Answers

            var aYes = new clsAnswerMC() { Name = "Yes", Id = 1 };
            var aNo = new clsAnswerMC() { Name = "No", Id = 2 };

            Answers = new List<clsAnswerMC>() { aYes, aNo };

            #endregion

            #region Questions

            var q_1_1 = new clsQuestionMC() { Name = "1 1", Id = 11, Description = "1 1", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_1_2 = new clsQuestionMC() { Name = "1 2", Id = 12, Description = "1 2", Category = oQcScheduling, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_1_3 = new clsQuestionMC() { Name = "1 3", Id = 13, Description = "1 3", Category = oQcScheduling, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_1_4 = new clsQuestionMC() { Name = "1 4", Id = 14, Description = "1 4", Category = oQcScheduling, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_1_5 = new clsQuestionRange() { Name = "1 5", Id = 15, Description = "1 5", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_1_6 = new clsQuestionRange() { Name = "1 6", Id = 16, Description = "1 6", Category = oQcScheduling, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_1_7 = new clsQuestionRange() { Name = "1 7", Id = 17, Description = "1 7", Category = oQcScheduling, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_1_8 = new clsQuestionRange() { Name = "1 8", Id = 18, Description = "1 8", Category = oQcScheduling, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_2_1 = new clsQuestionMC() { Name = "2 1", Id = 21, Description = "2 1", Category = oQcDispatching, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_2_2 = new clsQuestionMC() { Name = "2 2", Id = 22, Description = "2 2", Category = oQcDispatching, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_2_3 = new clsQuestionMC() { Name = "2 3", Id = 23, Description = "2 3", Category = oQcDispatching, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_2_4 = new clsQuestionMC() { Name = "2 4", Id = 24, Description = "2 4", Category = oQcDispatching, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_2_5 = new clsQuestionRange() { Name = "2 5", Id = 25, Description = "2 5", Category = oQcDispatching, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_2_6 = new clsQuestionRange() { Name = "2 6", Id = 26, Description = "2 6", Category = oQcDispatching, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_2_7 = new clsQuestionRange() { Name = "2 7", Id = 27, Description = "2 7", Category = oQcDispatching, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_2_8 = new clsQuestionRange() { Name = "2 8", Id = 28, Description = "2 8", Category = oQcDispatching, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_3_1 = new clsQuestionMC() { Name = "3 1", Id = 31, Description = "3 1", Category = oQcExeMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_3_2 = new clsQuestionMC() { Name = "3 2", Id = 32, Description = "3 2", Category = oQcExeMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_3_3 = new clsQuestionMC() { Name = "3 3", Id = 33, Description = "3 3", Category = oQcExeMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_3_4 = new clsQuestionMC() { Name = "3 4", Id = 34, Description = "3 4", Category = oQcExeMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_3_5 = new clsQuestionRange() { Name = "3 5", Id = 35, Description = "3 5", Category = oQcExeMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_3_6 = new clsQuestionRange() { Name = "3 6", Id = 36, Description = "3 6", Category = oQcExeMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_3_7 = new clsQuestionRange() { Name = "3 7", Id = 37, Description = "3 7", Category = oQcExeMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_3_8 = new clsQuestionRange() { Name = "3 8", Id = 38, Description = "3 8", Category = oQcExeMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_4_1 = new clsQuestionMC() { Name = "4 1", Id = 41, Description = "4 1", Category = oQcResMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_4_2 = new clsQuestionMC() { Name = "4 2", Id = 42, Description = "4 2", Category = oQcResMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_4_3 = new clsQuestionMC() { Name = "4 3", Id = 43, Description = "4 3", Category = oQcResMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_4_4 = new clsQuestionMC() { Name = "4 4", Id = 44, Description = "4 4", Category = oQcResMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_4_5 = new clsQuestionRange() { Name = "4 5", Id = 45, Description = "4 5", Category = oQcResMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_4_6 = new clsQuestionRange() { Name = "4 6", Id = 46, Description = "4 6", Category = oQcResMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_4_7 = new clsQuestionRange() { Name = "4 7", Id = 47, Description = "4 7", Category = oQcResMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_4_8 = new clsQuestionRange() { Name = "4 8", Id = 48, Description = "4 8", Category = oQcResMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_5_1 = new clsQuestionMC() { Name = "5 1", Id = 51, Description = "5 1", Category = oQcDefMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_5_2 = new clsQuestionMC() { Name = "5 2", Id = 52, Description = "5 2", Category = oQcDefMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_5_3 = new clsQuestionMC() { Name = "5 3", Id = 53, Description = "5 3", Category = oQcDefMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_5_4 = new clsQuestionMC() { Name = "5 4", Id = 54, Description = "5 4", Category = oQcDefMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_5_5 = new clsQuestionRange() { Name = "5 5", Id = 55, Description = "5 5", Category = oQcDefMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_5_6 = new clsQuestionRange() { Name = "5 6", Id = 56, Description = "5 6", Category = oQcDefMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_5_7 = new clsQuestionRange() { Name = "5 7", Id = 57, Description = "5 7", Category = oQcDefMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_5_8 = new clsQuestionRange() { Name = "5 8", Id = 58, Description = "5 8", Category = oQcDefMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_6_1 = new clsQuestionMC() { Name = "6 1", Id = 61, Description = "6 1", Category = oQcDC, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_6_2 = new clsQuestionMC() { Name = "6 2", Id = 62, Description = "6 2", Category = oQcDC, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_6_3 = new clsQuestionMC() { Name = "6 3", Id = 63, Description = "6 3", Category = oQcDC, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_6_4 = new clsQuestionMC() { Name = "6 4", Id = 64, Description = "6 4", Category = oQcDC, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_6_5 = new clsQuestionRange() { Name = "6 5", Id = 65, Description = "6 5", Category = oQcDC, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_6_6 = new clsQuestionRange() { Name = "6 6", Id = 66, Description = "6 6", Category = oQcDC, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_6_7 = new clsQuestionRange() { Name = "6 7", Id = 67, Description = "6 7", Category = oQcDC, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_6_8 = new clsQuestionRange() { Name = "6 8", Id = 68, Description = "6 8", Category = oQcDC, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_7_1 = new clsQuestionMC() { Name = "7 1", Id = 71, Description = "7 1", Category = oQcAnalysis, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_7_2 = new clsQuestionMC() { Name = "7 2", Id = 72, Description = "7 2", Category = oQcAnalysis, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_7_3 = new clsQuestionMC() { Name = "7 3", Id = 73, Description = "7 3", Category = oQcAnalysis, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_7_4 = new clsQuestionMC() { Name = "7 4", Id = 74, Description = "7 4", Category = oQcAnalysis, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_7_5 = new clsQuestionRange() { Name = "7 5", Id = 75, Description = "7 5", Category = oQcAnalysis, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_7_6 = new clsQuestionRange() { Name = "7 6", Id = 76, Description = "7 6", Category = oQcAnalysis, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_7_7 = new clsQuestionRange() { Name = "7 7", Id = 77, Description = "7 7", Category = oQcAnalysis, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_7_8 = new clsQuestionRange() { Name = "7 8", Id = 78, Description = "7 8", Category = oQcAnalysis, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };

            var q_8_1 = new clsQuestionMC() { Name = "8 1", Id = 81, Description = "8 1", Category = oQcTracking, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_8_2 = new clsQuestionMC() { Name = "8 2", Id = 82, Description = "8 2", Category = oQcTracking, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_8_3 = new clsQuestionMC() { Name = "8 3", Id = 83, Description = "8 3", Category = oQcTracking, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_8_4 = new clsQuestionMC() { Name = "8 4", Id = 84, Description = "8 4", Category = oQcTracking, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            var q_8_5 = new clsQuestionRange() { Name = "8 5", Id = 85, Description = "8 5", Category = oQcTracking, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };
            var q_8_6 = new clsQuestionRange() { Name = "8 6", Id = 86, Description = "8 6", Category = oQcTracking, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 };
            var q_8_7 = new clsQuestionRange() { Name = "8 7", Id = 87, Description = "8 7", Category = oQcTracking, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 };
            var q_8_8 = new clsQuestionRange() { Name = "8 8", Id = 88, Description = "8 8", Category = oQcTracking, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 };


            QuestionData = new List<clsAbstractQuestions>()
            {

                q_1_1,
                q_1_2,
                q_1_3,
                q_1_4,
                q_1_5,
                q_1_6,
                q_1_7,
                q_1_8,
                q_2_1,
                q_2_2,
                q_2_3,
                q_2_4,
                q_2_5,
                q_2_6,
                q_2_7,
                q_2_8,
                q_3_1,
                q_3_2,
                q_3_3,
                q_3_4,
                q_3_5,
                q_3_6,
                q_3_7,
                q_3_8,
                q_4_1,
                q_4_2,
                q_4_3,
                q_4_4,
                q_4_5,
                q_4_6,
                q_4_7,
                q_4_8,
                q_5_1,
                q_5_2,
                q_5_3,
                q_5_4,
                q_5_5,
                q_5_6,
                q_5_7,
                q_5_8,
                q_6_1,
                q_6_2,
                q_6_3,
                q_6_4,
                q_6_5,
                q_6_6,
                q_6_7,
                q_6_8,
                q_7_1,
                q_7_2,
                q_7_3,
                q_7_4,
                q_7_5,
                q_7_6,
                q_7_7,
                q_7_8,
                q_8_1,
                q_8_2,
                q_8_3,
                q_8_4,
                q_8_5,
                q_8_6,
                q_8_7,
                q_8_8
            };


            #endregion

            #region Software
            
            //softwares
            var oSoftware1 = new clsSoftware() { Name = "Software 1" };
            var oS1A1_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_1 };
            var oS1A1_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_2 };
            var oS1A1_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_3 };
            var oS1A1_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_4 };
            var oS1A1_5 = new clsSoftwareAnswerRange() { Value = 0, Question = q_1_5 };
            var oS1A1_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_1_6 };
            var oS1A1_7 = new clsSoftwareAnswerRange() { Value = 2, Question = q_1_7 };
            var oS1A1_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_1_8 };
            var oS1A2_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_1 };
            var oS1A2_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_2 };
            var oS1A2_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_3 };
            var oS1A2_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_4 };
            var oS1A2_5 = new clsSoftwareAnswerRange() { Value = 4, Question = q_2_5 };
            var oS1A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS1A2_7 = new clsSoftwareAnswerRange() { Value = 6, Question = q_2_7 };
            var oS1A2_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_2_8 };
            var oS1A3_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_1 };
            var oS1A3_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_2 };
            var oS1A3_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_3 };
            var oS1A3_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_4 };
            var oS1A3_5 = new clsSoftwareAnswerRange() { Value = 8, Question = q_3_5 };
            var oS1A3_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_3_6 };
            var oS1A3_7 = new clsSoftwareAnswerRange() { Value = 10, Question = q_3_7 };
            var oS1A3_8 = new clsSoftwareAnswerRange() { Value = 0, Question = q_3_8 };
            var oS1A4_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_1 };
            var oS1A4_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_2 };
            var oS1A4_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_3 };
            var oS1A4_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_4 };
            var oS1A4_5 = new clsSoftwareAnswerRange() { Value = 1, Question = q_4_5 };
            var oS1A4_6 = new clsSoftwareAnswerRange() { Value = 2, Question = q_4_6 };
            var oS1A4_7 = new clsSoftwareAnswerRange() { Value = 3, Question = q_4_7 };
            var oS1A4_8 = new clsSoftwareAnswerRange() { Value = 4, Question = q_4_8 };
            var oS1A5_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_1 };
            var oS1A5_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_2 };
            var oS1A5_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_3 };
            var oS1A5_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_4 };
            var oS1A5_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_5 };
            var oS1A5_6 = new clsSoftwareAnswerRange() { Value = 6, Question = q_5_6 };
            var oS1A5_7 = new clsSoftwareAnswerRange() { Value = 7, Question = q_5_7 };
            var oS1A5_8 = new clsSoftwareAnswerRange() { Value = 8, Question = q_5_8 };
            var oS1A6_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_1 };
            var oS1A6_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_2 };
            var oS1A6_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_3 };
            var oS1A6_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_4 };
            var oS1A6_5 = new clsSoftwareAnswerRange() { Value = 9, Question = q_6_5 };
            var oS1A6_6 = new clsSoftwareAnswerRange() { Value = 10, Question = q_6_6 };
            var oS1A6_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_6_7 };
            var oS1A6_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_6_8 };
            var oS1A7_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_1 };
            var oS1A7_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_2 };
            var oS1A7_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_3 };
            var oS1A7_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_4 };
            var oS1A7_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_7_5 };
            var oS1A7_6 = new clsSoftwareAnswerRange() { Value = 3, Question = q_7_6 };
            var oS1A7_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_7_7 };
            var oS1A7_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_8 };
            var oS1A8_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_1 };
            var oS1A8_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_2 };
            var oS1A8_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_3 };
            var oS1A8_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_4 };
            var oS1A8_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_8_5 };
            var oS1A8_6 = new clsSoftwareAnswerRange() { Value = 7, Question = q_8_6 };
            var oS1A8_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_8_7 };
            var oS1A8_8 = new clsSoftwareAnswerRange() { Value = 9, Question = q_8_8 };

            var oSoftware2 = new clsSoftware() { Name = "Software 2" };
            var oS2A1_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_1 };
            var oS2A1_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_2 };
            var oS2A1_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_3 };
            var oS2A1_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_4 };
            var oS2A1_5 = new clsSoftwareAnswerRange() { Value = 10, Question = q_1_5 };
            var oS2A1_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_1_6 };
            var oS2A1_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_1_7 };
            var oS2A1_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_1_8 };
            var oS2A2_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_1 };
            var oS2A2_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_2 };
            var oS2A2_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_3 };
            var oS2A2_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_4 };
            var oS2A2_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_2_5 };
            var oS2A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS2A2_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_2_7 };
            var oS2A2_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_2_8 };
            var oS2A3_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_1 };
            var oS2A3_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_2 };
            var oS2A3_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_3 };
            var oS2A3_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_4 };
            var oS2A3_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_3_5 };
            var oS2A3_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_3_6 };
            var oS2A3_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_3_7 };
            var oS2A3_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_3_8 };
            var oS2A4_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_1 };
            var oS2A4_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_2 };
            var oS2A4_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_3 };
            var oS2A4_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_4 };
            var oS2A4_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_4_5 };
            var oS2A4_6 = new clsSoftwareAnswerRange() { Value = 3, Question = q_4_6 };
            var oS2A4_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_4_7 };
            var oS2A4_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_8 };
            var oS2A5_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_1 };
            var oS2A5_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_2 };
            var oS2A5_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_3 };
            var oS2A5_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_4 };
            var oS2A5_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_5_5 };
            var oS2A5_6 = new clsSoftwareAnswerRange() { Value = 7, Question = q_5_6 };
            var oS2A5_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_5_7 };
            var oS2A5_8 = new clsSoftwareAnswerRange() { Value = 9, Question = q_5_8 };
            var oS2A6_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_1 };
            var oS2A6_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_2 };
            var oS2A6_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_3 };
            var oS2A6_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_4 };
            var oS2A6_5 = new clsSoftwareAnswerRange() { Value = 10, Question = q_6_5 };
            var oS2A6_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_6_6 };
            var oS2A6_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_6_7 };
            var oS2A6_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_6_8 };
            var oS2A7_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_1 };
            var oS2A7_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_2 };
            var oS2A7_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_3 };
            var oS2A7_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_4 };
            var oS2A7_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_7_5 };
            var oS2A7_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_6 };
            var oS2A7_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_7_7 };
            var oS2A7_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_7_8 };
            var oS2A8_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_1 };
            var oS2A8_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_2 };
            var oS2A8_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_3 };
            var oS2A8_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_4 };
            var oS2A8_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_8_5 };
            var oS2A8_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_8_6 };
            var oS2A8_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_8_7 };
            var oS2A8_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_8_8 };

            var oSoftware3 = new clsSoftware() { Name = "Software 3" };
            var oS3A1_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_1 };
            var oS3A1_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_2 };
            var oS3A1_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_3 };
            var oS3A1_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_4 };
            var oS3A1_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_5 };
            var oS3A1_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_6 };
            var oS3A1_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_7 };
            var oS3A1_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_8 };
            var oS3A2_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_1 };
            var oS3A2_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_2 };
            var oS3A2_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_3 };
            var oS3A2_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_4 };
            var oS3A2_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_5 };
            var oS3A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS3A2_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_7 };
            var oS3A2_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_8 };
            var oS3A3_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_1 };
            var oS3A3_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_2 };
            var oS3A3_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_3 };
            var oS3A3_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_4 };
            var oS3A3_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_5 };
            var oS3A3_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_6 };
            var oS3A3_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_7 };
            var oS3A3_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_8 };
            var oS3A4_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_1 };
            var oS3A4_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_2 };
            var oS3A4_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_3 };
            var oS3A4_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_4 };
            var oS3A4_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_5 };
            var oS3A4_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_6 };
            var oS3A4_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_7 };
            var oS3A4_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_8 };
            var oS3A5_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_1 };
            var oS3A5_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_2 };
            var oS3A5_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_3 };
            var oS3A5_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_4 };
            var oS3A5_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_5 };
            var oS3A5_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_6 };
            var oS3A5_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_7 };
            var oS3A5_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_8 };
            var oS3A6_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_1 };
            var oS3A6_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_2 };
            var oS3A6_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_3 };
            var oS3A6_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_4 };
            var oS3A6_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_5 };
            var oS3A6_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_6 };
            var oS3A6_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_7 };
            var oS3A6_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_8 };
            var oS3A7_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_1 };
            var oS3A7_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_2 };
            var oS3A7_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_3 };
            var oS3A7_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_4 };
            var oS3A7_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_5 };
            var oS3A7_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_6 };
            var oS3A7_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_7 };
            var oS3A7_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_8 };
            var oS3A8_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_1 };
            var oS3A8_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_2 };
            var oS3A8_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_3 };
            var oS3A8_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_4 };
            var oS3A8_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_5 };
            var oS3A8_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_6 };
            var oS3A8_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_7 };
            var oS3A8_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_8 };

            //software linking
            Softwares = new List<(clsSoftware, List<clsAbstractSoftwareAnswer>)>()
            {
                (oSoftware1,new List<clsAbstractSoftwareAnswer>(){
                    oS1A1_1,
                    oS1A1_2,
                    oS1A1_3,
                    oS1A1_4,
                    oS1A1_5,
                    oS1A1_6,
                    oS1A1_7,
                    oS1A1_8,
                    oS1A2_1,
                    oS1A2_2,
                    oS1A2_3,
                    oS1A2_4,
                    oS1A2_5,
                    oS1A2_6,
                    oS1A2_7,
                    oS1A2_8,
                    oS1A3_1,
                    oS1A3_2,
                    oS1A3_3,
                    oS1A3_4,
                    oS1A3_5,
                    oS1A3_6,
                    oS1A3_7,
                    oS1A3_8,
                    oS1A4_1,
                    oS1A4_2,
                    oS1A4_3,
                    oS1A4_4,
                    oS1A4_5,
                    oS1A4_6,
                    oS1A4_7,
                    oS1A4_8,
                    oS1A5_1,
                    oS1A5_2,
                    oS1A5_3,
                    oS1A5_4,
                    oS1A5_5,
                    oS1A5_6,
                    oS1A5_7,
                    oS1A5_8,
                    oS1A6_1,
                    oS1A6_2,
                    oS1A6_3,
                    oS1A6_4,
                    oS1A6_5,
                    oS1A6_6,
                    oS1A6_7,
                    oS1A6_8,
                    oS1A7_1,
                    oS1A7_2,
                    oS1A7_3,
                    oS1A7_4,
                    oS1A7_5,
                    oS1A7_6,
                    oS1A7_7,
                    oS1A7_8,
                    oS1A8_1,
                    oS1A8_2,
                    oS1A8_3,
                    oS1A8_4,
                    oS1A8_5,
                    oS1A8_6,
                    oS1A8_7,
                    oS1A8_8
                }),
                (oSoftware2,new List<clsAbstractSoftwareAnswer>(){
                    oS2A1_1,
                    oS2A1_2,
                    oS2A1_3,
                    oS2A1_4,
                    oS2A1_5,
                    oS2A1_6,
                    oS2A1_7,
                    oS2A1_8,
                    oS2A2_1,
                    oS2A2_2,
                    oS2A2_3,
                    oS2A2_4,
                    oS2A2_5,
                    oS2A2_6,
                    oS2A2_7,
                    oS2A2_8,
                    oS2A3_1,
                    oS2A3_2,
                    oS2A3_3,
                    oS2A3_4,
                    oS2A3_5,
                    oS2A3_6,
                    oS2A3_7,
                    oS2A3_8,
                    oS2A4_1,
                    oS2A4_2,
                    oS2A4_3,
                    oS2A4_4,
                    oS2A4_5,
                    oS2A4_6,
                    oS2A4_7,
                    oS2A4_8,
                    oS2A5_1,
                    oS2A5_2,
                    oS2A5_3,
                    oS2A5_4,
                    oS2A5_5,
                    oS2A5_6,
                    oS2A5_7,
                    oS2A5_8,
                    oS2A6_1,
                    oS2A6_2,
                    oS2A6_3,
                    oS2A6_4,
                    oS2A6_5,
                    oS2A6_6,
                    oS2A6_7,
                    oS2A6_8,
                    oS2A7_1,
                    oS2A7_2,
                    oS2A7_3,
                    oS2A7_4,
                    oS2A7_5,
                    oS2A7_6,
                    oS2A7_7,
                    oS2A7_8,
                    oS2A8_1,
                    oS2A8_2,
                    oS2A8_3,
                    oS2A8_4,
                    oS2A8_5,
                    oS2A8_6,
                    oS2A8_7,
                    oS2A8_8
                }),
                (oSoftware3,new List<clsAbstractSoftwareAnswer>(){
                    oS3A1_1,
                    oS3A1_2,
                    oS3A1_3,
                    oS3A1_4,
                    oS3A1_5,
                    oS3A1_6,
                    oS3A1_7,
                    oS3A1_8,
                    oS3A2_1,
                    oS3A2_2,
                    oS3A2_3,
                    oS3A2_4,
                    oS3A2_5,
                    oS3A2_6,
                    oS3A2_7,
                    oS3A2_8,
                    oS3A3_1,
                    oS3A3_2,
                    oS3A3_3,
                    oS3A3_4,
                    oS3A3_5,
                    oS3A3_6,
                    oS3A3_7,
                    oS3A3_8,
                    oS3A4_1,
                    oS3A4_2,
                    oS3A4_3,
                    oS3A4_4,
                    oS3A4_5,
                    oS3A4_6,
                    oS3A4_7,
                    oS3A4_8,
                    oS3A5_1,
                    oS3A5_2,
                    oS3A5_3,
                    oS3A5_4,
                    oS3A5_5,
                    oS3A5_6,
                    oS3A5_7,
                    oS3A5_8,
                    oS3A6_1,
                    oS3A6_2,
                    oS3A6_3,
                    oS3A6_4,
                    oS3A6_5,
                    oS3A6_6,
                    oS3A6_7,
                    oS3A6_8,
                    oS3A7_1,
                    oS3A7_2,
                    oS3A7_3,
                    oS3A7_4,
                    oS3A7_5,
                    oS3A7_6,
                    oS3A7_7,
                    oS3A7_8,
                    oS3A8_1,
                    oS3A8_2,
                    oS3A8_3,
                    oS3A8_4,
                    oS3A8_5,
                    oS3A8_6,
                    oS3A8_7,
                    oS3A8_8
                })
            };
            
            #endregion

            desTranslation1 = new Dictionary<eComparisonOperator, string>()
            {
                [eComparisonOperator.Equal] = "=",
                [eComparisonOperator.NotEqual] = "!=",
                [eComparisonOperator.LessThan] = "<",
                [eComparisonOperator.LessThanOrEqualTo] = "<=",
                [eComparisonOperator.GreaterThan] = ">",
                [eComparisonOperator.GreaterThanOrEqualTo] = ">="
            };
            desTranslation2 = new Dictionary<string, eComparisonOperator>()
            {
                ["="] = eComparisonOperator.Equal,
                ["!="] = eComparisonOperator.NotEqual,
                ["<"] = eComparisonOperator.LessThan,
                ["<="] = eComparisonOperator.LessThanOrEqualTo,
                [">"] = eComparisonOperator.GreaterThan,
                [">="] = eComparisonOperator.GreaterThanOrEqualTo
            };
            MadeData = true;

        }
        private static void MakeData1()
        {
            if (MadeData) return;

            #region Categories
            Categories = new List<clsQuestionCategory>();

            var oQcScheduling = new clsQuestionCategory() { Id = 1, Name = "Detailed Scheduling" };
            var oQcDispatching = new clsQuestionCategory() { Id = 2, Name = "Dispatching" };
            var oQcExeMgt = new clsQuestionCategory() { Id = 3, Name = "Execution Management" };
            var oQcResMgt = new clsQuestionCategory() { Id = 4, Name = "Resource Management" };
            var oQcDefMgt = new clsQuestionCategory() { Id = 5, Name = "Definition Management" };
            var oQcDC = new clsQuestionCategory() { Id = 6, Name = "Data Collection" };
            var oQcAnalysis = new clsQuestionCategory() { Id = 7, Name = "Analysis" };
            var oQcTracking = new clsQuestionCategory() { Id = 8, Name = "Tracking" };

            Categories.Add(oQcScheduling);
            Categories.Add(oQcDispatching);
            Categories.Add(oQcExeMgt);
            Categories.Add(oQcResMgt);
            Categories.Add(oQcDefMgt);
            Categories.Add(oQcDC);
            Categories.Add(oQcAnalysis);
            Categories.Add(oQcTracking);

            #endregion

            #region Answers

            var aYes = new clsAnswerMC() { Name = "Yes", Id = 1 };
            var aNo = new clsAnswerMC() { Name = "No", Id = 2 };
            var aMaybe = new clsAnswerMC() { Name = "Maybe", Id = 3 };
            var aBulb = new clsAnswerMC() { Name = "Bulbasaur", Id = 41 };
            var aCharmander = new clsAnswerMC() { Name = "Charmander", Id = 42 };
            var aSquirtle = new clsAnswerMC() { Name = "Squirtle", Id = 43 };

            Answers = new List<clsAnswerMC>() { aYes, aNo, aMaybe, aBulb, aCharmander, aSquirtle };

            #endregion

            #region Questions

            var qIntrested = new clsQuestionMC() { Name = "Scheduling", Id = 1, Description = "Do you need scheduling functionalities?", Category = oQcScheduling, CategoryMinimum = 0, CategoryMaximum = 100 };
            qIntrested.PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo };

            var q3 = new clsQuestionMC() { Id = 2, Name = "Yes No Maybe", Description = "Yes, No or Maybe?" };
            q3.PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo, aMaybe };

            var qRange_0_10 = new clsQuestionRange() { Id = 3, Name = "Range Question", Description = "Rate this question?", Minimum = 0, Maximum = 10 };
            var qRange_5_33 = new clsQuestionRange() { Id = 4, Name = "blabla", Description = "Rate this question?", Minimum = 5, Maximum = 33 };

            var qPokemon = new clsQuestionMC() { Id = 5, Name = "Pokemon", Description = "Who is your favorite starter?" };
            qPokemon.PossibleAnswers = new List<clsAnswerMC>() { aBulb, aCharmander, aSquirtle };

            QuestionData = new List<clsAbstractQuestions>()
            {
                qIntrested,
                q3,
                qRange_0_10,
                qRange_5_33,
                qPokemon
            };

            #endregion

            #region Software

            //softwares
            var oSoftware1 = new clsSoftware() { Name = "Software 1" };
            var oSoftware2 = new clsSoftware() { Name = "Software 2" };
            var oSoftware3 = new clsSoftware() { Name = "Software 3" };

            var oS1A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };
            var oS2A1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = qIntrested };
            var oS3A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };

            var oS1A2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q3 };
            var oS2A2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q3 };
            var oS3A2 = new clsSoftwareAnswerMC() { Answer = aMaybe, Question = q3 };

            var oS1A3 = new clsSoftwareAnswerRange() { Value = 3, Question = qRange_0_10 };
            var oS2A3 = new clsSoftwareAnswerRange() { Value = 4, Question = qRange_0_10 };
            var oS3A3 = new clsSoftwareAnswerRange() { Value = 7, Question = qRange_0_10 };

            var oS1A4 = new clsSoftwareAnswerRange() { Value = 30, Question = qRange_5_33 };
            var oS2A4 = new clsSoftwareAnswerRange() { Value = 5, Question = qRange_5_33 };
            var oS3A4 = new clsSoftwareAnswerRange() { Value = 19, Question = qRange_5_33 };

            var oS1A5 = new clsSoftwareAnswerMC() { Answer = aCharmander, Question = qPokemon };
            var oS2A5 = new clsSoftwareAnswerMC() { Answer = aCharmander, Question = qPokemon };
            var oS3A5 = new clsSoftwareAnswerMC() { Answer = aSquirtle, Question = qPokemon };

            //software linking
            Softwares = new List<(clsSoftware, List<clsAbstractSoftwareAnswer>)>()
            {
                (oSoftware1,new List<clsAbstractSoftwareAnswer>(){oS1A1,oS1A2,oS1A3,oS1A4,oS1A5}),
                (oSoftware2,new List<clsAbstractSoftwareAnswer>(){oS2A1,oS2A2,oS2A3,oS2A4,oS2A5}),
                (oSoftware3,new List<clsAbstractSoftwareAnswer>(){oS3A1,oS3A2,oS3A3,oS3A4,oS3A5})
            };

            #endregion

            desTranslation1 = new Dictionary<eComparisonOperator, string>()
            {
                [eComparisonOperator.Equal] = "=",
                [eComparisonOperator.NotEqual] = "!=",
                [eComparisonOperator.LessThan] = "<",
                [eComparisonOperator.LessThanOrEqualTo] = "<=",
                [eComparisonOperator.GreaterThan] = ">",
                [eComparisonOperator.GreaterThanOrEqualTo] = ">="
            };
            desTranslation2 = new Dictionary<string, eComparisonOperator>()
            {
                ["="] = eComparisonOperator.Equal,
                ["!="] = eComparisonOperator.NotEqual,
                ["<"] = eComparisonOperator.LessThan,
                ["<="] = eComparisonOperator.LessThanOrEqualTo,
                [">"] = eComparisonOperator.GreaterThan,
                [">="] = eComparisonOperator.GreaterThanOrEqualTo
            };
            MadeData = true;

        }

        public static List<clsAbstractQuestions> GetQuestions(List<clsQuestionFilter> loFilters)
        //public static Dictionary<clsQuestionCategory, List<clsAbstractQuestions>> GetQuestions(List<clsQuestionFilter> loFilters)
        {
            return QuestionFilterAnalyzator(loFilters);
        }
        private static List<clsAbstractQuestions> QuestionFilterAnalyzator(List<clsQuestionFilter> loFilters)
        //private static Dictionary<clsQuestionCategory, List<clsAbstractQuestions>> QuestionFilterAnalyzator(List<clsQuestionFilter> loFilters)     
        {

            //todo: update with db connection
            var loCategories = Categories;
            //todo: update with db connection
            var loQuestions = GetQuestions();

            var doiCategories = new Dictionary<clsQuestionCategory, int>();
            loFilters.ForEach(f =>
            {
                foreach (var c in loCategories)
                {
                    if (c.Id == f.CategoryId)
                    {
                        doiCategories.Add(c, f.Value);
                        break;
                    }
                }
            });

            var result = new List<clsAbstractQuestions>();

            loQuestions.ForEach(q =>
            {
                if (doiCategories.ContainsKey(q.Category))
                {
                    var value = doiCategories[q.Category];
                    if (value >= q.CategoryMinimum && value <= q.CategoryMaximum)
                        result.Add(q);
                }
            });

            List<clsAbstractQuestions> SortedList = result.OrderBy(r => r.Category.Id).ToList();

            return SortedList;//.GroupBy(x => x.Category).ToDictionary(g=>g.Key, g=>g.ToList());
        }
        /*
        private static List<clsAbstractQuestions> QuestionFilterAnalyzator(List<clsQuestionFilter> loFilters)
        {
            //todo: update with db connection
            var loCategories = Categories; 
            var loQuestions = QuestionData;
            //todo: update with db connection

            var doiCategories = new Dictionary<clsQuestionCategory, int>();
            loFilters.ForEach(f =>
            {
                foreach(var c in loCategories)
                {
                    if (c.Id == f.CategoryId)
                    {
                        doiCategories.Add(c, f.Value);
                        break;
                    }
                }
            });

            var result = new List<clsAbstractQuestions>();

            loQuestions.ForEach(q =>
            {
                if (doiCategories.ContainsKey(q.Category)) 
                { 
                    var value = doiCategories[q.Category];
                    if (value >= q.CategoryMinimum && value <= q.CategoryMaximum)
                        result.Add(q);
                }
            });

            List<clsAbstractQuestions> SortedList = result.OrderBy(r => r.Category.Id).ToList();

            //var a = SortedList.GroupBy(x => x.Category);

            return SortedList;
        }
         
         */

        private  static List<clsUserOverview> loUserOverviews = new List<clsUserOverview>();
        private static void ManageUserOverviews()
        {
            var dtNow = DateTime.Now;
            for (int i = loUserOverviews.Count() - 1; i > 0; i--)
                if (loUserOverviews[i].ExpireDate < dtNow)
                    loUserOverviews.RemoveAt(i);
            
        }

        public static List<StringInt> AnalyzeQuestionAnswers(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
        {
            var result = Analyzator(Softwares, ResultConvertor(oMC, oRange));
            loUserOverviews.Add(result);
            return SimplifyAnswers(result);
        }
        //private static List<clsUserSoftwareOverviewSimple> SimplifyAnswers(clsUserOverview x)
        private static List<StringInt> SimplifyAnswers(clsUserOverview x)
        {
            if (x == null) return null;
            var loResult = new List<StringInt>();
            x.Overviews.ForEach(o => loResult.Add(new StringInt() { Number = Convert.ToInt32(o.Percentage), Text = o.Software.ToString() }));
            return loResult;
        }

        public static clsUserOverview GetDetailedAnalyzedQuestionAnswers(string sUserId)
        {
            clsUserOverview result = null;
            foreach (var uo in loUserOverviews)
            {
                if (uo.User == sUserId)
                {
                    result = uo;
                    break;
                }
            }
            ManageUserOverviews();
            return result;
        }

        private static List<clsAbstractUserAnswer> ResultConvertor(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
        {
            var loResult = new List<clsAbstractUserAnswer>();
            var loQuestions = GetQuestions();

            if (oMC != null)
                oMC.ForEach(mc =>
            {
                foreach(var q in loQuestions)
                {
                    if (q.Id == mc.QuestionId)
                    {
                        var qmc = (clsQuestionMC)q;
                        var answers = qmc.PossibleAnswers.Where(a => a.Id == mc.AnswerId).ToList();
                        if (answers == null || answers.Count() == 0)
                            throw new Exception($"Given answer isn't possible for question {qmc}!");
                        if (answers.Count() > 2) //unpossible --> only possible with database error
                            throw new Exception();
                        var answer = answers[0];

                        var x = new clsUserAnswerMC();
                        x.Answer = answer;
                        x.Question = qmc;
                        loResult.Add(x);
                    }
                }
            });
            if (oRange != null)
                oRange.ForEach(r =>
            {
                foreach (var q in loQuestions)
                {
                    if (q.Id == r.QuestionId)
                    {
                        var qr = (clsQuestionRange)q;

                        var x = new clsUserAnswerRange();
                        x.Value = r.Value;
                        x.Operator = QuestionHelp.Translate(r.Operator);
                        loResult.Add(x);
                    }
                }
            });

            return loResult;
        }

        private static List<clsAbstractQuestions> GetQuestions()
        {
            //todo: update with db connection
            return QuestionData;
            //todo: update with db connection
        }

        // Wat doen met vragen die de gebruiker beantwoord heeft, maar de software niet? --> telt dit negatief, telt dit niet ...
        private static clsUserOverview Analyzator(List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> loSoftwares, List<clsAbstractUserAnswer> loResultList)
        {
            var oOverview = new clsUserOverview();

            loSoftwares.ForEach(s =>
            {
                var oUSAO = new clsUserSoftwareOverview(s.Item1);
                               
                foreach (var r in loResultList)
                {

                    clsQuestionRange qr = null;
                    clsQuestionMC qmc = null;
                    clsUserAnswerRange rr = null;
                    clsUserAnswerMC rmc = null;

                    if (r is clsUserAnswerMC)
                    {
                        rmc = (clsUserAnswerMC)r;
                        qmc = rmc.Question;
                    }
                    else if (r is clsUserAnswerRange)
                    {
                        rr = (clsUserAnswerRange)r;
                        qr = rr.Question;
                    }

                    clsUserSoftwareAnswerMatch oMatch = null;

                    foreach (var a in s.Item2)
                    {
                        if (a is clsSoftwareAnswerMC)
                        {
                            var amc = (clsSoftwareAnswerMC)a;
                            var q = amc.Question;
                            if (q == qmc)
                                oMatch = new clsUserSoftwareAnswerMatch(amc, rmc);

                        }
                        else if (a is clsSoftwareAnswerRange)
                        {
                            var ar = (clsSoftwareAnswerRange)a;
                            var q = ar.Question;
                            if (q == qr)
                                oMatch = new clsUserSoftwareAnswerMatch(qr, ar, rr);
                        }

                        if (oMatch != null)
                            break;
                    }

                    if(oMatch != null)
                        oUSAO.AddAnswer(oMatch);
                }

                oOverview.AddOverview(oUSAO);
            });

            return oOverview;
        }

        /*
        private static clsUserSoftwareOverview Analyzator_v0(List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> loSoftwares, List<clsAbstractUserAnswer> loResultList)
        {
            var oOverview = new clsUserOverview();

            var result = new List<List<object>>();

            var loQuestions = new List<object>();
            var loAnswers = new List<object>();

            loQuestions.Add("Questions");
            loQuestions.Add("");
            loAnswers.Add("Answers");
            loAnswers.Add("");


            foreach (var r in loResultList)
            {
                clsAbstractQuestions q = null;
                string answer = null;
                if (r is clsUserAnswerMC)
                {
                    var rmc = (clsUserAnswerMC)r;

                    q = rmc.Question;

                    answer = rmc.Answer.Name;
                }
                else if (r is clsUserAnswerRange)
                {
                    var rr = (clsUserAnswerRange)r;

                    q = rr.Question;
                    var desTranslation = new Dictionary<eComparisonOperator, string>()
                    {
                        [eComparisonOperator.Equal] = "=",
                        [eComparisonOperator.NotEqual] = "!=",
                        [eComparisonOperator.LessThan] = "<",
                        [eComparisonOperator.LessThanOrEqualTo] = "<=",
                        [eComparisonOperator.GreaterThan] = ">",
                        [eComparisonOperator.GreaterThanOrEqualTo] = ">="
                    };
                    answer = desTranslation[rr.Operator] + rr.Value;
                }
                loQuestions.Add(q.Description);
                loAnswers.Add(answer);
            }

            result.Add(loQuestions);
            result.Add(loAnswers);

            loSoftwares.ForEach(s =>
            {
                var soft = s.Item1;

                var loS = new List<object>();

                var iMatch = 0;
                foreach (var r in loResultList)
                {

                    clsQuestionRange qr = null;
                    clsQuestionMC qmc = null;
                    clsUserAnswerRange rr = null;
                    clsUserAnswerMC rmc = null;

                    if (r is clsUserAnswerMC)
                    {
                        rmc = (clsUserAnswerMC)r;
                        qmc = rmc.Question;
                    }
                    else if (r is clsUserAnswerRange)
                    {
                        rr = (clsUserAnswerRange)r;
                        qr = rr.Question;
                    }

                    s.Item2.ForEach(a =>
                    {
                        clsMatch m = null;
                        if (a is clsSoftwareAnswerMC)
                        {
                            var amc = (clsSoftwareAnswerMC)a;
                            var q = amc.Question;
                            if (q == qmc)
                                m = new clsMatch(qmc, amc, rmc);

                        }
                        else if (a is clsSoftwareAnswerRange)
                        {
                            var ar = (clsSoftwareAnswerRange)a;
                            var q = ar.Question;
                            if (q == qr)
                                m = new clsMatch(qr, ar, rr);


                        }

                        if (m != null)
                        {
                            if (m.Match) iMatch++;
                            loS.Add(m);
                        }


                    });

                }

                loS.Insert(0, Math.Round((100.0 * iMatch / loResultList.Count), 2).ToString() + "%");
                loS.Insert(0, soft.Name);
                result.Add(loS);
            });

            return result;
        }
        */

    }
}
