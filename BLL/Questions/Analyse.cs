﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;

namespace BLL
{
    public static partial class clsBLL
    {
        static clsBLL()
        {
            desTranslation1 = new Dictionary<eComparisonOperator, string>()
            {
                [eComparisonOperator.Equal] = "=",
                [eComparisonOperator.NotEqual] = "!=",
                [eComparisonOperator.LessThan] = "<",
                [eComparisonOperator.LessThanOrEqualTo] = "<=",
                [eComparisonOperator.GreaterThan] = ">",
                [eComparisonOperator.GreaterThanOrEqualTo] = ">="
            };
            desTranslation2 = new Dictionary<string, eComparisonOperator>()
            {
                ["="] = eComparisonOperator.Equal,
                ["!="] = eComparisonOperator.NotEqual,
                ["<"] = eComparisonOperator.LessThan,
                ["<="] = eComparisonOperator.LessThanOrEqualTo,
                [">"] = eComparisonOperator.GreaterThan,
                [">="] = eComparisonOperator.GreaterThanOrEqualTo
            };
        }
        
        private static Dictionary<eComparisonOperator, string> desTranslation1;
        private static Dictionary<string, eComparisonOperator> desTranslation2;

        /// <summary>
        /// save  each result for an hour --> so people can download it, but only when they have an account for the website 
        /// possible to make an account if they don't have one
        /// </summary>
        private static List<clsUserOverview> loUserOverviews = new List<clsUserOverview>();

        public static clsUserOverview GetDetailedAnalyzedQuestionAnswers(string sUserId)
        {
            clsUserOverview result = null;
            foreach (var uo in loUserOverviews)
            {
                if (uo.User == sUserId)
                {
                    result = uo;
                    break;
                }
            }
            ManageUserOverviews();
            return result;
        }
        private static void ManageUserOverviews()
        {
            var dtNow = DateTime.Now;
            for (int i = loUserOverviews.Count() - 1; i > 0; i--)
                if (loUserOverviews[i].ExpireDate < dtNow)
                    loUserOverviews.RemoveAt(i);
        }

        public static List<StringInt> AnalyzeQuestionAnswers(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
        {
            var result = Analyzator(clsStatic.GetSoftwareWithAnswers(), ResultConvertor(oMC, oRange));
            loUserOverviews.Add(result);
            return SimplifyAnswers(result);
        }

        private static List<StringInt> SimplifyAnswers(clsUserOverview x)
        {
            if (x == null) return null;
            var loResult = new List<StringInt>();
            x.Overviews.ForEach(o => loResult.Add(new StringInt() { Number = Convert.ToInt32(o.Percentage), Text = o.Software.ToString() }));
            return loResult;
        }
     
        private static List<clsAbstractUserAnswer> ResultConvertor(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
        {
            var loResult = new List<clsAbstractUserAnswer>();
            var loQuestions = GetQuestions();

            if (oMC != null)
                oMC.ForEach(mc =>
                {
                    foreach (var q in loQuestions)
                    {
                        if (q.Id == mc.QuestionId)
                        {
                            var qmc = (clsQuestionMC)q;
                            var answers = qmc.PossibleAnswers.Where(a => a.Id == mc.AnswerId).ToList();
                            if (answers == null || answers.Count() == 0)
                                throw new Exception($"Given answer isn't possible for question {qmc}!");
                            if (answers.Count() > 2) //unpossible --> only possible with database error
                                throw new Exception();
                            var answer = answers[0];

                            var x = new clsUserAnswerMC();
                            x.Answer = answer;
                            x.Question = qmc;
                            loResult.Add(x);
                        }
                    }
                });
            if (oRange != null)
                oRange.ForEach(r =>
                {
                    foreach (var q in loQuestions)
                    {
                        if (q.Id == r.QuestionId)
                        {
                            var qr = (clsQuestionRange)q;

                            var x = new clsUserAnswerRange();
                            x.Value = r.Value;
                            x.Operator = QuestionHelp.Translate(r.Operator);
                            x.Question = qr;
                            loResult.Add(x);
                        }
                    }
                });

            return loResult;
        }

        // Wat doen met vragen die de gebruiker beantwoord heeft, maar de software niet? --> telt dit negatief, telt dit niet ...
        private static clsUserOverview Analyzator(List<clsSoftwareWithAnswers> loSoftwares, List<clsAbstractUserAnswer> loResultList)
        {
            var oOverview = new clsUserOverview();

            loSoftwares.ForEach(s =>
            {
                var oUSAO = new clsUserSoftwareOverview(s);

                foreach (var r in loResultList)
                {

                    clsQuestionRange qr = null;
                    clsQuestionMC qmc = null;
                    clsUserAnswerRange rr = null;
                    clsUserAnswerMC rmc = null;

                    if (r is clsUserAnswerMC)
                    {
                        rmc = (clsUserAnswerMC)r;
                        qmc = rmc.Question;
                    }
                    else if (r is clsUserAnswerRange)
                    {
                        rr = (clsUserAnswerRange)r;
                        qr = rr.Question;
                    }

                    clsUserSoftwareAnswerMatch oMatch = null;

                    foreach (var a in s.Answers)
                    {
                        if (a is clsSoftwareAnswerMC)
                        {
                            var amc = (clsSoftwareAnswerMC)a;
                            var q = amc.Question;
                            if (q.Equals(qmc))
                                oMatch = new clsUserSoftwareAnswerMatch(amc, rmc);

                        }
                        else if (a is clsSoftwareAnswerRange)
                        {
                            var ar = (clsSoftwareAnswerRange)a;
                            var q = ar.Question;
                            if (q.Equals(qr))
                                oMatch = new clsUserSoftwareAnswerMatch(qr, ar, rr);
                        }

                        if (oMatch != null)
                            break;
                    }

                    if (oMatch != null)
                        oUSAO.AddAnswer(oMatch);
                }

                oOverview.AddOverview(oUSAO);
            });

            return oOverview;
        }
 
    }
}
