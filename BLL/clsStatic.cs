﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DAL.DB;

namespace BLL
{
    // Static variables so we don't query the database all the time
    internal class clsStatic
    {

        private static List<clsBaseModel> _lstActivities;
        private static List<clsBaseModel> _lstCategories;
        private static List<clsBaseModel> _lstFunctions;
        private static List<clsBaseModel> _lstSectors;
        private static List<clsBaseModel> _lstSoftwares;
        private static List<clsBaseModel> _lstCompanies;
        private static List<clsBaseModel> _lstCases;

        public static List<clsBaseModel> Activities
        {
            get
            {
                if (_lstActivities == null)
                    _lstActivities = clsDB.Get_Activities();
                return _lstActivities;
            }
            set
            {
                _lstActivities = value;
            }
        }
        public static List<clsBaseModel> Categories
        {
            get
            {
                if (_lstCategories == null)
                    _lstCategories = clsDB.Get_Categories();
                return _lstCategories;
            }
            set
            {
                _lstCategories = value;
            }
        }
        public static List<clsBaseModel> Functions
        {
            get
            {
                if (_lstFunctions == null)
                    _lstFunctions = clsDB.Get_Functions();
                return _lstFunctions;
            }
            set
            {
                _lstFunctions = value;
            }
        }
        public static List<clsBaseModel> Sectors
        {
            get
            {
                if (_lstSectors == null)
                    _lstSectors = clsDB.Get_Sectors();
                return _lstSectors;
            }
            set
            {
                _lstSectors = value;
            }
        }
        public static List<clsBaseModel> Softwares
        {
            get
            {
                if (_lstSoftwares == null)
                    _lstSoftwares = clsDB.Get_MesAll();
                return _lstSoftwares;
            }
            set
            {
                _lstSoftwares = value;
            }
        }
        public static List<clsBaseModel> Companies
        {
            get
            {
                if (_lstCompanies == null)
                    _lstCompanies = clsDB.Get_Companies();

                return _lstCompanies;
            }
            set
            {
                _lstCompanies = value;
            }
        }
        public static List<clsBaseModel> Cases
        {
            get
            {
                if (_lstCases == null)
                    _lstCases = clsDB.Get_Cases();
                return _lstCases;
            }
            set
            {
                _lstCases = value;
            }
        }

        private static List<clsBaseModel> _lstQuestionCategories;
        public static List<clsBaseModel> QuestionCategories
        {
            get
            {
                if (_lstQuestionCategories == null)
                    _lstQuestionCategories = clsDB.Get_QuestionCategories();
                return _lstQuestionCategories;
            }
            set
            {
                _lstQuestionCategories = value;
            }
        }
        private static List<clsBaseModel> _lstQuestions;
        public static List<clsBaseModel> Questions
        {
            get
            {
                if (_lstQuestions == null)
                    _lstQuestions = clsDB.Get_Questions();
                return _lstQuestions;
            }
            set
            {
                _lstQuestions = value;
            }
        }
        private static List<clsBaseModel> _lstQuestionAnswers;
        public static List<clsBaseModel> QuestionAnswers
        {
            get
            {
                if (_lstQuestionAnswers == null)
                    _lstQuestionAnswers = clsDB.Get_QuestionAnswers();
                return _lstQuestionAnswers;
            }
            set
            {
                _lstQuestionAnswers = value;
            }
        }

        // Check if object with this id exists, throws exception unless told not to.
        public static T getObject<T>(int iId, bool xException = true) where T : clsBaseModel
        {
            var lstStatic = getList<T>();
            clsBaseModel oObject = lstStatic.Find(x => x.Id == iId);
            if (oObject != null)
                return (T)oObject.Clone();
            else if (xException)
                throw new Exception($"Could not find a {typeof(T).Name} object with this Id.");
            else return null;
        }

        // Check if object with this name exists, throws exception unless told not to.
        public static T getObject<T>(string sName, bool xException = true) where T : clsBaseModel
        {
            var lstStatic = getList<T>();

            clsBaseModel oObject = lstStatic.Find(x => x.Name == sName);
            if (oObject != null)
                return (T)oObject.Clone();
            else if (xException)
                throw new Exception($"Could not find a {typeof(T).Name} object with this name.");
            else return null;
        }

        public static List<clsBaseModel> getList<T>()
        {
            List<clsBaseModel> lstStatic = null;

            if (typeof(T) == typeof(clsActivity))
                lstStatic = clsStatic.Activities;
            else if (typeof(T) == typeof(clsFunction))
                lstStatic = clsStatic.Functions;
            else if (typeof(T) == typeof(clsCategory))
                lstStatic = clsStatic.Categories;
            else if (typeof(T) == typeof(clsSector))
                lstStatic = clsStatic.Sectors;
            else if (typeof(T) == typeof(clsCompany))
                lstStatic = clsStatic.Companies;
            else if (typeof(T) == typeof(clsSoftware))
                lstStatic = clsStatic.Softwares;
            else if (typeof(T) == typeof(clsCase))
                lstStatic = clsStatic.Cases;
            else if (typeof(T) == typeof(clsQuestionCategory))
                lstStatic = clsStatic.QuestionCategories;
            else if (typeof(T) == typeof(clsAnswerMC))
                lstStatic = clsStatic.QuestionAnswers;
            else if (typeof(T) == typeof(clsAbstractQuestions))
                lstStatic = clsStatic.Questions;
            else if (typeof(T) == typeof(clsAbstractSoftwareAnswer))
                lstStatic = clsStatic.GetSoftwareAnswers().Cast<clsBaseModel>().ToList();
            else
                throw new Exception("Invalid type given.");

            return lstStatic;
        }

        private static Dictionary<int, List<clsAbstractSoftwareAnswer>> _dSoftwareAnswers = new Dictionary<int, List<clsAbstractSoftwareAnswer>>();
        public static List<clsAbstractSoftwareAnswer> GetSoftwareAnswers(clsSoftware x)
        {
            return GetSoftwareAnswers(x.Id);
        }
        public static List<clsAbstractSoftwareAnswer> GetSoftwareAnswers(int id)
        {
            if (!(_dSoftwareAnswers.ContainsKey(id)))
                _dSoftwareAnswers.Add(id, clsDB.Get_SoftwareAnswers(id));
            else if (_dSoftwareAnswers[id] == null)
                _dSoftwareAnswers[id] = clsDB.Get_SoftwareAnswers(id);
            return _dSoftwareAnswers[id];
        }

        public static void SetSoftwareAnswersToNull(clsSoftware x)
        {
            SetSoftwareAnswersToNull(x.Id);
        }
        public static void SetSoftwareAnswersToNull(int id)
        {
            if (_dSoftwareAnswers.ContainsKey(id)) _dSoftwareAnswers[id] = null;
        }

        public static List<clsAbstractSoftwareAnswer> GetSoftwareAnswers()
        {
            var loResult = new List<clsAbstractSoftwareAnswer>();

            List<int> keys = new List<int>(_dSoftwareAnswers.Keys);
            foreach (int key in keys)
            {
                if (_dSoftwareAnswers[key] == null)
                    _dSoftwareAnswers[key] = clsDB.Get_SoftwareAnswers(key);
                if (_dSoftwareAnswers[key] != null) //it's possible that this software doens't have answers --> don't add a null to the list
                    loResult.AddRange(_dSoftwareAnswers[key]);
            }

            return loResult;
        }
        public static void SetSoftwareAnswersToNullBySoftwareAnswer(int iSaId)
        {
            List<int> keys = new List<int>(_dSoftwareAnswers.Keys);
            foreach (int key in keys)
            {
                var val = _dSoftwareAnswers[key];
                var xFound = false;
                if (val != null) // will awlays be != null
                    foreach (var v in val)
                        if (v.Id == iSaId)
                        {
                            _dSoftwareAnswers[key] = null;
                            xFound = true;
                        }
                if (xFound) break;
            }
        }

        static clsStatic()
        {
            Softwares.ForEach(x =>
            {
                _dSoftwareAnswers[x.Id] = clsDB.Get_SoftwareAnswers(x.Id);
            });
        }

        public static List<clsSoftwareWithAnswers> GetSoftwareWithAnswers()
        {
            var loResult = new List<clsSoftwareWithAnswers>();

            List<int> keys = new List<int>(_dSoftwareAnswers.Keys);
            foreach (int key in keys)
            {
                if (_dSoftwareAnswers[key] == null)
                    _dSoftwareAnswers[key] = clsDB.Get_SoftwareAnswers(key);
                if (_dSoftwareAnswers[key] != null) //it's possible that this software doens't have answers --> don't add a null to the list
                {
                    var r = new clsSoftwareWithAnswers(getObject<clsSoftware>(key))
                    {
                        Answers = _dSoftwareAnswers[key]
                    };
                    loResult.Add(r);
                }
            }

            return loResult;
        }

    }
}
