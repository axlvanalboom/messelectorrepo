USE [master]
GO
/****** Object:  Database [MES4SME_Selector]    Script Date: 30/01/2020 11:04:16 ******/
CREATE DATABASE [MES4SME_Selector]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MES4SME_Selector', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\MES4SME_Selector.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MES4SME_Selector_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\MES4SME_Selector_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [MES4SME_Selector] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MES4SME_Selector].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MES4SME_Selector] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET ARITHABORT OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MES4SME_Selector] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MES4SME_Selector] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MES4SME_Selector] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MES4SME_Selector] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MES4SME_Selector] SET  MULTI_USER 
GO
ALTER DATABASE [MES4SME_Selector] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MES4SME_Selector] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MES4SME_Selector] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MES4SME_Selector] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MES4SME_Selector] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MES4SME_Selector] SET QUERY_STORE = OFF
GO
USE [MES4SME_Selector]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [MES4SME_Selector]
GO
/****** Object:  User [website]    Script Date: 30/01/2020 11:04:16 ******/
CREATE USER [website] FOR LOGIN [website] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [website]
GO
ALTER ROLE [db_datareader] ADD MEMBER [website]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [website]
GO
/****** Object:  Table [dbo].[Logging]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging](
	[logID] [nvarchar](255) NOT NULL,
	[logTime] [nvarchar](255) NOT NULL,
	[logType] [int] NOT NULL,
	[logParamS] [nvarchar](max) NULL,
	[logParamI] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogType]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogType](
	[ltPk] [int] IDENTITY(1,1) NOT NULL,
	[ltType] [nvarchar](255) NOT NULL,
	[ltTable] [nvarchar](255) NULL,
 CONSTRAINT [PK_LogType2] PRIMARY KEY CLUSTERED 
(
	[ltPk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cases]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cases](
	[casId] [int] IDENTITY(1,1) NOT NULL,
	[casName] [varchar](max) NOT NULL,
	[casDescription] [varchar](max) NULL,
	[casWebsite] [varchar](max) NULL,
	[casContent] [nvarchar](max) NULL,
	[casPublic] [int] NULL,
	[casLogoUrl] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[casId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[comId] [int] IDENTITY(1,1) NOT NULL,
	[comName] [varchar](max) NOT NULL,
	[comDescription] [varchar](max) NULL,
	[comWebsite] [varchar](max) NULL,
	[comAdress] [varchar](max) NULL,
	[comContactEmail] [varchar](max) NULL,
	[comContactTelefoon] [varchar](max) NULL,
	[comContent] [nvarchar](max) NULL,
	[comPublic] [int] NULL,
	[comLogoUrl] [varchar](max) NULL,
 CONSTRAINT [PK__Company__9052B5562F6D9AD6] PRIMARY KEY CLUSTERED 
(
	[comId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MesSoftware]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MesSoftware](
	[meId] [int] IDENTITY(1,1) NOT NULL,
	[meName] [varchar](max) NOT NULL,
	[meDescription] [varchar](max) NULL,
	[meWebsite] [varchar](max) NULL,
	[meContent] [nvarchar](max) NULL,
	[mePublic] [int] NULL,
	[meLogoUrl] [varchar](max) NULL,
 CONSTRAINT [PK__MesSoftw__7D0E9F4B1C96D05B] PRIMARY KEY CLUSTERED 
(
	[meId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[LogView]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LogView]
AS
SELECT        logId as ID, logTime as Time, ltType as Type, casName AS Parameter
FROM            logging JOIN
                         LogType ON logType = ltPk JOIN
                         Cases ON casID = logParamI
WHERE        ltTable = 'Cases'

UNION

SELECT        logId as ID, logTime as Time, ltType as Type, meName AS Parameter
FROM            logging JOIN
                         LogType ON logType = ltPk JOIN
                         MesSoftware ON meID = logParamI
WHERE        ltTable = 'MesSoftware'

UNION

SELECT         logId as ID, logTime as Time, ltType as Type,comName AS Paramter
FROM            logging JOIN
                         LogType ON logType = ltPk JOIN
                         Company ON comID = logParamI
WHERE        ltTable = 'Company'

UNION

SELECT         logId as ID, logTime as Time, ltType as Type, logParamS AS Parameter
FROM            logging JOIN
                         LogType ON logType = ltPk
WHERE        ltTable IS NULL
GO
/****** Object:  Table [dbo].[MeCatFuLink]    Script Date: 30/01/2020 11:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeCatFuLink](
	[mcflId] [int] IDENTITY(1,1) NOT NULL,
	[mcflMes] [int] NOT NULL,
	[mcflCategory] [int] NOT NULL,
	[mcflFunction] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[mcflId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CasCatFuLink]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CasCatFuLink](
	[ccflId] [int] IDENTITY(1,1) NOT NULL,
	[ccflCase] [int] NOT NULL,
	[ccflCategory] [int] NOT NULL,
	[ccflFunction] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ccflId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ComMeCasSeAcLink]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComMeCasSeAcLink](
	[cmcsalId] [int] IDENTITY(1,1) NOT NULL,
	[cmcsalCompany] [int] NOT NULL,
	[cmcsalActivity] [int] NOT NULL,
	[cmcsalMesSoftware] [int] NOT NULL,
	[cmcsalSector] [int] NULL,
	[cmcsalCase] [int] NULL,
 CONSTRAINT [PK__ComMeCas__A0DFFED7A2EE75B8] PRIMARY KEY CLUSTERED 
(
	[cmcsalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[crulID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
	[crulCompany] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[crulID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 1) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[FullLinkView]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FullLinkView]
AS
SELECT        dbo.ComMeCasSeAcLink.cmcsalCompany, dbo.ComMeCasSeAcLink.cmcsalActivity, dbo.ComMeCasSeAcLink.cmcsalMesSoftware, dbo.ComMeCasSeAcLink.cmcsalSector, dbo.ComMeCasSeAcLink.cmcsalCase, 
                         dbo.MeCatFuLink.mcflMes, dbo.MeCatFuLink.mcflCategory, dbo.MeCatFuLink.mcflFunction, dbo.CasCatFuLink.ccflCase, dbo.CasCatFuLink.ccflCategory, dbo.CasCatFuLink.ccflFunction, dbo.AspNetUserRoles.UserId, 
                         dbo.AspNetUserRoles.crulCompany
FROM            dbo.AspNetUserRoles CROSS JOIN
                         dbo.CasCatFuLink CROSS JOIN
                         dbo.ComMeCasSeAcLink CROSS JOIN
                         dbo.MeCatFuLink
GO
/****** Object:  View [dbo].[FullyLinked]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FullyLinked]
AS
SELECT        dbo.ComMeCasSeAcLink.cmcsalCompany AS Company, dbo.ComMeCasSeAcLink.cmcsalActivity AS Activity, dbo.ComMeCasSeAcLink.cmcsalMesSoftware AS Mes, dbo.ComMeCasSeAcLink.cmcsalSector AS Sector, 
                         dbo.ComMeCasSeAcLink.cmcsalCase AS Cases, dbo.MeCatFuLink.mcflCategory AS MesCategory, dbo.MeCatFuLink.mcflFunction AS MesFunction, dbo.CasCatFuLink.ccflCategory AS CaseCategory, 
                         dbo.CasCatFuLink.ccflFunction AS CaseFunction
FROM            dbo.ComMeCasSeAcLink FULL OUTER JOIN
                         dbo.MeCatFuLink ON dbo.ComMeCasSeAcLink.cmcsalMesSoftware = dbo.MeCatFuLink.mcflMes FULL OUTER JOIN
                         dbo.CasCatFuLink ON dbo.CasCatFuLink.ccflCase = dbo.ComMeCasSeAcLink.cmcsalCase
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[acId] [int] IDENTITY(1,1) NOT NULL,
	[acName] [varchar](max) NOT NULL,
	[acDescription] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[acId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[roDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NULL,
	[TwoFactorEnabled] [bit] NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NULL,
	[AccessFailedCount] [int] NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[usOccupation] [varchar](max) NULL,
	[usUsername] [varchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[catId] [int] IDENTITY(1,1) NOT NULL,
	[catName] [varchar](max) NOT NULL,
	[catDescription] [varchar](max) NULL,
 CONSTRAINT [PK__Category__17B6DD0600B23649] PRIMARY KEY CLUSTERED 
(
	[catId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 30/01/2020 11:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[contactId] [int] IDENTITY(1,1) NOT NULL,
	[contactEmail] [varchar](max) NOT NULL,
	[contactVerdeling] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[contactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Functions]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Functions](
	[fuId] [int] IDENTITY(1,1) NOT NULL,
	[fuName] [varchar](max) NOT NULL,
	[fuDescription] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[fuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sector]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sector](
	[seId] [int] IDENTITY(1,1) NOT NULL,
	[seName] [varchar](max) NOT NULL,
	[seDescription] [varchar](max) NULL,
 CONSTRAINT [PK__Sector__2C89FEAF81344FA6] PRIMARY KEY CLUSTERED 
(
	[seId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Activity] ON 

INSERT [dbo].[Activity] ([acId], [acName], [acDescription]) VALUES (1031, N'Supplier', NULL)
INSERT [dbo].[Activity] ([acId], [acName], [acDescription]) VALUES (1032, N'Integrator', NULL)
INSERT [dbo].[Activity] ([acId], [acName], [acDescription]) VALUES (2029, N'Consultant', NULL)
SET IDENTITY_INSERT [dbo].[Activity] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name], [roDescription]) VALUES (N'1', N'User', N'Regular user')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [roDescription]) VALUES (N'2', N'Owner', N'Content owner who may edit own content')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [roDescription]) VALUES (N'3', N'Administrator', N'Allmighty')
SET IDENTITY_INSERT [dbo].[AspNetUserRoles] ON 

INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (1036, N'2ce8a9b9-f0bc-4d13-b404-5521dc239931', N'3', 1)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2035, N'9bcd6745-4977-4ca2-955d-9362445cd2b9', N'3', 1)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2036, N'8b069764-d139-45ab-baa2-f5cd61cfd56e', N'1', NULL)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2037, N'e29b7ccf-4582-4c0e-9459-8fe02dde75f1', N'1', NULL)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2038, N'6e6ac9a9-6bc3-4daa-9f3a-542308cbba89', N'2', NULL)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2039, N'aa62d33d-bd26-4277-a3f2-a7ed3c619506', N'3', NULL)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2040, N'0181eaf7-d5a2-4dd0-b6ed-7e65154de139', N'3', 1)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2041, N'5b47d67e-4a93-42ff-956b-363a1a44ce28', N'2', NULL)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2042, N'32d4fd55-e8e4-4c3b-b74b-df4450ad8148', N'2', 2)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2043, N'2245fcde-36aa-4327-b5ef-d29c15d51964', N'2', 5)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2044, N'57475edd-2c8e-4265-8e9f-c58b6e9e4df8', N'2', 13)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2045, N'f2efa513-ccc7-435b-b841-23fbee271dd5', N'2', 15)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2046, N'cba182a1-cdda-4969-b5e6-116100b62deb', N'2', 10)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2047, N'f6b8acfa-19ae-4056-a209-ff2b5f056b29', N'2', 4)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2048, N'2e4bd6a5-990d-4515-810c-b38b619228ec', N'2', 12)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2049, N'9d60fe7d-78a8-4d54-9f2a-19b7f55efa92', N'2', 9)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2050, N'd1f62ad7-5f62-430c-bc96-b054af3d783e', N'2', 18)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2051, N'05b78f93-1aa5-4368-ab58-9213ebcdc7dd', N'2', 4)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2052, N'6f278493-1b4e-44ab-8fea-6d38fc8e6b4e', N'2', 8)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2053, N'cb35ca1e-c27f-4f8c-b54d-6a89f9971ceb', N'2', 7)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2054, N'd5ca5ec6-e414-47bc-a14d-0b59dcc7a6d4', N'2', 23)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2055, N'afcbe08b-dde9-4b95-badb-bd1d8a17df90', N'2', 21)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2056, N'796d574e-f237-4daa-b805-4ecb0045020e', N'2', 14)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2057, N'f8a7ede0-c49e-4a53-af97-44c78eea78e5', N'2', 12)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2058, N'ab551253-056d-43e3-8490-895f5d40d425', N'2', 24)
INSERT [dbo].[AspNetUserRoles] ([crulID], [UserId], [RoleId], [crulCompany]) VALUES (2059, N'00983947-b21a-4c5a-83c7-361056350656', N'2', NULL)
SET IDENTITY_INSERT [dbo].[AspNetUserRoles] OFF
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'00983947-b21a-4c5a-83c7-361056350656', N'matthias.schamp@ugent.be', 1, N'ADsqDlKu4uKBLQe26Yphw+/EfEkCkchZd42VUOR89o1/uldmRYU2zD/Pgi+xYMrxuQ==', N'0c392bff-461f-44df-8b19-35fe3d4c3262', N'056 24 12 21', 0, 0, NULL, 1, 0, N'matthias.schamp@ugent.be', N'Student', N'Matthias Schamp')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'0181eaf7-d5a2-4dd0-b6ed-7e65154de139', N'johannes.cottyn@ugent.be', 1, N'AKH9Gkh2LFymtyeJiXqsmnQ+wc0CQ/V2d4bGCE6XuKmcDtvjY8vpgAtujqFgU+JZ/Q==', N'4eaf2d37-ffd1-42ec-aa88-3bc3f077eb83', N'056 241 221', 0, 0, NULL, 1, 0, N'johannes.cottyn@ugent.be', N'Professor', N'Johannes Cottyn')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'05b78f93-1aa5-4368-ab58-9213ebcdc7dd', N'aurelie.de.brauwer@azumuta.com', 1, N'AIWXyunP+f7nVwwBjW2nfgv8cgp3yO+kbdaTooQUsUMv25s5XP8ETN9uTmXnnnaFAw==', N'086dd673-85ff-4f32-8f8c-028845dcea5c', N'+32479555913', 0, 0, NULL, 1, 0, N'aurelie.de.brauwer@azumuta.com', N'Marketing en Communicatie Verantwoordelijke', N'Aurélie De Brauwer')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'2245fcde-36aa-4327-b5ef-d29c15d51964', N'sarah@catsolutions.be', 1, N'ACtFLw/QeidTpGksOiI0jLo2ZqlxH0S9yNintOyKJ66fXnbA0lUZgi8gDinkzt04pw==', N'76bbd51c-a1f5-4901-9712-25971ecc61fc', N'0472 666 018', 0, 0, NULL, 1, 0, N'sarah@catsolutions.be', N'Office Manager', N'Sarah Van Dam')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'2ce8a9b9-f0bc-4d13-b404-5521dc239931', N'stijn.huysentruyt@ugent.be', 1, N'ALul2WECllQY2vIA8TQUOiBtCR9VPTNP4kKnBjWZ6u9eqctZAwZTHmXtmamVEPjn9g==', N'1a04d390-b026-40b3-a175-0a3c59d859f3', N'+32 (0)56 24 12 21', 0, 0, NULL, 1, 0, N'stijn.huysentruyt@ugent.be', N'Wetenschappelijk Medewerker', N'Stijn Huysentruyt')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'2e4bd6a5-990d-4515-810c-b38b619228ec', N'stefan.ruyters@gemsotec.com', 1, N'AF5DrSVsxlifptBYo409zcmq+G7NmBrGQIt46kTe9DS43oAYfw1cLiUlwm0SyoDhog==', N'20bdddb4-3153-4e5e-9308-87471b002665', N'0497402819', 0, 0, NULL, 1, 0, N'stefan.ruyters@gemsotec.com', N'Founder', N'Stefan Ruyters')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'32d4fd55-e8e4-4c3b-b74b-df4450ad8148', N'yme.bosma@agilitec.nl', 1, N'ALRaLbzAcTkwKAifjBTJDhufNcU2vg8n54jgH3JCchOirU+VX8HeipbPBNFFFxPKWw==', N'3cef53f3-4708-46f1-a825-ef5d56bd639a', N'+31 6 22 609657', 0, 0, NULL, 1, 0, N'yme.bosma@agilitec.nl', N'Owner', N'Yme Bosma')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'57475edd-2c8e-4265-8e9f-c58b6e9e4df8', N'tom.serru@ikologik.be', 1, N'AErUowMYTsbz1kh4b4H1gJzZQvceNUu0H2pLTKeTpGZi6/s8WGCu6XEhEd/hd9uz3Q==', N'bc31e9ea-9f44-49e2-9993-30010cd88463', N'+32478226931', 0, 0, NULL, 1, 0, N'tom.serru@ikologik.be', N'Partner', N'Tom Serru')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'5b47d67e-4a93-42ff-956b-363a1a44ce28', N'Vanalboom.axl@hotmail.com', 1, N'AESlbbE4hpGMbuIq5zVObgB6Vlhre7dxoG7MKcRhuq8CqUCY20txOpvmjho/X3BS3w==', N'f297d2f5-0a4a-4eca-a785-bd48be869be8', N'+32 (0)56 24 12 21', 0, 0, NULL, 1, 0, N'Vanalboom.axl@hotmail.com', N'Wetenschappelijk medewerker', N'Administrator')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'6e6ac9a9-6bc3-4daa-9f3a-542308cbba89', N'kevin.ketelers@ugent.be', 1, N'AIuSdwzg5eOauMTPJkYlEQFPl62LUeKiGPJ49EKuLeAEgZsUCMW5gtUKm5saXZx+lg==', N'2abbca2c-4858-42ba-af72-6714b152b8a1', N'056 24 12 21', 0, 0, NULL, 1, 0, N'kevin.ketelers@ugent.be', N'Researcher', N'Kevin Ketelers')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'6f278493-1b4e-44ab-8fea-6d38fc8e6b4e', N'carlier.l@contec.be', 1, N'AHB3Yjwzp06G84Kbpyq6N+E5W9nK4K1FvHZf7hjeVs06ksQ/AD7Q53eOgldeYbbUtg==', N'99dfffdb-90db-409d-ac57-1c0492fa5f64', N'0490446233', 0, 0, NULL, 1, 0, N'carlier.l@contec.be', N'Sales engineer', N'Lennart Carlier')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'796d574e-f237-4daa-b805-4ecb0045020e', N'niels.colson@ddeng.be', 1, N'AI7UuNL5Wk/z0xlb72aWg2JWRKE6vCftXjHZhFCkYT8L/z9VK9ZU3qpOaX7nF1gKXg==', N'9ceeae08-4ba1-4b3e-b671-2f9b2d41f249', N'0476680342', 0, 0, NULL, 1, 0, N'niels.colson@ddeng.be', N'Managing Director', N'Niels Colson')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'8b069764-d139-45ab-baa2-f5cd61cfd56e', N'stijnhuysentruyt@gmail.com', 1, N'APXEQZFeOQzN+qg8M/2vXGKctlVj57qJvlaLHca+dj7AgzB50MKzfFBM6gvAlA4vnQ==', N'ef33ca69-00c6-451e-a030-d0885e962437', N'+3256241221', 0, 0, NULL, 1, 0, N'stijnhuysentruyt@gmail.com', N'Wetenschappelijk Medewerker', N'Stijn Huysentruyt')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'9bcd6745-4977-4ca2-955d-9362445cd2b9', N'axl.vanalboom@ugent.be', 1, N'AIxjAbwWCLODpFyz6brXS8KSRTFpJQ78AWQ87sSMGjemN+L7hvbCtFL3SGTIRcb+8w==', N'71851f3c-2c1a-43c0-a230-84ddf0ce1f20', N'+32 (0)56 24 12 21', 0, 0, NULL, 1, 0, N'axl.vanalboom@ugent.be', N'Wetenschappelijk Medewerker', N'Axl Van Alboom')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'9d60fe7d-78a8-4d54-9f2a-19b7f55efa92', N'lies.verbauwhede@damatec.be', 1, N'AKEwZrJxM9+r+3n2sbWTKcDQ/fpFnrtrjgtbDpekEaOlnKqN4BK5GPEzHLK1m8YjFg==', N'23fa6870-1010-4c03-ad00-928f1ff009db', N'+32 499 36 41 57', 0, 0, NULL, 1, 0, N'lies.verbauwhede@damatec.be', N'Account Manager', N'Lies Verbauwhede')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'aa62d33d-bd26-4277-a3f2-a7ed3c619506', N'tijl.deneut@ugent.be', 1, N'AFj2+IY7l+zNNUMK46Cc6sD5g2UWBIljclTVaLpng0JiyiL89OMBdkSauuwtZRYdJg==', N'305ba39c-a51e-4edb-84f8-c7c765add8fb', N'056241211', 0, 0, NULL, 1, 0, N'tijl.deneut@ugent.be', N'None', N'Tijl Deneut')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'ab551253-056d-43e3-8490-895f5d40d425', N'bk@logflow.be', 1, N'ADfIbhqzXDQquwFcUtOn/DZdyKi/0amybxk0HPtXvvYxpGjsyvZnBoRDUk11/31Z3Q==', N'62f53f88-1a37-462a-9290-5950a12ed93a', N'050671422', 0, 0, NULL, 1, 0, N'bk@logflow.be', N'CIO', N'Bart Kindt')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'afcbe08b-dde9-4b95-badb-bd1d8a17df90', N'arne.bracke@logflow.be', 1, N'AE6Nu66kkEs8tdffPuqymhvPeXS5AunZjX6I7Bwne2D0La1ywGf1vECiO+JrTz8r5A==', N'38096d6e-3517-4e61-b9d5-b659e3335060', N'0485153886', 0, 0, NULL, 1, 0, N'arne.bracke@logflow.be', N'Senior lean coach', N'Arne Bracke')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'cb35ca1e-c27f-4f8c-b54d-6a89f9971ceb', N'bart.willems@cloudautomation.be', 1, N'AIYP5YFnvfzrX6gqbEXT+rYo2tzA39Df1b4Y+74I7s7CNm0Vl4nooUg5UG0zIx/3OQ==', N'fe31c376-2f01-4c1b-a0bb-79bd7cdb1e01', N'055890420', 0, 0, NULL, 1, 0, N'bart.willems@cloudautomation.be', N'Bestuurder', N'Bart Willems')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'cba182a1-cdda-4969-b5e6-116100b62deb', N'JeremyM@Dynamics.be', 1, N'AJXgG5IVhK7Zdph0m4twhdH6vxOTCA2M/WjQv8pcUEncTIZOAKWYFpFOzE2bcFxVRw==', N'2441bd2f-2305-4217-a26d-4158662d4b47', N'+32 56 42 81 00', 0, 0, NULL, 1, 0, N'JeremyM@Dynamics.be', N'Project Engineer', N'Jeremy Mahieu')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'd1f62ad7-5f62-430c-bc96-b054af3d783e', N'katrien.devulder@savaco.com', 1, N'ALFpveQMjiNg+nZEGTUn1UFkB358HvdeDfpytfnnnQcro1Q8qBXQvsHIg5530YUhwA==', N'764fb458-54d9-4820-883b-8aef0414d458', N'003256260336', 0, 0, NULL, 1, 0, N'katrien.devulder@savaco.com', N'Content Marketer', N'Katrien Devulder')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'd5ca5ec6-e414-47bc-a14d-0b59dcc7a6d4', N'marketing@scalefactory.eu', 1, N'AImlqOHDMsmfKPYkI99FTWIVOSEOMGDvz4rPvxcd/kUDJDgZ1cGbg2kpkJBXdrWYrg==', N'9eaf00e6-b457-4051-9d3c-5b9a95fb43e4', N'+32468413416', 0, 0, NULL, 1, 0, N'marketing@scalefactory.eu', N'Owner', N'Stijn Wijndaele')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'e29b7ccf-4582-4c0e-9459-8fe02dde75f1', N'Lauren.VanDeGinste@UGent.be', 1, N'AKBKM6cwIC/ZaPtocV97WtpWGXIg4m3PGSTs6uQmNdcDbv6bNPJhY6oDY9Yh8X82Vw==', N'a469c0e1-c285-44c6-a40a-bba4ef1aec21', N'056241221', 0, 0, NULL, 1, 0, N'Lauren.VanDeGinste@UGent.be', N'PhD Student', N'Lauren Van Der Ginste')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'f2efa513-ccc7-435b-b841-23fbee271dd5', N'kristof@one-two.com', 1, N'AJsLKry70eGTXctI38OZxGsXphTd8CShdUGR2LGM+nv127r8P6riVj47cSh6L+4Mvg==', N'0a7360eb-800c-4ec6-819a-04628fb9d4fd', N'0478785388', 0, 0, NULL, 1, 0, N'kristof@one-two.com', N'zaakvoerder', N'Kristof Wittouck')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'f6b8acfa-19ae-4056-a209-ff2b5f056b29', N'batist.leman@azumuta.com', 1, N'AMJP/+OuPP1ht0gssdBpWyhAmsji23pVLNAPiWJmKlpggxq+V3OgkgasNS0BH7di2Q==', N'bfd76868-a1fb-4e7e-af86-33ab8953d099', N'+32499346069', 0, 0, NULL, 1, 0, N'batist.leman@azumuta.com', N'CEO', N'Batist Leman')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [usOccupation], [usUsername]) VALUES (N'f8a7ede0-c49e-4a53-af97-44c78eea78e5', N'geert.sergoyne@gemsotec.com', 1, N'AAuipt7lgc9oqM85mUZQaOzCQaZBMr55GjftWUuUpvUzvSmOr/Eb+O7lScK+952TfQ==', N'bad25c85-2052-46d8-81ac-1dc2a36369c0', N'0032473423536', 0, 0, NULL, 1, 0, N'geert.sergoyne@gemsotec.com', N'CEO', N'Geert Sergoyne')
SET IDENTITY_INSERT [dbo].[CasCatFuLink] ON 

INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (18, 27, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (19, 27, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (20, 28, 1056, 1108)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (21, 28, 1056, 1103)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (22, 28, 1056, 1107)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (23, 31, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (24, 31, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (25, 31, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (26, 32, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (27, 32, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (28, 33, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (29, 33, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (30, 33, 1056, 1103)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (31, 33, 1056, 1102)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (32, 33, 1056, 1108)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (33, 33, 1056, 1107)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (34, 33, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (35, 34, 1056, 1103)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (36, 34, 1056, 1102)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (37, 34, 1056, 1107)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (38, 34, 1056, 1108)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (39, 35, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (40, 35, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (41, 38, 1058, 1103)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (42, 38, 1058, 1107)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (43, 38, 1058, 1108)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (44, 38, 1056, 1103)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (45, 38, 1056, 1107)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (46, 38, 1056, 1108)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (47, 29, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (48, 29, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (49, 29, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (50, 30, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (51, 30, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (52, 30, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (53, 37, 1059, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (54, 37, 1059, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (55, 37, 1057, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (56, 37, 1057, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (57, 37, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (58, 37, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (59, 37, 1058, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (60, 37, 1058, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (61, 36, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (62, 36, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (63, 36, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (64, 39, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (65, 39, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (66, 41, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (67, 41, 1056, 1109)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (68, 42, 1056, 1110)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (69, 42, 1056, 1111)
INSERT [dbo].[CasCatFuLink] ([ccflId], [ccflCase], [ccflCategory], [ccflFunction]) VALUES (70, 42, 1056, 1109)
SET IDENTITY_INSERT [dbo].[CasCatFuLink] OFF
SET IDENTITY_INSERT [dbo].[Cases] ON 

INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (27, N'compound omgeving MES', N'Verhoog de output van bereidingsmassa door variatie in doorlooptijd, veroorzaakt door vertragingen in de productie te verminderen, met een initiële focus op eliminatie van wachttijd.', N'https://www.myplantfloor.nl/over-ons/', NULL, NULL, NULL)
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (28, N'Addax Motors', N'Azumuta maakt gebruiksvriendelijke werkinstructies mogelijk, waar verschillende opties in één product kunnen verwerkt worden op een gemakkelijke manier.
De operator ziet enkel de stappen die van toepassing zijn op een bepaalde bestelling en kan gemakkelijk de werkinstructie volgen. Het opnemen van de meetwaarden gaat automatisch door een koppeling met de tool op de fabrieksvloer.
', N'https://www.addaxmotors.com/nl/', NULL, 0, N'Images/20200108164027.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (29, N'Altachem ', N'Altachem NV, deel van de Lindal groep, is wereldwijd de nummer 1 in de ontwikkeling en productie
van ventielen, applicatietools en accessoires voor 1 component 
PU-schuim in drukhouders. Om de productie efficiëntie verder te verhogen was een totaal inzicht van de machines nodig. Door het loggen van de procesdata, inclusief stilstanden, kon de OEE (Overall Equipment Effectiveness) bekomen worden. ', N'http://www.altachem.com/', NULL, NULL, N'Images/20200108143615.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (30, N'De Keyser', N'Vleesfabrikant De Keyser is een van de belangrijkste Belgische private label producenten van fijne vleeswaren in varken, kip en kalkoen. Ze wensten hun energieverbruik in kaart te brengen om betere inzichten te verkrijgen en de gegevens van de energie leverancier af te kunnen toetsen. Hierdoor konden ze energiepieken én eerder nodig geachte extra investeringen vermijden.', N'http://www.dekeysermeatproducts.com/nl', NULL, NULL, N'Images/20200109101557.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (31, N'C-MEC', N'Hun huidige systeem zorgde voor menig frustratie. Het was ten eerste een oud systeem, zonder structureel onderhoudsplan. Er werden ook geen updates uitgevoerd, tenzij tegen een torenhoge kostprijs. Gevolg? Een zeer traag systeem, dat meer dan eens uit het niets uitviel. Daardoor liepen ze bij C-MEC een enorme technologische achterstand op. Aan de noden van gebruikers, klanten en leveranciers werd al lang niet meer beantwoord.', N'https://catsolutions.be/nl/referenties/cmec/', NULL, NULL, N'Images/20200108164401.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (32, N'Line Performance System', N'Stilstanden worden automatisch geregistreerd en gecategoriseerd zodat bij de dagelijkse teamvergadering de major downs en de grootste productieverliezen gevisualiseerd worden aan de hand van grafieken.
Het is een watchdogsysteem die continu waakt over de efficiëntie van de productie waarbij niet enkel de grote maar ook de sluimerende verliezen naar boven komen.
', N'https://actware.be/machinebouw', NULL, NULL, N'Images/20200108164617.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (33, N'Duracell verpakking', N'De volledige verpakkingsvloer is omgebouwd van manuele papier gebaseerde naar volautomatische productie omgeving. De sturing vanuit het workflow gebeuren (Push methode), waardoor enkel data aan operator aangeboden en gevraagd wordt op noodzakelijke momenten, herleid de productievloer tot volledig LEAN manufacturing opzet. ', N'http://duracell', NULL, 0, N'Images/20200108164949.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (34, N'Soleras Advanced Coatings', N'Onze klant, Soleras Advanced Coatings te Deinze ging  succesvol live met onze MES - toepassing ECHO. Alle operatoren kunnen daardoor voortaan snel en mobiel aan de slag op de tablet die bij de werkpost staat om parameters te controleren en registreren. Dankzij de volledige digitalisering van de werkinstructies, werd het papierwerk volledig uit de fabriek verbannen, de foutenmarge verkleind & zijn snellere analyses mogelijk. ', N'https://www.soleras.com/', NULL, NULL, N'Images/20200108165131.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (35, N'Algist Bruggeman', N'Algist Bruggeman had geen zicht of bepaalde recepten beter evolueren in één reactor ten opzichte van een andere en of de actuele productie overeenkomt met de referentie. Dit kwam door opvolging van de productie op papier. Deze papieren werden dan manueel overgebracht naar verschillende databronnen, wat leidt tot tijdverlies en een hogere foutenratio. Factry implementeerde hun Historian module, wat zorgt voor data-acquisitie van de volledige productiesite. Hierbij werden ook nog een aantal custom MES-ontwikkelingen geïmplementeerd (receptbeheer en versiecontrole, integratie met ERP/planning, integratie met LIMS …)', N'https://algistbruggeman.be/', NULL, NULL, N'Images/20200108165504.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (36, N'Homifreez', N'Homifreez is een producent van diepgevroren groenten, aardappelen en fruit. Ze produceren zowel onder Homifreez als onder private labels en voeren wereldwijd uit. Er wordt ingezet op duurzaamheid op een flexibele, kwalitatieve en klantvriendelijke manier.  Hun wens was het kunnen monitoren van de energie verbruiken en productie stilstanden.', N'http://www.homifreez.com/nl/home-1.htm', NULL, NULL, N'Images/20200109114809.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (37, N'Extremis', N'Bij Extremis worden duurzame en tijdloze buitenmeubels ontwerpen en geproduceerd. Dit in hun vestigingen te Poperinge, Japan en de VS. Ze wensten ze hun montagetijden in kaart brengen om zo verliezen en kwaliteit beter te kunnen opvolgen.', N'https://www.extremis.com/nl', NULL, NULL, N'Images/20200109135212.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (38, N'AGCO', N'AGCO is the world''s largest manufacturer of machinery
and equipment focused solely on the agricultural industry.
With some of the most well respected, forward-thinking collection
of brands under AGCO, we''re not just manufacturing machines, we''re manufacturing a brighter future for farms everywhere. Due to a high variety in the machines configurations the operators need more detailed information during the assembly. Improvement of the instructions in-line are increasing both throughput and quality.', N'https://www.agcocorp.com/', NULL, NULL, N'Images/20200109141109.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (39, N'Kooima Company', N'For over 30 years Kooima Company has been manufacturing replacement forage harvester parts, combine parts, mixer wagon knives, header adapters and more! We are an independent manufacturer of replacement parts for a variety of companies such as John Deere®, Claas®, New Holland® and Krone®, because of this we are able to produce quality parts at competitive prices. Try the Kooima Direct Advantage today! You will eliminate the middleman by communicating with a one source dealer, distributor and manufacturer so we understand your questions and needs.', N'https://www.kooima.com/', NULL, NULL, N'Images/20200109152534.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (41, N'Nyobe', N'NYOBE NV, Belgium''s premier producer of high-grade nylon 6 polymers and PA6 BCF yarns.

Catering experience and excellence to a rapidly evolving industry, we are specialised in the proactive development and international distribution of intermediate nylon products that are a staple of the compounding and carpet spinning markets.', N'https://www.nyobe.be/', NULL, NULL, NULL)
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (42, N'Sirris Factory 4.0', N'Industrie 4.0 gaat niet alleen om het automatiseren van productieprocessen waarbij grote reeksen worden afgewerkt. Het omvat ook optimalisaties binnen de productie van kleine reeksen met veel varianten. Om dit te demonstreren bouwde Sirris, het onderzoekscentrum van en voor de technologische industrie, een Factory 4.0 demonstrator in haar Smart & Digital Factory te Kortrijk. De demonstrator is een waarheidsgetrouwe weergave van een assemblagelijn voor kleine reeksen en toont de mogelijkheden van Industrie 4.0 om dergelijke productieprocessen efficiënter te laten verlopen. Zo is de productielijn opgedeeld in zes cellen, waardoor sneller een nieuwe reeks kan worden opgestart of verschillende batches op elkaar kunnen worden afgestemd. Ook door de operator te ondersteunen met onder andere cobots en digitale werkinstructies, wordt het productieproces geoptimaliseerd. De demonstrator is volledig gebouwd in lijn met de principes van QRM (Quick Response Manufacturing), kaizen en lean manufacturing.
', N'https://www.sirris.be/nl/factory4-0', NULL, NULL, N'Images/20200109153134.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (43, N'Quality Management', N'Quality Management systems describe how production is organized to ensure high quality and safety. Next, this system needs to be implemented by your workforce. You probably provide paper lists and manage Excel sheets to keep track if everything is performed well. With GoRound you can cut the paperwork, make it fully digital and make checklists, tasks and quality chechs available on smartphone and tablet.', N'https://www.gemsotec.com/internal-food-control/', NULL, NULL, N'Images/20200122214836.png')
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (44, N'Operator rounds and plant inspections', N'In process industry you want your operators to keep their eyes and ears open for problems in the plant. To structure this you made numerous inspection lists that include all relevant points to be visually inspected. With GoRound you make this fully digital, which enables to follow up in real time issues reported in the field and to easily draw relevant KPIs. ', N'https://www.gemsotec.com/goround/', NULL, NULL, NULL)
INSERT [dbo].[Cases] ([casId], [casName], [casDescription], [casWebsite], [casContent], [casPublic], [casLogoUrl]) VALUES (45, N'Quality checks in Warehousing', N'Large warehousing companies, especially for high demanding purposes, routinely perform quality checks to comply with 5S or 6S auditing. Get insight in the status of your warehouse, anytime and anywhere, with the GoRound app.', N'http://www.gemsotec.com/goround', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Cases] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([catId], [catName], [catDescription]) VALUES (1056, N'Production', NULL)
INSERT [dbo].[Category] ([catId], [catName], [catDescription]) VALUES (1057, N'Maintenance', NULL)
INSERT [dbo].[Category] ([catId], [catName], [catDescription]) VALUES (1058, N'Quality', NULL)
INSERT [dbo].[Category] ([catId], [catName], [catDescription]) VALUES (1059, N'Inventory', NULL)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[ComMeCasSeAcLink] ON 

INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (29, 2, 1031, 10, 1173, 27)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (31, 4, 1031, 11, 1170, 28)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (34, 6, 1031, 13, 1185, 29)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (35, 6, 1031, 13, 1172, 30)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (36, 5, 1031, 14, 1175, 31)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (41, 7, 1031, 15, 1173, 32)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (47, 8, 1031, 24, 1173, 33)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (48, 9, 1031, 25, 1178, 34)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (50, 11, 1031, 26, 1172, 35)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (55, 13, 1031, 30, 1172, 36)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (56, 15, 1031, 31, 1179, 37)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (60, 16, 1031, 34, 1177, 38)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (61, 17, 1031, 35, 1177, 39)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (63, 21, 1032, 37, 1183, 41)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (64, 1, 1031, 38, NULL, NULL)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (65, 18, 1032, 36, 1185, 42)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (66, 12, 1031, 39, 1172, 43)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (67, 12, 1031, 39, 1173, 44)
INSERT [dbo].[ComMeCasSeAcLink] ([cmcsalId], [cmcsalCompany], [cmcsalActivity], [cmcsalMesSoftware], [cmcsalSector], [cmcsalCase]) VALUES (68, 12, 1031, 39, 1186, 45)
SET IDENTITY_INSERT [dbo].[ComMeCasSeAcLink] OFF
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (1, N'Universiteit Gent', N'Universteit Gent, campus Kortrijk.', N'https://www.ugent.be/campus-kortrijk/nl', N'Graaf Karel De Goedelaan 5, 8500 Kortrijk', N'isye@ugent.be', N'+3256241221', N'{"time":1579016528361,"blocks":[{"type":"header","data":{"text":"Dit gedeelte is volledig naar wens in te vullen","level":2}},{"type":"paragraph","data":{"text":"Om inhoud aan de pagina toe te voegen of aan te passen moet de <b><i>editor mode </i></b>geactiveerd worden.<br>Dit kan via het edit icoon rechtsboven de titel."}},{"type":"paragraph","data":{"text":"Vervolgens kan je verschillende types inhoud toevoegen."}},{"type":"paragraph","data":{"text":"Begin eenvoudigweg te typen om gewone tekst te in te voegen. <br>Door tekst te selecteren verkrijg je een popup waarin je kan kiezen voor vet gedrukt, cursief of gemarkeerde tekst. <a href=\"https://www.isye.be/project/mes4sme/\">Er kan ook een link toegevoegd worden aan de tekst</a>."}},{"type":"paragraph","data":{"text":"Druk op enter om een nieuwe blok aan te maken, via het plus teken kan een menu geopend worden om andere types inhoud toe te voegen:"}},{"type":"image","data":{"file":{"url":"Images/20200109164844.png","extension":{},"name":{},"Fileurl":"C:\\inetpub\\wwwroot\\FileChecking\\20200109164844.png"},"caption":"Verschillende types inhoud toevoegen, dit is trouwens een onderschrift van bovenstaande figuur.","withBorder":false,"stretched":false,"withBackground":false}},{"type":"list","data":{"style":"ordered","items":["Je kan titels toevoegen&nbsp;","Lijsten","Figuren (enkel de gekende formaten: png, jpg en jpeg worden toegestaan!)","Bijlages (.pdf!)","Links naar andere pagina''s","Youtube of Vimeo links kunnen eenvoudigweg in een nieuwe blok geplakt worden."]}},{"type":"embed","data":{"service":"youtube","source":"https://www.youtube.com/watch?v=ItYz8lLvZNY","embed":"https://www.youtube.com/embed/ItYz8lLvZNY","width":580,"height":320,"caption":"Met opnieuw een onderschrift"}},{"type":"paragraph","data":{"text":"Ook op de help pagina (?) kan je de handleiding terugvinden, hieronder als bijlage toegevoegd:"}},{"type":"attaches","data":{"file":{"url":"Files/MES4SME_OwnerGuide_V1.pdf","name":"MES4SME_OwnerGuide_V1","extension":"MES4SME_OwnerGuide_V1"},"title":"MES4SME_OwnerGuide_V1"}},{"type":"paragraph","data":{"text":"Ook een weblink kan als blok opgenomen worden binnen de inhoud."}},{"type":"linkTool","data":{"link":"https://www.ugent.be/campus-kortrijk/nl","meta":{"description":"De UGent Campus Kortrijk biedt academische bachelor- en masteropleidingen industrieel ingenieur aan in de industriële en bio-industriële wetenschappen.","image":{}}}},{"type":"header","data":{"text":"Tot hier gaat de geconfigureerde content","level":2}},{"type":"paragraph","data":{"text":"Bewaar wel af en toe jullie voortgang en zeker voor het verlaten van de editor mode. "}},{"type":"paragraph","data":{"text":"Let wel op! Bij het aanpassen van de content, is deze pagina niet meer publiek toegankelijk, tot de administrators hun goedkeuring hebben gegeven.&nbsp;"}},{"type":"paragraph","data":{"text":"Succes!&nbsp;"}}],"version":"2.15.1"}', 1, N'Images\20191218120813.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (2, N'Agilitec', N'Agilitec staat voor Agile Information Technology. Onze standaard software oplossingen voor MES/OEE, Data Management, WMS en CRM hebben als overeenkomst dat ze zich razendsnel laten aanpassen aan elke omgeving, eis of situatie zonder dat er maatwerk aan te pas komt. Onze software specialisten spreken uw taal en blinken uit in het vertalen van uw verbetervoorstellen naar handige oplossingen die meteen terugvloeien in het standaard product. Hierdoor zijn we in staat maatwerk te bieden voor de prijs van standaard kortom Agile ondernemen. Lenig, nietwaar?', N'https://www.agilitec.eu/', N'Minervum 7210, 4817 ZJ Breda, Nederland', N'info@agilitec.nl', N'+31765156572', N'{"time":1579010792046,"blocks":[],"version":"2.15.1"}', 0, N'Images/20191218150923.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (3, N'ATS', N'ATS Groep is een multidisciplinaire technologiegroep die zich specialiseert in het creëren van een duurzame, innovatieve en efficiënte productie- en werkomgeving door het realiseren van totaalprojecten in elektro, mechanica en distributie voor een waaier aan sectoren.', N'https://www.atsgroep.be/nl/automatisering', N'Karel De Roosestraat 15, 9820  Merelbeke', N'info@atsgroep.be', N'+3292100411', N'{"time":1579034601143,"blocks":[],"version":"2.15.1"}', 0, N'Images\20191218150913.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (4, N'Azumuta', N'Manage your factory with Azumuta
Manufacturing software for more productive teams.

All your work instructions, audits, improvement items and competency management in one workspace.', N'https://www.azumuta.com/', N'Technologypark 122, 9052 Ghent, Belgium', N'contact@azumuta.com', N'+3292771844', NULL, NULL, N'Images\20191218151045.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (5, N'CAT-Solutions', N'Wij hebben een jarenlange ervaring op het vlak van implementatie van oerdegelijke ERP-software in de KMO-markt in België. We weten precies met welke problemen u en uw KMO vandaag worstelt, of u nu produceert of handelt. Ons sterk team en diverse partnerships maken het ons mogelijk om snel te schakelen. Onze mensen zijn getraind in het begeleiden van de klanten bij het nemen van duurzame beslissingen en de opvolging ervan. ', N'https://catsolutions.be/nl/', N'Beneluxlaan 1A, 8500 Kortrijk', N'sales@catsolutions.be', N'+3256960200', NULL, NULL, N'Images\20191218151159.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (6, N'Catael', N'De focus van onze activiteiten ligt op industriële software applicaties: PLC gebaseerde oplossingen die klassieke standaard besturingen overstijgen. Ook de interface tussen PLC sturingen en MES/ERP applicaties behoort tot de focus van CATAEL.', N'https://www.catael.be/', N'Doornikserijksweg 149, 8510 Bellegem-Kortrijk', N' info@catael.be', N'+32474979377', N'{"time":1578561315618,"blocks":[],"version":"2.15.1"}', 0, N'Images\20191218151454.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (7, N'CloudAutomation', N'Maak uw bedrijf klaar
voor industrie 4.0 met actware.

Verbeter productkwaliteit en maximaliseer de productiviteit door je eigen smart factory te creëren', N'https://www.actware.be/', N'Deerlijkseweg 55, B-8790 Waregem', N'info@actware.be', N'+3255890420', NULL, NULL, N'Images\20191220150114.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (8, N'Contec', N'Contec is actief in een breed scala van sectoren, waaronder de (petro) chemie, farmaceutische producten en de voedingssector. Bovendien beperkt Contec zijn activiteiten niet tot België. Met een heel aantal succesvolle internationale projecten in onze portefeuille zijn we in verschillende landen verspreid over de hele wereld.', N'https://contec.be/nl/home-page/', N'Spinnerijstraat99/17, B-8500 Kortrijk', N'info@contec.be', N'+3256226218', NULL, NULL, N'Images\20191220150121.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (9, N'Damatec', N'Damatec helps businesses to become a better data driven company.  Data from various business applications, machines and the cloud is our source to give you better insights, to control your organization and to automate business processes.', N'https://www.damatec.be/', N'Casinoplein 5, B-8500 Kortrijk', N'info@damatec.be', N'+32492946299', NULL, NULL, N'Images\20191220150128.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (10, N'Dynamics', N'Wij verzorgen alle
automatiserings-aspecten van uw productieproces. Zowel elektrisch als softwarematig.', N'https://www.dynamics.be', N'Krommebeekstraat 48,  B-8930 Menen', N'info@dynamics.be', N'+3256428100', NULL, NULL, N'Images\20191220150154.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (11, N'Factry', N'Operational Intelligence made easy.
Refreshing data insights for radical process improvement', N'https://www.factry.io/', N'Oktrooiplein 1, B-9000 Gent', N'info@factry.io', N'+329399095', NULL, NULL, N'Images\20191220150200.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (12, N'Gemsotec', N'Smart and mobile operations with the GoRound app
Perform inspections and work instructions from your fingertips, anywhere and anytime!', N'https://www.gemsotec.com/', N'Professor Roger Van Overstraetenplein 1, B-3000 Leuven', N'info@gemsotec.com', N'+32497402819', NULL, NULL, N'Images\20191220150206.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (13, N'Ikologik', N'Ikologik is een betrouwbare partner voor procesmonitoring in de industrie. Via software voor visualisatie, rapportering en slimme analyse van verschillende procesdata, brengen wij de prestaties van uw productie-installaties voortdurend in beeld.', N'https://www.ikologik.com/', N'Sint-Amanduslaan 38, B-8730 Beernem', N'info@ikologik.be', N'+3250731330', NULL, NULL, N'Images\20191220150212.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (14, N'DD Engineering', N'Ons digitale engineeringteam verbetert de efficiëntie en doorvoer van uw installaties en processen via slimme, digitale oplossingen.

Als onafhankelijke expert hebben we expertise van meerdere hardware- en software-oplossingen van topniveau.
Hierdoor zijn we in staat om u steeds de beste digitale oplossing voor uw specifieke installatie of bedrijf aan te bieden en u een volledige connectiviteit tussen alle systemen te garanderen.', N'https://www.ddeng.be/nl/digital', N'Ottergemsesteenweg 703, 9000 Gent', N'digital@ddeng.be', N'+3257421521', NULL, NULL, N'Images/20200115134100.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (15, N'One-Two', N'Weet wat er gebeurt in uw bedrijf!

De eenvoudigste registratie-oplossing voor: tijdregistratie, werkuren per project en per activiteit, status van een opdracht, locatie van goederen, materiaalverbruik, productie-opvolging, ...', N'https://www.one-two.com', N'Sparrestraat 1, B-8890 Moorslede', N'info@one-two.com', N'051721400', NULL, NULL, N'Images\20191220150222.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (16, N'Proceedix', N'Your procedures, work instructions and inspections finally made paperless and mobile', N'https://proceedix.com/', N'Kortrijksesteenweg 1142, B-9051 Ghent', N'info@proceedix.com', N'+3293959309', N'{"time":1578472788440,"blocks":[{"type":"paragraph","data":{"text":"fgdgssgg"}},{"type":"list","data":{"style":"ordered","items":["rrte","g","fdgfdg"]}}],"version":"2.15.1"}', 0, N'Images\20191220150230.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (17, N'Propos', N'PROPOS software vindt zijn oorsprong in de praktijk. Het is ontwikkeld bij BOSCH Scharnieren en Metaal, een Nederlands productiebedrijf dat wereldwijd voorop loopt op het gebied van procesverbeteren met Quick Response Manufacturing (QRM) en Lean Manufacturing. Ter ondersteuning van deze succesvolle QRM en Lean implementatie is PROPOS software ontwikkeld.

', N'https://www.propos-software.nl/', N'Voltastraat 84, N-7006 RW Doetinchem', N'info@propos-software.nl', N'+31850479865', NULL, NULL, N'Images\20191220150235.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (18, N'Savaco', N'Savaco is een professionele IT-dienstverlener gespecialiseerd in oplossingen met impact. Als technologieverspreider gaan wij steeds voor kwaliteitsvolle IT-oplossingen die bedrijven en organisaties helpen om hun digitale ambities te realiseren.', N'https://www.savaco.com', N'Beneluxpark 19, B-8500 Kortrijk', N'info@savaco.com', N'+3256260361', NULL, NULL, N'Images\20191220150247.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (19, N'Solvice', N'Every business operation has its planning puzzles.
Solvice solves every planning puzzle and optimizes every operation for better performance and higher profitability.
On a single powerful software platform.', N'https://www.solvice.io/', N'Vlasgaardstraat 52, B-9000 Gent', N'info@solvice.io', N'+32474817877', NULL, NULL, N'Images\20191220150257.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (20, N'Techwin', N'TECHWIN, uw partner in software voor de automatisatie van uw schrijnwerkerij.', N'https://www.techwin.be', N'Brusselsesteenweg 267, B-2800 Mechelen', N'info@techwin.be', N'+3215446464', NULL, NULL, N'Images\20191220150304.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (21, N'Veltion', N'Levering te laat? Stukken weggooien? Altijd zoeken in het magazijn? Instabiele planning?

Wij zorgen ervoor dat dit niet meer gebeurt. Onze coaches begeleiden je met de logistiek, de administratie en de productie- of bouwprocessen van je bedrijf. ', N'https://www.veltion.be/', N'Adelaarsstraat 11/001,0 B-9051 Sint-Denijs-Westrem', N'info@veltion.be', N'+32472359516', NULL, NULL, N'Images\20191220150314.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (22, N'IBA', N'It is our mission to bring transparency to the world of industrial production, power generation and energy distribution plants. By means of an iba system, the user can understand and master the growing technological complexity of automated processes and mechatronic systems.', N'https://www.iba-ag.com/en/start/', N'Kerkstraat 108, 9050 Gentbrugge', N'support@iba-benelux.com', N'+3292262304', NULL, NULL, N'Images/20200108152442.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (23, N'Scalefactory', N'With Scalefactory you are not only a client, a customer or someone we just do business with. You are one part of an enterprising partnership. We believe in the best of both worlds. Helping your business grow by leveraging your Salesforce project, while we continuously improve our services to all our clients. Past, present and future!', N'https://scalefactory.eu/', N'Voorhavenlaan 31/008, 9000 Gent', N'contact@scalefactory.eu', N'+32486779494', NULL, NULL, N'Images/20200109100204.png')
INSERT [dbo].[Company] ([comId], [comName], [comDescription], [comWebsite], [comAdress], [comContactEmail], [comContactTelefoon], [comContent], [comPublic], [comLogoUrl]) VALUES (24, N'Logflow ', N'Wij creëren uw efficiënte logistieke flow door het: 
verlagen van de operationele kosten; verhogen van uw klantenservice; herdenken van uw processen; verhogen van uw flexibiliteit; verhogen van de graad van automatisatie; bepalen van de optimale opslagtechnieken; creëren van de perfecte tracering', N'https://www.logflow.be/nl/home', N'Heidelbergstraat 18A/003, B-8210 Loppem', N'info@logflow.be', N'3250671420', NULL, NULL, N'Images/20200121090924.png')
SET IDENTITY_INSERT [dbo].[Company] OFF
SET IDENTITY_INSERT [dbo].[Contacts] ON 

INSERT [dbo].[Contacts] ([contactId], [contactEmail], [contactVerdeling]) VALUES (1, N'mes4sme@ugent.be', N'To')
SET IDENTITY_INSERT [dbo].[Contacts] OFF
SET IDENTITY_INSERT [dbo].[Functions] ON 

INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1102, N'Resource management', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1103, N'Definition management', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1106, N'Detailed scheduling', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1107, N'Dispatching', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1108, N'Execution management', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1109, N'Data collection', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1110, N'Analysis', NULL)
INSERT [dbo].[Functions] ([fuId], [fuName], [fuDescription]) VALUES (1111, N'Tracking', NULL)
SET IDENTITY_INSERT [dbo].[Functions] OFF
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928377', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928377', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928378', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928379', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928398', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928426', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928427', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928434', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928436', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928441', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928442', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928460', 6, NULL, 29)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928461', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928463', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928465', 6, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928466', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928466', 6, NULL, 29)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928467', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928469', 6, NULL, 29)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928470', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928470', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928471', 6, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928472', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928472', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928473', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928474', 4, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928476', 4, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928476', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928477', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928478', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928479', 2, NULL, 16)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928480', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928481', 2, NULL, 16)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928481', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928482', 4, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928483', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928484', 6, NULL, 35)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578928485', 2, NULL, 11)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014748', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014755', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014756', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014757', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014770', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014772', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014777', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014778', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014784', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014785', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014787', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014789', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014789', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014798', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014799', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014801', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014813', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014818', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579014822', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016450', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016456', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5IEOZH62VXTXU3CWCTHTCD7CUCTQ16M58AOBEOYTX5NS6F4JXWA476M1LC3XYQBZLXZTPASY4Y6GZOFPHK2F5R8BXW0BUB5G4YHIQL4XELAJAGC66NPNY633ZX52KQD230MBG796PFAURZBRU3EGWRR84NS3PCMYIHPJ4BZ9OS8T5JMAVNHQL7GMING3OGQN2MMJJ2XS', N'1579016458', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016461', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5IEOZH62VXTXU3CWCTHTCD7CUCTQ16M58AOBEOYTX5NS6F4JXWA476M1LC3XYQBZLXZTPASY4Y6GZOFPHK2F5R8BXW0BUB5G4YHIQL4XELAJAGC66NPNY633ZX52KQD230MBG796PFAURZBRU3EGWRR84NS3PCMYIHPJ4BZ9OS8T5JMAVNHQL7GMING3OGQN2MMJJ2XS', N'1579016461', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016468', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016470', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016486', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016490', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579016498', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'EF1DT8DVBSHC46MLBDBQ56UICU0S7C03ZZMFFC3KKV168OS69AIF0HXHVTB8HUY81OMFTHF8K1GKFXKYC4T64UCSBFUVB4C062ETBNM25J59BNC7IN6MA1EGM4EOXU7PYPNMT1WYR1BUZDRXCWNX8ZZFY7HGJ8FKMPKVAZOR0H4J0TRINQLAGJQ8ZIYVTPPTV0IJ25XN', N'1579016906', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'EF1DT8DVBSHC46MLBDBQ56UICU0S7C03ZZMFFC3KKV168OS69AIF0HXHVTB8HUY81OMFTHF8K1GKFXKYC4T64UCSBFUVB4C062ETBNM25J59BNC7IN6MA1EGM4EOXU7PYPNMT1WYR1BUZDRXCWNX8ZZFY7HGJ8FKMPKVAZOR0H4J0TRINQLAGJQ8ZIYVTPPTV0IJ25XN', N'1579016916', 2, NULL, 15)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017167', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017171', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017210', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017214', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017239', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579017241', 2, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9QIE5946D92HQHAFFBUG34X877DUJFSNWYECH5VJVGK8A2HVFCOCIXKE3KH86JX7QOU1D87KQKVWTY7E0RPG1PF8N9I6XXMLTNSVHQSGEEW7CYVFMCHP4QFBJVNEZAW37JDFGL6K9ICX3I6ZNDTVP7QCHQMVCNRDR392UP9BG7PWOSIAZX0WRBNA3B8Z9KHV08FAJEF8', N'1578990619', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9QIE5946D92HQHAFFBUG34X877DUJFSNWYECH5VJVGK8A2HVFCOCIXKE3KH86JX7QOU1D87KQKVWTY7E0RPG1PF8N9I6XXMLTNSVHQSGEEW7CYVFMCHP4QFBJVNEZAW37JDFGL6K9ICX3I6ZNDTVP7QCHQMVCNRDR392UP9BG7PWOSIAZX0WRBNA3B8Z9KHV08FAJEF8', N'1578990624', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9QIE5946D92HQHAFFBUG34X877DUJFSNWYECH5VJVGK8A2HVFCOCIXKE3KH86JX7QOU1D87KQKVWTY7E0RPG1PF8N9I6XXMLTNSVHQSGEEW7CYVFMCHP4QFBJVNEZAW37JDFGL6K9ICX3I6ZNDTVP7QCHQMVCNRDR392UP9BG7PWOSIAZX0WRBNA3B8Z9KHV08FAJEF8', N'1578990628', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'E5PCQYU4V30UANQ91HOJ6TTH4XZ38YVZ3Z0KA63N8MFNXXNF2YCHZ6TULHI29157HA0KXG85NBKTOGTGJTAKVO97GF82YANHQHGW6CSVZH3YV9GFXBTQ0VM2B3ESIFGMXDDGA7KGBOVNVKMG82FWR7Q55NVD89NTBQAR69F01MPL99H0Q7TPAU5ZWHLWMHIBBBMEK4K9', N'1579019077', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'XP5FSF55G4WAQ8TQVK4YYSOUDOK7XDQVXU1VYZHRLQQRUHUDS5S9AVLFBLBE47HCNAN6G715DNNHB6953HP2VRVBJWUNYDWNINDT4Y6UDW5WIDDELFKXSPR6OLWTNOH9099Y2QBEK4Y8V93GTPBS28526HESCNGYP65IDPM5DGNX0TKPRQ98MYH2EG1ULSC6YATZDVCE', N'1579019394', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034337', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034344', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034357', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034368', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034370', 4, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034384', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'K7882EJJGQUL9PTQZMC9FR5J5HEVNFMTZLD1U6CZNEH06PSQKNIFVQD2CYDUAX6BDBVYM253JNVBKEMQZXKA7H7IHLZ52SYZHFX534ESRO6I4CR7MX5MN6E6DMQACAA0Y53FRQW4T2SSLYOF32TFXMMLA02QODQRNFU9IMP5AA2UNVVO8LHKQ5DMSSE0YFQQLBEFEO6Q', N'1579034386', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034633', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034668', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034772', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034784', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034788', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034794', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034801', 4, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034817', 4, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035664', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035708', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035716', 2, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035722', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035730', 2, NULL, 13)
GO
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035735', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035741', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035748', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035752', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035760', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012482', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012485', 4, NULL, 24)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012486', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012487', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012488', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012488', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012564', 6, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012565', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012565', 6, NULL, 29)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012566', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012567', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012568', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012569', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012570', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012571', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932101', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932102', 2, NULL, 3)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932103', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932103', 4, NULL, 24)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932104', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932104', 6, NULL, 27)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932105', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932106', 4, NULL, 25)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932106', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932107', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932108', 6, NULL, 27)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932108', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932109', 4, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932110', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932111', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932112', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4X76B8RGY6SEGVGKVBOM752PZ8OCYY6B6VI0U5AC3W870JCOGMLZ4RC85RGCUPR99QHUNQ0ZALSK75ZQ695LNHHOTJ5DY8SOE87JBRA0WVZS92BA20FUTGW2I2BEUXAB3DG97PPP8EY2RYP0I5K82757MFBIP6F9UXZ5WBCML0WOR4KTNK5ZMCI5IIX1OG9B8ZH49TEG', N'1578932112', 4, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932113', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932114', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932114', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932115', 4, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932115', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932116', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932116', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932117', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9QIE5946D92HQHAFFBUG34X877DUJFSNWYECH5VJVGK8A2HVFCOCIXKE3KH86JX7QOU1D87KQKVWTY7E0RPG1PF8N9I6XXMLTNSVHQSGEEW7CYVFMCHP4QFBJVNEZAW37JDFGL6K9ICX3I6ZNDTVP7QCHQMVCNRDR392UP9BG7PWOSIAZX0WRBNA3B8Z9KHV08FAJEF8', N'1578932117', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932119', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932120', 4, NULL, 11)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932121', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932122', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'X7NN8CMEMU08BEU87PYAYQWJUXDOX1J6O1IGM48GJW9LUS9GB95DSQLHKE3L97QUK4TJ6OVDRR1CQLZCX7NX5FN8PA5HBXHLFOAZ8ZJWO2WBZJ09P0XBU2ZFQV45TNB3JP7CL9E7X0JCTMIGK52K9G0MZ4IM5NDA032JMYYQW0N4SWVEMYVXV13CZUNQ01BKWJMZU94W', N'1578932122', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012572', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012573', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012573', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'WL172UUWF9JE9PDL8R75QSUZUQMGJLBLN1KAOGPNEC2QV5Y4GQZSXMRE0BNLINM4770HEC61CT0NZDEY1SA3WTYBKFKM3EF0Z8TXERZDT7QDVEOX7YT0NTHOJZ792T87OAX1UQTK26JYCUQF0WOEVPMG0EOPU0K98SZURXCRFI7RJTMZJ1IGF4M8YQBPCEZBVK0T56YD', N'1579012574', 2, NULL, 8)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014841', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014945', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014949', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014955', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014971', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'T79MS57H2YG2SE4EG05AVWPBT559224NDEWH5AQ3T4XN77TRPVNEAXS6R73WA7O943YOSXJFJCQ09CDVMNUHOZMQVZ07H43VQYSEIE82MI48G17P4HKWNGUFQI237L7PI4KKU8NHO14G4OJ41KCATHIJDMY9MF5BKOZMRX563CHVMOLC98BWI76BWAHHJN5J636PY7NE', N'1579014972', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579015601', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579015602', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579015907', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'PGGQ9W7ZBW9NEI2MSXZQPXWPH8ZTS5NCV7GC1PCADRFXORFYAT6BH7Y1T0JB7NHFI72GBZVDVYM7S8B03BS99M4YJPSAGAD0KAPQZ8WL73PTU0JVYTI64HG5CE7WHV6UVWWSTVA4OHBF7LDTG7LX2H5D3TOUB9DL6478D1CDSNRXGA80AOPJJJ2O0TY9GQ2OD0JCOFPP', N'1579015911', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YB1R0IE5W0WVID0T4LA4ZSTFZA6O6YY5NYXWO5D06BDIQLTH7GBXF28GSV20L087C0HTPDE9HJ7XOKYNQZQ0WBZVZQE2CNPGDFGZD913BCP64L1H6OJWJILAAW489M6441TIE9KR7WYTV31OZ75XD2654FYRE34ZGVCD1SC7E1ZWT1OH5QJBRJ0B4YO8K3MD29N91YWG', N'1579030378', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YB1R0IE5W0WVID0T4LA4ZSTFZA6O6YY5NYXWO5D06BDIQLTH7GBXF28GSV20L087C0HTPDE9HJ7XOKYNQZQ0WBZVZQE2CNPGDFGZD913BCP64L1H6OJWJILAAW489M6441TIE9KR7WYTV31OZ75XD2654FYRE34ZGVCD1SC7E1ZWT1OH5QJBRJ0B4YO8K3MD29N91YWG', N'1579030380', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YB1R0IE5W0WVID0T4LA4ZSTFZA6O6YY5NYXWO5D06BDIQLTH7GBXF28GSV20L087C0HTPDE9HJ7XOKYNQZQ0WBZVZQE2CNPGDFGZD913BCP64L1H6OJWJILAAW489M6441TIE9KR7WYTV31OZ75XD2654FYRE34ZGVCD1SC7E1ZWT1OH5QJBRJ0B4YO8K3MD29N91YWG', N'1579030383', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YB1R0IE5W0WVID0T4LA4ZSTFZA6O6YY5NYXWO5D06BDIQLTH7GBXF28GSV20L087C0HTPDE9HJ7XOKYNQZQ0WBZVZQE2CNPGDFGZD913BCP64L1H6OJWJILAAW489M6441TIE9KR7WYTV31OZ75XD2654FYRE34ZGVCD1SC7E1ZWT1OH5QJBRJ0B4YO8K3MD29N91YWG', N'1579030398', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YB1R0IE5W0WVID0T4LA4ZSTFZA6O6YY5NYXWO5D06BDIQLTH7GBXF28GSV20L087C0HTPDE9HJ7XOKYNQZQ0WBZVZQE2CNPGDFGZD913BCP64L1H6OJWJILAAW489M6441TIE9KR7WYTV31OZ75XD2654FYRE34ZGVCD1SC7E1ZWT1OH5QJBRJ0B4YO8K3MD29N91YWG', N'1579030432', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033856', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033864', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033877', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033893', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033900', 2, NULL, 9)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033928', 2, NULL, 9)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033958', 4, NULL, 25)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033970', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579033973', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034004', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034016', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034018', 4, NULL, 15)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034028', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034066', 6, NULL, 39)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034097', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034103', 2, NULL, 8)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034110', 4, NULL, 24)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034118', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034122', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034129', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034541', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034544', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034563', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034564', 2, NULL, 3)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'06LOFZPYQWUIZ2G4X18OL7ZKTU2YFNPXCSC6VW7IV0UE4Z2ZWFK4LXS2GB5U61SDH7FGZKIA3Z3UMVQ32DB9CULO9K0U1NM4IR77TWAM6JBV48PUVB23ZSAWV1GPTYJQN8M5YSJGDZC3T0TB4G90USFZQKL2PU3I9VH5WU5C1W0APGD5HEVNYT553XOOHR0S5438SXPJ', N'1579034606', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035763', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035769', 6, NULL, 27)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035774', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035781', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035789', 5, NULL, NULL)
GO
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035798', 6, NULL, 31)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035811', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035816', 2, NULL, 9)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'MYEWT17PFNSC6GASBIQ0IYG5UMILQPJTO0710Q31VZVKXNTFBWE68GIXSJCGSV8ZE8YFIJ2VOZETD5IH2DK4LNVIRKIROWIA15GYZN713SRJBYUWA4NPKDO0MPY8ALG2QE2HZTSOFTA25MH20WQTJ2ZXQPMJZ91SMZ266M7RM8J1CW4AI5HO4WSYOE0JHIZG1SPGTK6N', N'1579035821', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'KWHU1W55C5DZTIZ5IH9MY0MWQ6JRCUI736FIARU6VRK4Z7KH5SYWSMTTJUE5MN2SWG6WDR40XD1GPR6SVCWT6VPQNV8PNZTHZ2HTBYENX78OUME2YVJK1IWA0PJRTON8TTQXKNQ77I58XA5ARF11PJPRFG2WPX4JUA4B6RRLFMMSMYN0TI9NXVTJE9RDU4RKOWOWF4EA', N'1579068948', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'KWHU1W55C5DZTIZ5IH9MY0MWQ6JRCUI736FIARU6VRK4Z7KH5SYWSMTTJUE5MN2SWG6WDR40XD1GPR6SVCWT6VPQNV8PNZTHZ2HTBYENX78OUME2YVJK1IWA0PJRTON8TTQXKNQ77I58XA5ARF11PJPRFG2WPX4JUA4B6RRLFMMSMYN0TI9NXVTJE9RDU4RKOWOWF4EA', N'1579068960', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579075120', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579075125', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579075206', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579075208', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579076178', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579076189', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8C3LXI4P9ZJG9ERATZT5CM32URWVD2GPBUDIIXGJFDC7I229OTWAVAMVJA1GWJMGS3FOBI15TKZ8K6U2YXGUGG4AIR9I69X605OOWN8G10QLKNLNP8R8C29M9EGTIT8TX3HORGFLZA2PZTZ4GW27RY385BORGO7I17YRF65J2GJC9UXQPMNV0AC89JTP1YEKDUY14IYJ', N'1579076984', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'0SFKP18LO8C4BLR05ZR6NQSEC88P65VHXD4H51SIKHA3VJJ1C76F026KAAIR2TPSM5G7QK5EGIVFPZS2YHM88QQ5GUH4SBQ12SQ0CMK2B28RG14KSQ2UGQT6OL06DAVF24N7J1ZM7T7FS8ZPZ6MCJ5UL8UM1VOZBH3ZMN7B00QXXTNOPWLHGXT56JC8YP80T458YJMB1', N'1579077547', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4WM2I72UG6FKGSOY72CL9NL10E2XPZGJJPMBGDYMJOSS2PMLRK1QK02RLUCDEEFR06B1FMR3E0BU428K2QGE709TX50WAVHFJB26OUGE37LTYHWSV1KY7ATA6SEKW42TT5L3AC6HKBLAOP4PJHBJKTIIS295Q5R8E2A253ST5DN5YTWRXYBP2TIAXDKICA0OUM5VPBTP', N'1579082556', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4WM2I72UG6FKGSOY72CL9NL10E2XPZGJJPMBGDYMJOSS2PMLRK1QK02RLUCDEEFR06B1FMR3E0BU428K2QGE709TX50WAVHFJB26OUGE37LTYHWSV1KY7ATA6SEKW42TT5L3AC6HKBLAOP4PJHBJKTIIS295Q5R8E2A253ST5DN5YTWRXYBP2TIAXDKICA0OUM5VPBTP', N'1579082558', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579082940', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579082944', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579082964', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579082965', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083230', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083238', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083240', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083275', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083276', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083277', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ZSNGFMT9GSI24CFUU9F2SJQ5M8LLXABVEVZKGB4KTLCT18URHKBP616PBR4HBILFRWXJN5NSF4JHH7DON4EM9VYNYKIZHWJK97BFKLEL7SJCRZHDNP3HD9F57KUCXYKTU8HK58EC03PRLW58CEM23CHA1FIA0BYIH0STS7HPL0RKUYTLOVASMS0S1HSZZELYEGJVEMQF', N'1579083278', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083384', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083388', 2, NULL, 18)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083415', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083417', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083420', 6, NULL, 42)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'66QCV7QDW7VFAG6ZA92LOSZHP2FNP8HCF5VPBY740PD2SGY8KUUETE1OHJW21CYXZT9FUQXMC8U8O6YIEGD4OCSBDGH2BECX1QD9HSO36IBVT2ALUOYV93JWA7BNI4L856H73ZBV1J8FB6RLG6HWPQY7HQ8JQAUC6DZWVCS5ND3Z5DVEQ0HMWYSOIJ0M01E1Z5I7G40D', N'1579083429', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'OMJMX8CD44303IPIVLOLMFEV8CA94Y7VAE7KOF1JHXS68O8RQAL5QO1JIYOV8DJF6H0YLF4PTEEZWHXUEST4MBEVNXB01GKDNM90XJGLG6MMJBWAB4RPUO24DELXDDYN3ALJI664MHYQZ2LLABZ6Z5T9TEXT7SHCH8SJ0M2E8WDM0MEK2AWCO1X8QM0DJM639VJ1KCUX', N'1579084091', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'OMJMX8CD44303IPIVLOLMFEV8CA94Y7VAE7KOF1JHXS68O8RQAL5QO1JIYOV8DJF6H0YLF4PTEEZWHXUEST4MBEVNXB01GKDNM90XJGLG6MMJBWAB4RPUO24DELXDDYN3ALJI664MHYQZ2LLABZ6Z5T9TEXT7SHCH8SJ0M2E8WDM0MEK2AWCO1X8QM0DJM639VJ1KCUX', N'1579084096', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084096', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084102', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084106', 4, NULL, 24)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084121', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084123', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084124', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084128', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JU8C2HIH9M076FXQKT2OWY5VZJMFDNAZP8332VGIDSE6V97472Q0KGAUZMNS5DEVLAR8M3XZNTKEU8AZLRHK7KPJ7IEQHXNANSVUR8RDF0N056A0MXGN54YBVHBDZGSZGK0BNPHNADEH7ZOAINDTQAET4U9K7NO8IXUVF8N6P3N2ZQE53A5LD8IGFHH3Q7PWYF9JE25J', N'1579084129', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084645', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084647', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084652', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084656', 2, NULL, 18)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084674', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'AGEXWM3NQDQ7SD5JPOQ80UQSO1DUR40AEVK64VT2N3MMRRDVKZ30M7SPZWJ5JJ6VMP2628P34XU7HEA6XSJ1AAYHC5GIMYTE719BZH2LK95YMVHVVMH19EUM1V372HRGEQ9QE8HQZ8RAE95RXUW9R1E3HWKDN5SDSZU6W1QWF0CBWTI20MJHUSZMMGISYDD3POSOO845', N'1579084676', 2, NULL, 8)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DA30ZCGXOSNAWLSAAHTPT88CXIVHB4N1RNDUY9XJ9VDFZCHE5SOHOPMFB0CRR8WTEWSIH8LBUKQ742NVARLSQGMCB8S76F2A8UACT967GMWE9OKINRQ9XED8KQHJQOEOSP9K1KBBUOLDTIDWYJTPZ2N4QC2D1XR2IHVTP9RCQOOI2APCMUVG4YZ0BE4ZS7J77T4LWUGY', N'1579084774', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DA30ZCGXOSNAWLSAAHTPT88CXIVHB4N1RNDUY9XJ9VDFZCHE5SOHOPMFB0CRR8WTEWSIH8LBUKQ742NVARLSQGMCB8S76F2A8UACT967GMWE9OKINRQ9XED8KQHJQOEOSP9K1KBBUOLDTIDWYJTPZ2N4QC2D1XR2IHVTP9RCQOOI2APCMUVG4YZ0BE4ZS7J77T4LWUGY', N'1579084967', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DA30ZCGXOSNAWLSAAHTPT88CXIVHB4N1RNDUY9XJ9VDFZCHE5SOHOPMFB0CRR8WTEWSIH8LBUKQ742NVARLSQGMCB8S76F2A8UACT967GMWE9OKINRQ9XED8KQHJQOEOSP9K1KBBUOLDTIDWYJTPZ2N4QC2D1XR2IHVTP9RCQOOI2APCMUVG4YZ0BE4ZS7J77T4LWUGY', N'1579084971', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086576', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086578', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086583', 2, NULL, 17)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086587', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086588', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'2R9KNFCQW01HCK8TS5RKF1USERLIFDP2O6ONZPM6COC1GC5XVL31C6TB6QF643KTK83F6U4DTUI1Y2MCWXX3KY0SNNQIJPCIDTQHJZ1AR0HRX2MEEDKGFQIWIK2SEV3DS9XFLE266XMTQI4VAMDGF6CWK4987I556E9YEH2AKNJSDYF9QGS2RX7V6A4RE49Y1RXLJGR3', N'1579086590', 2, NULL, 8)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091045', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091055', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091059', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091063', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091065', 4, NULL, 11)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091071', 6, NULL, 28)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091074', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091074', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091076', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091077', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091078', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091079', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B6QTHNOD809QZ0LLSEESSF1XNXCOXBYUL3COJD0HMVC2ZEV47TQS2085E3GTLAQ6BOE5X7COO6G7D98JCHREVG2V5GJQ7G6LFF0K9ZGARCD5TB2F57QDTML4VTARXVQHA527JXJNBQWSC6B3OEV9Z32345ZQYZM5V65YJ46HYDBCZ22YRHDSARRHC8CN82YME07GBMTT', N'1579090736', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B6QTHNOD809QZ0LLSEESSF1XNXCOXBYUL3COJD0HMVC2ZEV47TQS2085E3GTLAQ6BOE5X7COO6G7D98JCHREVG2V5GJQ7G6LFF0K9ZGARCD5TB2F57QDTML4VTARXVQHA527JXJNBQWSC6B3OEV9Z32345ZQYZM5V65YJ46HYDBCZ22YRHDSARRHC8CN82YME07GBMTT', N'1579090741', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B6QTHNOD809QZ0LLSEESSF1XNXCOXBYUL3COJD0HMVC2ZEV47TQS2085E3GTLAQ6BOE5X7COO6G7D98JCHREVG2V5GJQ7G6LFF0K9ZGARCD5TB2F57QDTML4VTARXVQHA527JXJNBQWSC6B3OEV9Z32345ZQYZM5V65YJ46HYDBCZ22YRHDSARRHC8CN82YME07GBMTT', N'1579090742', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339896', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339900', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339902', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339914', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339927', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'L57KVZ09KGF0K5OTVONVGI05IDFCT4EAS2RN6BNI6W6ZS0AQACQFRLLCEBBSUYQQCFISNAXUAEZTMAMRDYCQN9SGQPJ494YIGPXNSSC9P6ENW1ZDYTP5W9DCWCB812UFPQN6N99UVCBQ4FUYZFGPP3WGMU0QQEIWM97GO30YU4JT8WIRSNIVWE0W3GU2JSER4OD5IOJ6', N'1579339976', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506037', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091082', 4, NULL, 11)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'F4JR5FFXCGQ4TND18192OL903DOHCTQTOO3OG0THBYGWVP1YZR54L4935SX108US07PETZUHH20ZFMFOMZRUYVHUP9SYC2QDEX5YAWFA7XWTDJYSNJPBQ94EZ5YUG6FPMXVRQQOZUQXB9YQGKAK3NPWE319E7U3ASHHQYGB65ZQ9L3FAOSJGVLC7AQYZRJ19XWS2RARE', N'1579180204', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'40TB2RJAYBGLJP0Q2VVMARD0XHEZC3NOKLJGOBQXXPELTJBYYH63LJ8NW2NVNT84X2A8GUB1E8YVM2HNFPA57UCBM5TWX5KDQJX1ICSDFFYMUNB4VAP7TD7DZ9WKBPEJYUECEPED77Z160Q84YAXPMUNMUBJPHB0BOYAJ8ZF5L9WOFG9TYQNOR3L910UN5Y0590RCQZ6', N'1579371017', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'40TB2RJAYBGLJP0Q2VVMARD0XHEZC3NOKLJGOBQXXPELTJBYYH63LJ8NW2NVNT84X2A8GUB1E8YVM2HNFPA57UCBM5TWX5KDQJX1ICSDFFYMUNB4VAP7TD7DZ9WKBPEJYUECEPED77Z160Q84YAXPMUNMUBJPHB0BOYAJ8ZF5L9WOFG9TYQNOR3L910UN5Y0590RCQZ6', N'1579371043', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'40TB2RJAYBGLJP0Q2VVMARD0XHEZC3NOKLJGOBQXXPELTJBYYH63LJ8NW2NVNT84X2A8GUB1E8YVM2HNFPA57UCBM5TWX5KDQJX1ICSDFFYMUNB4VAP7TD7DZ9WKBPEJYUECEPED77Z160Q84YAXPMUNMUBJPHB0BOYAJ8ZF5L9WOFG9TYQNOR3L910UN5Y0590RCQZ6', N'1579371049', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506298', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506306', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506311', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506315', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DEK0FR5A5B221ABO5TUN6K1GMQPKU1INX1Z5D7PA77O0PDKDSBI1E882NEOYCVQE3PEALGK7DTP86A1IGM1SSQJY26R8S17MQJ798K1M0KYUTA6MNGS038VMGPHPYMOLWDRWD1J5UP1H1ZF29EVMJHSSXU3TDMXO6N7PSKJV6WFNGNVQJOPT1BBPV98T802I2NLO10K', N'1579506320', 2, NULL, 18)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'S14276IGF5R4736BEOOQ87R0G992LPO70DRTOKAJI5ZQVWTHIDKSQQNVJDV76Q5ZQTD3MMW3OFICIBFUQ2UIF5RK05KKYCM2999CSZ55QQEKIKNI8BQE5XTUB3DQCMFL2QCIBCB06Z1Y1GCQSBMXVH6MQOAYWO2EL3R1HEG590HF55351X8B9RZVT02WCNDS6XN8YKIN', N'1579598791', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598826', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'S14276IGF5R4736BEOOQ87R0G992LPO70DRTOKAJI5ZQVWTHIDKSQQNVJDV76Q5ZQTD3MMW3OFICIBFUQ2UIF5RK05KKYCM2999CSZ55QQEKIKNI8BQE5XTUB3DQCMFL2QCIBCB06Z1Y1GCQSBMXVH6MQOAYWO2EL3R1HEG590HF55351X8B9RZVT02WCNDS6XN8YKIN', N'1579598948', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598961', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598962', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091083', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091879', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091881', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091884', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091889', 2, NULL, 1)
GO
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091902', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091904', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R46LU268XP5J1PKOJ2LSEU2UCUULKSDW9S3QJ754MO774725EUQ7R4C6BMJAHUYELIJJW4EL7RGZX4ZE1ZH0CGFEQ6O7Y5W102WWXTJJPEA9YMJGWEISM8CFCF3ZPHXC4O94IDQIP8K7FGQDXIKTWEP2ZRTSZU3IR7EJNO8TY9COXF96YZBH7DP09GE0U8S5ECQIBG0I', N'1579091906', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092547', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092645', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092650', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092654', 4, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092661', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092664', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579093032', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091084', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091101', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091103', 4, NULL, 15)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091105', 4, NULL, 15)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091107', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091108', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091110', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091138', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091139', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091140', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091145', 6, NULL, 42)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091183', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091185', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091374', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091375', 4, NULL, 11)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091377', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598963', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598964', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'4RX590RUVF79P1F3D6HRMU2WWZJHR5GQOVGR43V2JHB7PEDFST41C1BKZBJKRJD08QV4VX9KLOIFT5GSWDZ8AONWU512UKK9EL89SIT4TUPXT51441YNUEZTC8KZ5GZBEIAH6H9NU4OQ8ZDYE783V7P9VP6U849N4OTL4T8X1L3Z0IUN5VV8I6PG3BWRLPOZ5BVUF23U', N'1579598977', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'S14276IGF5R4736BEOOQ87R0G992LPO70DRTOKAJI5ZQVWTHIDKSQQNVJDV76Q5ZQTD3MMW3OFICIBFUQ2UIF5RK05KKYCM2999CSZ55QQEKIKNI8BQE5XTUB3DQCMFL2QCIBCB06Z1Y1GCQSBMXVH6MQOAYWO2EL3R1HEG590HF55351X8B9RZVT02WCNDS6XN8YKIN', N'1579599567', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DMVARJP8AX9EQY6UV0PIECIIHUMEPJY72RESNITENM3ZZN9KX1JGA50BDM267LS18BM8UNGNGL51ITS4VCQY9S770CPAPXHC3C0YBA0JU59ZA93U38K4441QDPIR8G7TD38F242ULHDESH71J5GRMMH9LKUW2ILO8QVGAX19F0SX0W75SQID9QG1V93SJG1D5ZFBW00', N'1579623498', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DMVARJP8AX9EQY6UV0PIECIIHUMEPJY72RESNITENM3ZZN9KX1JGA50BDM267LS18BM8UNGNGL51ITS4VCQY9S770CPAPXHC3C0YBA0JU59ZA93U38K4441QDPIR8G7TD38F242ULHDESH71J5GRMMH9LKUW2ILO8QVGAX19F0SX0W75SQID9QG1V93SJG1D5ZFBW00', N'1579623500', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'5DMVARJP8AX9EQY6UV0PIECIIHUMEPJY72RESNITENM3ZZN9KX1JGA50BDM267LS18BM8UNGNGL51ITS4VCQY9S770CPAPXHC3C0YBA0JU59ZA93U38K4441QDPIR8G7TD38F242ULHDESH71J5GRMMH9LKUW2ILO8QVGAX19F0SX0W75SQID9QG1V93SJG1D5ZFBW00', N'1579623501', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708668', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708670', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708678', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708686', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708687', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708690', 2, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708696', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579091378', 6, NULL, 35)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I6SQ9XXIIQMCS4DT2ON3V72FUGE43XGSHC7VUIMYU42U81QBIDWIBNLLDN53S2OUJ45P6R8TCZJ0FIG20DW0A8R31WSGNTF1QHKE6AE0ECFLEUF5GDCPFYTLMBYK92GTURK77C6086W72GZHQW9JQZW2Y915L41M6W0WJH7EGMRJBVTGMR4M26C45SJ8KBOEFJBFPD7I', N'1579091487', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I6SQ9XXIIQMCS4DT2ON3V72FUGE43XGSHC7VUIMYU42U81QBIDWIBNLLDN53S2OUJ45P6R8TCZJ0FIG20DW0A8R31WSGNTF1QHKE6AE0ECFLEUF5GDCPFYTLMBYK92GTURK77C6086W72GZHQW9JQZW2Y915L41M6W0WJH7EGMRJBVTGMR4M26C45SJ8KBOEFJBFPD7I', N'1579091488', 2, NULL, 3)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'Z4DDIM0CPUS4AOU9U501794F8NE2TN1ITVX738WT2HSZSUE0ZCVTMGKZA7F5I7SC3JKO1XFVJH90QEXNINDR2I6NK76630K4YQ6IJZ8C6AJFGUJ6J1W44BJVN6D30J2T6FB9IPETPBU180H0YAWBEQ9XGL4MWELXI0JBSJX4XDP2QSWXZMF0W0OQFO36U27KZ6TIQFIO', N'1579091540', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091636', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091637', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091641', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091651', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091658', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091668', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M2OAAHGYYGWJ9VBRL36DQ1J5GFCU9OGTD0N3MOP8HEINFXKE5B6XJE8VTS45EWZJACFIHHE7N76LS5ZS48Y7SSK912K3TU4VP8NCRC6X69KLIXCX3LAF711R2GQP7LO2DK6URXB2BBATI80EZWAHTYYXG6JLVR0VUSH81SFGKIE61II4PMZKKGIPWINVN0GULP8002NH', N'1579091672', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092004', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092055', 2, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092063', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092068', 2, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092090', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'D4EJ90FGPVQ3A2XMVH5VADYHMMIICF2CAG9R9LTUNKSSMD2YKM8ZJH29G67WR7EJ2O242GNJXKHZVF5JUU70HC23OALDO7CQGIZ8K85E9YC9ACOAAYC79OC0V9P24J50H0SMQ78WZ31SA33LVLCZ4FR1G9MLSCJZ3L1JUCTDZQHFXT01GEHTHF5CJ1GRUXEWTCZYCT1T', N'1579098985', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'D4EJ90FGPVQ3A2XMVH5VADYHMMIICF2CAG9R9LTUNKSSMD2YKM8ZJH29G67WR7EJ2O242GNJXKHZVF5JUU70HC23OALDO7CQGIZ8K85E9YC9ACOAAYC79OC0V9P24J50H0SMQ78WZ31SA33LVLCZ4FR1G9MLSCJZ3L1JUCTDZQHFXT01GEHTHF5CJ1GRUXEWTCZYCT1T', N'1579098988', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'IFDDF4O4O2MP2AJZG3RW235BBHICQMDRXIS75RO2QIWLF3K76BGH79LOJCB7WLOBZ51TRL4ZPZGTHU5I6LGPG5UY6EY3PQFE4VVKB0PVGQ2H32GYG3ALE954KDXNXBK8TBZW9UGF5UI0DM4E79WVP494Z1A8DB8FF3X2DUZVX4PU0F07ITQXOYSZB46HUHC4WCUKZCAM', N'1579102962', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'IFDDF4O4O2MP2AJZG3RW235BBHICQMDRXIS75RO2QIWLF3K76BGH79LOJCB7WLOBZ51TRL4ZPZGTHU5I6LGPG5UY6EY3PQFE4VVKB0PVGQ2H32GYG3ALE954KDXNXBK8TBZW9UGF5UI0DM4E79WVP494Z1A8DB8FF3X2DUZVX4PU0F07ITQXOYSZB46HUHC4WCUKZCAM', N'1579102974', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'IFDDF4O4O2MP2AJZG3RW235BBHICQMDRXIS75RO2QIWLF3K76BGH79LOJCB7WLOBZ51TRL4ZPZGTHU5I6LGPG5UY6EY3PQFE4VVKB0PVGQ2H32GYG3ALE954KDXNXBK8TBZW9UGF5UI0DM4E79WVP494Z1A8DB8FF3X2DUZVX4PU0F07ITQXOYSZB46HUHC4WCUKZCAM', N'1579102976', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'IFDDF4O4O2MP2AJZG3RW235BBHICQMDRXIS75RO2QIWLF3K76BGH79LOJCB7WLOBZ51TRL4ZPZGTHU5I6LGPG5UY6EY3PQFE4VVKB0PVGQ2H32GYG3ALE954KDXNXBK8TBZW9UGF5UI0DM4E79WVP494Z1A8DB8FF3X2DUZVX4PU0F07ITQXOYSZB46HUHC4WCUKZCAM', N'1579102978', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103946', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103949', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103954', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103956', 2, NULL, 9)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103962', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103965', 2, NULL, 18)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103973', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103976', 4, NULL, 30)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6A997NXTHT7DJRE24HBPZHDDDXJQYVPDGK2MZ48432AQLIE1G6YNQM9Y655KDH5VD468Y2CAJHW7HX15SSK16CW4KDP38X0V7OMRLGCN4XEI0BRIDA1V7DZBHBYUMIBVRAA2LKUMQ67I8OEIVG086FY3ZLJJRRAVWS1ZBZ73PPFAGQD37P9GSDZMLHDAAK485LWPXOH6', N'1579092097', 2, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708912', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579708914', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709421', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709425', 2, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709486', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709491', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709494', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709640', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6UXS1ZD126N5BAW1QFH5424AI2NT2U97FTJWXFAJB0WK6BH4IV2B93WEZWJ8LT8I7PZS19EKE74EFUEGVSLG278L6F6A6C7R1OMRLZDIRR7B9S8FSCN2H1QAQOVVDDOC9MM1HX55PDZKA7MOG1S3ZEKA7MA7ERFND4BWFH85HRNDCF9TBGPLVUN5CM34W7088LGF50J9', N'1579709642', 2, NULL, 10)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103982', 2, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579161957', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579161976', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579161981', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579161990', 6, NULL, 36)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579161998', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579162002', 2, NULL, 5)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579162135', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'VMFBJJPAWGIQMPTPKHXD01W6ZTV0FTO6QA8FBN4ECLDW8TIISCPMNWI3ZIM0XY5D1M9MROO6MSAPXA8GLNHDDBS41EQ2BERNQA4WT61EYT94V3GYKGOJR2AZ3FUM8V5IBX9WFLWOWDOUBYJ871S4ZH6XAF3OLOFCX1283TT9TAXNNQXE6WOGDAS40LWARICI7M6K6KEO', N'1579246142', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103987', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579103990', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'JA8VOPDUAC0UUK0QK70IYRBP2V7YUQIFSENN07LV0LGRQSJLLE91M1Y29S23SPAU4VBQ7M0MWESMLT8KLVMQRQ30PVOHFRDKLWX5Q3O8NXEHJLQPUSR6DTDN2KAL8SHFAIBY5D4SHNC3MVREE8XX9HH1TKDF2DA4P9GBK2UQY9Z0RJBH36G95UF3BJFVAY0DMHNMOQ3M', N'1579104002', 2, NULL, 8)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'G1FXF7Q8SHAOL3UJOM4NQPN3RQ1PEC6P0TC34TBJI18KKCHJN1CTZ7V1OLFYFA3PLVY200P4LUEYQCTPJSCMAMZ37KOSONDI8S8ZOOSWMHHE36LM20OX8QP8554PL6Q8Q607D3B5APA2FV9B9CZY8QJRD93SUCLQ1JX6FNI8YTXTQW3K52QJG19AWI4D9SK7DTB57D5V', N'1579106228', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I84ENEWIS43X71G1WX7CI9LPTVQB96QPOP2CADTBGVSDYHP8NADSBVOJWOV2XVIVNNJR8D1TD66XQI0LGSY838O2ATQVR1NVM70L879KJUOEZ12OXKVE6J1O9RRPWFBMSKKTSPNJSNZIUJCI0C7HFJYDFLLRPUXDSCWPRJ6H6MVTUS4ZOIYUI3GJVHNGD6380VZSXW1R', N'1579162993', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I84ENEWIS43X71G1WX7CI9LPTVQB96QPOP2CADTBGVSDYHP8NADSBVOJWOV2XVIVNNJR8D1TD66XQI0LGSY838O2ATQVR1NVM70L879KJUOEZ12OXKVE6J1O9RRPWFBMSKKTSPNJSNZIUJCI0C7HFJYDFLLRPUXDSCWPRJ6H6MVTUS4ZOIYUI3GJVHNGD6380VZSXW1R', N'1579162996', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I84ENEWIS43X71G1WX7CI9LPTVQB96QPOP2CADTBGVSDYHP8NADSBVOJWOV2XVIVNNJR8D1TD66XQI0LGSY838O2ATQVR1NVM70L879KJUOEZ12OXKVE6J1O9RRPWFBMSKKTSPNJSNZIUJCI0C7HFJYDFLLRPUXDSCWPRJ6H6MVTUS4ZOIYUI3GJVHNGD6380VZSXW1R', N'1579162999', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'I84ENEWIS43X71G1WX7CI9LPTVQB96QPOP2CADTBGVSDYHP8NADSBVOJWOV2XVIVNNJR8D1TD66XQI0LGSY838O2ATQVR1NVM70L879KJUOEZ12OXKVE6J1O9RRPWFBMSKKTSPNJSNZIUJCI0C7HFJYDFLLRPUXDSCWPRJ6H6MVTUS4ZOIYUI3GJVHNGD6380VZSXW1R', N'1579163001', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'B7982SFSYTLTN4V9L93Q4DJC9ZFB64ELSFSA3MX8BHB13XXCUXH8Z7V8X78DNDV3O9AILVG9M5OXCSIMBCQIWGVUTUM6NUV8VHM4XP09RIW4T85FYWBKXNGHNPY1IJW1JQB4FOYWMQ1OU1BVYHI1TLKIAFQH191HW4VD6B9J7O7GV2XWODCAB4I7F33A7R5MZT20F8YW', N'1579163133', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'FZK2XFSWRWCH5C5Z5N4J5ZEATW382LC1RHOSSNVQY1YR3VDXZAWJ2RWQVKJAMU3FUK0YXNGR1OVEAWH7TGI2Y289V86IEUDE7T7S5P9M4ESO6SP9DLRGLBQ1L0DZQ7NGKQGRZYCDG57UKUDCS7E2D726CFGEMJ661LEPL8KOJN9PE6R1XAHS2UDMYKPTR7168A28PY1U', N'1579164330', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'FZK2XFSWRWCH5C5Z5N4J5ZEATW382LC1RHOSSNVQY1YR3VDXZAWJ2RWQVKJAMU3FUK0YXNGR1OVEAWH7TGI2Y289V86IEUDE7T7S5P9M4ESO6SP9DLRGLBQ1L0DZQ7NGKQGRZYCDG57UKUDCS7E2D726CFGEMJ661LEPL8KOJN9PE6R1XAHS2UDMYKPTR7168A28PY1U', N'1579164331', 1, NULL, NULL)
GO
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'FZK2XFSWRWCH5C5Z5N4J5ZEATW382LC1RHOSSNVQY1YR3VDXZAWJ2RWQVKJAMU3FUK0YXNGR1OVEAWH7TGI2Y289V86IEUDE7T7S5P9M4ESO6SP9DLRGLBQ1L0DZQ7NGKQGRZYCDG57UKUDCS7E2D726CFGEMJ661LEPL8KOJN9PE6R1XAHS2UDMYKPTR7168A28PY1U', N'1579164334', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'FZK2XFSWRWCH5C5Z5N4J5ZEATW382LC1RHOSSNVQY1YR3VDXZAWJ2RWQVKJAMU3FUK0YXNGR1OVEAWH7TGI2Y289V86IEUDE7T7S5P9M4ESO6SP9DLRGLBQ1L0DZQ7NGKQGRZYCDG57UKUDCS7E2D726CFGEMJ661LEPL8KOJN9PE6R1XAHS2UDMYKPTR7168A28PY1U', N'1579164355', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'FZK2XFSWRWCH5C5Z5N4J5ZEATW382LC1RHOSSNVQY1YR3VDXZAWJ2RWQVKJAMU3FUK0YXNGR1OVEAWH7TGI2Y289V86IEUDE7T7S5P9M4ESO6SP9DLRGLBQ1L0DZQ7NGKQGRZYCDG57UKUDCS7E2D726CFGEMJ661LEPL8KOJN9PE6R1XAHS2UDMYKPTR7168A28PY1U', N'1579164362', 2, NULL, 6)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YR7GU2K2Z9CAQDPB0OB9YXZ5WY8CF2WST7E1V50DW9XGYMBFYZVX4WAA2CT63HW7MTQT8XP5X8MYASGKW3OQG9Y76D9TL4JPMZ3E5RGAJ095JLAJ5IJA3G1N5TW5Y6T3Y7MKZCPFWNGQ92XVDZXDVJVF8QJM5LLMHGGM9ZU23DOQALNC5B28JQHPMGLI3CKKB3I3WLWN', N'1579179726', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171212', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DK14XWPLM42I1J9UNPSBXUML0RY2QGO84H1FRVBPAAH6E88KRM3DMZ0T7QDG8X5FP4NUS3D1NCAWMQG1SNKDNKWERIIAR5RMFCQKVO2YJB0JZDHRKMXVN22EHICJVDKSVXRKA6GDUV3LUMPOB00FH69NJLCMLTDG1IXBV7ROFUA18RU42YI4NEKZYEAY332BSXUMYO2E', N'1579190710', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DK14XWPLM42I1J9UNPSBXUML0RY2QGO84H1FRVBPAAH6E88KRM3DMZ0T7QDG8X5FP4NUS3D1NCAWMQG1SNKDNKWERIIAR5RMFCQKVO2YJB0JZDHRKMXVN22EHICJVDKSVXRKA6GDUV3LUMPOB00FH69NJLCMLTDG1IXBV7ROFUA18RU42YI4NEKZYEAY332BSXUMYO2E', N'1579190716', 2, NULL, 4)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'DK14XWPLM42I1J9UNPSBXUML0RY2QGO84H1FRVBPAAH6E88KRM3DMZ0T7QDG8X5FP4NUS3D1NCAWMQG1SNKDNKWERIIAR5RMFCQKVO2YJB0JZDHRKMXVN22EHICJVDKSVXRKA6GDUV3LUMPOB00FH69NJLCMLTDG1IXBV7ROFUA18RU42YI4NEKZYEAY332BSXUMYO2E', N'1579190717', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246124', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246138', 2, NULL, 2)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246145', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246160', 4, NULL, 13)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246166', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246169', 6, NULL, 38)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246183', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'M9ULATFY8SAEE5F3N4M9GJDAENUAQ3C4TNH8CBI49BVGAWA0SQLZZYQD5THW9EHEPGU0XH0MWRAEX69SM3S9SNE57DCS4V543R14BJCLZ9DHXLP8TYKFWIVCBZ3UFISAWGKGFFQC1GJTK6HWYD8FLZHV5Z2LQSGRRCVRAJ3OHHNP8C1FSH6BJMZ69ZKQMFWJA2CHLZVO', N'1579246185', 4, NULL, 26)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302866', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302868', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302870', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171219', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171220', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171221', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171222', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171224', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171224', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171224', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171225', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171253', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171257', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171273', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171280', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171281', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171284', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171309', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171327', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171328', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171329', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171330', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171332', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171333', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171335', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171336', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171337', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171338', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171340', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171340', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171342', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'R1WUXO3NQSWPY53QIEVB3BUDA4AEYMWFXQW6GYW29BLEE2VIBQJEXQYPHST3OYZE5R2IJ7KTS2B2ZQ9HJ2CHCSSH1GZXE2OKMHIXAN9EF0W4O600TLUWVRBN8VQCHXOZO0QSHMH628CL5Y25Q8E3I2Z4F4YP667SL7PXPISHEX3XOZSK2VVISVQVBVMNX196ATA8IEPI', N'1579171343', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YR7GU2K2Z9CAQDPB0OB9YXZ5WY8CF2WST7E1V50DW9XGYMBFYZVX4WAA2CT63HW7MTQT8XP5X8MYASGKW3OQG9Y76D9TL4JPMZ3E5RGAJ095JLAJ5IJA3G1N5TW5Y6T3Y7MKZCPFWNGQ92XVDZXDVJVF8QJM5LLMHGGM9ZU23DOQALNC5B28JQHPMGLI3CKKB3I3WLWN', N'1579179722', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YR7GU2K2Z9CAQDPB0OB9YXZ5WY8CF2WST7E1V50DW9XGYMBFYZVX4WAA2CT63HW7MTQT8XP5X8MYASGKW3OQG9Y76D9TL4JPMZ3E5RGAJ095JLAJ5IJA3G1N5TW5Y6T3Y7MKZCPFWNGQ92XVDZXDVJVF8QJM5LLMHGGM9ZU23DOQALNC5B28JQHPMGLI3CKKB3I3WLWN', N'1579179728', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YR7GU2K2Z9CAQDPB0OB9YXZ5WY8CF2WST7E1V50DW9XGYMBFYZVX4WAA2CT63HW7MTQT8XP5X8MYASGKW3OQG9Y76D9TL4JPMZ3E5RGAJ095JLAJ5IJA3G1N5TW5Y6T3Y7MKZCPFWNGQ92XVDZXDVJVF8QJM5LLMHGGM9ZU23DOQALNC5B28JQHPMGLI3CKKB3I3WLWN', N'1579179736', 2, NULL, 23)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'YR7GU2K2Z9CAQDPB0OB9YXZ5WY8CF2WST7E1V50DW9XGYMBFYZVX4WAA2CT63HW7MTQT8XP5X8MYASGKW3OQG9Y76D9TL4JPMZ3E5RGAJ095JLAJ5IJA3G1N5TW5Y6T3Y7MKZCPFWNGQ92XVDZXDVJVF8QJM5LLMHGGM9ZU23DOQALNC5B28JQHPMGLI3CKKB3I3WLWN', N'1579179813', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8N7X8W2AV80PLXPGEJPISLPW7QMYKNTOBFHIOO8OXXETTMY9HLXHVIVX8L0RJDZIOLNGQG20LAU4H5CU2NR8ZL22TUJCXQEU75EH8RTBT76UY0PCIGZ6W1SDTVW67ZC46A6L15VK4QP1PIE3EH3R1I7GMBYN5XD35BYX3DLNUOBGM0XKQ31Z6UDXDZ90HIPHMV6NTR9B', N'1579181043', 2, NULL, 23)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8N7X8W2AV80PLXPGEJPISLPW7QMYKNTOBFHIOO8OXXETTMY9HLXHVIVX8L0RJDZIOLNGQG20LAU4H5CU2NR8ZL22TUJCXQEU75EH8RTBT76UY0PCIGZ6W1SDTVW67ZC46A6L15VK4QP1PIE3EH3R1I7GMBYN5XD35BYX3DLNUOBGM0XKQ31Z6UDXDZ90HIPHMV6NTR9B', N'1579181049', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'8N7X8W2AV80PLXPGEJPISLPW7QMYKNTOBFHIOO8OXXETTMY9HLXHVIVX8L0RJDZIOLNGQG20LAU4H5CU2NR8ZL22TUJCXQEU75EH8RTBT76UY0PCIGZ6W1SDTVW67ZC46A6L15VK4QP1PIE3EH3R1I7GMBYN5XD35BYX3DLNUOBGM0XKQ31Z6UDXDZ90HIPHMV6NTR9B', N'1579181051', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'KGIUIOT1TGOZJ6LOGHBBLKG2AUHWSGFDWISS6Y3S78BTT69566O9UQ2A9PUYH7JNAKCAN3SROSENIWIDVLZV2FTFQSSQDEK9YYSI49SDO6UBMP2NHQD5IXV516T5KBOKHEYTE9JW3VH5VR3A0FJXBP1ADXR7PX3MSK3H1IIIGQBWQZJJCSEE20I2NXQQSMGG24NKYWJR', N'1579528543', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'KGIUIOT1TGOZJ6LOGHBBLKG2AUHWSGFDWISS6Y3S78BTT69566O9UQ2A9PUYH7JNAKCAN3SROSENIWIDVLZV2FTFQSSQDEK9YYSI49SDO6UBMP2NHQD5IXV516T5KBOKHEYTE9JW3VH5VR3A0FJXBP1ADXR7PX3MSK3H1IIIGQBWQZJJCSEE20I2NXQQSMGG24NKYWJR', N'1579528545', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528576', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528606', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528606', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528621', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528622', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528635', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528636', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528643', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528643', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528644', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'7BQUNFUNNH46SYU1BA4D2104J54VM9VPIXH1JXOICCDKRKXDKVIX2GZFLWKSK8BOWOX5R3ROVWAZYPM9H1L007XC7CQXM67EBZYCW2WRD4BGFAR5JLKTXLXY1U7PM4BZY6Y81KMOE4X65R77UUNBWOQ0C9KB5HEI3MRJLSD985O2X03JXA42R9CKP8629KT70ODTAXVC', N'1579528650', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'KGIUIOT1TGOZJ6LOGHBBLKG2AUHWSGFDWISS6Y3S78BTT69566O9UQ2A9PUYH7JNAKCAN3SROSENIWIDVLZV2FTFQSSQDEK9YYSI49SDO6UBMP2NHQD5IXV516T5KBOKHEYTE9JW3VH5VR3A0FJXBP1ADXR7PX3MSK3H1IIIGQBWQZJJCSEE20I2NXQQSMGG24NKYWJR', N'1579528671', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597890', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597893', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597895', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597902', 2, NULL, 14)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597920', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597921', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597922', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'17IWE0EL562ENRERSWVWT97TXLWJSQ7N1K17W28ZYM5SH4G1L96RSX22AY93D43O9PMCZH40Y68GEOL3E48SUQ4ES9683EV3SQT2RECDEG2WZWXWNFYI5L4TUIU0K66TKEYY4TMA24NOXIP5LKVXQN7GWAHJTM3IJILU89133XQB8P5L2YP9LID7GRB4LCRQBFEP747L', N'1579597925', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611684', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611696', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611702', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611711', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611720', 2, NULL, 12)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611744', 2, NULL, 12)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611746', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611754', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611778', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611781', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611791', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611795', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611798', 2, NULL, 12)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611853', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611855', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611863', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611865', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579611876', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579612157', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'9CUUP3TLTIGUMJ8XPY91NULEM8V451MUNFIR7E4UFSHIJHWYJAJWJZFO0QHRHYQ16WHHBQ0L4PV4ND4EL6V5R0KXA31R6NMQCPMXAPW2K824EKBXVCGKDUKFLUHHZIPRCF271HE5K2VT8QTZ2D53HBNSFGMQIODBSL18SE6O8KAK90H4ARPVNVKB2J1U5YX5YUAXZUGY', N'1579612159', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579725752', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579725768', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579725782', 4, NULL, 39)
GO
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579725996', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726000', 4, NULL, 39)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726005', 6, NULL, 43)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726065', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726069', 2, NULL, 12)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726073', 6, NULL, 43)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726121', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726125', 6, NULL, 43)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'6B1D0UW915W28PVDLAR54C5J9EIUAWPC1IHGTATRYRMBB75Q6LA0HQW4GREVLC8FPTB0HPTLQ9HNZ4HCJ2XYVY5KVNAB68GWSGVR5EJR2YJHJJELJBLTOKU0Y1F3ORPS3V4SIZGFSG66OXALJY14WXHIW43E8PCKJ1OYPVDT5GAINXLA03U0ZV6GNV6N6BGDDVXXIM6X', N'1579726247', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302871', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302873', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302876', 2, NULL, 1)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302903', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302904', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302905', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302905', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302932', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302934', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302935', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302961', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302963', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302965', 1, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302984', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302997', 8, N'act1031 ', NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580302999', 8, N'act1031 act2029 ', NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580303004', 8, N'act1031 ', NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580303007', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580303009', 5, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'27A7TOCIYH27WK06QAQMVZC56FWJZZOO46VLXIQMC5UCWMZ1PVT432BD5FAO22S82ND0ZPIHUDJ0F9IUP44DFUVB4SPSICUKMVKJI8CANLK0SL866Q878O3I2B6HA09SS9OTZ9LR2303K4TTTSW1DXM71BRHC59WM1HT7OH7FRFEX987SMXREIO5WQ1XSEO74ERSW9WE', N'1580303019', 3, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ESXVB060C5LL7EE66YG89AQUSYABEMXD6SI735EJ80MNGV72VGLU2PIDBDDNZ5WZHIPP5GDBU69JXDN61IQ7ADKQKF5Z20TDDWF7GS7ODK95Q9P8ZGWROYYE88EZKNJ4N5FA7P0JLB40DOALZ79VECZP8A9HNHEZBFSU3PAKQNGN38BU83ANNX0KG0C7QS5E7EY75PRK', N'1580309950', 7, NULL, NULL)
INSERT [dbo].[Logging] ([logID], [logTime], [logType], [logParamS], [logParamI]) VALUES (N'ESXVB060C5LL7EE66YG89AQUSYABEMXD6SI735EJ80MNGV72VGLU2PIDBDDNZ5WZHIPP5GDBU69JXDN61IQ7ADKQKF5Z20TDDWF7GS7ODK95Q9P8ZGWROYYE88EZKNJ4N5FA7P0JLB40DOALZ79VECZP8A9HNHEZBFSU3PAKQNGN38BU83ANNX0KG0C7QS5E7EY75PRK', N'1580309956', 8, N'fun1110 ', NULL)
SET IDENTITY_INSERT [dbo].[LogType] ON 

INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (1, N'Companies', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (2, N'Company Detail', N'Company')
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (3, N'Software', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (4, N'Software Detail', N'MesSoftware')
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (5, N'Cases', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (6, N'Case Detail', N'Cases')
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (7, N'Headpage', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (8, N'MainSearch', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (9, N'Searchcompanies', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (10, N'Searchsoftware', NULL)
INSERT [dbo].[LogType] ([ltPk], [ltType], [ltTable]) VALUES (11, N'Searhcases', NULL)
SET IDENTITY_INSERT [dbo].[LogType] OFF
SET IDENTITY_INSERT [dbo].[MeCatFuLink] ON 

INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (24, 13, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (25, 13, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (27, 10, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (28, 10, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (29, 11, 1056, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (30, 11, 1058, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (31, 11, 1057, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (32, 11, 1056, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (33, 11, 1057, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (34, 11, 1058, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (35, 11, 1056, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (36, 11, 1058, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (37, 11, 1057, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (38, 14, 1059, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (39, 14, 1059, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (40, 14, 1059, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (41, 14, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (42, 14, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (43, 14, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (44, 15, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (45, 15, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (46, 24, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (47, 24, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (48, 24, 1056, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (49, 24, 1056, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (50, 24, 1056, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (51, 24, 1056, 1102)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (52, 24, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (53, 25, 1056, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (54, 25, 1056, 1102)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (55, 25, 1056, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (56, 25, 1056, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (57, 26, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (58, 26, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (59, 30, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (60, 30, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (64, 31, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (65, 31, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (66, 38, 1057, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (67, 38, 1059, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (68, 38, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (69, 38, 1058, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (70, 38, 1057, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (71, 38, 1057, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (72, 38, 1059, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (73, 38, 1059, 1102)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (74, 38, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (75, 38, 1056, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (76, 38, 1058, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (77, 38, 1057, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (78, 38, 1059, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (79, 26, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (80, 30, 1057, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (81, 31, 1057, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (82, 31, 1057, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (83, 31, 1059, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (84, 31, 1059, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (85, 31, 1058, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (86, 31, 1058, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (87, 37, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (88, 37, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (89, 37, 1057, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (90, 37, 1057, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (91, 37, 1059, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (92, 37, 1059, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (93, 37, 1058, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (94, 37, 1058, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (95, 34, 1056, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (96, 34, 1056, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (97, 34, 1056, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (98, 34, 1057, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (99, 34, 1057, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (100, 34, 1057, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (101, 34, 1058, 1108)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (102, 34, 1058, 1107)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (103, 34, 1058, 1103)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (104, 35, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (105, 35, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (106, 36, 1056, 1109)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (107, 36, 1056, 1110)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (108, 36, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (109, 13, 1056, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (110, 30, 1057, 1111)
INSERT [dbo].[MeCatFuLink] ([mcflId], [mcflMes], [mcflCategory], [mcflFunction]) VALUES (111, 30, 1056, 1111)
SET IDENTITY_INSERT [dbo].[MeCatFuLink] OFF
SET IDENTITY_INSERT [dbo].[MesSoftware] ON 

INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (10, N'MyPlantFloor', N'MyPlantFloor is ’s werelds eerste configureerbare out-of-the-box MOM/MES oplossing. Kenmerken als innovatieve & onderscheidende functionaliteit, korte implementatietijd en lage onderhoudskosten garanderen een extreem korte terug-verdien-tijd. Dit alles maakt het ook interessant voor het midden- en kleinbedrijf.

Toepasbaar in elke procesomgeving (ook batch!) van elk formaat in meerdere talen die online wisselbaar zijn.

MyPlantFloor wordt in Nederland ontwikkeld door Agilitec BV.', N'https://www.myplantfloor.nl/', NULL, NULL, N'Images/20200108153213.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (11, N'Azumuta', N'All your work instructions, audits, improvement items and competency management in one workspace.', N'https://www.azumuta.com/en/features/', NULL, NULL, N'Images/20191218151045.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (13, N'SMART', N'SMART werd ontwikkeld voor klanten met een nood aan meer inzicht in bepaalde facetten van hun bedrijfsproces. Of het nu om energieverbruik, productieparameters of toegangscontrole gaat, de basis blijft gelijk, parameters worden opgemeten en op een overzichtelijke manier weergegeven.

Met SMART wordt een marktsegment ingevuld waarvoor uitgebreide MES pakketten of analysesoftware overkill zijn, maar waar toch een zekere mate van overzicht en rapportering vereist is.

De eenvoud van het systeem en het modulaire concept houden de instap laag en garanderen dat de gebruiker enkel betaalt wat hij/zij nodig heeft.', N'https://www.catael.be/diensten/s-m-a-r-t/', NULL, NULL, N'Images/20200108143333.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (14, N'ProfitPlus', N'business software voor de vooruitstrevende ondernemer', N'https://profitplus.be/nl/', NULL, NULL, N'Images/20200108164244.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (15, N'Actware production', N'Elk bedrijf werkt op een andere manier en elke machine is verschillend. Daarom is een plug & play pakket een utopie. Een correcte inventarisatie en technische analyse is vereist bij de integratie van een OEE pakket. Dit aspect beschouwen wij als de voorstudie van het project. Bij de voorstudie wordt nagegaan op welke manier data uit de machine kan gehaald worden en met welke databronnen het pakket kan communiceren. Sommige machines beschikken over een communicatiepoort, andere niet en vereisen de installatie van specifieke sensoren, afhankelijk van de toepassing. Onze technische kennis van machinebouw is hierbij onontbeerlijk.', N'https://actware.be/productie', NULL, NULL, N'Images/20200109100747.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (24, N'CPMS 3 ', N'CPMS is a light weight framework fully compatible with industrial standards allowing us to modify production processes immediately, connect any equipment or legacy system and integrate new technologies on the spot.', N'https://contec.be/nl/manufacturing-operations-management/', N'{"time":1578936465736,"blocks":[],"version":"2.15.1"}', NULL, N'Images/20200108164939.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (25, N'Mes ECHO', N'Digitalisation of work instructions 
', N'https://www.damatec.be/', NULL, NULL, N'Images/20200108165143.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (26, N'Factry Historian', N'Factry Historian acts as a powerful, easy to use data collection platform for real-time data from your existing production systems. There is no limit on the amount of data you wish to collect, neither on the amount of users or integrations. Because the Historian is built on the same technologies that power the Internet, we can assure a robust and scalable infrastructure that benefits single plants and truly shines across multiple production sites.', N'https://www.factry.io/historian/', NULL, NULL, N'Images/20191220150200.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (30, N'Ikologik Industrial Cloud', N'Een optimaal productieproces met een minimum aan operationele kosten: het is de vurige wens van elke
productieverantwoordelijke in de maakindustrie. Een rationeel beheer van energie- en waterverbruik en
talrijke andere parameters is in dat opzicht van het grootste belang.
Met een industrieel cloudplatform voor datalogging, -monitoring en rapportering speelt Ikologik hier naadloos
op in. Sinds de opstart in 2015 legden wij ons volledig toe op de ontwikkeling van intelligente software. Hierdoor
kunnen we allerlei procesdata ontvangen, bewaren en verwerken. Zo krijgen productieverantwoordelijken
elke dag opnieuw een grondig overzicht van alle processen. Slimme analyse van die gegevens leidt tot de
opmaak van gedetailleerde rapporten, die onze klanten toelaten om bepaalde afwijkingen snel te detecteren
en, indien nodig, ook meteen in te grijpen.', N'https://www.ikologik.com/nl/solution/', NULL, NULL, N'Images/20191220150212.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (31, N'Work 2020', N'De eenvoudigste registratie-oplossing voor: tijdregistratie, werkuren per project en per activiteit, status van een opdracht, locatie van goederen, materiaalverbruik, productie-opvolging, ...', N'http://work.one-two.com/', NULL, NULL, N'Images/20200114164914.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (34, N'Proceedix', N'On the Proceedix authoring admin platform, you personally manage your workflows on the shop floor, in a lab or in the field. You can digitize work instructions, checklists and SOPs in no time. Any work instruction or inspection document that your operator or technician frequently uses at work can be set up in Proceedix.', N'https://proceedix.com/platform', NULL, NULL, N'Images/20191220150230.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (35, N'Propos', N'Werkt u volgens de methoden van Lean Manufacturing of QRM? Of wilt u deze in uw bedrijf invoeren? PROPOS helpt u die invoer te vergemakkelijken en helpt u om uw processen continu te verbeteren.

PROPOS is gebaseerd op de filosofie van Quick Response Manufacturing (QRM). QRM is het continue terugdringen van de doorlooptijd. Verspillingen verdwijnen daarmee uit het systeem. QRM maakt gebruik van dezelfde technieken als Lean Manufacturing maar is toegespitst op klantordergestuurde productiebedrijven met een stappenproductie, een grote productvariëteit en sterk wisselende ordergroottes.', N'https://www.propos-software.nl/qrm-planningssoftware/', NULL, NULL, N'Images/20191220150235.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (36, N'PTC Thingworx', N'Build Better IIoT Solutions with the Best IIoT Platform
To turn the promise of the industrial Internet of Things (IIoT) into a powerful reality, you need a platform purpose-built for industrial applications. With extensive domain expertise built on nearly two decades of IoT innovation, PTC''s ThingWorx is an IIoT platform with the functionality and flexibility needed to drive a rapid ROI—while offering the security and scalability required to expand IIoT solutions throughout your enterprise.', N'https://www.ptc.com/en/products/iiot/thingworx-platform', NULL, NULL, N'Images/20200109162229.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (37, N'Power BI', N'Microsoft Power BI is een interactieve tool voor het visualiseren van data, ontwikkeld en uitgegeven door het Amerikaanse softwarebedrijf Microsoft. Het doel is om business intelligence beschikbaar te maken voor de eindgebruiker. Met Microsoft Power BI kunnen verschillende soorten data gecombineerd worden, om in een visueel dashboard managementinformatie weer te geven. Power BI is beschikbaar als Software as a Service (SaaS) en als onderdeel van de Microsoft Office 365-suite.', N'https://powerbi.microsoft.com/nl-nl/', NULL, NULL, NULL)
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (38, N'Flexas', N'Dit is een fictieve software die voor iedereen zichtbaar is. Op deze pagina zullen jullie zien wat er mogelijk is in te stellen op de website binnen jullie company, software en case pagina&apos;s.', N'http://ugent.be', NULL, 1, N'Images/20191218110418.png')
INSERT [dbo].[MesSoftware] ([meId], [meName], [meDescription], [meWebsite], [meContent], [mePublic], [meLogoUrl]) VALUES (39, N'GoRound', N'What if there were an app that could assist you with every scheduled inspection, instruction or task?
An app that centralized all operations at hand into one intuitive platform? An app suited for every
possible department within your company?
Wouldn’t that be great! In all our years of experience in operations, we never found that app – so we
developed it. Here’s GoRound, an intuitive digital partner that lets you perform inspections and
work instructions with the swipe of a finger – anywhere, anytime. 
With GoRound, you can easily create checklists and inspection rounds. Assign tasks to specific
employees, manage them effortlessly and generate reports for a convenient overview. All the data
you need to improve your operations is at your fingertips. Analyze and explore the intelligence, and use it to
your advantage.', N'http://www.gemsotec.com/goround', NULL, NULL, N'Images/20200122214620.png')
SET IDENTITY_INSERT [dbo].[MesSoftware] OFF
SET IDENTITY_INSERT [dbo].[Sector] ON 

INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1170, N'Automotive', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1171, N'Building & construction', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1172, N'Food', N'Voedingsindustrie')
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1173, N'Chemicals & cosmetics', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1174, N'Aerospace', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1175, N'Metal', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1176, N'Electrical machines', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1177, N'Agriculture', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1178, N'Equipment', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1179, N'Furniture', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1180, N'Medical devices', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1181, N'Pharmaceutical', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1182, N'Military', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1183, N'Plastics & rubbers', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1184, N'Water', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1185, N'Assembly', NULL)
INSERT [dbo].[Sector] ([seId], [seName], [seDescription]) VALUES (1186, N'Industry', NULL)
SET IDENTITY_INSERT [dbo].[Sector] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 30/01/2020 11:04:18 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 30/01/2020 11:04:18 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 30/01/2020 11:04:18 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_RoleId]    Script Date: 30/01/2020 11:04:18 ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 30/01/2020 11:04:18 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 30/01/2020 11:04:18 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_Company] FOREIGN KEY([crulCompany])
REFERENCES [dbo].[Company] ([comId])
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_Company]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Cases] FOREIGN KEY([ccflCase])
REFERENCES [dbo].[Cases] ([casId])
GO
ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Cases]
GO
ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Category] FOREIGN KEY([ccflCategory])
REFERENCES [dbo].[Category] ([catId])
GO
ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Category]
GO
ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Functions] FOREIGN KEY([ccflFunction])
REFERENCES [dbo].[Functions] ([fuId])
GO
ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Functions]
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Activity] FOREIGN KEY([cmcsalActivity])
REFERENCES [dbo].[Activity] ([acId])
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Activity]
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Cases] FOREIGN KEY([cmcsalCase])
REFERENCES [dbo].[Cases] ([casId])
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Cases]
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Company] FOREIGN KEY([cmcsalCompany])
REFERENCES [dbo].[Company] ([comId])
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Company]
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_MesSoftware] FOREIGN KEY([cmcsalMesSoftware])
REFERENCES [dbo].[MesSoftware] ([meId])
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_MesSoftware]
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Sector] FOREIGN KEY([cmcsalSector])
REFERENCES [dbo].[Sector] ([seId])
GO
ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Sector]
GO
ALTER TABLE [dbo].[Logging]  WITH CHECK ADD  CONSTRAINT [FK_Logging_LogType] FOREIGN KEY([logType])
REFERENCES [dbo].[LogType] ([ltPk])
GO
ALTER TABLE [dbo].[Logging] CHECK CONSTRAINT [FK_Logging_LogType]
GO
ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_Category] FOREIGN KEY([mcflCategory])
REFERENCES [dbo].[Category] ([catId])
GO
ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_Category]
GO
ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_Functions] FOREIGN KEY([mcflFunction])
REFERENCES [dbo].[Functions] ([fuId])
GO
ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_Functions]
GO
ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_MesSoftware] FOREIGN KEY([mcflMes])
REFERENCES [dbo].[MesSoftware] ([meId])
GO
ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_MesSoftware]
GO
/****** Object:  StoredProcedure [dbo].[Add_Activity]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Activity]
       @acName NVARCHAR(MAX),
	   @acDescription NVARCHAR(MAX) = null
AS 

set nocount on;

BEGIN
	declare @ID int;
	declare @X int;

	exec Check_Activity
			@acName = @acName,
			@acID = @ID output
	
	if @ID is null
		begin
			insert into Activity(acName,acDescription) values (@acName,@acDescription);

			set @X = 1
			select @X as Result
		END
	ELSE
		begin
			set @X = 0
			select @X as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Case]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Case]
       @casName NVARCHAR(MAX),
	   @casDescription NVARCHAR(MAX) = null,
	   @casWebsite NVARCHAR(MAX) = null
AS 

set nocount on;

BEGIN
	declare @ID int;
	declare @X int;

	exec Check_Case
			@casName = @casName,
			@casID = @ID output
	
	if @ID is null
		begin
			insert into Cases(casName,casDescription,casWebsite) 
             values (@casName,@casDescription,@casWebsite)

			set @X = 1
			select @X as Result
		END
	ELSE
		begin
			set @X = 0
			select @X as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_CaseCategoryFunctionLink]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Add_CaseCategoryFunctionLink]
						@Case NVARCHAR(MAX),
						@Category NVARCHAR(MAX),
						@Function NVARCHAR(MAX)  

AS 
BEGIN 
	declare @casId int
	declare @catId int
	declare @fuId int
	declare @Result int

	exec Check_Case
			@casName = @Case,
			@casID = @casId output	
	exec Check_Category
			@catName = @category,
			@catID = @catID output
	exec Check_Function
			@fuName = @Function,
			@fuID = @fuID output

	if (@casId is not null) and (@catId is not null) and (@fuId is not null)
		begin		
			declare @lr bit

			exec Check_CaseCategoryFunctionBelongToMes
					@Case = @Case,
					@Function = @Function,
					@Category = @Category,
					@Result = @lr output
			
			if (@lr=1)
				begin
					
					declare @ls bit

					exec Check_CaseCategoryFunction_Notlinked
						@Case = @Case,
						@Category = @Category,
						@Function = @Function,
						@Result = @ls output
					
					if (@ls=1)
						begin
							insert into CasCatFuLink (ccflCase, ccflCategory, ccflFunction)
							 values (
							 (select casId from Cases where casName = @Case),
							 (select catId from Category where catName = @Category),
							 (select fuId from Functions where fuName = @Function));
							set @Result = 1;
						end
					else
						set @Result = 0	;

				end
			else
				set @Result = 0	;
			
		end
	else		
		set @Result = 0	;	


	select @Result as Result
	
end
 
GO
/****** Object:  StoredProcedure [dbo].[Add_Category]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Category]
       @catName NVARCHAR(MAX),
	   @catDescription nvarchar(max) = null
AS	

set nocount on;

BEGIN
	declare @ID int;
	declare @xResult int;

	exec Check_Category
			@catName = @catName,
			@catID = @ID output
	
	if @ID is null
		begin
			insert into Category (catName,catDescription) 
			values (@catName,@catDescription);

			set @xResult = 1
			select @xResult as Result
		END
	ELSE
		begin
			set @xResult = 0
			select @xResult as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Company]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Company] 
       @comName                     NVARCHAR(MAX), 
       @comDescription              NVARCHAR(MAX)  = NULL   , 
       @comWebsite					NVARCHAR(MAX)  = NULL   , 
       @comContactEmail				NVARCHAR(MAX)  = NULL   ,
	   @comContactTelefoon			NVARCHAR(MAX)  = NULL
	   
AS 

set nocount on;

BEGIN
	declare @comID int;
	declare @xResult int;

	exec Check_Company 
			@comName = @comName,
			@comID = @comID output
	
	if @comID is null
		Begin
			INSERT INTO Company (comName, comDescription, comWebsite,	comContactEmail, comContactTelefoon) 
			VALUES ( @comName, @comDescription, @comWebsite,	@comContactEmail, @comContactTelefoon);
			
			set @xResult = 1
			select @xResult as Result
		END
	ELSE
		begin
			set @xResult = 0
			select @xResult as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_CompanyMesCaseSectorActivityLink]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Add_CompanyMesCaseSectorActivityLink]
       @Company NVARCHAR(MAX),
	   @Mes NVARCHAR(MAX),
	   @Case NVARCHAR(MAX),
	   @Sector NVARCHAR(MAX),
	   @Activity NVARCHAR(MAX)  
AS 
BEGIN 
	declare @comId int
	declare @meId int
	declare @casId int
	declare @seId int
	declare @acId int
	declare @Result int

	exec Check_Company
			@comName = @Company,
			@comID = @comId output
	exec Check_MesSoftware 
			@meName = @Mes,
			@meID = @meID output	
	exec Check_Case
			@casName = @Case,
			@casID = @casID output
	exec Check_Sector
			@seName = @Sector,
			@seID = @seID output
	exec Check_Activity
			@acName = @Activity,
			@acID = @acID output

	if (@comId is not null) and (@meId is not null) and ((@casId is not null) or(@Case is null)) and ((@seId is not null)or(@Sector is null)) and (@acId is not null)
		begin	
		
			declare @cnl bit
			exec Check_Case_NotLinked
				@Case = @Case,
				@Result = @cnl output
			
			if (@cnl = 1)
				begin
					declare @lr bit

					exec Check_CompanyMesCaseSectorActivity_NotLinked
						@Company = @Company,
						@Mes = @Mes,
						@Case = @Case,
						@Sector = @Sector,
						@Activity = @Activity,
						@Result = @lr output 
			
					if (@lr=1)
						begin
							insert into ComMeCasSeAcLink(cmcsalCompany, cmcsalMesSoftware,cmcsalSector,cmcsalActivity,cmcsalCase) 
							values (
							(select comId from Company where comName = @Company),
							(select meId from MesSoftware where meName = @Mes),
							(select seId from Sector where seName = @Sector),
							(select acId from Activity where acName = @Activity),
							(select casId from Cases where casName = @Case)
							);
							set @Result = 1;
						end
					else
						set @Result = 0	;
				end
			else
				set @Result = 0	;		
		end
	else		
		set @Result = 0	;	


	select @Result as Result
END 



GO
/****** Object:  StoredProcedure [dbo].[Add_CompanyRoleUserLink]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Add_CompanyRoleUserLink]
	@Company nvarchar(max),
	@Role nvarchar(max),
	@User nvarchar(max)
as
begin

	declare @comID int;
	declare @roID nvarchar(max);
	declare @usID nvarchar(max);
	declare @Result int;

	exec Check_Company 
			@comName = @Company,
			@comID = @comID output
	exec Check_Roles
		@Role = @Role,
		@ID = @roId output
	exec Check_User	
		@User = @User,
		@ID = @usID output
	
	if ((@comId is not null) or (@Company is null)) and (@roID is not null) and (@usId is not null)
		begin
			declare @X bit
			
			exec Check_CompanyRoleUser_Notlinked
				@Company = @Company,
				@Role = @Role,
				@User = @User,
				@Result = @X output

			if @x = 1
				begin

					insert into AspNetUserRoles(crulCompany,RoleId,UserId)
					values (
					(select comId from Company where comName = @Company),
					(select ID from AspNetRoles where [Name] = @Role),
					(select ID from AspNetUsers where UserName = @User)
					);
					set @Result = 1	;

				end
			else
				set @Result = 0	;


		end
	else		
		set @Result = 0	;	


	select @Result as Result

end
GO
/****** Object:  StoredProcedure [dbo].[Add_ContentPath]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_ContentPath]
       @conContentPath NVARCHAR(MAX),
	   @conPublic bit = 0
AS 

set nocount on;

BEGIN
	declare @ID int;
	declare @X bit;

	exec Check_ContentPath
			@conContentPath = @conContentPath,
			@conID = @ID output
	
	if @ID is null
		begin
			insert into Content(conContentPath,conPublic) values (@conContentPath,@conPublic);

			set @X = 1
			select @X as Result
		END
	ELSE
		begin
			set @X = 0
			select @X as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Function]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Function]
       @fuName NVARCHAR(MAX),
	   @fuDescription NVARCHAR(MAX) = null
AS 

set nocount on;

BEGIN
	declare @ID int;
	declare @X int;

	exec Check_Function
			@fuName = @fuName,
			@fuID = @ID output
	
	if @ID is null
		begin
			insert into Functions (fuName, fuDescription) values (@fuName,@fuDescription);

			set @X = 1
			select @X as Result
		END
	ELSE
		begin
			set @X = 0
			select @X as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_MesCategoryFunctionLink]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_MesCategoryFunctionLink]
       @Mes NVARCHAR(MAX),
	   @Category NVARCHAR(MAX),
	   @Function NVARCHAR(MAX)
AS 
BEGIN 

	declare @meId int
	declare @catId int
	declare @fuId int
	declare @Result int

	exec Check_MesSoftware 
			@meName = @Mes,
			@meID = @meID output	
	exec Check_Category
			@catName = @category,
			@catID = @catID output
	exec Check_Function
			@fuName = @Function,
			@fuID = @fuID output

	if (@meId is not null) and (@catId is not null) and (@fuId is not null)
		begin		
			declare @lr bit

			exec Check_MesCategoryFunction_Notlinked
				@Mes = @Mes,
				@Category = @Category,
				@Function = @Function,
				@Result = @lr output
			
			if (@lr=1)
				begin
					insert into MeCatFuLink (mcflMes, mcflCategory, mcflFunction)
					values (
					(select meId from MesSoftware where meName = @Mes),
					(select catId from Category where catName = @Category),
					(select fuId from Functions where fuName = @Function));
					set @Result = 1;
				end
			else
				set @Result = 0	;
			
		end
	else		
		set @Result = 0	;	


	select @Result as Result
END 

GO
/****** Object:  StoredProcedure [dbo].[Add_MesSoftware]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_MesSoftware]
       @meName NVARCHAR(MAX), 
       @meDescription NVARCHAR(MAX)  = NULL   , 
       @meWebsite NVARCHAR(MAX)  = NULL 
AS 

set nocount on;

BEGIN
	declare @meID int;
	declare @xResult int;

	exec Check_MesSoftware 
			@meName = @meName,
			@meID = @meID output
	
	if @meID is null
		begin
			insert into MesSoftware (meName, meDescription, meWebsite) 
			values (@meName,@meDescription,@meWebsite);

			set @xResult = 1
			select @xResult as Result
		END
	ELSE
		begin
			set @xResult = 0
			select @xResult as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Sector]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Sector]
       @seName NVARCHAR(MAX),
	   @seDescription NVARCHAR(MAX) = null
AS 

set nocount on;

BEGIN
	declare @ID int;
	declare @X int;

	exec Check_Sector
			@seName = @seName,
			@seID = @ID output
	
	if @ID is null
		begin
			insert into Sector(seName,seDescription) values (@seName, @seDescription);

			set @X = 1
			select @X as Result
		END
	ELSE
		begin
			set @X = 0
			select @X as Result
		end
END
GO
/****** Object:  StoredProcedure [dbo].[AddLog]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddLog]
       @Time NVARCHAR(MAX),
	   @Company int,
	   @Software int,
	   @Case int  
AS 
BEGIN 

	if (@Company is not null and @Software is null and @Case is null)
		begin
			insert into Logging(logTime,logCompany,logSoftware,logCase) 
			values (@Time,@Company,null,null);
			select *  from Logging;
		end		
	else 
		begin
			if (@Company is  null and @Software is not null and @Case is null)
				begin
					insert into Logging(logTime,logCompany,logSoftware,logCase) 
					values (@Time,null,@Software,null);
					select *  from Logging;
				end
			else
				begin
					if (@Company is  null and @Software is null and @Case is not null)
						begin
							insert into Logging(logTime,logCompany,logSoftware,logCase) 
							values (@Time,null,null,@Case);
							select *  from Logging;
						end
					else
						select 'wrong input parameters';
				end
				
		end						
END
GO
/****** Object:  StoredProcedure [dbo].[Check_Activity]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Activity]
       @acName NVARCHAR(MAX),
	   @acID int OUTPUT  
AS 
BEGIN 
	select top(1) @acID = acId from Activity where acName = @acName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_Case]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Case]
       @casName NVARCHAR(MAX),
	   @casID int OUTPUT  
AS 
BEGIN 
	select top(1) @casID = casId from Cases where casName = @casName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_Case_NotLinked]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[Check_Case_NotLinked]
       @Case NVARCHAR(MAX),
	   @Result bit output 
AS 
BEGIN 
	
	declare @ID int;

	select top(1) @ID = cmcsalId from ComMeCasSeAcLink join Cases on casId=cmcsalCase where casName = @Case

	if @ID is null
		set @Result = 1
	else
		set @Result = 0
END 



GO
/****** Object:  StoredProcedure [dbo].[Check_CaseCategoryFunction_NotLinked]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Check_CaseCategoryFunction_NotLinked]
	   @Case NVARCHAR(MAX),
	   @Category NVARCHAR(MAX),
	   @Function NVARCHAR(MAX),
	   @Result	bit OUTPUT  

AS 
BEGIN 
	declare @ID int;
	select top(1) @ID = ccflId from CasCatFuLink
                    join Cases on casId=ccflCase
                    join Category on catId=ccflCategory
                    join Functions on fuId = ccflFunction
                    where casName = @Case and catName = @Category and fuName = @Function

	if @ID is null
		set @Result = 1
	else
		set @Result = 0

END 
GO
/****** Object:  StoredProcedure [dbo].[Check_CaseCategoryFunctionBelongToMes]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Check_CaseCategoryFunctionBelongToMes]
						@Case NVARCHAR(MAX),
						@Category NVARCHAR(MAX),
						@Function NVARCHAR(MAX),
						@Result bit output 

AS 
BEGIN 
	declare @Id int;
	Select  @Id = mcflId from MeCatFuLink
                    Join Category on catId= mcflCategory
                    Join Functions on fuId = mcflFunction
                    where mcflMes =
                    (select top(1) cmcsalMesSoftware from ComMeCasSeAcLink join Cases on casId = cmcsalCase where casName = @Case)
					and catName= @Category and fuName = @Function;
	if @id is not null
		set @Result = 1
	else
		set @Result = 0
end
 
GO
/****** Object:  StoredProcedure [dbo].[Check_Category]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Category]
       @catName NVARCHAR(MAX),
	   @catID int OUTPUT  
AS 
BEGIN 
	select top(1) @catID = catId from Category where catName = @catName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_Company]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Company]
       @comName NVARCHAR(MAX),
	   @comID int OUTPUT  
AS 
BEGIN 
	select top(1) @comID = comId from Company where comName = @comName;
END 
GO
/****** Object:  StoredProcedure [dbo].[Check_CompanyMesCaseSectorActivity_Notlinked]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Check_CompanyMesCaseSectorActivity_Notlinked]
						@Company NVARCHAR(MAX),
						@Mes NVARCHAR(MAX),
						@Case NVARCHAR(MAX),
						@Sector NVARCHAR(MAX),
						@Activity NVARCHAR(MAX), 
						@Result	bit OUTPUT  

AS 
BEGIN 
	declare @ID int;
	if @Sector is not null
		
		if @Case is not null
			select top(1) @ID= cmcsalId from ComMeCasSeAcLink
                    join Company on comId=cmcsalCompany
                    join MesSoftware on meId=cmcsalMesSoftware                                 
                    join Activity on acId=cmcsalActivity
                    left join Sector on seId=cmcsalSector
                    left join Cases on casId=cmcsalCase
					where meName = @Mes and comName =@Company and acName = @Activity and casName = @Case and seName = @Sector
		else
			select top(1) @ID=  cmcsalId from ComMeCasSeAcLink
                    join Company on comId=cmcsalCompany
                    join MesSoftware on meId=cmcsalMesSoftware                                 
                    join Activity on acId=cmcsalActivity
                    left join Sector on seId=cmcsalSector
                    left join Cases on casId=cmcsalCase
					where meName = @Mes and comName =@Company and acName = @Activity and seName = @Sector

	else
		select top(1) @ID=  cmcsalId from ComMeCasSeAcLink
                    join Company on comId=cmcsalCompany
                    join MesSoftware on meId=cmcsalMesSoftware                                 
                    join Activity on acId=cmcsalActivity
                    left join Sector on seId=cmcsalSector
                    left join Cases on casId=cmcsalCase
                    where meName = @Mes and comName =@Company and acName = @Activity 

	if @ID is null
		set @Result = 1
	else
		set @Result = 0
	
end
 
GO
/****** Object:  StoredProcedure [dbo].[Check_CompanyRoleUser_NotLinked]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Check_CompanyRoleUser_NotLinked]
       @Company nvarchar(max),
	   @Role nvarchar(max),
	   @User nvarchar(max),
	   @Result	bit OUTPUT  
AS 
BEGIN 
	declare @ID int;

	if @Company is not null
			select top(1) @ID = crulId from AspNetUserRoles
                    join Company on comId=crulCompany
                    join AspNetRoles on Id=RoleId
                    join AspNetUsers on [AspNetUsers].Id = UserId
                    where comName = @Company and [AspNetRoles].[Name] = @Role and [AspNetUsers].[UserName] = @User
		else
			select top(1) @ID = crulId from AspNetUserRoles
                    join AspNetRoles on Id=RoleId
                    join AspNetUsers on [AspNetUsers].Id = UserId
                    where  [AspNetRoles].[Name] = @Role and [AspNetUsers].[UserName] = @User
	if @ID is null
		set @Result = 1
	else
		set @Result = 0

END 
GO
/****** Object:  StoredProcedure [dbo].[Check_ContentPath]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_ContentPath]
       @conContentPath NVARCHAR(MAX),
	   @conID int OUTPUT  
AS 
BEGIN 
	select top(1) @conID = conId FROM Content where conContentPath = @conContentPath;
END  
GO
/****** Object:  StoredProcedure [dbo].[Check_Function]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Function]
       @fuName NVARCHAR(MAX),
	   @fuID int OUTPUT  
AS 
BEGIN 
	select top(1) @fuID = fuId from Functions where fuName = @fuName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_MesCaseFunctionExist]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_MesCaseFunctionExist]
       @Mes NVARCHAR(MAX),
	   @Case NVARCHAR(MAX),
	   @Function NVARCHAR(MAX),
	   @Result	bit OUTPUT  
AS 
BEGIN 

	declare @meId int
	declare @casId int
	declare @fuId int

	select @meId = meId from MesSoftware where meName = @Mes;
    select @casId = catId from Category where catName = @Case;
    select @fuId = fuId from Functions where fuName = @Function;

	if (@meId is not null) and (@casId is not null) and (@fuid is not null)
		begin
			set @Result = 1
		end
	else
		begin
			set @Result = 0
		end

END 

GO
/****** Object:  StoredProcedure [dbo].[Check_MesCategoryFunction_NotLinked]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_MesCategoryFunction_NotLinked]
       @Mes NVARCHAR(MAX),
	   @Category NVARCHAR(MAX),
	   @Function NVARCHAR(MAX),
	   @Result	bit OUTPUT  

AS 
BEGIN 
	declare @ID int;
	select top(1) @ID = mcflId from MeCatFuLink
                    join MesSoftware on meId=mcflMes
                    join Category on catId=mcflCategory
                    join Functions on fuId = mcflFunction
                    where meName = @Mes and catName = @Category and fuName = @Function

	if @ID is null
		set @Result = 1
	else
		set @Result = 0

END 
GO
/****** Object:  StoredProcedure [dbo].[Check_MesSoftware]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_MesSoftware]
       @meName NVARCHAR(MAX),
	   @meID int OUTPUT  
AS 
BEGIN 
	select top(1) @meID = meId from MesSoftware where meName = @meName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_Roles]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Check_Roles]
	@Role nvarchar(max),
	@ID nvarchar(128) OUTPUT  
AS 
BEGIN 
	select top(1) @ID = ID from AspNetRoles where [Name] = @Role;
end
GO
/****** Object:  StoredProcedure [dbo].[Check_Sector]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Check_Sector]
       @seName NVARCHAR(MAX),
	   @seID int OUTPUT  
AS 
BEGIN 
	select top(1) @seID = seId from Sector where seName = @seName;
END  


GO
/****** Object:  StoredProcedure [dbo].[Check_User]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Check_User]
	@User nvarchar(max),
	@ID nvarchar(128) OUTPUT  
AS 
BEGIN 
	select top(1) @ID = ID from AspNetUsers where UserName = @User;
end
GO
/****** Object:  StoredProcedure [dbo].[GetLogs]    Script Date: 30/01/2020 11:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLogs] 
AS
BEGIN
	
	Select logId, logTime, ltType, ltTable,ltPrefix, logParamS, logParamI
	from logging
	join LogType2 on logType = ltPk
	order by logTime asc;


END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[39] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AspNetUserRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CasCatFuLink"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ComMeCasSeAcLink"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 136
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "MeCatFuLink"
            Begin Extent = 
               Top = 6
               Left = 686
               Bottom = 136
               Right = 856
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2565
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1500
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FullLinkView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FullLinkView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[30] 4[25] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ComMeCasSeAcLink"
            Begin Extent = 
               Top = 5
               Left = 305
               Bottom = 174
               Right = 488
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CasCatFuLink"
            Begin Extent = 
               Top = 5
               Left = 540
               Bottom = 137
               Right = 689
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MeCatFuLink"
            Begin Extent = 
               Top = 7
               Left = 97
               Bottom = 135
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2565
         Alias = 2475
         Table = 3660
         Output = 1455
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FullyLinked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FullyLinked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[19] 2[37] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LogView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LogView'
GO
USE [master]
GO
ALTER DATABASE [MES4SME_Selector] SET  READ_WRITE 
GO
