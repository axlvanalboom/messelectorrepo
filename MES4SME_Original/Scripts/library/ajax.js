﻿
// Function to request or send data to server side.
function serverRequest(url, success, data) {

    // Get data
    if (data != undefined) {
        $.ajax({
            type: "POST",
            url: window.location.protocol + "//" + window.location.host + "/" + url,
            dataType: "json",
            data: data,
            success: function (response) {
                success(response);
            },
            error: function (xhr, textStatus, errorThrown) {
                //error(xhr, textStatus, errorThrown);
                console.log(xhr, textStatus, errorThrown);
            }
        });

    }
    else {

        // Post data
        $.ajax({
            type: "GET",
            url: window.location.protocol + "//" + window.location.host + "/" + url,
            success: function (response) {
                success(response);
            },
            error: function (xhr, textStatus, errorThrown) {
                //error(xhr, textStatus, errorThrown);
                console.log(xhr, textStatus, errorThrown);
            }
        });

    }
}
