﻿// Communication layer to backend
// Contains all backend functions

// SECTORS

// Get sectors
function getSectors(success) {
    serverRequest('User/getSectors', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        } 
    });
}

// Add sector
function addSector(name, description, success) {
    serverRequest('Owner/addSector', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        } 
    },
        {
            oSector: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit sector
function editSector(id, name, description, success) {
    serverRequest('Admin/editSector', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oSector: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete sector
function deleteSector(id, success) {
    serverRequest('Admin/deleteSector', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// FUNCTIONS

// Get Functions
function getFunctions(success) {
    serverRequest('User/getFunctions', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Add Function
function addFunction(name, description, success) {
    serverRequest('Admin/addFunction', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oFunction: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Function
function editFunction(id, name, description, success) {
    serverRequest('Admin/editFunction', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oFunction: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Function
function deleteFunction(id, success) {
    serverRequest('Admin/deleteFunction', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// CATEGORY

// Get Categorys
function getCategories(success) {
    serverRequest('User/getCategories', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Add Category
function addCategory(name, description, success) {
    serverRequest('Admin/addCategory', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCategory: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Category
function editCategory(id, name, description, success) {
    serverRequest('Admin/editCategory', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCategory: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Category
function deleteCategory(id, success) {
    serverRequest('Admin/deleteCategory', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// ACTIVITY

// Get Activities
function getActivities(success) {
    serverRequest('User/getActivities', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Add Activity
function addActivity(name, description, success) {
    serverRequest('Admin/addActivity', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oActivity: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Activity
function editActivity(id, name, description, success) {
    serverRequest('Admin/editActivity', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oActivity: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Activity
function deleteActivity(id, success) {
    serverRequest('Admin/deleteActivity', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// COMPANY

// Get companies
function getCompanies(success) {
    serverRequest('User/getCompanies', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get companies linked
function getCompaniesLinked(success) {
    serverRequest('User/getCompaniesLinked', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get company by id 
function getCompanyById(id, success) {
    serverRequest('User/getCompanyById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Get company by id 
function getCompanyLinkedById(id, success) {
    serverRequest('User/getCompanyLinkedById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Add company
function addCompany(name, description, website, logoUrl, email, telephone, address, success) {
    serverRequest('Admin/addCompany', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCompany: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl,
                Email: email,
                Telephone: telephone,
                Address: address
            }
        }
    );
}

// Edit company
function editCompany(id, name, description, website, logoUrl, email, telephone, address, success) {
    serverRequest('Owner/editCompany', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCompany: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl,
                Email: email,
                Telephone: telephone,
                Address: address
            }
        }
    );
}

// Delete company
function deleteCompany(id, success) {
    serverRequest('Admin/deleteCompany', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}


// SOFTWARE

// Get Software
function getSoftwares(success) {
    serverRequest('User/getSoftwares', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get Software linked
function getSoftwaresLinked(success) {
    serverRequest('User/getSoftwaresLinked', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get Software by id 
function getSoftwareById(id, success) {
    serverRequest('User/getSoftwareById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Get Software by id 
function getSoftwareLinkedById(id, success) {
    serverRequest('User/getSoftwareLinkedById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Add software
function addSoftware(name, description, website, logoUrl, companyId, activityId, success) {
    serverRequest('Admin/addSoftware', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oSoftware: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            },
            Company: companyId,
            Activity: activityId
        }
    );
}

// edit software
function editSoftware(id, name, description, website, logoUrl, success) {
    serverRequest('Owner/editSoftware', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oSoftware: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            }
        }
    );
}

// Delete software
function deleteSoftware(id, success) {
    serverRequest('Admin/deleteSoftware', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// CASES

// Get Case
function getCases(success) {
    serverRequest('User/getCases', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get Case linked
function getCasesLinked(success) {
    serverRequest('User/getCasesLinked', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get Case by id 
function getCaseById(id, success) {
    serverRequest('User/getCaseById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Get Case by id 
function getCaseLinkedById(id, success) {
    serverRequest('User/getCaseLinkedById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Add case
function addCase(name, description, website, logoUrl, companyId, softwareId, sectorId, activityId, success) {
    serverRequest('Admin/addCase', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCase: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            },
            Company: companyId,
            Software: softwareId,
            Sector: sectorId,
            Activity: activityId
        }
    );
}

// Edit case
function editCase(id, name, description, website, logoUrl, success) {
    serverRequest('Owner/editCase', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            oCase: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            }
        }
    );
}

// Delete case
function deleteCase(id, success) {
    serverRequest('Admin/deleteCase', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Edit case sector
function editCaseSector(caseId, sectorId, success) {
    serverRequest('Admin/editCaseSector', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            CaseId: caseId,
            SectorId: sectorId
        }
    );
}

// CONTENT

// Get company content
function getCompanyContent(id, success) {
    serverRequest('User/getCompanyContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Get software content
function getSoftwareContent(id, success) {
    serverRequest('User/getSoftwareContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Get case content
function getCaseContent(id, success) {
    serverRequest('User/getCaseContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Edit company content
function editCompanyContent(id, content, success) {
    serverRequest('Owner/editCompanyContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id,
            Content: content
        }
    );
}

// Edit software content
function editSoftwareContent(id, content, success) {
    serverRequest('Owner/editSoftwareContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id,
            Content: content
        }
    );
}

// Edit case content
function editCaseContent(id, content, success) {
    serverRequest('Owner/editCaseContent', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id,
            Content: content
        }
    );
}

// LINKS

function addSoftwareCompanyActivityLink(softwareId, companyId, activityId, success) {
    serverRequest('Admin/addSoftwareCompanyActivityLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            SoftwareId: softwareId,
            CompanyId: companyId,
            ActivityId: activityId
        }
    );
}

function deleteSoftwareCompanyActivityLink(softwareId, companyId, activityId, success) {
    serverRequest('Admin/deleteSoftwareCompanyActivityLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            SoftwareId: softwareId,
            CompanyId: companyId,
            ActivityId: activityId
        }
    );
}

function addSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, success) {
    serverRequest('Admin/addSoftwareCategoryFunctionLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            SoftwareId: softwareId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function deleteSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, success) {
    serverRequest('Admin/deleteSoftwareCategoryFunctionLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            SoftwareId: softwareId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function addCaseCategoryFunctionLink(caseId, categoryId, functionId, success) {
    serverRequest('Admin/addCaseCategoryFunctionLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            CaseId: caseId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function deleteCaseCategoryFunctionLink(caseId, categoryId, functionId, success) {
    serverRequest('Admin/deleteCaseCategoryFunctionLink', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            CaseId: caseId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function editCaseCompanySoftware(caseId, companyId, softwareId, success) {
    serverRequest('Admin/editCaseCompanySoftware', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            CaseId: caseId,
            CompanyId: companyId,
            SoftwareId: softwareId
        }
    );
}

function getSearchResults(links, success) {
    serverRequest('User/getSearchResults', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Links: links
        }
    );
}

// USERS

// Get users
function getUsers(success) {
    serverRequest('Admin/getUsers', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get users linked
function getUsersLinked(success) {
    serverRequest('Admin/getUsersLinked', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    });
}

// Get user by id 
function getUserById(id, success) {
    serverRequest('Admin/getUserById', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}

// Edit user 
function editUser(id, email, phoneNumber, userName, occupation, success) {
    serverRequest('User/editUser', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id,
            Email: email,
            PhoneNumber: phoneNumber,
            UserName: userName,
            Occupation: occupation
        }
    );
}

// Edit user 
function editUserComRo(id, companyId, roleId, success) {
    serverRequest('Admin/editUserComRo', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id,
            Company: companyId,
            Role: roleId
        }
    );
}

// Delete user by id 
function deleteUser(id, success) {
    serverRequest('Admin/deleteUser', function (result) {
        if (result.Message !== null && result.Message.length > 0) {
            statusMessage(result.Message);
        }
        if (success !== undefined && result.Status > 0) {
            success(result.Data);
        }
    },
        {
            Id: id
        }
    );
}


// HELPERS

// Display return message
function statusMessage(message) {
    console.log(message);
}