﻿// User column definition
var _arroUserColumns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly'
        //width: '5%'
    },
    {
        data: 'UserName',
        title: 'Name',
        //width: '20%'
    },
    {
        data: 'Email',
        title: 'Email',
        //width: '10%'
    },
    {
        data: 'PhoneNumber',
        title: 'Telephone',
        //width: '10%'
    },
    {
        data: 'Occupation',
        title: 'Occupation',
        //width: '10%'
    }
];
