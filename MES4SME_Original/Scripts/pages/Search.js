﻿var _oData = [], _oSearch;

// Add choices from result
function addGroup(data, type, id) {
    // Check data
    if (data == undefined || data.length == 0)
        return;

    // Create choices
    var choices = [];
    for (var i = 0; i < data.length; i++) {
        choices.push({
            label: data[i].Name,
            value: data[i].Name,
            customProperties: {
                type: type,
                id: data[i].Id
            }
        });
    }

    // Create group
    var group = {
        label: type,
        value: type,
        id: id,
        choices: choices
    };

    // Add group to search field
    _oSearch.setChoices([group]);
}

// Get the search results from the server
function getResults() {

    // Get data from search field and initiate link object
    var arroItems = _oSearch.getValue(),
        links = {
            Company: [],
            Activity: [],
            Mes: [],
            Sector: [],
            MesCategory: [],
            MesFunction: [],
            CaseCategory: [],
            CaseFunction: [],
            Case: []
        }

    // Add items to link
    for (var i = 0; i < arroItems.length; i++) {
        switch (arroItems[i].groupId) {
            case 1:
                links.Sector.push(arroItems[i].customProperties.id);
                break;
            case 2:
                links.MesFunction.push(arroItems[i].customProperties.id);
                links.CaseFunction.push(arroItems[i].customProperties.id);
                break;
            case 3:
                links.MesCategory.push(arroItems[i].customProperties.id);
                links.CaseCategory.push(arroItems[i].customProperties.id);
                break;
            case 4:
                links.Activity.push(arroItems[i].customProperties.id);
                break;
            case 5:
                links.Company.push(arroItems[i].customProperties.id);
                break;
            case 6:
                links.Mes.push(arroItems[i].customProperties.id);
                break;
            case 7:
                links.Case.push(arroItems[i].customProperties.id);
                break;
            default:
                break;
        }
    }

    // Get the results from the server
    getSearchResults(links, function (result) {
        processResults(result);
    });
}

// Process the search results
function processResults(result) {
    console.log(result);
    $('#result').html(JSON.stringify(result));
}

// Startup function
$(function () {

    
    _oSearch = new Choices('#choices-multiple-labels', {
        removeItemButton: true,
        choices: _oData
    });

    _oSearch.passedElement.element.addEventListener(
        'addItem',
        function (event) {
            getResults();
        },
    );

    _oSearch.passedElement.element.addEventListener(
        'removeItem',
        function (event) {
            getResults();
        },
    );

    // Get the results from the server
    getSearchResults(null, function (result) {
        processResults(result);
    });

    getSectors(function (result) {
        addGroup(result, 'Sectors', 1);
    });

    getFunctions(function (result) {
        addGroup(result, 'Functions', 2);
    });

    getCategories(function (result) {
        addGroup(result, 'Categories', 3);
    });

    getActivities(function (result) {
        addGroup(result, 'Activities', 4);
    });

    getCompanies(function (result) {
        addGroup(result, 'Companies', 5);
    });

    getSoftwares(function (result) {
        addGroup(result, 'Software packages', 6);
    });

    getCases(function (result) {
        addGroup(result, 'Cases', 7);
    });

});


