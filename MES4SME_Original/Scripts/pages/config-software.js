﻿// Software config definition
var _oSoftwareConfig = {}, _oActSel, _oCompSel;

// Column definition
_oSoftwareConfig.columns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        width: '30%'
    },
    {
        data: 'Description',
        title: 'Description',
        width: '40%'
    },
    {
        data: 'Website',
        title: 'Website',
        width: '25%'
    },
    {
        data: 'Public',
        title: 'Public',
        width: '5%',
        type: 'select',
        options: [false, true]
    },
    {
        data: 'LogoUrl',
        title: 'Logo',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'CasesSectors',
        title: 'CasesSectors',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'CompanyActivities',
        title: 'CompanyActivities',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'CategoryFunctions',
        title: 'CategoryFunctions',
        visible: false,
        type: 'hidden'
    },
    {
        data: null,
        title: 'Company',
        visible: false,
        type: 'select',
        addOnly: true
    },
    {
        data: null,
        title: 'Activity',
        visible: false,
        type: 'select',
        addOnly: true
    }
];

// Function definition
_oSoftwareConfig.addData = function (datatable, rowdata, success, error) {

    var companyId = _oCompSel.val();
    var activityId = _oActSel.val();

    addSoftware(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, companyId, activityId, function (data) {

        rowdata.Id = data;
        rowdata.LogoUrl = null;
        rowdata.CompanyActivities = [];
        rowdata.CategoryFunctions = [];
        rowdata.Cases = [];
        rowdata.CompanyActivities.push({
            Company: {
                Id: companyId,
                Name: _oCompanies.find(function (element) { if (element.Id == companyId) return element; }).Name
            },
            Activity: {
                Id: activityId,
                Name: _oActivities.find(function (element) { if (element.Id == activityId) return element; }).Name
            }
        });

        success(rowdata);
    });

};

_oSoftwareConfig.editData = function (datatable, rowdata, success, error) {
    editSoftware(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, function () {
        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(rowdata, _oTemp.rowId)).show();
        _oTable.config.afterOpenRow();
        success(rowdata);
    });
};

_oSoftwareConfig.deleteData = function (datatable, rowdata, success, error) {
    deleteSoftware(rowdata.Id, function () {
        success();
    });
};

// Render inside of row
_oSoftwareConfig.renderOpenRow = function (data, indexes) {
    // Set temp object
    _oTemp = data;
    _oTemp.rowId = indexes;

    // Build cases sectors content
    var sCases = '';
    for (var i = 0; i < data.CasesSectors.length; i++) {
        if (i == 0)
            sCases += '<tr><td>Cases:</td><td>Sector ' + data.CasesSectors[i].Sector.Name + ': ' + data.CasesSectors[i].Case.Name + '</td></tr>';
        else
            sCases += '<tr><td></td><td>Sector ' + data.CasesSectors[i].Sector.Name + ': ' + data.CasesSectors[i].Case.Name + '</td></tr>';
    }

    // Build company activities content
    var sCompany = '<tr><td>Company activities:</td>';
    for (var i = 0; i < data.CompanyActivities.length; i++) {
        sCompany += `<td>` + data.CompanyActivities[i].Company.Name + `</td><td>` + data.CompanyActivities[i].Activity.Name + `</td>
                        <td><button class="deleteCompAct"  data-com="` + data.CompanyActivities[i].Company.Id + `" 
                                                        data-act="` + data.CompanyActivities[i].Activity.Id + `" 
                                                        data-index="` + i + `">Delete</button></td>
                        </tr><tr><td></td>`;
    }
    sCompany += '<td><select id="CompanyToAdd"></select></td><td><select id="ActivityToAdd"></select></td><td><button id="AddCompAct">Add</button></td></tr>';

    // Build category functions content
    var sCategory = '<tr><td>Category functions:</td>';
    for (var i = 0; i < data.CategoryFunctions.length; i++) {
        sCategory += `<td>` + data.CategoryFunctions[i].Name + `</td>`;
        for (var j = 0; j < data.CategoryFunctions[i].Functions.length; j++) {
            sCategory += `<td>` + data.CategoryFunctions[i].Functions[j].Name + `</td>
                            <td><button class="deleteCatFunc"   data-cat="` + data.CategoryFunctions[i].Id + `"
                                                                data-func="` + data.CategoryFunctions[i].Functions[j].Id + `"
                                                                data-cat-index="` + i + `"
                                                                data-func-index="` + j + `">Delete</button></td >
                            </tr><tr>`;
            if (j < data.CategoryFunctions[i].Functions.length - 1)
                sCategory += `<td></td><td></td>`;
        }
        sCategory += `<td></td>`;
    }
    sCategory += '<td><select id="CategoryToAdd"></select></td><td><select id="FunctionToAdd"></select></td><td><button id="AddCatFunc">Add</button></td></tr>';

    // Build logo content
    var sLogo = '';
    if (data.LogoUrl !== undefined && data.LogoUrl !== null)
        sLogo += '<img alt="Logo" src="' + data.LogoUrl + '" height="36px" style="float:left;"></td><td>';
    sLogo += '<input type="file" name="Logo" accept="image/*" onchange="uploadLogo(this.files);" style="padding:8px 10px;">';

    // Build total table
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Id:</td>' +
        '<td>' + data.Id + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Logo:</td>' +
        '<td>' + sLogo + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Description:</td>' +
        '<td>' + data.Description + '</td>' +
        '</tr>' +
        sCategory + 
        sCompany +
        sCases +
        '</table>';
};

// do stuff after the table is initialised
_oSoftwareConfig.afterDrawing = function () {

    // Get activities list
    getActivities(function (result) {
        _oActivities = result;
    });

    // Get company list
    getCompanies(function (result) {
        _oCompanies = result;
    });

    // Get category list
    getCategories(function (result) {
        _oCategories = result;
    });

    // Get functions list
    getFunctions(function (result) {
        _oFunctions = result;
    });
}

// do stuff after the row is opened
_oSoftwareConfig.afterOpenRow = function () {

    // Fill in company list
    for (var i = 0; i < _oCompanies.length; i++) {
        $("#CompanyToAdd").append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
    }

    // Fill in activities list
    for (var i = 0; i < _oActivities.length; i++) {
        $("#ActivityToAdd").append(`<option value="${_oActivities[i].Id}">${_oActivities[i].Name}</option>`);
    }

    // Fill in category list
    for (var i = 0; i < _oCategories.length; i++) {
        $("#CategoryToAdd").append(`<option value="${_oCategories[i].Id}">${_oCategories[i].Name}</option>`);
    }

    // Fill in functions list
    for (var i = 0; i < _oFunctions.length; i++) {
        $("#FunctionToAdd").append(`<option value="${_oFunctions[i].Id}">${_oFunctions[i].Name}</option>`);
    }

    // Bind delete buttons to delete links
    $(".deleteCompAct").each(function (index) {
        $(this).click(function () {
            if (confirm("Are you sure you want to delete this link?")) {
                // Delete the link
                var companyId = parseInt($(this).attr('data-com'));
                var activityId = parseInt($(this).attr('data-act'));
                var index = parseInt($(this).attr('data-index'));
                var softwareId = _oTemp.Id;
                deleteSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result) {
                    // Remove the link from the temp object
                    _oTemp.CompanyActivities.splice(index, 1);

                    // Render the row again
                    _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                    _oTable.config.afterOpenRow();
                });
            }
        });
    });

    // Bind add button to add link
    $("#AddCompAct").click(function () {
        if (confirm("Are you sure you want to add this link?")) {
            // Add the link
            var companyId = parseInt($("#CompanyToAdd").val());
            var activityId = parseInt($("#ActivityToAdd").val());
            var softwareId = _oTemp.Id;
            addSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result) {
                // Add the link from the temp object
                _oTemp.CompanyActivities.push({
                    Company: {
                        Id: companyId,
                        Name: $("#CompanyToAdd option:selected").text()
                    },
                    Activity: {
                        Id: activityId,
                        Name: $("#ActivityToAdd option:selected").text()
                    }
                });
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
            });
        }
    });

    // Bind delete buttons to delete links
    $(".deleteCatFunc").each(function (index) {
        $(this).click(function () {
            if (confirm("Are you sure you want to delete this link?")) {
                // Delete the link
                var categoryId = parseInt($(this).attr('data-cat'));
                var functionId = parseInt($(this).attr('data-func'));
                var categoryObjectId = parseInt($(this).attr('data-cat-index'));
                var functionObjectId = parseInt($(this).attr('data-func-index'));
                var softwareId = _oTemp.Id;
                deleteSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, function (result) {
                    // Remove the link from the temp object
                    _oTemp.CategoryFunctions[categoryObjectId].Functions.splice(functionObjectId, 1);
                    if (_oTemp.CategoryFunctions[categoryObjectId].Functions.length == 0)
                        _oTemp.CategoryFunctions.splice(categoryObjectId, 1);

                    // Render the open row again
                    _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                    _oTable.config.afterOpenRow();
                });
            }
        });
    });

    // Bind add button to add link
    $("#AddCatFunc").click(function () {
        if (confirm("Are you sure you want to add this link?")) {
            // Add the link
            var categoryId = parseInt($("#CategoryToAdd").val());
            var functionId = parseInt($("#FunctionToAdd").val());
            var softwareId = _oTemp.Id;
            addSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, function (result) {
                // Add the link to the temp object
                var i;
                for (i = 0; i < _oTemp.CategoryFunctions.length; i++) {
                    if (_oTemp.CategoryFunctions[i].Id == categoryId)
                        break;
                }
                if (i == _oTemp.CategoryFunctions.length)
                {
                    // Add category if it did not exist 
                    _oTemp.CategoryFunctions.push({
                        Id: categoryId,
                        Name: $("#CategoryToAdd option:selected").text(),
                        Functions: []
                    });
                }

                // Add function
                _oTemp.CategoryFunctions[i].Functions.push({
                    Id: functionId,
                    Name: $("#FunctionToAdd option:selected").text()
                });

                // Render the open row again
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
            });
        }
    });
}

// do stuff before the add window is opening
_oSoftwareConfig.onOpenAddModal = function () {

    _oCompSel = $('.modal-body select[name="Company"]');
    for (var i = 0; i < _oCompanies.length; i++) {
        _oCompSel.append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
    }

    _oActSel = $('.modal-body select[name="Activity"]');
    for (var i = 0; i < _oActivities.length; i++) {
        _oActSel.append(`<option value="${_oActivities[i].Id}">${_oActivities[i].Name}</option>`);
    }

}