﻿// Company config definition
var _oCompanyConfig = {};

// Column definition
_oCompanyConfig.columns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        width: '30%'
    },
    {
        data: 'Website',
        title: 'Website',
        width: '25%'
    },
    {
        data: 'Email',
        title: 'Email',
        width: '25%'
    },
    {
        data: 'Telephone',
        title: 'Telephone',
        width: '15%'
    },
    {
        data: 'Public',
        title: 'Public',
        width: '5%',
        type: 'select',
        options: [false, true]
    },
    {
        data: 'Description',
        title: 'Description',
        visible: false
    },
    {
        data: 'Address',
        title: 'Address',
        visible: false
    },
    {
        data: 'LogoUrl',
        title: 'Logo',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'Contacts',
        title: 'Contacts',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'CasesSectors',
        title: 'CasesSectors',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'SoftwareActivities',
        title: 'SoftwareActivities',
        visible: false,
        type: 'hidden'
    }
];

// Function definition
_oCompanyConfig.addData = function (datatable, rowdata, success, error) {
    addCompany(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata.Address, function (data) { rowdata.Id = data; success(rowdata); });
};

_oCompanyConfig.editData = function (datatable, rowdata, success, error) {
    editCompany(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata.Address, function () {
        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(rowdata, _oTemp.rowId)).show();
        _oTable.config.afterOpenRow();
        success(rowdata);
    });
};

_oCompanyConfig.deleteData = function (datatable, rowdata, success, error) { deleteCompany(rowdata.Id, function () { success(); }); };

// Render inside of row
_oCompanyConfig.renderOpenRow = function (data, indexes) {
    // Set temp object
    _oTemp = data;
    _oTemp.rowId = indexes;

    // Build cases sectors content
    var sCases = '';
    for (var i = 0; i < data.CasesSectors.length; i++) {
        if (i == 0)
            sCases += '<tr><td>Cases:</td><td>Sector ' + data.CasesSectors[i].Sector.Name + ': ' + data.CasesSectors[i].Case.Name + '</td></tr>';
        else
            sCases += '<tr><td></td><td>Sector ' + data.CasesSectors[i].Sector.Name + ': ' + data.CasesSectors[i].Case.Name + '</td></tr>';
    }

    // Build software activities content
    var sSoftware = '<tr><td>Software activities:</td>';
    for (var i = 0; i < data.SoftwareActivities.length; i++) {
        sSoftware += `<td>` + data.SoftwareActivities[i].Software.Name + `</td><td>` + data.SoftwareActivities[i].Activity.Name + `</td>
                        <td><button class="deleteLink"  data-mes="` + data.SoftwareActivities[i].Software.Id + `" 
                                                        data-act="` + data.SoftwareActivities[i].Activity.Id + `" 
                                                        data-index="` + i + `">Delete</button></td>
                        </tr><tr><td></td>`;
    }
    sSoftware += '<td><select id="SoftwareToAdd"></select></td><td><select id="ActivityToAdd"></select></td><td><button id="AddSoftAct">Add</button></td></tr>';

    // Build contacts content
    var sContacts = '';
    for (var i = 0; i < data.Contacts.length; i++) {
        if (i == 0)
            sContacts += '<tr><td>Contacts:</td><td>' + data.Contacts[i].UserName + ': ' + data.Contacts[i].Occupation + '</td></tr>';
        else
            sContacts += '<tr><td></td><td>' + data.Contacts[i].UserName + ': ' + data.Contacts[i].Occupation + '</td></tr>';
    }

    // Build logo content
    var sLogo = '';
    if (data.LogoUrl !== undefined && data.LogoUrl !== null)
        sLogo += '<img alt="Logo" src="' + data.LogoUrl + '" height="36px" style="float:left;"></td><td>';
    sLogo += '<input type="file" name="Logo" accept="image/*" onchange="uploadLogo(this.files);" style="padding:8px 10px;">';

    // Build total table
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Id:</td>' +
        '<td>' + data.Id + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Address:</td>' +
        '<td>' + data.Address + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Logo:</td>' +
        '<td>' + sLogo + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Description:</td>' +
        '<td>' + data.Description + '</td>' +
        '</tr>' +
        sContacts + 
        sSoftware +
        sCases +
        '</table>';
};

// do stuff after the table is initialised
_oCompanyConfig.afterDrawing = function () {

    // Get activities list
    getActivities(function (result) {
        _oActivities = result;
    });

    // Get software list
    getSoftwares(function (result) {
        _oSoftwares = result;
    });

}

// do stuff after the row is opened
_oCompanyConfig.afterOpenRow = function () {

    // Fill in software list
    for (var i = 0; i < _oSoftwares.length; i++) {
        $("#SoftwareToAdd").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
    }

    // Fill in activities list
    for (var i = 0; i < _oActivities.length; i++) {
        $("#ActivityToAdd").append(`<option value="${_oActivities[i].Id}">${_oActivities[i].Name}</option>`);
    }

    // Bind delete buttons to delete links
    $(".deleteLink").each(function (index) {
        $(this).click( function () {
            if (confirm("Are you sure you want to delete this link?")) {
                // Delete the link
                var softwareId = parseInt($(this).attr('data-mes'));
                var activityId = parseInt($(this).attr('data-act'));
                var index = parseInt($(this).attr('data-index'));
                var companyId = _oTemp.Id;
                deleteSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result) {
                    // Remove the link from the temp object
                    _oTemp.SoftwareActivities.splice(index, 1);

                    // Render the row again
                    _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                    _oTable.config.afterOpenRow();
                });
            }
        });
    });

    // Bind add button to add link
    $("#AddSoftAct").click(function () {
        if (confirm("Are you sure you want to add this link?")) {
            // Add the link
            var softwareId = parseInt($("#SoftwareToAdd").val());
            var activityId = parseInt($("#ActivityToAdd").val());
            var companyId = _oTemp.Id;
            addSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result) {
                // Add the link from the temp object
                _oTemp.SoftwareActivities.push({
                    LinkId: Math.floor((Math.random() * 100000) + 1),
                    Software: {
                        Id: softwareId,
                        Name: $("#SoftwareToAdd option:selected").text()
                    },
                    Activity: {
                        Id: activityId,
                        Name: $("#ActivityToAdd option:selected").text()
                    }
                });
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
            });
        }
    });

}