﻿var _oTable = null,
    _oEditor = null,
    _arroData = [],
    _oTemp = {},
    _oActivities = [],
    _oSoftwares = [],
    _oCategories = [],
    _oFunctions = [],
    _oCompanies = [];

// draw table with editing 
function drawTable(data, config) {

    // Clear current table
    if (_oTable !== null) {
        _oTable.clear().destroy();
        _oTable = null;
        _oTemp = {};
        _oActivities = [];
        _oSoftwares = [];
        _oCategories = [];
        _oFunctions = [];
        _oCompanies = [];
        $('#table').empty();
    }

    // Draw table
    _oTable = $('#table').DataTable({
        sPaginationType: 'full_numbers',
        autoWidth: false,
        data: data,
        columns: config.columns,
        dom: 'Blfrtip',        // Buttons, length, filter,..
        select: 'single',
        //responsive: true,
        altEditor: true,     // Enable altEditor
        buttons: [{
            text: 'Add',
            name: 'add'        // do not change name
        },
        {
            extend: 'selected', // Bind to Selected row
            text: 'Edit',
            name: 'edit'        // do not change name
        },
        {
            extend: 'selected', // Bind to Selected row
            text: 'Delete',
            name: 'delete'      // do not change name
        }],
        onAddRow: function (datatable, rowdata, success, error) {
            _oTable.config.addData(datatable, rowdata, success, error);
        },
        onDeleteRow: function (datatable, rowdata, success, error) {
            _oTable.config.deleteData(datatable, rowdata, success, error);
        },
        onEditRow: function (datatable, rowdata, success, error) {
            _oTable.config.editData(datatable, rowdata, success, error);
        },
        onOpenAddModal: function () {
            if (_oTable.config.onOpenAddModal !== undefined)
                _oTable.config.onOpenAddModal();
        }
    });


    // Set config
    _oTable.config = config;

    _oTable.on('select', function (e, dt, type, indexes) {
        if (_oTable.config.renderOpenRow !== undefined) {
            var row = _oTable.row(indexes);
            row.child(_oTable.config.renderOpenRow(row.data(), indexes)).show();
            if (_oTable.config.afterOpenRow !== undefined)
                _oTable.config.afterOpenRow();
        }
    });

    _oTable.on('deselect', function (e, dt, type, indexes) {
        if (_oTable.config.renderOpenRow !== undefined) {
            var row = _oTable.row(indexes);
            row.child.hide();
        }
    });

    if (_oTable.config.afterDrawing !== undefined)
        _oTable.config.afterDrawing();

}

// Add data
function addData(datatable, rowdata, success, error) {
    console.log(rowdata);

    switch ($('#selector').val()) {
        case 'Companies':
            addCompany(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata.Address, function () { success(rowdata); });
            break;
        case 'Softwares':
            addSoftware(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, function () { success(rowdata); });
            break;
        case 'Cases':
            addCase(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, function () { success(rowdata); });
            break;
        case 'Users':
            statusMessage("Cannot add a new user, need to register to do that.");
            break;
        case 'Sectors':
            addSector(rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Activities':
            addActivity(rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Functions':
            addFunction(rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Categories':
            addCategory(rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        default:
            break;
    }

    return rowdata;
}

// Edit data
function editData(datatable, rowdata, success, error) {
    console.log(rowdata);

    switch ($('#selector').val()) {
        case 'Companies':
            editCompany(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata, function () { success(rowdata); });
            break;
        case 'Softwares':
            editSoftware(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, function () { success(rowdata); });
            break;
        case 'Cases':
            editCase(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, function () { success(rowdata); });
            break;
        case 'Users':
            editUser(rowdata.Id, rowdata.Email, rowdata.PhoneNumber, rowdata.UserName, rowdata.Occupation, function () { success(rowdata); });
            break;
        case 'Sectors':
            editSector(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Activities':
            editActivity(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Functions':
            editFunction(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        case 'Categories':
            editCategory(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); });
            break;
        default:
            break;
    }
}

// Delete data
function deleteData(datatable, rowdata, success, error) {
    console.log(rowdata);

    switch ($('#selector').val()) {
        case 'Companies':
            deleteCompany(rowdata.id, function () { changeSelector(); });
            break;
        case 'Softwares':
            deleteSoftware(rowdata.id, function () { changeSelector(); });
            break;
        case 'Cases':
            deleteCase(rowdata.id, function () { changeSelector(); });
            break;
        case 'Users':
            deleteUser(rowdata.id, function () { changeSelector(); });
            break;
        case 'Sectors':
            deleteSector(rowdata.id, function () { changeSelector(); });
            break;
        case 'Activities':
            deleteActivity(rowdata.id, function () { changeSelector(); });
            break;
        case 'Functions':
            deleteFunction(rowdata.id, function () { changeSelector(); });
            break;
        case 'Categories':
            deleteCategory(rowdata.id, function () { changeSelector(); });
            break;
        default:
            break;
    }

    return rowdata;
}

// Change selector
function changeSelector() {

    switch ($('#selector').val()) {
        case 'Companies':
            // Get companies
            getCompaniesLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oCompanyConfig);
            });
            break;
        case 'Softwares':
            // Get softwares
            getSoftwaresLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oSoftwareConfig);
            });
            break;
        case 'Cases':
            // Get cases
            getCases(function (result) {
                _arroData = result;
                //drawTable(_arroData, _arroAdvColumns);
            });
            break;
        case 'Users':
            // Get users
            getUsers(function (result) {
                _arroData = result;
                //drawTable(_arroData, _arroUserColumns);
            });
            break;
        case 'Sectors':
            // Get sectors
            getSectors(function (result) {
                _arroData = result;
                drawTable(_arroData, _oSectorConfig);
            });
            break;
        case 'Activities':
            // Get activities
            getActivities(function (result) {
                _arroData = result;
                drawTable(_arroData, _oActivityConfig);
            });
            break;
        case 'Functions':
            // Get Functions
            getFunctions(function (result) {
                _arroData = result;
                drawTable(_arroData, _oFunctionConfig);
            });
            break;
        case 'Categories':
            // Get categories
            getCategories(function (result) {
                _arroData = result;
                drawTable(_arroData, _oCategoryConfig);
            });
            break;
        default:
            break;
    }
}

// upload logo function (can be used for softwares and cases as well)
function uploadLogo(files) {
    console.log(files);
    var formData = new FormData();
    var imageFile = files[0];
    _oTemp.LogoUrl = 'Images/' + imageFile.name;
    formData.append("image", imageFile);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Owner/uploadImage", true);
    xhr.addEventListener("load", function (evt) { uploadComplete(evt); }, false);
    xhr.addEventListener("error", function (evt) { uploadFailed(evt); }, false);
    xhr.send(formData);
}

// upload complete
function uploadComplete(evt) {
    if (evt.target.status == 200) {
        statusMessage("Logo uploaded successfully.");
        // edit the object
        _oTable.config.editData(null, _oTemp, function () {
            // render the row content again
            _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
            _oTable.config.afterOpenRow();
        });
    }
    else
        statusMessage("Error Uploading File");
}

// upload failed
function uploadFailed(evt) {
    statusMessage("There was an error attempting to upload the file.");
}

// Startup function
$(function() {

    // Run change selection on default value
    $('#selector').val('Softwares');

    changeSelector();

});