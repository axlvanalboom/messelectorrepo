﻿
// Advanced base class column definition
var _arroAdvColumns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        width: '20%'
    },
    {
        data: 'Public',
        title: 'Public'
    },
    {
        data: 'Website',
        title: 'Website'
    },
    {
        data: 'Description',
        title: 'Description',
        width: '50%'
    },
    {
        data: 'LogoUrl',
        title: 'Logo'
    }
];