﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MES4SME.Models
{
    public class ContactViewModel
    {

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string EmailBody { get; set; }

        [Required]
        [Display(Name = "Email Sender")]
        public string Sender { get; set; }

    }
}