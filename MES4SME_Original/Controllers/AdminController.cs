﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BEC;

namespace MES4SME.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        #region "Users"

        public JsonResult getUsers()
        {
            return Json(clsBLL.getUsers(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUserById(string Id)
        {
            return Json(clsBLL.getUserById(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUsersLinked()
        {
            return Json(clsBLL.getUsersLinked(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editUser(clsUser oUser)
        {
            return Json(clsBLL.editUser(oUser), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editUserComRo(string Id, int Company, int Role)
        {
            return Json(clsBLL.editUserComRo(Id, Company, Role), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteUser(string Id)
        {
            return Json(clsBLL.deleteUser(Id), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Sector"
        public JsonResult editSector(clsSector oSector)
        {
            return Json(clsBLL.editSector(oSector), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteSector(int Id)
        {
            return Json(clsBLL.deleteSector(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Activity"
        public JsonResult addActivity(clsActivity oActivity)
        {
            return Json(clsBLL.addActivity(oActivity), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editActivity(clsActivity oActivity)
        {
            return Json(clsBLL.editActivity(oActivity), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteActivity(int Id)
        {
            return Json(clsBLL.deleteActivity(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Category"
        public JsonResult addCategory(clsCategory oCategory)
        {
            return Json(clsBLL.addCategory(oCategory), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editCategory(clsCategory oCategory)
        {
            return Json(clsBLL.editCategory(oCategory), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteCategory(clsCategory oCategory)
        {
            return Json(clsBLL.deleteCategory(oCategory.Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Function"
        public JsonResult addFunction(clsFunction oFunction)
        {
            return Json(clsBLL.addFunction(oFunction), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editFunction(clsFunction oFunction)
        {
            return Json(clsBLL.editFunction(oFunction), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteFunction(int Id)
        {
            return Json(clsBLL.deleteFunction(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Company"
        public JsonResult addCompany(clsCompany oCompany)
        {
            return Json(clsBLL.addCompany(oCompany), JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteCompany(clsCompany oCompany)
        {
            return Json(clsBLL.deleteCompany(oCompany), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Software"
        public JsonResult addSoftware(clsSoftware oSoftware, int Company, int Activity)
        {
            return Json(clsBLL.addSoftware(oSoftware, Company, Activity), JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteSoftware(int Id)
        {
            return Json(clsBLL.deleteSoftware(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Case"
        public JsonResult addCase(clsCase oCase, int Company, int Software, int Sector, int Activity)
        {
            return Json(clsBLL.addCase(oCase, Company, Software, Sector, Activity), JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteCase(int Id)
        {
            return Json(clsBLL.deleteCaseById(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editCaseSector(int CaseId, int SectorId)
        {
            return Json(clsBLL.editCaseSector(CaseId, SectorId), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Links"

        //addSoftwareCompanyActivityLink
        public JsonResult addSoftwareCompanyActivityLink(int SoftwareId, int CompanyId, int ActivityId)
        {
            return Json(clsBLL.addSoftwareCompanyActivityLink(SoftwareId, CompanyId, ActivityId), JsonRequestBehavior.AllowGet);
        }

        //deleteSoftwareCompanyActivityLink
        public JsonResult deleteSoftwareCompanyActivityLink(int SoftwareId, int CompanyId, int ActivityId)
        {
            return Json(clsBLL.deleteSoftwareCompanyActivityLink(SoftwareId, CompanyId, ActivityId), JsonRequestBehavior.AllowGet);
        }

        //addSoftwareCategoryFunctionLink
        public JsonResult addSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.addSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        //deleteSoftwareCategoryFunctionLink
        public JsonResult deleteSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.deleteSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        //addCaseCategoryFunctionLink
        public JsonResult addCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.addCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        //deleteCaseCategoryFunctionLink
        public JsonResult deleteCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.deleteCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        //editCaseCompanySoftware
        public JsonResult editCaseCompanySoftware(int CaseId, int CompanyId, int SoftwareId)
        {
            return Json(clsBLL.editCaseCompanySoftware(CaseId, CompanyId, SoftwareId), JsonRequestBehavior.AllowGet);
        }
        #endregion

        // View actions
        [Route("Config")]
        public ActionResult Config()
        {
            return View();
        }

        public JsonResult Users()
        {
            ViewBag.Users = true;
            return Json(null, JsonRequestBehavior.AllowGet);
        }


    }
}