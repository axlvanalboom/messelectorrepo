﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using BEC;
using BLL;
using System.IO;
using Newtonsoft.Json;

namespace MES4SME.Controllers
{
    // Collections of all owner functions
    [Authorize(Roles = "Administrator, Owner")]
    public class OwnerController : Controller
    {
        // View
        [Route("Owner")]
        public ActionResult Index()
        {
            return View();
        }

        // Sector
        public JsonResult addSector(clsSector oSector)
        {
            return Json(clsBLL.addSector(oSector), JsonRequestBehavior.AllowGet);
        }
        // Edit company
        public JsonResult editCompany(clsCompany oCompany)
        {
            return Json(clsBLL.editCompany(oCompany, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit software 
        public JsonResult editSoftware(clsSoftware oSoftware)
        {
            return Json(clsBLL.editSoftware(oSoftware, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit case
        public JsonResult editCase(clsCase oCase)
        {
            return Json(clsBLL.editCase(oCase, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit company content
        [ValidateInput(false)]
        public JsonResult editCompanyContent(int Id, string Content)
        {
            return Json(clsBLL.editCompanyContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit software content
        [ValidateInput(false)]
        public JsonResult editSoftwareContent(int Id, string Content)
        {
            return Json(clsBLL.editSoftwareContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit case content
        [ValidateInput(false)]
        public JsonResult editCaseContent(int Id, string Content)
        {
            return Json(clsBLL.editCaseContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Private userLevel function
        private byte userLevel()
        {
            byte bUserLevel = 1;
            if (User.IsInRole("Owner"))
                bUserLevel = 2;
            else if (User.IsInRole("Administrator"))
                bUserLevel = 3;
            return bUserLevel;
        }

        // Get user id
        private string userId()
        {
            return User.Identity.GetUserId();
        }

        // Upload attachement
        [HttpPost]
        public JsonResult uploadFile(HttpPostedFileBase file)
        {
            clsUploadResponse oResponse = new clsUploadResponse();

            try
            {
                if (file.ContentLength > 0)
                {
                    string sFileName = Path.GetFileName(file.FileName);
                    string[] arrsFile = sFileName.Split('.');
                    oResponse.file = new clsFileResponse();
                    string sPath = Path.Combine(Server.MapPath("~/Files"), sFileName);
                    oResponse.file.url = "Files/" + sFileName;
                    oResponse.file.name = arrsFile[0];
                    oResponse.file.extension = arrsFile[1];
                    file.SaveAs(sPath);

                    oResponse.success = true;
                }
            }
            catch (Exception e)
            {
                string sMessage = e.Message;
            }

            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }

        // Upload image
        [HttpPost]
        public JsonResult uploadImage(HttpPostedFileBase image)
        {
            clsUploadResponse oResponse = new clsUploadResponse();

            try
            {
                if (image.ContentLength > 0)
                {
                    string sFileName = Path.GetFileName(image.FileName);
                    oResponse.file = new clsFileResponse();
                    string sPath = Path.Combine(Server.MapPath("~/Images"), sFileName);
                    oResponse.file.url = "Images/" + sFileName;
                    image.SaveAs(sPath);

                    oResponse.success = true;
                }
            }
            catch (Exception e)
            {
                string sMessage = e.Message;
            }

            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }

        // Upload url
        public string uploadUrl(string url)
        {
            clsUploadResponse oResponse = new clsUploadResponse();

            try
            {
                // Get the metadata
                clsMetaInformation oMeta = clsBLL.GetMetaDataFromUrl(url);

                // Fill in metadata into return object
                if (oMeta.HasData)
                {
                    oResponse.meta.description = oMeta.Description;
                    oResponse.meta.title = oMeta.Title;
                    oResponse.meta.image.url = oMeta.ImageUrl;
                    oResponse.success = true;
                }
                else
                    oResponse.success = false;
            }
            catch (Exception e)
            {
                string sMessage = e.Message;
            }

            return JsonConvert.SerializeObject(oResponse);
        }
    }
}