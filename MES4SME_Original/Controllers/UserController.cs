﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using BLL;
using BEC;

namespace MES4SME.Controllers
{
    // Collection off all user data function
    public class UserController : Controller
    {
        // Views
        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Browse()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        // All getters
        public JsonResult getSectors()
        {
            return Json(clsBLL.getSectors(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getActivities()
        {
            return Json(clsBLL.getActivities(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFunctions()
        {
            return Json(clsBLL.getFunctions(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCategories()
        {
            return Json(clsBLL.getCategories(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanies()
        {
            return Json(clsBLL.getCompanies(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanyById(int Id)
        {
            return Json(clsBLL.getCompanyById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwares()
        {
            return Json(clsBLL.getSoftwares(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareById(int Id)
        {
            return Json(clsBLL.getSoftwareById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCases()
        {
            return Json(clsBLL.getCases(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCaseById(int Id)
        {
            return Json(clsBLL.getCaseById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanyContent(int Id)
        {
            return Json(clsBLL.getCompanyContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareContent(int Id)
        {
            return Json(clsBLL.getSoftwareContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCaseContent(int Id)
        {
            return Json(clsBLL.getCaseContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Linked getters

        public JsonResult getCompaniesLinked()
        {
            return Json(clsBLL.getCompaniesLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwaresLinked()
        {
            return Json(clsBLL.getSoftwareLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCasesLinked()
        {
            return Json(clsBLL.getCasesLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Id linked getters
        public JsonResult getCompanyLinkedById(int Id)  
        {
            return Json(clsBLL.getCompanyLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareLinkedById(int Id)
        {
            return Json(clsBLL.getSoftwareLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCaseLinkedById(int Id)
        {
            return Json(clsBLL.getCaseLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Search function
        public JsonResult getSearchResults(clsFullLinks Links) 
        {
            return Json(clsBLL.getFullyLinked(Links, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // User
        public JsonResult editUser(clsUser oUser)
        {
            if (userLevel() == 3 || User.Identity.GetUserId() == oUser.Id)
                return Json(clsBLL.editUser(oUser), JsonRequestBehavior.AllowGet);
            else
                return Json(new clsReturn(0, "You may only edit your own data.", null), JsonRequestBehavior.AllowGet);
        }

        // Private userLevel function
        private byte userLevel()
        {
            byte bUserLevel = 1;
            if (User.IsInRole("Owner"))
                bUserLevel = 2;
            else if (User.IsInRole("Administrator"))
                bUserLevel = 3;
            return bUserLevel;
        }

        // Get user id
        private string userId()
        {
            return User.Identity.GetUserId();
        }
    }
}