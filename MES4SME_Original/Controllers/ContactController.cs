﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BLL;
using MES4SME.Models;

namespace MES4SME.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> SendContactEmail(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                clsBLL.SendContactEmail(model.Sender, model.Subject, model.EmailBody);
                return View("EmailSend");
            }
            return View();
        }
            
    }
}