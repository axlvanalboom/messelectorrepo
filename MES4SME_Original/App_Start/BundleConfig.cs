﻿using System.Web;
using System.Web.Optimization;

namespace MES4SME
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-3.3.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.validate.min"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/default/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/default/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/library").Include(
                      "~/Scripts/library/ajax.js",
                      "~/Scripts/library/ajaxFunctions.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/editorjs").Include(
                      "~/Scripts/library/editorjs/editor.js",
                      "~/Scripts/library/editorjs/editor-image.js",
                      "~/Scripts/library/editorjs/editor-attachements.js",
                      "~/Scripts/library/editorjs/editor-header.js",
                      "~/Scripts/library/editorjs/editor-link.js",
                      "~/Scripts/library/editorjs/editor-quote.js",
                      "~/Scripts/library/editorjs/editor-marker.js",
                      "~/Scripts/library/editorjs/editor-paragraph.js",
                      "~/Scripts/library/editorjs/editor-table.js",
                      "~/Scripts/library/editorjs/editor-list.js",
                      "~/Scripts/library/editorjs/editor-embed.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                      "~/Scripts/library/datatables/datatables.js",
                      "~/Scripts/library/datatables/datatables-buttons.js",
                      "~/Scripts/library/datatables/datatables-select.js",
                      "~/Scripts/library/datatables/datatables-responsive.js",
                      "~/Scripts/library/datatables/datatables-editor.js",
                      "~/Scripts/library/datatables/datatables-bootstrap.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
                      