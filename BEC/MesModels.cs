﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace BEC
{

    #region "Extra"
    // To get no error
    static class Program
    {
        static void Main() { }
    }

    #endregion

    #region "Frontend objects"

    // Response class for editorjs
    public class clsUploadResponse
    {
        public bool success { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string errorMessage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public clsFileResponse file { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public clsUrlResponse meta { get; set; }
        public clsUploadResponse()
        {
            file = new clsFileResponse();
            meta = new clsUrlResponse();
        }
    }

    // Response class for upload file
    public class clsFileResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string url { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string extension { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Fileurl { get; set; }
        public clsFileResponse() { }
    }

    // Response class for upload url
    public class clsUrlResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string title { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string description { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public clsUrlImage image { get; set; }
        public clsUrlResponse()
        {
            image = new clsUrlImage();
        }
    }

    // Reponse image url
    public class clsUrlImage 
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string url { get; set; }
        public clsUrlImage() { }
    }

    // Meta information
    public class clsMetaInformation
    {
        public bool HasData { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string ImageUrl { get; set; }
        public string SiteName { get; set; }

        public clsMetaInformation(string url)
        {
            Url = url;
            HasData = false;
        }

        public clsMetaInformation(string url, string title, string description, string keywords, string imageUrl, string siteName)
        {
            Url = url;
            Title = title;
            Description = description;
            Keywords = keywords;
            ImageUrl = imageUrl;
            SiteName = siteName;
        }
    }

    #endregion

    #region "Data models"

    // Base model class
    public abstract class clsBaseModel : ICloneable, IEquatable<clsBaseModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public clsBaseModel(string sName, string sDescription)
        {
            Name = sName;
            Description = sDescription;
        }
        public clsBaseModel(int iId, string sName, string sDescription)
        {
            Id = iId;
            Name = sName;
            Description = sDescription;
        }
        public clsBaseModel()
        {

        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public bool Equals(clsBaseModel other)
        {
            if (other == null)
                return false;

            return this.Id.Equals(other.Id) &&
                (
                    object.ReferenceEquals(this.Name, other.Name) ||
                    this.Name != null &&
                    this.Name.Equals(other.Name)
                ) &&
                (
                    object.ReferenceEquals(this.Description, other.Description) ||
                    this.Description != null &&
                    this.Description.Equals(other.Description)
                ); 
        }

        public override string ToString()
        {
            return Name;
        }
    }

    // Advanded base model class
    public abstract class clsAdvancedBaseModel : clsBaseModel, IEquatable<clsAdvancedBaseModel>
    {
        public string Website { get; set; }
        public string Content { get; set; }
        public string LogoUrl { get; set; }
        public Boolean Public { get; set; }

        public clsAdvancedBaseModel(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl) : base(iId, sName, sDescription)
        {
            Website = sWebsite;
            LogoUrl = sLogoUrl;
        }

        public clsAdvancedBaseModel()
        {

        }
        public bool Equals(clsAdvancedBaseModel other)
        {
            if (other == null)
                return false;

            return base.Equals(other) && (this.Public == other.Public) &&
                (
                    object.ReferenceEquals(this.Website, other.Website) ||
                    this.Website != null &&
                    this.Website.Equals(other.Website)
                );
        }
    }

    // Link software activity class
    public class clsSoftwareLink : IEquatable<clsSoftwareLink>
    {
        public int LinkId { get; set; }
        public clsBaseModel Activity { get; set; }
        public clsBaseModel Software { get; set; }

        public clsSoftwareLink(int iLinkId, clsBaseModel oActivity, clsBaseModel oSoftware)
        {
            LinkId = iLinkId;
            Activity = oActivity;
            Software = oSoftware;
        }
        public bool Equals(clsSoftwareLink other)
        {
            if (other == null)
                return false;

            return this.LinkId.Equals(other.LinkId) &&
                (
                    object.ReferenceEquals(this.Activity, other.Activity) ||
                    this.Activity != null &&
                    this.Activity.Equals(other.Activity)
                ) &&
                (
                    object.ReferenceEquals(this.Software, other.Software) ||
                    this.Software != null &&
                    this.Software.Equals(other.Software)
                );
        }
    }

    // Link company activity class
    public class clsCompanyLink : IEquatable<clsCompanyLink>
    {
        public int LinkId { get; set; }
        public clsBaseModel Activity { get; set; }
        public clsBaseModel Company { get; set; }
        public clsCompanyLink(int iLinkId, clsBaseModel oActivity, clsBaseModel oCompany)
        {
            LinkId = iLinkId;
            Activity = oActivity;
            Company = oCompany;
        }
        public bool Equals(clsCompanyLink other)
        {
            if (other == null)
                return false;

            return this.LinkId.Equals(other.LinkId) &&
                (
                    object.ReferenceEquals(this.Activity, other.Activity) ||
                    this.Activity != null &&
                    this.Activity.Equals(other.Activity)
                ) &&
                (
                    object.ReferenceEquals(this.Company, other.Company) ||
                    this.Company != null &&
                    this.Company.Equals(other.Company)
                );
        }
    }

    // Link case sector class
    public class clsCaseLink : IEquatable<clsCaseLink>
    {
        public int LinkId { get; set; }
        public clsBaseModel Case { get; set; }
        public clsBaseModel Sector { get; set; }
        public clsCaseLink(int iLinkId, clsBaseModel oCase, clsBaseModel oSector)
        {
            LinkId = iLinkId;
            Case = oCase;
            Sector = oSector;
        }
        public bool Equals(clsCaseLink other)
        {
            if (other == null)
                return false;

            return this.LinkId.Equals(other.LinkId) &&
                (
                    object.ReferenceEquals(this.Case, other.Case) ||
                    this.Case != null &&
                    this.Case.Equals(other.Case)
                ) &&
                (
                    object.ReferenceEquals(this.Sector, other.Sector) ||
                    this.Sector != null &&
                    this.Sector.Equals(other.Sector)
                );
        }
    }

    // Return model
    public class clsReturn
    {
        public byte Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public clsReturn(byte bStatus, string sMessage, object oData)
        {
            Status = bStatus;
            Message = sMessage;
            Data = oData;
        }

        public clsReturn()
        {
        }
    }

    // Activity model
    public class clsActivity : clsBaseModel
    {
        public clsActivity() { }
        public clsActivity(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public bool Equals(clsActivity other) { return base.Equals(other); }
    }

    // Sector model
    public class clsSector : clsBaseModel
    {
        public clsSector() { }
        public clsSector(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public bool Equals(clsActivity other) { return base.Equals(other); }
    }
        
    // Company model
    public class clsCompany : clsAdvancedBaseModel
    {
        public clsCompany(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl, string sEmail, string sTelephone, string sAddress) : base(iId, sName, sDescription, sWebsite, sLogoUrl)
        {
            Email = sEmail;
            Telephone = sTelephone;
            Address = sAddress;
        }

        public clsCompany() { }

        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
    }

    // Linked company model
    public class clsCompanyLinked : clsCompany
    {
        public List<clsUser> Contacts { get; set; }
        public List<clsSoftwareLink> SoftwareActivities { get; set; }

        public List<clsCaseLink> CasesSectors { get; set; }
        public clsCompanyLinked(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl, string sEmail, string sTelephone, string sAddress) : base(iId, sName, sDescription, sWebsite, sLogoUrl, sEmail, sTelephone, sAddress)
        {
            SoftwareActivities = new List<clsSoftwareLink>();
            CasesSectors = new List<clsCaseLink>();
            Contacts = new List<clsUser>();
        }
        public clsCompanyLinked(clsCompany oCompany) : base(oCompany.Id, oCompany.Name, oCompany.Description, oCompany.Website, oCompany.LogoUrl, oCompany.Email, oCompany.Telephone, oCompany.Address)
        {
            Public = oCompany.Public;
            SoftwareActivities = new List<clsSoftwareLink>();
            CasesSectors = new List<clsCaseLink>();
            Contacts = new List<clsUser>();
        }
        public clsCompanyLinked()
        {
            SoftwareActivities = new List<clsSoftwareLink>();
            CasesSectors = new List<clsCaseLink>();
            Contacts = new List<clsUser>();
        }
    }


    // Software model
    public class clsSoftware : clsAdvancedBaseModel
    {
        public clsSoftware(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl) : base(iId, sName, sDescription, sWebsite, sLogoUrl) { }

        public clsSoftware() { }
    }

    // Software models with all links
    public class clsSoftwareLinked : clsSoftware
    {
        public List<clsCompanyLink> CompanyActivities { get; set; }

        public List<clsCategoryFunctions> CategoryFunctions { get; set; }

        public List<clsCaseLink> CasesSectors { get; set; }

        public clsSoftwareLinked(int iId, string sName, string sDescription, string sWebsite, string sAddress) : base(iId, sName, sDescription, sWebsite, sAddress)
        {
            CompanyActivities = new List<clsCompanyLink>();
            CategoryFunctions = new List<clsCategoryFunctions>();
            CasesSectors = new List<clsCaseLink>();
        }

        public clsSoftwareLinked(clsSoftware oSoftware) : base(oSoftware.Id, oSoftware.Name, oSoftware.Description, oSoftware.Website, oSoftware.LogoUrl)
        {
            Public = oSoftware.Public;
            CompanyActivities = new List<clsCompanyLink>();
            CategoryFunctions = new List<clsCategoryFunctions>();
            CasesSectors = new List<clsCaseLink>();
        }

        public clsSoftwareLinked()
        {
            CompanyActivities = new List<clsCompanyLink>();
            CategoryFunctions = new List<clsCategoryFunctions>();
            CasesSectors = new List<clsCaseLink>();
        }

       
    }


    // Case model
    public class clsCase : clsAdvancedBaseModel
    {
        public clsCase(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl) : base(iId, sName, sDescription, sWebsite, sLogoUrl) { }
        public clsCase() { }
    }

    // Linked case model
    public class clsCaseLinked : clsCase
    {
        public clsSoftware Software { get; set; }
        public clsSector Sector { get; set; }
        public clsCompany Company { get; set; }
        public clsActivity Activity { get; set; }
        public List<clsCategoryFunctions> CategoryFunctions { get; set; }
        public clsCaseLinked(int iId, string sName, string sDescription, string sWebsite, string sLogoUrl) : base(iId, sName, sDescription, sWebsite, sLogoUrl)
        {
            CategoryFunctions = new List<clsCategoryFunctions>();
        }
        public clsCaseLinked(clsCase oCase) : base(oCase.Id, oCase.Name, oCase.Description, oCase.Website, oCase.LogoUrl) 
        {
            Public = oCase.Public;
            CategoryFunctions = new List<clsCategoryFunctions>();
        }
        public clsCaseLinked()
        {
            CategoryFunctions = new List<clsCategoryFunctions>();
        }
    }

    // Function model
    public class clsFunction : clsBaseModel
    {
        public clsFunction() { }
        public clsFunction(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public bool Equals(clsActivity other) { return base.Equals(other); }
    }

    // Category model
    public class clsCategory : clsBaseModel
    {
        public clsCategory(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public bool Equals(clsActivity other) { return base.Equals(other); }
        public clsCategory() {  }
    }

    // Category model with the functions per category inside
    public class clsCategoryFunctions : clsCategory
    {
        public List<clsFunction> Functions { get; set; }
        public clsCategoryFunctions(int iId, string sName, string sDescription) : base(iId, sName, sDescription)
        {
            Functions = new List<clsFunction>();
        }

        public clsCategoryFunctions(clsCategory oCategory) : base()
        {
            Id = oCategory.Id;
            Name = oCategory.Name;
            Description = oCategory.Description;
            Functions = new List<clsFunction>();
        }

        public clsCategoryFunctions()
        {
            Functions = new List<clsFunction>();
        }

    }

    // User class
    public class clsUser : ICloneable
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Occupation { get; set; }
        public bool EmailConfirmed { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    // Linked user 
    public class clsUserLinked : clsUser
    {
        public clsUserLinked(clsUser oUser)
        {
            Id = oUser.Id;
            Email = oUser.Email;
            PhoneNumber = oUser.PhoneNumber;
            UserName = oUser.UserName;
            Occupation = oUser.Occupation;
            EmailConfirmed = oUser.EmailConfirmed;
        }
        public clsCompany Company { get; set; }
        public clsRole Role { get; set; }
    }

    // Role model
    public class clsRole : clsBaseModel
    {
        public clsRole(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    #endregion

    #region "Link models"

    // Return model for link query
    public class clsLink
    {
        public int Id { get; set; }
        public int Company { get; set; }
        public int Activity { get; set; }
        public int Mes { get; set; }
        public int Sector { get; set; }
        public int Case { get; set; }
        public int Category { get; set; }
        public int Function { get; set; }
        public int Role { get; set; }
        public string User { get; set; }
    }   
    // Get model for link query
    public class clsLinks
    {
        public List<int> Company { get; set; }
        public List<int> Activity { get; set; }
        public List<int> Mes { get; set; }
        public List<int> Sector { get; set; }
        public List<int> Case { get; set; }
        public List<int> Category { get; set; }
        public List<int> Function { get; set; }
        public List<int> Role { get; set; }
        public List<string> User { get; set; }

        public clsLinks()
        {
            Company = new List<int>();
            Activity = new List<int>();
            Mes = new List<int>();
            Sector = new List<int>();
            Case = new List<int>();
            Category = new List<int>();
            Function = new List<int>();
            Role = new List<int>();
            User = new List<string>();
        }
    }

    public class clsFullLinks
    {
        public List<int> Company { get; set; }
        public List<int> Activity { get; set; }
        public List<int> Mes { get; set; }
        public List<int> Sector { get; set; }
        public List<int> Case { get; set; }
        public List<int> MesCategory { get; set; }
        public List<int> MesFunction { get; set; }
        public List<int> CaseCategory { get; set; }
        public List<int> CaseFunction { get; set; }
        public List<string> String { get; set; }
    }
    public class clsFullLink
    {
        public int Company { get; set; }
        public int Activity { get; set; }
        public int Mes { get; set; }
        public int Sector { get; set; }
        public int Case { get; set; }
        public int MesCategory { get; set; }
        public int MesFunction { get; set; }
        public int CaseCategory { get; set; }
        public int CaseFunction { get; set; }
    }
    public class clsFullyLinked
    {
        public List<clsBaseModel> Company { get; set; }
        public List<clsBaseModel> Mes { get; set; }
        public List<clsBaseModel> Case { get; set; }
        public clsFullyLinked()
        {
            Company = new List<clsBaseModel>();
            Mes = new List<clsBaseModel>();
            Case = new List<clsBaseModel>();
        }
    }
    #endregion
}




