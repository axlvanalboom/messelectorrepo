﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEC
{
    public class clsLog
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string IdVisible { get; set; }

        [Required]
        public string Time { get; set; }

        [Required]
        public string Type { get; set; }

        public string Parameter { get; set; }
    }

    public class clsDayAmountVisitors
    {
        [Required]
        public int Amount { get; set; }
        [Required]
        public string Day { get; set; }

        public List<string> Users { get; set; }
    }

    public class clsUserViews
    {
        public string UserId { get; set; }
        public List<clsViews> Views { get; set; }
    }
    public class clsViews
    {
        public clsViews(string sTime, string sType, string sParameter = null)
        {
            Time = sTime;
            Type = sType;
            Parameter = sParameter;
        }
        public string Time { get; set; }
        public string Type { get; set; }
        public string Parameter { get; set; }

    }

    public class StringInt
    { 
        public string Text { get; set; }
        public int Number { get; set; }
    }

}
