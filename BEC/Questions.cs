﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NCalc;

namespace BEC
{
    
    public static class QuestionHelp
    {
        private static Dictionary<eComparisonOperator, string> desTranslation = new Dictionary<eComparisonOperator, string>()
        {
            [eComparisonOperator.Equal] = "==",
            [eComparisonOperator.NotEqual] = "!=",
            [eComparisonOperator.LessThan] = "<",
            [eComparisonOperator.LessThanOrEqualTo] = "<=",
            [eComparisonOperator.GreaterThan] = ">",
            [eComparisonOperator.GreaterThanOrEqualTo] = ">="
        };

        private static Dictionary<string, eComparisonOperator> desReverseTranslation = new Dictionary<string, eComparisonOperator>()
        {
            ["=="] = eComparisonOperator.Equal,
            ["!="] = eComparisonOperator.NotEqual,
            ["<"] = eComparisonOperator.LessThan,
            ["<="] = eComparisonOperator.LessThanOrEqualTo,
            [">"] = eComparisonOperator.GreaterThan,
            [">="] = eComparisonOperator.GreaterThanOrEqualTo     
        };

        public static Dictionary<eComparisonOperator, string> Translation()
        {
            return desTranslation;
        }
        public static Dictionary<string, eComparisonOperator> ReverseTranslation()
        {
            return desReverseTranslation;
        }

        public static eComparisonOperator Translate(string sOperator)
        {
            if (desReverseTranslation.ContainsKey(sOperator))
                return desReverseTranslation[sOperator];
            throw new Exception("wrong key");
        }

        private static Random random = new Random();
        public static string GetRandomId()
        {
            try
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, 200)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
            catch { return null; }
        }
    }

    public class clsAnswerMC : clsBaseModel
    {
        public clsAnswerMC() { }
        public clsAnswerMC(string sName, string sDescription) : base(sName, sDescription) { }
        public clsAnswerMC(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    public class clsQuestionCategory : clsBaseModel
    {
        public clsQuestionCategory() { }
        public clsQuestionCategory(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public clsQuestionCategory(string sName, string sDescription) : base(sName, sDescription) { }
    }

    public class clsQuestionFilter 
    {
        public int Value { get; set; }
        //public clsQuestionCategory Category { get; set; }
        public int CategoryId { get; set; }
    }

    public abstract class clsAbstractQuestions : clsBaseModel
    {
        public int CategoryMinimum { get; set; }
        public int CategoryMaximum { get; set; }
        public clsQuestionCategory Category { get; set; }
        public clsAbstractQuestions() { }
        public clsAbstractQuestions(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public clsAbstractQuestions(int iId, string sName, string sDescription, clsQuestionCategory oCategory, int iCategoryMinimum, int iCategoryMaximum) : base(iId, sName, sDescription) 
        { 
            Category = oCategory;
            CategoryMinimum = iCategoryMinimum;
            CategoryMaximum = iCategoryMaximum;
        }
    }

    public class clsQuestionRange : clsAbstractQuestions
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public clsQuestionRange() { }
        public clsQuestionRange(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public clsQuestionRange(int iId, string sName, string sDescription, clsQuestionCategory oCategory, int iCategoryMinimum, int iCategoryMaximum, int iMin, int iMax) : base(iId, sName, sDescription, oCategory, iCategoryMinimum, iCategoryMaximum) 
        {
            Minimum = iMin;
            Maximum = iMax;
        }
    }

    public class clsQuestionMC: clsAbstractQuestions
    {
        public List<clsAnswerMC> PossibleAnswers { get; set; }
        public clsQuestionMC() { }
        public clsQuestionMC(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
        public clsQuestionMC(int iId, string sName, string sDescription, clsQuestionCategory oCategory, int iCategoryMinimum, int iCategoryMaximum, List<clsAnswerMC> loPossibleAnswers) : base(iId, sName, sDescription, oCategory, iCategoryMinimum, iCategoryMaximum) { PossibleAnswers = loPossibleAnswers; }
    }

    public class clsSoftwareWithAnswers : clsSoftware
    {
        public List<clsAbstractSoftwareAnswer> Answers { get; set; }

        public clsSoftwareWithAnswers(int iId, string sName, string sDescription, string sWebsite, string sAddress) : base(iId, sName, sDescription, sWebsite, sAddress)
        {
        }

        public clsSoftwareWithAnswers(clsSoftware oSoftware) : base(oSoftware.Id, oSoftware.Name, oSoftware.Description, oSoftware.Website, oSoftware.LogoUrl)
        {
            Public = oSoftware.Public;
        }

        public clsSoftwareWithAnswers()
        {
        }
    }

    public abstract class clsAbstractSoftwareAnswer : clsBaseModel 
    {
        private string _sName;
        public new string Name { get { return _sName; } set { _sName = Guid.NewGuid().ToString(); } }
        public int QuestionId { get; protected set; }
        public clsAbstractSoftwareAnswer() { }
        public clsAbstractSoftwareAnswer(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    public class clsSoftwareAnswerRange: clsAbstractSoftwareAnswer
    {
        public int Value { get; set; }

        private clsQuestionRange _Question;
        public clsQuestionRange Question { get { return _Question; } set { _Question = value; QuestionId = value.Id; } }

        public clsSoftwareAnswerRange() { }
        public clsSoftwareAnswerRange(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }
    public class clsSoftwareAnswerMC : clsAbstractSoftwareAnswer
    {
        public clsAnswerMC Answer { get; set; }
        private clsQuestionMC _Question;
        public clsQuestionMC Question { get { return _Question; } set { _Question = value; QuestionId = value.Id; } }

        public clsSoftwareAnswerMC() { }
        public clsSoftwareAnswerMC(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    // classes below not needed in db

    public abstract class clsAbstractUserAnswer : clsBaseModel
    {
        //public virtual clsAbstractQuestions Question { get; set; }
        public clsAbstractUserAnswer() { }
        public clsAbstractUserAnswer(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    public class clsUserAnswerMC: clsAbstractUserAnswer
    {
        public clsAnswerMC Answer { get; set; }
        public clsQuestionMC Question { get; set; }
        public clsUserAnswerMC() { }
        public clsUserAnswerMC(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    public class clsUserAnswerRange : clsAbstractUserAnswer
    {
        public int Value { get; set; }
        public eComparisonOperator Operator { get; set; }
        public clsQuestionRange Question { get; set; }
        public clsUserAnswerRange() { }
        public clsUserAnswerRange(int iId, string sName, string sDescription) : base(iId, sName, sDescription) { }
    }

    public enum eComparisonOperator
    {
        GreaterThan,
        LessThan,
        Equal,
        GreaterThanOrEqualTo,
        LessThanOrEqualTo,
        NotEqual
    }

    public class clsUserOverview
    {
        public string User { get; private set; }
        public DateTime ExpireDate { get; private set; }
        public List<clsUserSoftwareOverview> Overviews { get; private set; }
        public clsUserOverview()
        {
            var dtNow = DateTime.Now;
            ExpireDate = dtNow.AddHours(1);
            Overviews = new List<clsUserSoftwareOverview>();
            User = QuestionHelp.GetRandomId();
        }
        public void AddOverview(clsUserSoftwareOverview oOverview)
        {
            if (oOverview == null) throw new ArgumentNullException(nameof(oOverview));
            Overviews.Add(oOverview);
        }
    }

    public class clsUserSoftwareOverview
    {
        public double Percentage { get; private set; }
        public clsSoftware Software { get; private set; }
        public List<clsUserSoftwareAnswerMatch> Answers { get; private set; }

        public clsUserSoftwareOverview(clsSoftware oSoftware)
        {
            if (oSoftware == null) throw new ArgumentNullException(nameof(oSoftware));
            Software = oSoftware;
            Answers = new List<clsUserSoftwareAnswerMatch>();
        }
        public void AddAnswer(clsUserSoftwareAnswerMatch oAnswer)
        {
            if (oAnswer == null) throw new ArgumentNullException(nameof(oAnswer));
            Answers.Add(oAnswer);
            CalculatePercentage();
        }
        private void CalculatePercentage()
        {
            if (Answers.Count() == 0) return;

            var iMatches = 0;
            Answers.ForEach(a =>
            {
                if (a.Match)
                    iMatches++;
            });
            Percentage = 100 * iMatches / Answers.Count();
        }

        public override string ToString()
        {
            return Software + "("+ Math.Round(Percentage, 2).ToString() + "%)";
        }
    }

    public class clsUserSoftwareAnswerMatch
    {
        public bool Match { get; private set; }
        public clsAbstractSoftwareAnswer Software { get; private set; }
        public clsAbstractUserAnswer User { get; private set; }

        public clsUserSoftwareAnswerMatch(clsSoftwareAnswerMC oSoftware, clsUserAnswerMC oUser)
        {
            if (oSoftware == null) throw new ArgumentNullException(nameof(oSoftware));
            if (oUser == null) throw new ArgumentNullException(nameof(oUser));

            //check
            if (!(oSoftware.Question.Equals(oUser.Question)))
                throw new Exception("Software and User question don't match!");

            Match = false;
            if (oSoftware.Answer.Equals(oUser.Answer))
                Match = true;

            Software = oSoftware;
            User = oUser;
        }

        public clsUserSoftwareAnswerMatch(clsQuestionRange oQuestion, clsSoftwareAnswerRange oSoftware, clsUserAnswerRange oUser)
        {
            if (oSoftware == null) throw new ArgumentNullException(nameof(oSoftware));
            if (oUser == null) throw new ArgumentNullException(nameof(oUser));

            //check
            if (!oSoftware.Question.Equals(oQuestion) || !oUser.Question.Equals(oQuestion))
                throw new Exception("Software and User question don't match!");


            var desTranslation = QuestionHelp.Translation();

            var sValidation = oSoftware.Value.ToString() + desTranslation[oUser.Operator] + oUser.Value;

            Expression e = new Expression(sValidation);

            Match = false;
            if ((bool)e.Evaluate())
                Match = true;

            Software = oSoftware;
            User = oUser;
        }
        
    }

    /*
    public class clsUserSoftwareOverviewSimple
    {
        public string Software { get; set; }
        public double Percentage { get; set; }
    }
    */

    //to be removed?
    public class clsMatch
    {
        public clsAbstractQuestions Question { get; private set; }
        public bool Match { get; private set; }
        
        public clsMatch(clsAbstractQuestions oQuestion, bool xMatch)
        {
            if (oQuestion == null) throw new ArgumentNullException(nameof(oQuestion));
            Question = oQuestion;
            Match = xMatch;
        }

        public clsMatch(clsQuestionMC oQuestion, clsSoftwareAnswerMC oSoftware, clsUserAnswerMC oWanted)
        {
            if (oQuestion == null) throw new ArgumentNullException(nameof(oQuestion));
            if (oSoftware == null) throw new ArgumentNullException(nameof(oSoftware));
            if (oWanted == null) throw new ArgumentNullException(nameof(oWanted));

            //check
            if (oSoftware.Question != oQuestion || oWanted.Question != oQuestion)
                throw new Exception("wrong setup");

            Question = oQuestion;
            Match = false;
            if (oSoftware.Answer == oWanted.Answer)
                Match = true;
        }

        public clsMatch(clsQuestionRange oQuestion, clsSoftwareAnswerRange oSoftware, clsUserAnswerRange oWanted)
        {
            if (oQuestion == null) throw new ArgumentNullException(nameof(oQuestion));
            if (oSoftware == null) throw new ArgumentNullException(nameof(oSoftware));
            if (oWanted == null) throw new ArgumentNullException(nameof(oWanted));

            //check
            if (oSoftware.Question != oQuestion || oWanted.Question != oQuestion)
                throw new Exception("wrong setup");

            Question = oQuestion;
            Match = false;

            var desTranslation = new Dictionary<eComparisonOperator, string>()
            {
                [eComparisonOperator.Equal] = "==",
                [eComparisonOperator.NotEqual] = "!=",
                [eComparisonOperator.LessThan] = "<",
                [eComparisonOperator.LessThanOrEqualTo] = "<=",
                [eComparisonOperator.GreaterThan] = ">",
                [eComparisonOperator.GreaterThanOrEqualTo] = ">="
            };

            var sValidation = oSoftware.Value.ToString() + desTranslation[oWanted.Operator] + oWanted.Value;

            Expression e = new Expression(sValidation);

            if ((bool)e.Evaluate())
                Match = true;
            
        }
    }


    public class clsInternalResultMC
    {
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
    }
    public class clsInternalResultRange
    {
        public int QuestionId { get; set; }
        public int Value { get; set; }
        public string Operator { get; set; }
    }

}
