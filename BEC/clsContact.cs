﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEC
{
    public class clsContacts
    {
        public List<clsContact> To { get; set; }
        public List<clsContact> Cc { get; set; }
        public List<clsContact> Bcc { get; set; }
    }
    public class clsContact
    {
        public string Email { get; set; }
        public string Verdeling { get; set; }
        public int Id { get; set; }
    }
}
