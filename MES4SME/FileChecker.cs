﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;

namespace MES4SME
{
    public class FileChecker
    {

        public static string CheckImage(string sPath, string sFileName)
        {
            try
            {
                byte[] arroImage = File.ReadAllBytes(sPath);

                Bitmap bmp;
                using (var ms = new MemoryStream(arroImage))
                {
                    bmp = new Bitmap(ms);

                    string sSavePath = "FileChecking\\" + sFileName;

                    bmp.Save(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory + sSavePath), ImageFormat.Png);
                    return sSavePath;
                }
            }
            catch (Exception e)
            {

                throw new Exception("This file can't be uploaded!");
            }


        }

        public static bool CheckPdf(string sPath)
        {
            try
            {
                byte[] arroFile = File.ReadAllBytes(sPath);

                if (arroFile == null || arroFile.Length <= 4)
                { return false; }

                //each pdf file starts with %PDF-
                if (!(arroFile[0] == 0x25 && arroFile[1] == 0x50 && arroFile[2] == 0x44 && arroFile[3] == 0x46 && arroFile[4] == 0x2D))
                { return false; }

                //folowed by the version
                //possible versions: 1.0,1.1,1.2,1.3,1.4.1.5,1.6,1.7 en ?2.0?
                //implemented: 1.1-1.7
                if (!(arroFile[5] == 0x31 && arroFile[6] == 0x2E))
                { return false; }
                if (!(arroFile[7] == 0x31 || arroFile[7] == 0x32 || arroFile[7] == 0x33 || arroFile[7] == 0x34 || arroFile[7] == 0x35 || arroFile[7] == 0x36 || arroFile[7] == 0x37))
                { return false; }

                //each pdf files end with: %%EOF(.)(.) (sometimes extra dots are added
                int iLength = arroFile.Length;

                //check for %%EOL
                if (arroFile[iLength - 5] == 0x25 &&
                    arroFile[iLength - 4] == 0x25 &&
                    arroFile[iLength - 3] == 0x45 &&
                    arroFile[iLength - 2] == 0x4F &&
                    arroFile[iLength - 1] == 0x46)
                { return true; }
                else
                {
                    //check for %%EOL.
                    if (arroFile[iLength - 6] == 0x25 &&
                        arroFile[iLength - 5] == 0x25 &&
                        arroFile[iLength - 4] == 0x45 &&
                        arroFile[iLength - 3] == 0x4F &&
                        arroFile[iLength - 2] == 0x46 &&
                        (arroFile[iLength - 1] == 0x0D || arroFile[iLength - 1] == 0x0A))
                    { return true; }
                    else
                    {
                        //check for %%EOL..
                        if (arroFile[iLength - 7] == 0x25 &&
                            arroFile[iLength - 6] == 0x25 &&
                            arroFile[iLength - 5] == 0x45 &&
                            arroFile[iLength - 4] == 0x4F &&
                            arroFile[iLength - 3] == 0x46 &&
                            arroFile[iLength - 2] == 0x0D &&
                            arroFile[iLength - 1] == 0x0A)
                        { return true; }
                        else { return false; }
                    }
                }

            }
            catch (Exception)
            {

                throw new Exception("This file can't be uploaded!");
            }


        }

        public static bool FileComparison(string sPath, string sFileToCompare)
        {
            byte[] oFile = File.ReadAllBytes(sPath);
            byte[] oCompare = File.ReadAllBytes(sFileToCompare);
            if (oFile.SequenceEqual(oCompare))
                return true;

            return false;
        }
        public static string FileComparison(string sPath, List<string> oFilesToCompare)
        {
            byte[] oFile = File.ReadAllBytes(sPath);

            foreach (string s in oFilesToCompare)
            {
                byte[] oCompare = File.ReadAllBytes(s);
                if (oFile.SequenceEqual(oCompare))
                    return s;
            }

            return null;
        }

        /// <summary>
        /// Checks the given file if it contains a virus
        /// returns TRUE if the file contains a virus
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static bool CheckVirus(string sPath)
        {
            try
            {

                System.Diagnostics.Process process = new System.Diagnostics.Process();

                process.StartInfo = new System.Diagnostics.ProcessStartInfo();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.FileName = "C:\\Program Files\\Windows Defender\\MpCmdRun.exe";
                process.StartInfo.Arguments = "-Scan -ScanType 3 -File \"" + sPath + "\"";

                process.Start();

                // Synchronously read the standard output of the spawned process. 
                string output = process.StandardOutput.ReadToEnd();

                process.WaitForExit();

                return !output.Contains("no threats");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                return false;
            }
        }
    
    }
}