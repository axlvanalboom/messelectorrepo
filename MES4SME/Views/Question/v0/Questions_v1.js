﻿//function DisplayQuestionResultOverview(iQuestions, iPartners) {
//    var sContent = '';

//    sContent += `
//                <div class="tableFixHead">
//                    <table class="table table-bordered">
                        
//                        <thead>
//                            <tr>
//                                <th>Questions</th>
//                                <th>Answers</th>`;
//    for (i = 0; i < iPartners; i++) {
//        sContent += `<th> <p>Partner ` + i + `<p><p>` + (Math.round((Math.random() * 101 + Number.EPSILON) * 100) / 100)+`</p></th>`;
//    }
    
//    sContent += `
//                            </tr>
//                        </thead>
//                        <tbody>`;
//    for (i = 0; i < iQuestions; i++) {
//        sContent += `   
//                            <tr>
//                                <td>Q`+i+`</td>
//                                <td>A`+ i + `</td>`;

//        for (j = 0; j < iPartners; j++) {
//            if (Math.random() < 0.5) {
//                sContent += `<td><i class="far fa-times-circle" style="color:red; font-size: 2em;" aria-hidden="true"></i></td>`;
//            }
//            else {
//                sContent += `<td><i class="far fa-check-circle" style="color:green; font-size: 2em;" aria-hidden="true"></i></td>`;
//            }
//        }
//        sContent += "</tr>";

//    }
                            
//    sContent +=   `                  
//                        </tbody>
//                    </table>
//                </div>
//                `;


//    $("#Extra").append(sContent);
//    $("#Extra").show();

//}

function DisplayQuestionResultOverview(result) {
    var sContent = '';

    var iHead = 2;

    sContent += `
                <div class="tableFixHead">
                    <table class="table table-bordered">
                        <thead>`;

    var iRows = result[0].length;
    var iColumns = result.length;

    var prefix = "th"

    for (i = 0; i < iRows; i++) {
        if (i == iHead) {
            sContent += `</thead>
                        <tbody>`;
            prefix = "td"
        }
        sContent += "<tr>"
        for (j = 0; j < iColumns; j++) {
            sContent += "<" + prefix + ">";
            var r = result[j][i];
            if (typeof r === 'string' || r instanceof String) {
                sContent += r ;
            }
            else {
                if (r.Match) {
                    sContent += `<i class="far fa-check-circle" style="color:green; font-size: 2em;" aria-hidden="true"></i>`;
                }
                else {
                    sContent += ` <i class="far fa-times-circle" style="color: red; font-size: 2em;" aria-hidden="true"></i>`;
                }
            }
            sContent +="</" + prefix + ">"
        }
        sContent += "</tr>"
    }

    sContent += `                  
                        </tbody>
                    </table>
                </div>
                `;
  
    $("#Extra").append(sContent);
    $("#Extra").show();

}

function GetTestData() {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Extra/TestData",
        success: function (result) {
            DisplayQuestionResultOverview(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

}

function updateTextInput(id, val) {
    console.log(id);
    document.getElementById(id).value = val;
}

function onRangeChange(rangeInputElmt, listener) {

    var inputEvtHasNeverFired = true;

    var rangeValue = { current: undefined, mostRecent: undefined };

    rangeInputElmt.addEventListener("input", function (evt) {
        inputEvtHasNeverFired = false;
        rangeValue.current = evt.target.value;
        if (rangeValue.current !== rangeValue.mostRecent) {
            listener(evt);
        }
        rangeValue.mostRecent = rangeValue.current;
    });

    rangeInputElmt.addEventListener("change", function (evt) {
        if (inputEvtHasNeverFired) {
            listener(evt);
        }
    });

}

function ShowQuestions(result) {
    var sContent = ``;

    for (i = 0; i < result.length; i++) {
        var r = result[i];

        sContent += `<div id="question_` + i +`">
                        <button class="collapsible" id="` + i + `" data-applicable="N">` + r.Name +`</button>
                        <div class="content">
                            <p>` + r.Description +`</p>
                            <form>`;

        if (r.PossibleAnswers != null) {
            for (j = 0; j < r.PossibleAnswers.length; j++) {
                var a = r.PossibleAnswers[j]
                sContent += `   <input type="radio" name="choice` + i + `" value="` + a.Name + `"> ` + a.Name + ` `;
            }
        }
        else {
            sContent += `
                                
                                <input type="range" name="rangeInput" min="` + a.Minimum + `" max="` + a.Maximum + `" onchange="updateTextInput('textInput` + i + `', this.value);">
                                <input type="text" id="textInput` + i + `" value="">
                                <select>
                                    <option value="=">=</option>
                                    <option value="!=">!=</option>
                                    <option value=">">></option>
                                    <option value=">=">>=</option>
                                    <option value="<"><</option>
                                    <option value="<="><=</option>
                                </select>
                    `;
        } 

         sContent += `      </form>
                        </div>
                    </div>`;
    }



    $("#Questions").append(sContent);
    $("#Questions").show();
    SetUp();
}

function GetQuestions() {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Extra/GetQuestions",
        success: function (result) {
            ShowQuestions(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}


function SetUp() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            console.log(this.id);
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            var d = document.getElementById("question_" + this.id);
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
                d.dataset.applicable = "N";
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
                d.dataset.applicable = "y";
            }

        });
    }
}

$(function () {
    //GetTestData();
    GetQuestions();
});
