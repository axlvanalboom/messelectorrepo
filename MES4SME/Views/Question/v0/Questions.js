﻿
function updateTextInput(id, val) {
    document.getElementById(id).innerHTML = val;
}
//todo: delete
function onRangeChange(rangeInputElmt, listener) {

    var inputEvtHasNeverFired = true;

    var rangeValue = { current: undefined, mostRecent: undefined };

    rangeInputElmt.addEventListener("input", function (evt) {
        inputEvtHasNeverFired = false;
        rangeValue.current = evt.target.value;
        if (rangeValue.current !== rangeValue.mostRecent) {
            listener(evt);
        }
        rangeValue.mostRecent = rangeValue.current;
    });

    rangeInputElmt.addEventListener("change", function (evt) {
        if (inputEvtHasNeverFired) {
            listener(evt);
        }
    });

}

var dsoQuestions = {};
var dsxApplicable = {};
var dsoMC = {};

function changeApplicable(id) {
    var x = dsxApplicable[id];
    dsxApplicable[id] = !x;
    //console.log("toggled: " + id);
}

function ShowQuestions(result) {

    dsoQuestions = {};

    var sContent = `<div id="questions">`;

    for (i = 0; i < result.length; i++) {
        var r = result[i];

        sContent += `<div id="question_` + i + `" style="padding:0.5rem;">
                        <button class="collapsible" id="button_` + i + `" onclick=changeApplicable("button_` + i + `")>` + r.Name +`</button>
                        <div class="content" id="content_` + i + `">
                            <p>` + r.Description +`</p>
                            <div id="choice_` + i + `">`;

        if (r.PossibleAnswers != null) {
            for (j = 0; j < r.PossibleAnswers.length; j++) {
                var a = r.PossibleAnswers[j]
                sContent += `   <input type="radio" name="choice_` + i + `" value="radio_` + i + `_`+j+`"> ` + a.Name + ` `;
                dsoMC[`radio_` + i + `_` + j] = a;

            }
        }
        else {
            sContent += `
                                <div class="row" id="row_` + i + `">
                                    <div class="col-md-1" id="column_1_` + i + `">
                                        <select id="select_` + i + `">
                                            <option value="=">=</option>
                                            <option value="!=">!=</option>
                                            <option value=">">></option>
                                            <option value=">=">>=</option>
                                            <option value="<"><</option>
                                            <option value="<="><=</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1" id="column_2_` + i + `">
                                        <p id="textInput` + i + `"></p>
                                    </div>
                                    <div class="col-md-10" id="column_3_` + i + `">
                                        <input type="range" id="range` + i + `" name="rangeInput" min="` + r.Minimum + `" max="` + r.Maximum + `" onchange="updateTextInput('textInput` + i + `',this.value)" oninput="updateTextInput('textInput` + i +`',this.value)">
                                    </div>
                                </div>
                                <div class="row" style="height: 5px;"></div>
                    `;

        } 

        dsoQuestions["question_" + i] = r;
        dsxApplicable["button_" + i] = false;

        sContent += `          </div>
                            </div>
                        </div>`;

    }

    sContent += `</div> 
                <button class="btn btn-primary" id="submit" onclick="analyze()">Submit Quiz</button>`;

    $("#Main").append(sContent);
    $("#Main").show();

    SetUp();
}

function GetQuestions() {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Extra/GetQuestions",
        success: function (result) {
            //console.log(result);
            ShowQuestions(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}

function SetUp() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            //console.log(this.id);
            this.classList.toggle("active");
            var content = this.nextElementSibling;

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight+ "px";
            }

        });
    }

}

function analyze() {
    var children = $("#questions").children("div");

    var MC = [];
    var Range = [];

    for (i = 0; i < children.length; i++) {
        var c = children[i];
        var b = $("#" + c.id).children("button")[0];
        var xApplicable = dsxApplicable[b.id]

        if (xApplicable) {
            var q = dsoQuestions[c.id];

            if (q.PossibleAnswers != null) {

                var content = $("#"+c.id).children("div")[0]
                //console.log(content);
                var choice = $("#" + content.id).children("div")[0]
                //console.log(choice);
                var val = $("input[type='radio'][name='" + choice.id+"']:checked").val();;
                //console.log(val);
                var answer = dsoMC[val];
                //console.log(answer);

                var result =
                {
                    QuestionId: q.Id,
                    AnswerId: answer.Id
                };
                //console.log(result);

                MC.push(result);
            }
            else {

                // op column niveau
                //$("#"+($("#"+$("#"+ c.id).children("div")[0].id).children("div")[0].id)).children("div")[0].id

                // select
                // $("#"+($("#"+($("#"+($("#"+$("#question_3").children("div")[0].id).children("div")[0].id)).children("div")[0].id)).children("div")[0].id)).children("select")[0].id

                var content = $("#" + c.id).children("div")[0];
                //console.log(content);
                var choice = $("#" + content.id).children("div")[0];
                //console.log(choice);
                var row = $("#" + choice.id).children("div")[0];
                //console.log(row);
                var columns = $("#" + row.id).children("div");
                //console.log(columns);

                //select --> operator
                var select = $("#" + columns[0].id + " :selected").val();
                //console.log(select);
                
                //range --> value
                var Value = $("#" + columns[2].id).children("input")[0].value;
                //console.log(value);

                var result =
                {
                    QuestionId: q.Id,
                    Value: Value,
                    Operator: select
                };
                //console.log(result);

                Range.push(result);

            }


        }

    }

    //console.log(MC);
    //console.log(Range);

    $.ajax({
        type: "POST",
        url: window.location.protocol + "//" + window.location.host + "/Extra/postTest",
        dataType: "json",
        data: {
            oMC: MC,
            oRange: Range
        },
        success: function (result) {
            showAnalyzedData(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

}

function showAnalyzedData(result) {
    console.log(result);
    sessionStorage.setItem('AnalyzedData', JSON.stringify(result));

    cResult = result;

    var newwin = window.open("/Extra/Overview");

}

$(function () {
    GetQuestions();
});
