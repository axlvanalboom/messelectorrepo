﻿function DisplayQuestionResultOverview(result) {
    var sContent = '';

    var iHead = 2;

    sContent += `
                <div class="tableFixHead">
                    <table class="table table-bordered">
                        <thead>`;

    var iRows = result[0].length;
    var iColumns = result.length;

    var prefix = "th"

    for (i = 0; i < iRows; i++) {
        if (i == iHead) {
            sContent += `</thead>
                        <tbody>`;
            prefix = "td"
        }
        sContent += "<tr>"
        for (j = 0; j < iColumns; j++) {
            sContent += "<" + prefix + ">";
            var r = result[j][i];
            if (typeof r === 'string' || r instanceof String) {
                sContent += r ;
            }
            else {
                if (r.Match) {
                    sContent += `<i class="far fa-check-circle" style="color:green; font-size: 2em;" aria-hidden="true"></i>`;
                }
                else {
                    sContent += ` <i class="far fa-times-circle" style="color: red; font-size: 2em;" aria-hidden="true"></i>`;
                }
            }
            sContent +="</" + prefix + ">"
        }
        sContent += "</tr>"
    }

    sContent += `                  
                        </tbody>
                    </table>
                </div>
                `;
  
    $("#Extra").append(sContent);
    $("#Extra").show();

}

function GetTestData() {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Extra/TestData",
        success: function (result) {
            DisplayQuestionResultOverview(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

}

$(function () {
    //let data = sessionStorage.getItem('AnalyzedData');
    DisplayQuestionResultOverview(JSON.parse(sessionStorage.getItem("AnalyzedData")));
    //GetTestData();
});
