﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using BEC;

//namespace MES4SME.Views
//{
//    public class ExtraController : Controller
//    {

//        private static List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> Softwares;
//        private static List<clsAnswerMC> Answers;
//        private static List<clsQuestionCategory> Categories;
//        private static List<clsAbstractQuestions> QuestionData;
//        private static Dictionary<eComparisonOperator, string> desTranslation1;
//        private static Dictionary<string, eComparisonOperator> desTranslation2;

//        private static bool MadeData = false;
//        private void MakeData()
//        {
//            if (MadeData) return;

//            #region Categories
//            Categories = new List<clsQuestionCategory>();
//            Categories.Add(new clsQuestionCategory() {Id=1, Name = "Detailed Scheduling" });
//            Categories.Add(new clsQuestionCategory() {Id=2, Name = "Dispatching" });
//            Categories.Add(new clsQuestionCategory() {Id=3, Name = "Execution Management" });
//            Categories.Add(new clsQuestionCategory() {Id=4, Name = "Resource Management" });
//            Categories.Add(new clsQuestionCategory() {Id=5, Name = "Definition Management" });
//            Categories.Add(new clsQuestionCategory() {Id=6, Name = "Data Collection" });
//            Categories.Add(new clsQuestionCategory() {Id=7, Name = "Analysis" });
//            Categories.Add(new clsQuestionCategory() {Id=8, Name = "Tracking" });

//            #endregion

//            #region Answers

//            var aYes = new clsAnswerMC() { Name = "Yes", Id = 1 };
//            var aNo = new clsAnswerMC() { Name = "No", Id = 2 };
//            var aMaybe = new clsAnswerMC() { Name = "Maybe", Id = 3 };
//            var aBulb = new clsAnswerMC() { Name = "Bulbasaur", Id = 41 };
//            var aCharmander = new clsAnswerMC() { Name = "Charmander", Id = 42 };
//            var aSquirtle = new clsAnswerMC() { Name = "Squirtle", Id = 43 };

//            Answers = new List<clsAnswerMC>() { aYes, aNo, aMaybe, aBulb, aCharmander, aSquirtle };

//            #endregion

//            #region Questions

//            var qIntrested = new clsQuestionMC() { Name = "Scheduling", Id = 1, Description = "Do you need scheduling functionalities?" };
//            qIntrested.PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo };

//            var q3 = new clsQuestionMC() { Id = 2, Name = "Yes No Maybe", Description = "Yes, No or Maybe?" };
//            q3.PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo, aMaybe };

//            var qRange_0_10 = new clsQuestionRange() { Id = 3, Name = "Range Question", Description = "Rate this question?", Minimum = 0, Maximum = 10 };
//            var qRange_5_33 = new clsQuestionRange() { Id = 4, Name = "blabla", Description = "Rate this question?", Minimum = 5, Maximum = 33 };

//            var qPokemon = new clsQuestionMC() { Id = 5, Name = "Pokemon", Description = "Who is your favorite starter?" };
//            qPokemon.PossibleAnswers = new List<clsAnswerMC>() { aBulb, aCharmander, aSquirtle };

//            QuestionData = new List<clsAbstractQuestions>() 
//            { 
//                qIntrested,
//                q3,
//                qRange_0_10,
//                qRange_5_33,
//                qPokemon
//            };

//            #endregion

//            #region Software

//            //softwares
//            var oSoftware1 = new clsSoftware() { Name = "Software 1" };
//            var oSoftware2 = new clsSoftware() { Name = "Software 2" };
//            var oSoftware3 = new clsSoftware() { Name = "Software 3" };

//            var oS1A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };
//            var oS2A1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = qIntrested };
//            var oS3A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };

//            var oS1A2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q3 };
//            var oS2A2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q3 };
//            var oS3A2 = new clsSoftwareAnswerMC() { Answer = aMaybe, Question = q3 };

//            var oS1A3 = new clsSoftwareAnswerRange() { Value = 3, Question = qRange_0_10 };
//            var oS2A3 = new clsSoftwareAnswerRange() { Value = 4, Question = qRange_0_10 };
//            var oS3A3 = new clsSoftwareAnswerRange() { Value = 7, Question = qRange_0_10 };

//            var oS1A4 = new clsSoftwareAnswerRange() { Value = 30, Question = qRange_5_33 };
//            var oS2A4 = new clsSoftwareAnswerRange() { Value = 5, Question = qRange_5_33 };
//            var oS3A4 = new clsSoftwareAnswerRange() { Value = 19, Question = qRange_5_33 };

//            var oS1A5 = new clsSoftwareAnswerMC() { Answer = aCharmander, Question = qPokemon };
//            var oS2A5 = new clsSoftwareAnswerMC() { Answer = aCharmander, Question = qPokemon };
//            var oS3A5 = new clsSoftwareAnswerMC() { Answer = aSquirtle, Question = qPokemon };

//            //software linking
//            Softwares = new List<(clsSoftware, List<clsAbstractSoftwareAnswer>)>()
//            {
//                (oSoftware1,new List<clsAbstractSoftwareAnswer>(){oS1A1,oS1A2,oS1A3,oS1A4,oS1A5}),
//                (oSoftware2,new List<clsAbstractSoftwareAnswer>(){oS2A1,oS2A2,oS2A3,oS2A4,oS2A5}),
//                (oSoftware3,new List<clsAbstractSoftwareAnswer>(){oS3A1,oS3A2,oS3A3,oS3A4,oS3A5})
//            };

//            #endregion

//            desTranslation1 = new Dictionary<eComparisonOperator, string>()
//            {
//                [eComparisonOperator.Equal] = "=",
//                [eComparisonOperator.NotEqual] = "!=",
//                [eComparisonOperator.LessThan] = "<",
//                [eComparisonOperator.LessThanOrEqualTo] = "<=",
//                [eComparisonOperator.GreaterThan] = ">",
//                [eComparisonOperator.GreaterThanOrEqualTo] = ">="
//            };
//            desTranslation2 = new Dictionary<string, eComparisonOperator>()
//            {
//                ["="] = eComparisonOperator.Equal,
//                ["!="] = eComparisonOperator.NotEqual,
//                ["<"] = eComparisonOperator.LessThan,
//                ["<="]=eComparisonOperator.LessThanOrEqualTo,
//                [">"]=eComparisonOperator.GreaterThan,
//                [">="]=eComparisonOperator.GreaterThanOrEqualTo
//            };
//            MadeData = true;

//        }

//        // GET: Extra
//        public ActionResult Overview()
//        {
//            return View();
//        }
//        // GET: Extra
//        public ActionResult Questions()
//        {
//            return View();
//        }

//        public JsonResult GetQuestions()
//        {
//            MakeData();
//            return Json(QuestionData, JsonRequestBehavior.AllowGet);
//        }

 
//        public JsonResult postTest(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
//        {
//            List<clsAbstractResult> loResultList = new List<clsAbstractResult>();

//            if (oMC != null)
//                oMC.ForEach(mc =>
//                {
//                    var Answer = Answers.Where(a => a.Id == mc.AnswerId).ToList()[0];
//                    var Question = QuestionData.Where(q => q.Id == mc.QuestionId).ToList()[0];
//                    loResultList.Add(new clsResultMC() { Answer = Answer, Question = (clsQuestionMC)Question });
//                });

//            if (oRange != null)
//                oRange.ForEach(r =>
//                {
//                    var Question = QuestionData.Where(q => q.Id == r.QuestionId).ToList()[0];
//                    eComparisonOperator o = desTranslation2[r.Operator];
                   
//                    loResultList.Add(new clsResultRange() { Value = r.Value, Question = (clsQuestionRange)Question, Operator = o });
//                });

//            var result = Analyzator(Softwares, loResultList);

//            //List<clsAbstractResult>
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }
         
//        public JsonResult TestData()
//        {
//            //answers
//            var aYes = new clsAnswerMC() { Name = "Yes" };
//            var aNo = new clsAnswerMC() { Name = "No" };
//            var aMaybe = new clsAnswerMC() { Name = "Maybe" };

//            //questions
//            var qIntrested = new clsQuestionMC() { Name = "Scheduling", Description = "Do you need scheduling functionalities?" };
//            qIntrested.PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo };

//            var q3 = new clsQuestionMC() { Name = "Yes No Maybe", Description = "Yes, No or Maybe?" };

//            var qRange_0_10 = new clsQuestionRange() { Name = "Range Question", Description = "Rate this question?", Minimum = 0, Maximum = 10 };

//            //softwares
//            var oSoftware1 = new clsSoftware() { Name = "Software 1" };
//            var oSoftware2 = new clsSoftware() { Name = "Software 2" };
//            var oSoftware3 = new clsSoftware() { Name = "Software 3" };

//            var oS1A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };
//            var oS2A1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = qIntrested };
//            var oS3A1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = qIntrested };

//            var oS1A2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q3 };
//            var oS2A2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q3 };
//            var oS3A2 = new clsSoftwareAnswerMC() { Answer = aMaybe, Question = q3 };

//            var oS1A3 = new clsSoftwareAnswerRange() { Value = 3, Question = qRange_0_10 };
//            var oS2A3 = new clsSoftwareAnswerRange() { Value = 4, Question = qRange_0_10 };
//            var oS3A3 = new clsSoftwareAnswerRange() { Value = 7, Question = qRange_0_10 };

//            //software linking
//            var oSoftwares = new List<(clsSoftware, List<clsAbstractSoftwareAnswer>)>()
//            {
//                (oSoftware1,new List<clsAbstractSoftwareAnswer>(){oS1A1,oS1A2,oS1A3}),
//                (oSoftware2,new List<clsAbstractSoftwareAnswer>(){oS2A1,oS2A2,oS2A3}),
//                (oSoftware3,new List<clsAbstractSoftwareAnswer>(){oS3A1,oS3A2,oS3A3})
//            };

//            //user
//            var oR1 = new clsResultMC() { Question = qIntrested, Answer = aYes };
//            var oR2 = new clsResultMC() { Question = q3, Answer = aNo };
//            var oR3 = new clsResultRange() { Question = qRange_0_10, Value = 5, Operator = eComparisonOperator.GreaterThanOrEqualTo };

//            var ResultList = new List<clsAbstractResult>()
//            {
//                oR1, oR2, oR3
//            };

//            var result = Analyzator(oSoftwares, ResultList);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        public object[,] Analyzator_v0(List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> loSoftwares, List<clsAbstractResult> loResultList)
//        {

//            var result = new object[2 + loResultList.Count(), 2 + loSoftwares.Count()];

//            result[0, 0] = "Questions";
//            result[0, 1] = "Answers";

//            var iRow = 2;
//            foreach (var r in loResultList)
//            {
//                clsAbstractQuestions q = null;
//                string answer = null;
//                if (r is clsResultMC)
//                {
//                    var rmc = (clsResultMC)r;

//                    q = rmc.Question;

//                    answer = rmc.Answer.Name;
//                }
//                else if (r is clsResultRange)
//                {
//                    var rr = (clsResultRange)r;

//                    q = rr.Question;
//                    var desTranslation = new Dictionary<eComparisonOperator, string>()
//                    {
//                        [eComparisonOperator.Equal] = "=",
//                        [eComparisonOperator.NotEqual] = "!=",
//                        [eComparisonOperator.LessThan] = "<",
//                        [eComparisonOperator.LessThanOrEqualTo] = "<=",
//                        [eComparisonOperator.GreaterThan] = ">",
//                        [eComparisonOperator.GreaterThanOrEqualTo] = ">="
//                    };
//                    answer = desTranslation[rr.Operator] + rr.Value;
//                }

//                result[iRow, 0] = q.Description;
//                result[iRow, 1] = answer;

//                iRow++;
//            }



//            var iCol = 2;
//            loSoftwares.ForEach(s =>
//            {
//                var soft = s.Item1;
//                result[0, iCol] = soft.Name;
                

//                iRow = 2;
//                var iMatch = 0;
//                foreach (var r in loResultList)
//                {

//                    clsQuestionRange qr = null;
//                    clsQuestionMC qmc = null;
//                    clsResultRange rr = null;
//                    clsResultMC rmc = null;

//                    if (r is clsResultMC )
//                    {
//                        rmc = (clsResultMC)r;
//                        qmc = rmc.Question;
//                    }
//                    else if (r is clsResultRange)
//                    {
//                        rr = (clsResultRange)r;
//                        qr = rr.Question;
//                    }

//                    s.Item2.ForEach(a =>
//                    {
//                        clsMatch m = null;
//                        if (a is clsSoftwareAnswerMC)
//                        {
//                            var amc = (clsSoftwareAnswerMC)a;
//                            var q = amc.Question;
//                            if (q == qmc)
//                                m = new clsMatch(qmc, amc, rmc);
            
//                        }
//                        else if (a is clsSoftwareAnswerRange)
//                        {
//                            var ar = (clsSoftwareAnswerRange)a;
//                            var q = ar.Question;
//                            if (q == qr)
//                                m = new clsMatch(qr, ar, rr);
                            
                               
//                        }
//                        if (m!= null)
//                        {
//                            if (m.Match) iMatch++;
//                            result[iRow, iCol] = m;
//                            iRow++;
//                        }

//                    });

//                }

//                result[1, iCol] = (iMatch/ loResultList.Count).ToString() +"%";
//                iCol++;

//            });

//            return result;
        
        
//        }
//        public List<List<object>> Analyzator(List<(clsSoftware, List<clsAbstractSoftwareAnswer>)> loSoftwares, List<clsAbstractResult> loResultList)
//        {

//            var result = new List<List<object>>();

//            var loQuestions = new List<object>();
//            var loAnswers = new List<object>();

//            loQuestions.Add("Questions");
//            loQuestions.Add("");
//            loAnswers.Add("Answers");
//            loAnswers.Add("");

//            foreach (var r in loResultList)
//            {
//                clsAbstractQuestions q = null;
//                string answer = null;
//                if (r is clsResultMC)
//                {
//                    var rmc = (clsResultMC)r;

//                    q = rmc.Question;

//                    answer = rmc.Answer.Name;
//                }
//                else if (r is clsResultRange)
//                {
//                    var rr = (clsResultRange)r;

//                    q = rr.Question;
//                    var desTranslation = new Dictionary<eComparisonOperator, string>()
//                    {
//                        [eComparisonOperator.Equal] = "=",
//                        [eComparisonOperator.NotEqual] = "!=",
//                        [eComparisonOperator.LessThan] = "<",
//                        [eComparisonOperator.LessThanOrEqualTo] = "<=",
//                        [eComparisonOperator.GreaterThan] = ">",
//                        [eComparisonOperator.GreaterThanOrEqualTo] = ">="
//                    };
//                    answer = desTranslation[rr.Operator] + rr.Value;
//                }
//                loQuestions.Add(q.Description);
//                loAnswers.Add(answer);
//            }

//            result.Add(loQuestions);
//            result.Add(loAnswers);

//            loSoftwares.ForEach(s =>
//            {
//                var soft = s.Item1;

//                var loS = new List<object>();
                
//                var iMatch = 0;
//                foreach (var r in loResultList)
//                {

//                    clsQuestionRange qr = null;
//                    clsQuestionMC qmc = null;
//                    clsResultRange rr = null;
//                    clsResultMC rmc = null;

//                    if (r is clsResultMC)
//                    {
//                        rmc = (clsResultMC)r;
//                        qmc = rmc.Question;
//                    }
//                    else if (r is clsResultRange)
//                    {
//                        rr = (clsResultRange)r;
//                        qr = rr.Question;
//                    }

//                    s.Item2.ForEach(a =>
//                    {
//                        clsMatch m = null;
//                        if (a is clsSoftwareAnswerMC)
//                        {
//                            var amc = (clsSoftwareAnswerMC)a;
//                            var q = amc.Question;
//                            if (q == qmc)
//                                m = new clsMatch(qmc, amc, rmc);

//                        }
//                        else if (a is clsSoftwareAnswerRange)
//                        {
//                            var ar = (clsSoftwareAnswerRange)a;
//                            var q = ar.Question;
//                            if (q == qr)
//                                m = new clsMatch(qr, ar, rr);


//                        }

//                        if (m != null)
//                        {
//                            if( m.Match) iMatch++;
//                            loS.Add(m);
//                        }


//                    });

//                }

//                loS.Insert(0, Math.Round((100.0 * iMatch / loResultList.Count), 2).ToString() + "%");
//                loS.Insert(0, soft.Name);
//                result.Add(loS);
//            });

//            return result;
//        }

//        // GET: Extra
//        public ActionResult SelectTest()
//        {
//            return View();
//        }

//        public JsonResult GetQuestionsCategories()
//        {
//            MakeData();
//            return Json(Categories, JsonRequestBehavior.AllowGet);
//        }


//    }



//}