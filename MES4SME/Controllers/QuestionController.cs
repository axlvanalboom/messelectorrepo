﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BEC;
using BLL;

namespace MES4SME.Views
{
    //[Authorize(Roles = "Administrator")]
    public class QuestionController : Controller
    {

        public ActionResult Overview(){return View();}
        public ActionResult Detail(){return View();}
        public ActionResult Result(){return View();}

        public JsonResult GetQuestions_Overview()
        {
            return Json(clsBLL.GetQuestionCategories(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuestions(List<clsQuestionFilter> loFilters)
        {
            return Json(clsBLL.GetQuestions(loFilters), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AnalyzeQuestionAnswers(List<clsInternalResultMC> oMC, List<clsInternalResultRange> oRange)
        {
            return Json(MES4SME.Models.Helper.CreateLogCraphVisualisation(clsBLL.AnalyzeQuestionAnswers(oMC, oRange),"Result"), JsonRequestBehavior.AllowGet);
        }

 
    }

}