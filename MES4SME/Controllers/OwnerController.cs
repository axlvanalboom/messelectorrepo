﻿using BEC;
using BLL;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MES4SME.Controllers
{
    // Collections of all owner functions
    [Authorize(Roles = "Administrator, Owner")]
    public class OwnerController : Controller
    {
        // View
        [Route("Owner")]
        public ActionResult Index()
        {
            return View();
        }

        // Config view
        [Route("Config")]
        public ActionResult Config()
        {
            return View();
        }

        // Get config companies
        public JsonResult getConfigCompanies()
        {
            return Json(clsBLL.getConfigCompanies(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Get config softwares
        public JsonResult getConfigSoftwares()
        {
            return Json(clsBLL.getConfigSoftwares(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit company
        public JsonResult editCompany(clsCompany oCompany)
        {
            return Json(clsBLL.editCompany(oCompany, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Add software
        public JsonResult addSoftware(clsSoftware oSoftware, int Company, int Activity)
        {
            return Json(clsBLL.addSoftware(oSoftware, Company, Activity, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit software 
        public JsonResult editSoftware(clsSoftware oSoftware)
        {
            // inclusief checks nog doen! TODO
            return Json(clsBLL.editSoftware(oSoftware, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Add case
        public JsonResult addCase(clsCase oCase, int Company, int Software, int Sector, int Activity)
        {
            return Json(clsBLL.addCase(oCase, Company, Software, Sector, Activity, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit case
        public JsonResult editCase(clsCase oCase)
        {
            return Json(clsBLL.editCase(oCase, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit company content
        [ValidateInput(false)]
        public JsonResult editCompanyContent(int Id, string Content)
        {
            return Json(clsBLL.editCompanyContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit software content
        [ValidateInput(false)]
        public JsonResult editSoftwareContent(int Id, string Content)
        {
            return Json(clsBLL.editSoftwareContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Edit case content
        [ValidateInput(false)]
        public JsonResult editCaseContent(int Id, string Content)
        {
            return Json(clsBLL.editCaseContentById(Id, Content, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Linked config getters

        public JsonResult getConfigCompaniesLinked()
        {
            byte bUserLevel = userLevel();
            return Json(clsBLL.getCompaniesLinked(bUserLevel, userId(), bUserLevel == 3), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getConfigSoftwaresLinked()
        {
            byte bUserLevel = userLevel();
            return Json(clsBLL.getSoftwareLinked(bUserLevel, userId(), bUserLevel == 3), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getConfigCasesLinked()
        {
            byte bUserLevel = userLevel();
            return Json(clsBLL.getCasesLinked(bUserLevel, userId(), bUserLevel == 3), JsonRequestBehavior.AllowGet);
        }

        // Edit case sector
        public JsonResult editCaseSector(int CaseId, int SectorId)
        {
            return Json(clsBLL.editCaseSector(CaseId, SectorId), JsonRequestBehavior.AllowGet);
        }

        //editCaseCompanySoftware
        public JsonResult editCaseCompanySoftware(int CaseId, int CompanyId, int SoftwareId)
        {
            return Json(clsBLL.editCaseCompanySoftware(CaseId, CompanyId, SoftwareId, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        ////addSoftwareCategoryFunctionLink
        //public JsonResult addSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        //{
        //    return Json(clsBLL.addSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        //}

        ////deleteSoftwareCategoryFunctionLink
        //public JsonResult deleteSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        //{
        //    return Json(clsBLL.deleteSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        //}

        //addCaseCategoryFunctionLink
        public JsonResult addCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.addCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        //deleteCaseCategoryFunctionLink
        public JsonResult deleteCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.deleteCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // get company owner ids
        public JsonResult getOwnedCompanies()
        {
            return Json(clsBLL.getOwnedCompanies(userId()), JsonRequestBehavior.AllowGet);
        }

        // get softwares owner ids
        public JsonResult getOwnedSoftwares()
        {
            return Json(clsBLL.getOwnedSoftwares(userId()), JsonRequestBehavior.AllowGet);
        }

        // get cases owner ids
        public JsonResult getOwnedCases()
        {
            return Json(clsBLL.getOwnedCases(userId()), JsonRequestBehavior.AllowGet);
        }


        // Private userLevel function
        private byte userLevel()
        {
            byte bUserLevel = 1;
            if (User.IsInRole("Owner"))
                bUserLevel = 2;
            else if (User.IsInRole("Administrator"))
                bUserLevel = 3;
            return bUserLevel;
        }

        // Get user id
        private string userId()
        {
            return User.Identity.GetUserId();
        }

        // Upload attachement
        [HttpPost]
        public JsonResult uploadFile(HttpPostedFileBase file)
        {

            clsUploadResponse oResponse = new clsUploadResponse();

            try
            {
                if (file.ContentLength > 0)
                {
                    if (file.FileName.EndsWith(".pdf"))
                    {
                        string sPath = Path.Combine(Server.MapPath("~/FileChecking"), file.FileName);
                        file.SaveAs(sPath);
                        bool xResult = FileChecker.CheckPdf(sPath);

                        if (xResult)
                        {

                            if (FileChecker.CheckVirus(sPath))
                            {
                                // the file contains a virus
                                // it is already deleted!!! 
                                // Windows defender does this
                                throw new Exception("The file contains a VIRUS!");
                            }
                            else
                            {
                                // check if this file already is available

                                string[] allfiles = Directory.GetFiles(Server.MapPath("~/Files"));
                                var sCompare = MES4SME.FileChecker.FileComparison(sPath, allfiles.OfType<string>().ToList());
                                System.IO.File.Delete(sPath); // delete from filechecking

                                string[] arrsFile = file.FileName.Split('.');
                                string sFileName = "";
                                for (int i = 0; i < arrsFile.Length - 1; i++)
                                {
                                    sFileName += (arrsFile[i]);
                                    if (i != arrsFile.Length - 2) { sFileName += "_"; }
                                }
                                oResponse.file = new clsFileResponse();

                                if (sCompare != null)
                                {
                                    // file is already saved in the FILES map --> set url to this location!       
                                    var iIndex = sCompare.IndexOf("Files");
                                    string sUrlName = sCompare.Substring(iIndex + 6, sCompare.Length - iIndex - 6);
                                    oResponse.file.url = "Files/" + sUrlName;
                                }
                                else
                                {
                                    //it's not yet uploaded! --> upload it
                                    string sUrlName = "";
                                    sUrlName = sFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                                    sPath = Path.Combine(Server.MapPath("~/Files"), sUrlName);
                                    file.SaveAs(sPath);
                                    oResponse.file.url = "Files/" + sUrlName;
                                }

                                oResponse.file.name = sFileName;
                                oResponse.file.extension = ".pdf";
                                oResponse.success = true;
                            }

                            
                        }
                        else
                        {
                            System.IO.File.Delete(sPath); // delete from filechecking
                            throw new Exception("This is not a pdf file!");
                        }

                    }
                    else
                    {
                        throw new Exception("Wrong input type!");
                    }
                }
            }
            catch (Exception e)
            {
                oResponse.success = false;
                oResponse.file.url = null;
                oResponse.errorMessage = e.Message;
            }

            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }
         
        // Upload image
        [HttpPost]
        public JsonResult uploadImage(HttpPostedFileBase image)
        {
            clsUploadResponse oResponse = new clsUploadResponse();
            try
            {
                if (image.ContentLength > 0)
                {
                    if (image.FileName.EndsWith(".png") || image.FileName.EndsWith(".jpeg") || image.FileName.EndsWith(".jpg") || image.FileName.EndsWith(".jfif")) // dubbel check
                    {

                        string sFileName = DateTime.Now.ToString("yyyyMMddHHmmss") +".png";                       
                        string sPath = Path.Combine(Server.MapPath("~/FileChecking"), image.FileName);
                        image.SaveAs(sPath); // save in filechecking

                        string sSaveUrl = Path.Combine(Server.MapPath("~"), FileChecker.CheckImage(sPath, sFileName)); // check if it's really a image --> new file in filechecking!

                        System.IO.File.Delete(sPath); // delete the first file 

                        string[] allfiles = Directory.GetFiles(Server.MapPath("~/Images"));
                        var sCompare = MES4SME.FileChecker.FileComparison(sSaveUrl, allfiles.OfType<string>().ToList());

                        oResponse.file = new clsFileResponse();

                        if (sCompare != null)
                        {
                            //delete the file from filechecking
                            System.IO.File.Delete(sSaveUrl); 

                            var iIndex = sCompare.IndexOf("Images");
                            string sUrlName = sCompare.Substring(iIndex + 7, sCompare.Length - iIndex - 7);

                            oResponse.file.url = "Images/" + sUrlName;                          
                            //oResponse.file.Fileurl = sUrlName;
                            oResponse.success = true;
                        }
                        else 
                        {
                            // save the file in Images
                            byte[] oFile = System.IO.File.ReadAllBytes(sSaveUrl);
                            System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Images"), sFileName), oFile);

                            //delete it from filechecking
                            System.IO.File.Delete(sSaveUrl); // delete the first file 

                            // settings
                            oResponse.file.url = "Images/" + sFileName;
                            oResponse.file.Fileurl = sSaveUrl;
                            oResponse.success = true;
                        }
                    }
                    else
                    {
                        throw new Exception("Wrong input type!");
                    }

                }
            }
            catch (Exception e)
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/FileChecking"));
                foreach (FileInfo file in di.GetFiles()){ file.Delete();}

                string sMessage = e.Message;
                oResponse.success =false;
                oResponse.file.url = null;
                oResponse.errorMessage = e.Message;
            }

            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }

        // Upload url
        public string uploadUrl(string url)
        {
            
            clsUploadResponse oResponse = new clsUploadResponse();

            try
            {
                // Get the metadata
                clsMetaInformation oMeta = clsBLL.GetMetaDataFromUrl(url);

                // Fill in metadata into return object
                if (oMeta.HasData)
                {
                    oResponse.meta.description = oMeta.Description;
                    oResponse.meta.title = oMeta.Title;
                    oResponse.file.Fileurl = oMeta.ImageUrl;
                    oResponse.success = true;
                }
                else
                    oResponse.success = false;
            }
            catch (Exception e)
            {
                oResponse.success = false;
                oResponse.errorMessage = e.Message;
            }

            return JsonConvert.SerializeObject(oResponse);
        }
    }
}