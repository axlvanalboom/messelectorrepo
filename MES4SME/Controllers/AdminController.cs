﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BEC;

namespace MES4SME.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        #region "Users"

        public JsonResult getUsers()
        {
            return Json(clsBLL.getUsers(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUserById(string Id)
        {
            return Json(clsBLL.getUserById(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUsersLinked()
        {
            return Json(clsBLL.getUsersLinked(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editUser(clsUser oUser)
        {
            return Json(clsBLL.editUser(oUser), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editUserComRo(string Id, int Company, int Role)
        {
            return Json(clsBLL.editUserComRo(Id, Company, Role), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteUser(string Id)
        {
            return Json(clsBLL.deleteUser(Id), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Sector"

        public JsonResult addSector(clsSector oSector)
        {
            return Json(clsBLL.addSector(oSector), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editSector(clsSector oSector)
        {
            return Json(clsBLL.editSector(oSector), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteSector(int Id)
        {
            return Json(clsBLL.deleteSector(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Activity"
        public JsonResult addActivity(clsActivity oActivity)
        {
            return Json(clsBLL.addActivity(oActivity), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editActivity(clsActivity oActivity)
        {
            return Json(clsBLL.editActivity(oActivity), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteActivity(int Id)
        {
            return Json(clsBLL.deleteActivity(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Category"
        public JsonResult addCategory(clsCategory oCategory)
        {
            return Json(clsBLL.addCategory(oCategory), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editCategory(clsCategory oCategory)
        {
            return Json(clsBLL.editCategory(oCategory), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteCategory(clsCategory oCategory)
        {
            return Json(clsBLL.deleteCategory(oCategory.Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Function"
        public JsonResult addFunction(clsFunction oFunction)
        {
            return Json(clsBLL.addFunction(oFunction), JsonRequestBehavior.AllowGet);
        }
        public JsonResult editFunction(clsFunction oFunction)
        {
            return Json(clsBLL.editFunction(oFunction), JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteFunction(int Id)
        {
            return Json(clsBLL.deleteFunction(Id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Company"

        // Add company
        public JsonResult addCompany(clsCompany oCompany)
        {
            return Json(clsBLL.addCompany(oCompany), JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteCompany(clsCompany oCompany)
        {
            return Json(clsBLL.deleteCompany(oCompany), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editCompanyVisibility(int Id, bool Public)
        {
            return Json(clsBLL.editCompanyContentVisibilityById(Id, Public), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Software"

        public JsonResult deleteSoftware(int Id)
        {
            return Json(clsBLL.deleteSoftware(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editSoftwareVisibility(int Id, bool Public)
        {
            return Json(clsBLL.editSoftwareContentVisibilityById(Id, Public), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Case"

        public JsonResult deleteCase(int Id)
        {
            return Json(clsBLL.deleteCaseById(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult editCaseVisibility(int Id, bool Public)
        {
            return Json(clsBLL.editCaseContentVisibilityById(Id, Public), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Links"

        //addSoftwareCompanyActivityLink
        public JsonResult addSoftwareCompanyActivityLink(int SoftwareId, int CompanyId, int ActivityId)
        {
            return Json(clsBLL.addSoftwareCompanyActivityLink(SoftwareId, CompanyId, ActivityId), JsonRequestBehavior.AllowGet);
        }

        //deleteSoftwareCompanyActivityLink
        public JsonResult deleteSoftwareCompanyActivityLink(int SoftwareId, int CompanyId, int ActivityId)
        {
            return Json(clsBLL.deleteSoftwareCompanyActivityLink(SoftwareId, CompanyId, ActivityId), JsonRequestBehavior.AllowGet);
        }

        ////addSoftwareCategoryFunctionLink
        //public JsonResult addSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        //{
        //    return Json(clsBLL.addSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        //}

        ////deleteSoftwareCategoryFunctionLink
        //public JsonResult deleteSoftwareCategoryFunctionLink(int SoftwareId, int CategoryId, int FunctionId)
        //{
        //    return Json(clsBLL.deleteSoftwareCategoryFunctionLink(SoftwareId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        //}

        //addCaseCategoryFunctionLink
        public JsonResult addCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.addCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        //deleteCaseCategoryFunctionLink
        public JsonResult deleteCaseCategoryFunctionLink(int CaseId, int CategoryId, int FunctionId)
        {
            return Json(clsBLL.deleteCaseCategoryFunctionLink(CaseId, CategoryId, FunctionId), JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult Users()
        {
            ViewBag.Users = true;
            return Json(null, JsonRequestBehavior.AllowGet);
        }



    }
}