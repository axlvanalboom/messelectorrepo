﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BEC;
using MES4SME.Models;

namespace MES4SME.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AnalyzingController : Controller
    {
        [Route("Analyzing")]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getAmountOfVisitors()
        {
            List<clsDayAmountVisitors> oLogs = BLL.clsBLL.GetLogsAmountOfVisitors();

            List<StringInt> oLogsRight = new List<StringInt>();
            foreach (clsDayAmountVisitors o in oLogs)
            {
                oLogsRight.Add(new StringInt() { Number = o.Amount, Text = o.Day });
            }

            return Json(CreateLogCraphVisualisation(oLogsRight, "#Visitors"), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAmountOfVisitorsBetweenTime(string sTimeCode, string sStart, string sStop)
        {
            List<clsDayAmountVisitors> oLogs = BLL.clsBLL.GetLogsAmountOfVisitorsBetweenTime(sTimeCode, sStart, sStop);

            List<StringInt> oLogsRight = new List<StringInt>();
            foreach (clsDayAmountVisitors o in oLogs)
            {
                oLogsRight.Add(new StringInt() { Number = o.Amount, Text = o.Day });
            }

            return Json(CreateLogCraphVisualisation(oLogsRight, "#Visitors"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getVisitorsViews()
        {
            return Json(BLL.clsBLL.GetLogsUserViews(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getVisitorsViewsBetweenTime(string sTimeCode, string sStart, string sStop)
        {
            return Json(BLL.clsBLL.GetLogsUserViewsBetweenTime(sTimeCode, sStart, sStop), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAmountDetailViews(string sType)
        {
            return Json(BLL.clsBLL.GetLogsAmountDetailView(sType), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAmountDetailViewsBetweenTime(string sType,string sTimeCode, string sStart, string sStop)
        {
            return Json(BLL.clsBLL.GetLogsAmountDetailViewBetweenTime(sType,sTimeCode,sStart,sStop), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAmmountOfViewsOffTypeParameter(string sType, string sParameter)
        {
            List<StringInt> oLogs = BLL.clsBLL.GetAmmountOfViewsOffTypeParameter(sType, sParameter);
            return Json(CreateLogCraphVisualisation(oLogs, "#Views off " + sParameter + " per day"), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAmmountOfViewsOffTypeParameterBetweenTime(string sType,string sParameter, string sTimeCode, string sStart, string sStop)
        {
            List<StringInt> oLogs = BLL.clsBLL.GetAmmountOfViewsOffTypeParameterBetweenTime(sType, sParameter, sTimeCode, sStart, sStop);
            return Json(CreateLogCraphVisualisation(oLogs, "#Views off " + sParameter + " per day"), JsonRequestBehavior.AllowGet);
        }

        private LogGraphVisualization CreateLogCraphVisualisation(List<StringInt> oData, string sLabel)
        {
            LogGraphVisualization oLogVisual = new LogGraphVisualization();
            oLogVisual.Labels = new string[oData.Count];
            oLogVisual.DataSets = new List<DataSet>();
            oLogVisual.DataSets.Add(new DataSet());
            oLogVisual.DataSets[0].BackgroundColor = "rgba(54, 162, 235, 0.2)";
            oLogVisual.DataSets[0].BorderColor = "rgba(54, 162, 235, 1)";
            oLogVisual.DataSets[0].BorderWidth = 1;
            oLogVisual.DataSets[0].Label = sLabel;
            oLogVisual.DataSets[0].Data = new object[oData.Count];

            for (int i = 0; i < oData.Count; i++)
            {
                oLogVisual.DataSets[0].Data[i] = oData[i].Number;
                oLogVisual.Labels[i] = oData[i].Text;
            }

            return oLogVisual;
        }

    }


}



