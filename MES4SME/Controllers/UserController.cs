﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using BLL;
using BEC;

namespace MES4SME.Controllers
{
    // Collection off all user data function
    //[Authorize(Roles = "Administrator, Owner")]
    public class UserController : Controller
    {
        // Views
        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Browse()
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Headpage");
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [Route("Select")]
        //[Authorize(Roles = "Administrator")]
        public ActionResult Select()
        {
            return View();
        }

        [Route("Companies")]
        public ActionResult Companies() 
        {
            return View();
        }

        [Route("CompanyDetail")]
        public ActionResult CompanyDetail()
        {
            return View();
        }

        [Route("Software")]
        public ActionResult Softwares()
        {
            return View();
        }

        [Route("SoftwareDetail")]
        public ActionResult SoftwareDetail()
        {
            return View();
        }

        [Route("Cases")]
        public ActionResult Cases()
        {
            return View();
        }

        [Route("CaseDetail")]
        public ActionResult CaseDetail()
        {
            return View();
        }

        // All getters
        public JsonResult getSectors()
        {
            return Json(clsBLL.getSectors(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getActivities()
        {
            return Json(clsBLL.getActivities(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFunctions()
        {
            return Json(clsBLL.getFunctions(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCategories()
        {
            return Json(clsBLL.getCategories(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanies()
        {
            return Json(clsBLL.getCompanies(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanyById(int Id)
        {
            return Json(clsBLL.getCompanyById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwares()
        {
            return Json(clsBLL.getSoftwares(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareById(int Id)
        { 
            return Json(clsBLL.getSoftwareById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCases()
        {
            return Json(clsBLL.getCases(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCaseById(int Id)
        {
            return Json(clsBLL.getCaseById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCompanyContent(int Id)
        {
            return Json(clsBLL.getCompanyContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareContent(int Id)
        {
            return Json(clsBLL.getSoftwareContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCaseContent(int Id)
        {
            return Json(clsBLL.getCaseContentById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Linked getters

        public JsonResult getCompaniesLinked()
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Companies");
            return Json(clsBLL.getCompaniesLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwaresLinked()
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Software");
            return Json(clsBLL.getSoftwareLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCasesLinked()
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Cases");
            return Json(clsBLL.getCasesLinked(userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Id linked getters
        public JsonResult getCompanyLinkedById(int Id)
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Company Detail", Id);
            return Json(clsBLL.getCompanyLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSoftwareLinkedById(int Id)
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Software Detail", Id);
            return Json(clsBLL.getSoftwareLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCaseLinkedById(int Id)
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(), "Case Detail", Id);
            return Json(clsBLL.getCaseLinkedById(Id, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // Search function
        public JsonResult getSearchResults(clsFullLinks Links) 
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLogMainSearch(GetSessionId(),Links);
            return Json(clsBLL.getFullyLinked(Links, userLevel(), userId()), JsonRequestBehavior.AllowGet);
        }

        // User
        public JsonResult editUser(clsUser oUser)
        {
            if (userLevel() == 3 || User.Identity.GetUserId() == oUser.Id)
                return Json(clsBLL.editUser(oUser), JsonRequestBehavior.AllowGet);
            else
                return Json(new clsReturn(0, "You may only edit your own data.", null), JsonRequestBehavior.AllowGet);
        }

        // Private userLevel function
        private byte userLevel()
        {
            byte bUserLevel = 1;
            if (User.IsInRole("Owner"))
                bUserLevel = 2;
            else if (User.IsInRole("Administrator"))
                bUserLevel = 3;
            return bUserLevel;
        }

        // Get user id 
        private string userId()
        {
            return User.Identity.GetUserId();
        }

        private string GetSessionId()
        {
            string user = (string)(Session["ID"]);

            if (user == null)
            {
                user = clsBLL.GetRandomId();
                Session["ID"] = user;
            }
            return user;
        }
        
        public void SearchResult(string sType, string sParameter)
        {
            if (!User.IsInRole("Administrator"))
                clsBLL.AddLog(GetSessionId(),sType,sParameter);
        }

    }
}
