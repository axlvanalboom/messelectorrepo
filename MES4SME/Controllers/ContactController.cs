﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BLL;
using MES4SME.Models;
using Microsoft.AspNet.Identity;

namespace MES4SME.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SendEmail()
        {
            return View();
        }

        public async Task<ActionResult> EmailSend(SendEmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                clsBLL.SendContactEmail(model.FirstName + " " + model.LastName, model.Sender, model.Subject, model.EmailBody);
                return View();
            }
            return View("SendEmail", model);
        }

        public async Task<ActionResult> ConfirmEmail(SendEmailViewModel model)
        {
            return View();
        }

        //[Authorize(Roles = "Administrator, Owner, User")]
        public ActionResult Help()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Owner, User")]
        public async Task<ActionResult> AskForHelp(HelpViewModel model)
        {
            if (ModelState.IsValid)
            {               
                clsBLL.LetAdminKnow(User.Identity.GetUserName(), model.Subject, model.EmailBody);
                return View("EmailSend");
            }
            return View("Help", model);
        }

    }
}