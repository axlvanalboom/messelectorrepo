﻿"use strict";

function extractHostname(url) {
    var hostname; //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    } //find & remove port number


    hostname = hostname.split(':')[0]; //find & remove "?"

    hostname = hostname.split('?')[0];
    return hostname;
} // Render for editor JSON data


function renderEditorData(data) {
    // Initiate content
    var sHTML = "<div class=\"codex-editor\">\n                    <div class=\"codex-editor__redactor\" >"; // Loop blocks

    for (var i = 0; i < data.blocks.length; i++) {
        var obj = data.blocks[i]; // Add html by type

        switch (obj.type) {
            case 'paragraph':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"ce-paragraph cdx-block\">\n                                <p>".concat(obj.data.text, "</p>\n                                </div>\n                            </div>\n                         </div>\n");
                break;

            case 'image':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"cdx-block image-tool image-tool--filled\">\n                                    <div class=\"image-tool__image\">\n                                        <div class=\"image-tool__image-preloader\">\n                                        </div>\n                                        <img class=\"image-tool__image-picture\" src=\"".concat(obj.data.file.url, "\">\n                                    </div>");
                if (obj.data.caption !== undefined && obj.data.caption !== null && obj.data.caption.length > 0) sHTML += "<div class=\"cdx-input image-tool__caption\" contenteditable=\"false\" data-placeholder=\"Caption\">\n                                        ".concat(obj.data.caption, "\n                                    </div>");
                sHTML += "</div>\n                            </div>\n                        </div>\n";
                break;

            case 'header':
                sHTML += "<div class=\"ce-block\">\n                                <div class=\"ce-block__content\">\n                                    <div class=\"ce-paragraph cdx-block\">\n                                        <h".concat(obj.data.level, ">").concat(obj.data.text, "</h").concat(obj.data.level, ">\n                                    </div>\n                                </div>\n                            </div>\n");
                break;

            case 'raw':
                sHTML += "<div class=\"ce-block\">\n                                <div class=\"ce-block__content\">\n                                    <div class=\"ce-code\">\n                                        <code>".concat(obj.data.html, "</code>\n                                    </div>\n                                </div>\n                            </div>\n");
                break;

            case 'code':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"ce-code\">\n                                    <code>".concat(obj.data.code, "</code>\n                                </div>\n                            </div>\n                        </div>\n");
                break;

            case 'list':
                if (obj.data.style === 'unordered') {
                    var list = obj.data.items.map(function (item) {
                        return "<li class=\"cdx-list__item\">".concat(item, "</li>");
                    });
                    sHTML += "<div class=\"ce-block\">\n                                        <div class=\"ce-block__content\">\n                                            <div class=\"ce-paragraph cdx-block\">\n                                                <ul class=\"cdx-list--unordered\">".concat(list.join(''), "</ul>\n                                            </div>\n                                        </div>\n                                    </div>\n");
                } else {
                    var _list = obj.data.items.map(function (item) {
                        return "<li class=\"cdx-list__item\">".concat(item, "</li>");
                    });

                    sHTML += "<div class=\"ce-block\">\n                                    <div class=\"ce-block__content\">\n                                        <div class=\"ce-paragraph cdx-block\">\n                                            <ol class=\"cdx-list--ordered\">".concat(_list.join(''), "</ol>\n                                        </div>\n                                    </div>\n                                </div>\n");
                }

                break;

            case 'delimeter':
                sHTML += "<div class=\"ce-block\">\n                                <div class=\"ce-block__content\">\n                                    <div class=\"ce-delimiter cdx-block\"></div>\n                                </div>\n                            </div>\n";
                break;

            case 'attaches':
                sHTML += "<div class=\"ce-block\">\n                                    <div class=\"ce-block__content\"><div class=\"cdx-block attaches-tool\"><div class=\"cdx-attaches cdx-attaches--with-file\">\n                                        <div class=\"cdx-attaches__file-icon\">\n                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"32\" height=\"40\">\n                                                <g fill=\"#A8ACB8\" fill-rule=\"evenodd\"><path fill-rule=\"nonzero\" d=\"M17 0l15 14V3v34a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3h20-6zm0 2H3a1 1 0 0 0-1 1v34a1 1 0 0 0 1 1h26a1 1 0 0 0 1-1V14H17V2zm2 10h7.926L19 4.602V12z\"></path><path d=\"M7 22h18v2H7zm0 4h18v2H7zm0 4h18v2H7z\"></path></g>\n                                            </svg>\n                                        </div>\n                                        <div class=\"cdx-attaches__file-info\">\n                                            <div class=\"cdx-attaches__title\" contenteditable=\"false\">\n                                                ".concat(obj.data.file.name, "\n                                            </div>\n                                        </div>\n                                        <a class=\"cdx-attaches__download-button\" href=\"").concat(obj.data.file.url, "\" target=\"_blank\" rel=\"nofollow noindex noreferrer\">\n                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17pt\" height=\"17pt\" viewBox=\"0 0 17 17\"><path d=\"M9.457 8.945V2.848A.959.959 0 0 0 8.5 1.89a.959.959 0 0 0-.957.957v6.097L4.488 5.891a.952.952 0 0 0-1.351 0 .952.952 0 0 0 0 1.351l4.687 4.688a.955.955 0 0 0 1.352 0l4.687-4.688a.952.952 0 0 0 0-1.351.952.952 0 0 0-1.351 0zM3.59 14.937h9.82a.953.953 0 0 0 .953-.957.952.952 0 0 0-.953-.953H3.59a.952.952 0 0 0-.953.953c0 .532.425.957.953.957zm0 0\" fill-rule=\"evenodd\"></path></svg>\n                                        </a>\n                                    </div>\n                            </div>\n");
                break;

            case 'embed':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"cdx-block embed-tool\">\n                                    <preloader class=\"embed-tool__preloader\">\n                                        <div class=\"embed-tool__url\">".concat(obj.data.source, "</div>\n                                    </preloader>\n                                    <iframe width=\"100%\" \n                                            height=\"").concat(obj.data.height, "\" \n                                            src=\"").concat(obj.data.embed, "\" \n                                            frameborder=\"0\" \n                                            allow=\"accelerometer; \n                                            autoplay; \n                                            encrypted-media; \n                                            gyroscope; \n                                            picture-in-picture\" \n                                            allowfullscreen>\n                                    </iframe>");
                if (obj.data.caption !== undefined && obj.data.caption !== null && obj.data.caption.length > 0) sHTML += "<div class=\"cdx-input embed-tool__caption\" contenteditable=\"false\" data-placeholder=\"Enter a caption\">".concat(obj.data.caption, "</div>");
                sHTML += "      </div>\n                            </div>\n                        </div>\n";
                break;

            case 'table':
                var rows = obj.data.content.map(function (row) {
                    var cells = '<tr>';
                    row.map(function (cell) {
                        cells += "<td class=\"tc-table__cell\"><div class=\"tc-table__area\"><div class=\"tc-table__inp\" contenteditable=\"false\">".concat(cell, "</div></div></td>");
                    });
                    cells += '</tr>';
                    return cells;
                });
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"tc-editor cdx-block\">\n                                    <div class=\"tc-table__wrap\">\n                                        <table class=\"tc-table\"><tbody>".concat(rows.join(''), "</tbody></table>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n");
                break;

            case 'quote':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <blockquote class=\"cdx-block cdx-quote\">\n                                    <div class=\"cdx-input cdx-quote__text\" contenteditable=\"false\" data-placeholder=\"Enter a quote\" style=\"text-align:".concat(obj.data.alignment, "\">\n                                        ").concat(obj.data.text, "\n                                    </div>\n                                    <div class=\"cdx-input cdx-quote__caption\" contenteditable=\"false\" data-placeholder=\"Quote's author\" style=\"text-align:").concat(obj.data.alignment, "\">\n                                        ").concat(obj.data.caption, "\n                                    </div>\n                                </blockquote>\n                            </div>\n                        </div>\n");
                break;

            case 'linkTool':
                sHTML += "<div class=\"ce-block\">\n                            <div class=\"ce-block__content\">\n                                <div class=\"cdx-block\">\n                                    <div class=\"link-tool\">\n                                        <a class=\"link-tool__content link-tool__content--rendered\" target=\"_blank\" rel=\"nofollow noindex noreferrer\" href=\"".concat(obj.data.link, "\">");
                if (obj.data.meta.image !== undefined && obj.data.meta.image.url !== undefined && obj.data.meta.image.url !== null) sHTML += "<div class=\"link-tool__image\" style=\"background-image: url(".concat(obj.data.meta.image.url, ");\"></div>");
                if (obj.data.meta.title !== undefined && obj.data.meta.title !== null) sHTML += "<div class=\"link-tool__title\">".concat(obj.data.meta.title, "</div>");
                if (obj.data.meta.description !== undefined && obj.data.meta.description !== null) sHTML += "<p class=\"link-tool__description\">".concat(obj.data.meta.description, "</p>");
                if (obj.data.link !== undefined && obj.data.link !== null) sHTML += "<span class=\"link-tool__anchor\">".concat(extractHostname(obj.data.link), "</span>");
                sHTML += "</a>\n                                    </div>\n                                </div>\n                            </div>\n                         </div>\n";
                break;

            default:
                break;
        }
    } // Close elements


    sHTML += "</div>\n                <div class=\"codex-editor-overlay\">\n                    <div class=\"codex-editor-overlay__container\">\n                        <div class=\"codex-editor-overlay__rectangle\" style=\"display: none;\">\n                        </div>\n                    </div>\n                </div>\n            </div>"; // Return HTML

    return sHTML;
}