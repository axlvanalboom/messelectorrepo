﻿
function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

// Render for editor JSON data
function renderEditorData(data) {
    // Initiate content
    var sHTML = `<div class="codex-editor">
                    <div class="codex-editor__redactor" >`;

    // Loop blocks
    for (var i = 0; i < data.blocks.length; i++) {
        var obj = data.blocks[i];
        // Add html by type
        switch (obj.type) {
            case 'paragraph':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="ce-paragraph cdx-block">
                                <p>${obj.data.text}</p>
                                </div>
                            </div>
                         </div>\n`;
                break;
            case 'image':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="cdx-block image-tool image-tool--filled">
                                    <div class="image-tool__image">
                                        <div class="image-tool__image-preloader">
                                        </div>
                                        <img class="image-tool__image-picture" src="${obj.data.file.url}">
                                    </div>`;
                if (obj.data.caption !== undefined && obj.data.caption !== null && obj.data.caption.length > 0)
                    sHTML += `<div class="cdx-input image-tool__caption" contenteditable="false" data-placeholder="Caption">
                                        ${obj.data.caption}
                                    </div>`;
                sHTML += `</div>
                            </div>
                        </div>\n`;
                break;
            case 'header':
                sHTML += `<div class="ce-block">
                                <div class="ce-block__content">
                                    <div class="ce-paragraph cdx-block">
                                        <h${obj.data.level}>${obj.data.text}</h${obj.data.level}>
                                    </div>
                                </div>
                            </div>\n`;
                break;
            case 'raw':
                sHTML += `<div class="ce-block">
                                <div class="ce-block__content">
                                    <div class="ce-code">
                                        <code>${obj.data.html}</code>
                                    </div>
                                </div>
                            </div>\n`;
                break;
            case 'code':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="ce-code">
                                    <code>${obj.data.code}</code>
                                </div>
                            </div>
                        </div>\n`;
                break;
            case 'list':
                if (obj.data.style === 'unordered') {
                    const list = obj.data.items.map(item => {
                        return `<li class="cdx-list__item">${item}</li>`;
                    });
                    sHTML += `<div class="ce-block">
                                        <div class="ce-block__content">
                                            <div class="ce-paragraph cdx-block">
                                                <ul class="cdx-list--unordered">${list.join('')}</ul>
                                            </div>
                                        </div>
                                    </div>\n`;
                } else {
                    const list = obj.data.items.map(item => {
                        return `<li class="cdx-list__item">${item}</li>`;
                    });
                    sHTML += `<div class="ce-block">
                                    <div class="ce-block__content">
                                        <div class="ce-paragraph cdx-block">
                                            <ol class="cdx-list--ordered">${list.join('')}</ol>
                                        </div>
                                    </div>
                                </div>\n`;
                }
                break;
            case 'delimeter':
                sHTML += `<div class="ce-block">
                                <div class="ce-block__content">
                                    <div class="ce-delimiter cdx-block"></div>
                                </div>
                            </div>\n`;
                break;
            case 'attaches':
                sHTML += `<div class="ce-block">
                                    <div class="ce-block__content"><div class="cdx-block attaches-tool"><div class="cdx-attaches cdx-attaches--with-file">
                                        <div class="cdx-attaches__file-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="40">
                                                <g fill="#A8ACB8" fill-rule="evenodd"><path fill-rule="nonzero" d="M17 0l15 14V3v34a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3h20-6zm0 2H3a1 1 0 0 0-1 1v34a1 1 0 0 0 1 1h26a1 1 0 0 0 1-1V14H17V2zm2 10h7.926L19 4.602V12z"></path><path d="M7 22h18v2H7zm0 4h18v2H7zm0 4h18v2H7z"></path></g>
                                            </svg>
                                        </div>
                                        <div class="cdx-attaches__file-info">
                                            <div class="cdx-attaches__title" contenteditable="false">
                                                ${obj.data.file.name}
                                            </div>
                                        </div>
                                        <a class="cdx-attaches__download-button" href="${obj.data.file.url}" target="_blank" rel="nofollow noindex noreferrer">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17pt" height="17pt" viewBox="0 0 17 17"><path d="M9.457 8.945V2.848A.959.959 0 0 0 8.5 1.89a.959.959 0 0 0-.957.957v6.097L4.488 5.891a.952.952 0 0 0-1.351 0 .952.952 0 0 0 0 1.351l4.687 4.688a.955.955 0 0 0 1.352 0l4.687-4.688a.952.952 0 0 0 0-1.351.952.952 0 0 0-1.351 0zM3.59 14.937h9.82a.953.953 0 0 0 .953-.957.952.952 0 0 0-.953-.953H3.59a.952.952 0 0 0-.953.953c0 .532.425.957.953.957zm0 0" fill-rule="evenodd"></path></svg>
                                        </a>
                                    </div>
                            </div>\n`;
                break;
            case 'embed':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="cdx-block embed-tool">
                                    <preloader class="embed-tool__preloader">
                                        <div class="embed-tool__url">${obj.data.source}</div>
                                    </preloader>
                                    <iframe width="100%" 
                                            height="${obj.data.height}" 
                                            src="${obj.data.embed}" 
                                            frameborder="0" 
                                            allow="accelerometer; 
                                            autoplay; 
                                            encrypted-media; 
                                            gyroscope; 
                                            picture-in-picture" 
                                            allowfullscreen>
                                    </iframe>`;
                if (obj.data.caption !== undefined && obj.data.caption !== null && obj.data.caption.length > 0)
                    sHTML += `<div class="cdx-input embed-tool__caption" contenteditable="false" data-placeholder="Enter a caption">${obj.data.caption}</div>`;
                sHTML += `      </div>
                            </div>
                        </div>\n`;
                break;
            case 'table':
                var rows = obj.data.content.map(row => {
                    var cells = '<tr>';
                    row.map(cell => {
                        cells += `<td class="tc-table__cell"><div class="tc-table__area"><div class="tc-table__inp" contenteditable="false">${cell}</div></div></td>`;
                    });
                    cells += '</tr>';
                    return cells;
                });
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="tc-editor cdx-block">
                                    <div class="tc-table__wrap">
                                        <table class="tc-table"><tbody>${rows.join('')}</tbody></table>
                                    </div>
                                </div>
                            </div>
                        </div>\n`;
                break;
            case 'quote':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <blockquote class="cdx-block cdx-quote">
                                    <div class="cdx-input cdx-quote__text" contenteditable="false" data-placeholder="Enter a quote" style="text-align:${obj.data.alignment}">
                                        ${obj.data.text}
                                    </div>
                                    <div class="cdx-input cdx-quote__caption" contenteditable="false" data-placeholder="Quote's author" style="text-align:${obj.data.alignment}">
                                        ${obj.data.caption}
                                    </div>
                                </blockquote>
                            </div>
                        </div>\n`;
                break;
            case 'linkTool':
                sHTML += `<div class="ce-block">
                            <div class="ce-block__content">
                                <div class="cdx-block">
                                    <div class="link-tool">
                                        <a class="link-tool__content link-tool__content--rendered" target="_blank" rel="nofollow noindex noreferrer" href="${obj.data.link}">`;
                if (obj.data.meta.image !== undefined && obj.data.meta.image.url !== undefined && obj.data.meta.image.url !== null)
                    sHTML += `<div class="link-tool__image" style="background-image: url(${obj.data.meta.image.url});"></div>`;
                if (obj.data.meta.title !== undefined && obj.data.meta.title !== null)
                    sHTML += `<div class="link-tool__title">${obj.data.meta.title}</div>`;
                if (obj.data.meta.description !== undefined && obj.data.meta.description !== null)
                    sHTML += `<p class="link-tool__description">${obj.data.meta.description}</p>`;
                if (obj.data.link !== undefined && obj.data.link !== null)
                    sHTML += `<span class="link-tool__anchor">${extractHostname(obj.data.link)}</span>`;
                sHTML += `</a>
                                    </div>
                                </div>
                            </div>
                         </div>\n`;
                break;
            default:
                break;
        }
    }

    // Close elements
    sHTML += `</div>
                <div class="codex-editor-overlay">
                    <div class="codex-editor-overlay__container">
                        <div class="codex-editor-overlay__rectangle" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>`;

    // Return HTML
    return sHTML;
}
