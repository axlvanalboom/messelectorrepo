﻿// Communication layer to backend
// Contains all backend functions

// SECTORS


// Get sectors
function getSectors(success, error) {
    serverRequest('User/getSectors', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        } 
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Add sector
function addSector(name, description, success, error) {
    serverRequest('Admin/addSector', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oSector: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit sector
function editSector(id, name, description, success, error) {
    serverRequest('Admin/editSector', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        } 
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oSector: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete sector
function deleteSector(id, success, error) {
    serverRequest('Admin/deleteSector', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// FUNCTIONS

// Get Functions
function getFunctions(success, error) {
    serverRequest('User/getFunctions', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Add Function
function addFunction(name, description, success, error) {
    serverRequest('Admin/addFunction', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oFunction: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Function
function editFunction(id, name, description, success, error) {
    serverRequest('Admin/editFunction', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oFunction: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Function
function deleteFunction(id, success, error) {
    serverRequest('Admin/deleteFunction', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// CATEGORY

// Get Categorys
function getCategories(success, error) {
    serverRequest('User/getCategories', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Add Category
function addCategory(name, description, success, error) {
    serverRequest('Admin/addCategory', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCategory: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Category
function editCategory(id, name, description, success, error) {
    serverRequest('Admin/editCategory', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCategory: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Category
function deleteCategory(id, success, error) {
    serverRequest('Admin/deleteCategory', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// ACTIVITY

// Get Activities
function getActivities(success, error) {
    serverRequest('User/getActivities', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Add Activity
function addActivity(name, description, success, error) {
    serverRequest('Admin/addActivity', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oActivity: {
                Id: 0,
                Name: name,
                Description: description
            }
        }
    );
}

// Edit Activity
function editActivity(id, name, description, success, error) {
    serverRequest('Admin/editActivity', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oActivity: {
                Id: id,
                Name: name,
                Description: description
            }
        }
    );
}

// Delete Activity
function deleteActivity(id, success, error) {
    serverRequest('Admin/deleteActivity', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// COMPANY

// Get companies
function getCompanies(success, error) {
    serverRequest('User/getCompanies', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get owned companies
function getOwnedCompanies(success, error) {
    serverRequest('Owner/getOwnedCompanies', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get companies linked
function getCompaniesLinked(success, error) {
    serverRequest('User/getCompaniesLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get companies for config
function getConfigCompanies(success, error) {
    serverRequest('Owner/getConfigCompanies', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get companies linked for config
function getConfigCompaniesLinked(success, error) {
    serverRequest('Owner/getConfigCompaniesLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get company by id 
function getCompanyById(id, success, error) {
    serverRequest('User/getCompanyById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Get company by id 
function getCompanyLinkedById(id, success, error) {
    serverRequest('User/getCompanyLinkedById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Add company
function addCompany(name, description, website, logoUrl, email, telephone, address, success, error) {
    serverRequest('Admin/addCompany', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCompany: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl,
                Email: email,
                Telephone: telephone,
                Address: address
            }
        }
    );
}

// Edit company
function editCompany(id, name, description, website, logoUrl, email, telephone, address, success, error) {
    serverRequest('Owner/editCompany', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCompany: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl,
                Email: email,
                Telephone: telephone,
                Address: address
            }
        }
    );
}

// Delete company
function deleteCompany(id, success, error) {
    serverRequest('Admin/deleteCompany', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}


// SOFTWARE

// Get Software
function getSoftwares(success, error) {
    serverRequest('User/getSoftwares', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get owned softwares
function getOwnedSoftwares(success, error) {
    serverRequest('Owner/getOwnedSoftwares', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Software linked
function getSoftwaresLinked(success, error) {
    serverRequest('User/getSoftwaresLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Software  for config
function getConfigSoftwares(success, error) {
    serverRequest('Owner/getConfigSoftwares', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Software linked for config
function getConfigSoftwaresLinked(success, error) {
    serverRequest('Owner/getConfigSoftwaresLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Software by id 
function getSoftwareById(id, success, error) {
    serverRequest('User/getSoftwareById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Get Software by id 
function getSoftwareLinkedById(id, success, error) {
    serverRequest('User/getSoftwareLinkedById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Add software
function addSoftware(name, description, website, logoUrl, companyId, activityId, success, error) {
    serverRequest('Owner/addSoftware', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oSoftware: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            },
            Company: companyId,
            Activity: activityId
        }
    );
}

// edit software
function editSoftware(id, name, description, website, logoUrl, success, error) {
    serverRequest('Owner/editSoftware', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oSoftware: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            }
        }
    );
}

// Delete software
function deleteSoftware(id, success, error) {
    serverRequest('Admin/deleteSoftware', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// CASES

// Get Case
function getCases(success, error) {
    serverRequest('User/getCases', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get owned cases
function getOwnedCases(success, error) {
    serverRequest('Owner/getOwnedCases', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}


// Get Case linked
function getCasesLinked(success, error) {
    serverRequest('User/getCasesLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Case linked for config
function getConfigCasesLinked(success, error) {
    serverRequest('Owner/getConfigCasesLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get Case by id 
function getCaseById(id, success, error) {
    serverRequest('User/getCaseById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Get Case by id 
function getCaseLinkedById(id, success, error) {
    serverRequest('User/getCaseLinkedById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Add case
function addCase(name, description, website, logoUrl, companyId, softwareId, sectorId, activityId, success, error) {
    serverRequest('Owner/addCase', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCase: {
                Id: 0,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            },
            Company: companyId,
            Software: softwareId,
            Sector: sectorId,
            Activity: activityId
        }
    );
}

// Edit case
function editCase(id, name, description, website, logoUrl, success, error) {
    serverRequest('Owner/editCase', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            oCase: {
                Id: id,
                Name: name,
                Description: description,
                Website: website,
                LogoUrl: logoUrl
            }
        }
    );
}

// Delete case
function deleteCase(id, success, error) {
    serverRequest('Admin/deleteCase', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Edit case sector
function editCaseSector(caseId, sectorId, success, error) {
    serverRequest('Owner/editCaseSector', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            CaseId: caseId,
            SectorId: sectorId
        }
    );
}

// CONTENT

// Get company content
function getCompanyContent(id, success, error) {
    serverRequest('User/getCompanyContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Get software content
function getSoftwareContent(id, success, error) {
    serverRequest('User/getSoftwareContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Get case content
function getCaseContent(id, success, error) {
    serverRequest('User/getCaseContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Edit company content
function editCompanyContent(id, content, success, error) {
    serverRequest('Owner/editCompanyContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Content: content
        }
    );
}

// Edit company content
function editCompanyVisibility(id, public, success, error) {
    serverRequest('Admin/editCompanyVisibility', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Public: public
        }
    );
}

// Edit software content
function editSoftwareContent(id, content, success, error) {
    serverRequest('Owner/editSoftwareContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Content: content
        }
    );
}

// Edit company content
function editSoftwareVisibility(id, public, success, error) {
    serverRequest('Admin/editSoftwareVisibility', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Public: public
        }
    );
}

// Edit case content
function editCaseContent(id, content, success, error) {
    serverRequest('Owner/editCaseContent', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Content: content
        }
    );
}

// Edit company content
function editCaseVisibility(id, public, success, error) {
    serverRequest('Admin/editCaseVisibility', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Public: public
        }
    );
}

// LINKS

function addSoftwareCompanyActivityLink(softwareId, companyId, activityId, success, error) {
    serverRequest('Admin/addSoftwareCompanyActivityLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            SoftwareId: softwareId,
            CompanyId: companyId,
            ActivityId: activityId
        }
    );
}

function deleteSoftwareCompanyActivityLink(softwareId, companyId, activityId, success, error) {
    serverRequest('Admin/deleteSoftwareCompanyActivityLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            SoftwareId: softwareId,
            CompanyId: companyId,
            ActivityId: activityId
        }
    );
}

function addSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, success, error) {
    serverRequest('Owner/addSoftwareCategoryFunctionLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            SoftwareId: softwareId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function deleteSoftwareCategoryFunctionLink(softwareId, categoryId, functionId, success, error) {
    serverRequest('Owner/deleteSoftwareCategoryFunctionLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            SoftwareId: softwareId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function addCaseCategoryFunctionLink(caseId, categoryId, functionId, success, error) {
    serverRequest('Owner/addCaseCategoryFunctionLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            CaseId: caseId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function deleteCaseCategoryFunctionLink(caseId, categoryId, functionId, success, error) {
    serverRequest('Owner/deleteCaseCategoryFunctionLink', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            CaseId: caseId,
            CategoryId: categoryId,
            FunctionId: functionId
        }
    );
}

function editCaseCompanySoftware(caseId, companyId, softwareId, success, error) {
    serverRequest('Owner/editCaseCompanySoftware', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            CaseId: caseId,
            CompanyId: companyId,
            SoftwareId: softwareId
        }
    );
}

function getSearchResults(links, success, error) {
    serverRequest('User/getSearchResults', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Links: links
        }
    );
}

// USERS

// Get users
function getUsers(success, error) {
    serverRequest('Admin/getUsers', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get users linked
function getUsersLinked(success, error) {
    serverRequest('Admin/getUsersLinked', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    });
}

// Get user by id 
function getUserById(id, success, error) {
    serverRequest('Admin/getUserById', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}

// Edit user 
function editUser(id, email, phoneNumber, userName, occupation, success, error) {
    serverRequest('User/editUser', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Email: email,
            PhoneNumber: phoneNumber,
            UserName: userName,
            Occupation: occupation
        }
    );
}

// Edit user 
function editUserComRo(id, companyId, roleId, success, error) {
    serverRequest('Admin/editUserComRo', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id,
            Company: companyId,
            Role: roleId
        }
    );
}

// Delete user by id 
function deleteUser(id, success, error) {
    serverRequest('Admin/deleteUser', function (result) {
        if (success !== undefined && result.Status > 0) {
            success(result.Data, result.Message);
        }
        if (error !== undefined && result.Status == 0) {
            error(result.Message);
        } 
    },
        {
            Id: id
        }
    );
}


// Function to request or send data to server side.
function serverRequest(url, success, data, error) {

    // Get data
    if (data != undefined) {
        $.ajax({
            type: "POST",
            url: window.location.protocol + "//" + window.location.host + "/" + url,
            dataType: "json",
            data: data,
            success: function (response) {
                success(response);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr, textStatus, errorThrown);
            }
        });

    }
    else {

        // Post data
        $.ajax({
            type: "GET",
            url: window.location.protocol + "//" + window.location.host + "/" + url,
            success: function (response) {
                success(response);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr, textStatus, errorThrown);
            }
        });

    }
}
