﻿"use strict";

// HELPERS
// Display return message
function statusMessage(message, good, placeholder) {
    // Show in console
    console.log(message); // Set default placeholder

    var cont = placeholder === undefined ? '.site' : placeholder; // Remove old messages

    $(cont).parent().find('.alert-container').remove(); // Set class

    var sClass = 'primary';

    if (good == true) {
        sClass = 'success';
    }

    if (good == false) {
        sClass = 'danger';
    } // Build element


    if (placeholder === undefined) {
        message = "<div class=\"alert-container\" style=\"position:fixed;width:100%;padding:1em;z-index:1000;\">\n                        <div class=\"alert alert-".concat(sClass, "\" style=\"margin:0 auto;width:100%;\" role=\"alert\">\n                            <strong>").concat(message, "</strong>\n                        </div>\n                    </div>");
    } else {
        message = "<div class=\"alert-container\">\n                        <div class=\"alert alert-".concat(sClass, "\" role=\"alert\">\n                            <strong>").concat(message, "</strong>\n                        </div>\n                    </div>");
    } // Add to page


    $(cont).parent().append(message); // Auto fade out

    setTimeout(function () {
        $(cont).parent().find('.alert-container').fadeOut();
    }, 3000);
} // delay's any callback


function debounce(func, threshold, execAsap) {
    var timeout;
    return function debounced() {
        var obj = this,
            args = arguments;

        function delayed() {
            if (!execAsap) func.apply(obj, args);
            timeout = null;
        }

        ;
        if (timeout) window.clearTimeout(timeout); else if (execAsap) func.apply(obj, args);
        timeout = window.setTimeout(delayed, threshold || 100);
    };
}

function truncate(str, no_words) {
    if (str !== undefined && str !== null) return str.split(" ").splice(0, no_words).join(" "); else return '';
} // validate url


function getValidUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var newUrl = window.decodeURIComponent(url);
    newUrl = newUrl.trim().replace(/\s/g, "");

    if (/^(:\/\/)/.test(newUrl)) {
        return "http".concat(newUrl);
    }

    if (!/^(f|ht)tps?:\/\//i.test(newUrl)) {
        return "http://".concat(newUrl);
    }

    return newUrl;
}

; // Load case detail

function loadCase(caseId) {
    sessionStorage.setItem("caseId", caseId);
    window.location.href = "CaseDetail";
} // Load company detail


function loadCompany(companyId) {
    sessionStorage.setItem("companyId", companyId);
    window.location.href = "CompanyDetail";
} // Load software detail


function loadSoftware(softwareId) {
    sessionStorage.setItem("softwareId", softwareId);
    window.location.href = "SoftwareDetail";
} // Search tag


function searchTag(tag) {
    sessionStorage.setItem("searchTags", JSON.stringify([{
        value: tag
    }]));
    window.location.href = "/";
} // render regular card


function renderCard(object, clickCardFunc) {
    var arrsAddress = [];
    if (object.Address !== undefined && object.Address !== null) arrsAddress = object.Address.split(',');

    var sContent = "\n        <div class=\"col-md-6 col-lg-4 d-flex\"";           
    sContent += " id=\"ID_".concat(object.Id, "\"");

    sContent += " > \n <article class=\"w-100 card company block\">\n                <a class=\"overlay\" href=\"javascript:void(0);\" onclick=\"".concat(clickCardFunc, "(").concat(object.Id, ");\"  ></a>\n                    <div class=\"card-body\">");

    if (object.LogoUrl !== null) {
        sContent += "<div class=\"card-media\">\n                            <img src=\"".concat(object.LogoUrl, "\" class=\"img-fluid mx-auto d-block\" alt=\"").concat(object.Name, "\" href=\"javascript:void(0);\" onclick=\"").concat(clickCardFunc, "(").concat(object.Id, ");\"  />\n                        </div>");
    } else {
        sContent += "   <h3 class=\"card-title\">".concat(object.Name, "</h3>");
    }

    sContent += "       <div class=\"card-desc wysiwyg\">\n                                <p>".concat(truncate(object.Description, 15), "</p>\n                        </div>\n                    </div>");
    sContent += "<div class=\"card-footer\">";

    if (object.Company !== undefined || object.Software !== undefined || object.Activity !== undefined || object.Sector !== undefined) {
        sContent += "<ul class=\"properties-list inner\">";
        if (object.Company !== undefined) sContent += "<li><a href=\"javascript:void(0);\" onclick=\"loadCompany(".concat(object.Company.Id, ");\"><i class=\"far fa-industry\"></i>").concat(object.Company.Name, "</a></li>");
        if (object.Software !== undefined) sContent += "<li><a href=\"javascript:void(0);\" onclick=\"loadSoftware(".concat(object.Software.Id, ");\"><i class=\"far fa-laptop-code\"></i>").concat(object.Software.Name, "</a></li>");
        if (object.Activity !== undefined) sContent += "<li><a href=\"javascript:void(0);\" onclick='searchTag(\"".concat(object.Activity.Name, "\");'><i class=\"far fa-tasks\"></i>").concat(object.Activity.Name, "</a></li>");
        if (object.Sector !== undefined) sContent += "<li><a href=\"javascript:void(0);\" onclick='searchTag(\"".concat(object.Sector.Name, "\");'><i class=\"far fa-cogs\"></i>").concat(object.Sector.Name, "</a></li>");
        sContent += "</ul>";
    } else {
        if (arrsAddress.length > 0) {
            sContent += "<address class=\"address\">";

            for (var j = 0; j < 2; j++) {
                sContent += "<span class=\"street-address\">".concat(arrsAddress[j], "</span>");
            }

            sContent += "</address>";
        }
    }

    sContent += "<span class=\"btn btn-primary icon-after\" role=\"button\" href=\"javascript:void(0);\" onclick=\"".concat(clickCardFunc, "(").concat(object.Id, ");\">Read more<i class=\"far fa-arrow-right\"></i></span>\n                    </div>\n                \n            </article>\n        </div>");
    return sContent;
} // render user card


function renderUserCard(user) {
    return "<div class=\"col-sm-12 col-md-6\">\n                           <div class=\"card\">\n                                <div class=\"card-body\">\n                                    <div class=\"row\">\n                                        <div class=\"col-8\">\n                                            <h3 class=\"card-title\">".concat(user.UserName, "</h3>\n\n                                            <ul class=\"properties-list\">\n                                                <li><i class=\"far fa-user-tie\"></i>").concat(user.Occupation, "</li>\n                                                <li><i class=\"far fa-phone\"></i> <a href=\"tel:").concat(user.PhoneNumber, "\">").concat(user.PhoneNumber, "</a></li>\n                                                <li><i class=\"far fa-envelope\"></i> <a href=\"mailto:").concat(user.Email, "\">").concat(user.Email, "</a></li>\n                                            </ul>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>");
} // Get some icons, hardcoded by category name..


function getIconName(name) {
    var sIcon = '';

    switch (name) {
        case 'Production':
            sIcon = 'industry';
            break;

        case 'Maintenance':
            sIcon = 'tools';
            break;

        case 'Inventory':
            sIcon = 'boxes';
            break;

        case 'Quality':
            sIcon = 'vial';
            break;

        default:
            sIcon = 'tools';
            break;
    }

    return sIcon;
}

(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();