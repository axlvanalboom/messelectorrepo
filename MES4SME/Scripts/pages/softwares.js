﻿var _arroSoftwares = [], _arroFilteredObjects = [];

// Display the softwares
function displayObjects(page = 0) {
    var iLimit = 9,
        iPages = _arroFilteredObjects.length / iLimit,
        iFrom = page * iLimit,
        iTo = (page + 1) * iLimit,
        sContent = '';
    if (_arroFilteredObjects.length == 0) {
        sContent = '<p>No public software to display, log in as owner to see your content...</p>';
    }
    else {
        if (iTo > _arroFilteredObjects.length)
            iTo = _arroFilteredObjects.length;

        for (var i = iFrom; i < iTo; i++)
            sContent += renderCard(_arroFilteredObjects[i], 'loadSoftware');
    }

    $('#softwares').empty();
    $('#softwares').append(sContent);

    // Render pagination
    if (iPages > 1) {
        $(".section-footer .pagination").empty();
        for (var i = 0; i < iPages; i++) {
            $(".section-footer .pagination").append(`<li class="page-item"><a onclick="displayObjects(${i});" class="page-link" >${i + 1}</a></li>`);
        }
        $(".section-footer .pagination li:nth-child(" + (page + 1) + ")").addClass('active');
        $(".section-footer").show();
    }
    else {
        $(".section-footer").hide();
    }

    for (var i = 0; i < _arroFilteredObjects.length; i++) {
        var object = _arroFilteredObjects[i]
        console.log(object.Id);
        var id = "#ID_".concat(object.Id);
        $(id).on("auxclick", function (e) {
            if (e.which == 2) {
                window.open("SoftwareDetail?SoftwareId=" + e.currentTarget.id.split("_")[1], '_blank');
            }
        });
    }

}

// Show detail function 
function loadSoftware(softwareId) {
    sessionStorage.setItem("softwareId", softwareId);
    window.location.href = "SoftwareDetail";
}

// Search softwares
var sCheckValue = null;
var _myInterval = null;
function searchSoftwares() {
    var sTag = $("#input-search").val().toUpperCase();

    if (sTag.length > 1) {

        _arroFilteredObjects = [];

        for (var i = 0; i < _arroSoftwares.length; i++)
            if ((_arroSoftwares[i].Name !== null && _arroSoftwares[i].Name.toUpperCase().includes(sTag))
                || (_arroSoftwares[i].Description !== null && _arroSoftwares[i].Description.toUpperCase().includes(sTag)))
                _arroFilteredObjects.push(_arroSoftwares[i]);

        displayObjects();

        window.clearTimeout(_myInterval);
        _myInterval = setTimeout(function () {
            if (sTag === sCheckValue) {
                console.log(sTag);

                $.ajax({
                    type: "GET",
                    url: window.location.protocol + "//" + window.location.host + "/User/SearchResult",
                    data: {
                        sType: "Search Software",
                        sParameter: sTag
                    },
                    success: function (result) {

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr, textStatus, errorThrown);
                    }
                });


            }
        },
            1000);
        sCheckValue = sTag;

    } else {
        _arroFilteredObjects = _arroSoftwares;
        displayObjects();
    }
}

// Startup function
$(function () {

    getSoftwaresLinked(function (response) {
        _arroSoftwares = response;
        _arroFilteredObjects = response;
        displayObjects();
    });

    $("#search-form").on('keyup', function () {
        searchSoftwares();
    });

});