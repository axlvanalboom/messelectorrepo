﻿// Company config definition
var _oCompanyConfig = {};

// Column definition
_oCompanyConfig.columns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'hidden',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        width: '30%'
    },
    {
        data: 'Website',
        title: 'Website',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        width: '25%'
    },
    {
        data: 'Email',
        title: 'Email',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        width: '25%'
    },
    {
        data: 'Telephone',
        title: 'Telephone',
        pattern: '^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$',
        errorMsg: 'Please fill in a valid phone number.', 
        width: '15%'
    },
    {
        data: 'Address',
        title: 'Address',
        pattern: '[a-zA-Z]{2,}|[0-9-]{1,}',
        errorMsg: 'Please fill in a valid address.', 
        visible: false
    },
    {
        data: 'Description',
        title: 'Description',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        type: 'textarea',
        visible: false
    },
    {
        data: 'Public',
        title: 'Show',
        width: '5%',
        type: 'hidden',
        render: function (data, type, row) {
            if (data)
                return '<i class="far fa-eye" style="font-weight:900;"></i>';
            else
                return '<i class="far fa-eye-slash" style="font-weight:900;"></i>';
        }
    },
    {
        data: 'LogoUrl',
        title: 'Logo',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'Contacts',
        title: 'Contacts',
        visible: false,
        type: 'hidden',
        object: true
    },
    {
        data: 'CasesSectors',
        title: 'CasesSectors',
        visible: false,
        type: 'hidden',
        object: true
    },
    {
        data: 'SoftwareActivities',
        title: 'SoftwareActivities',
        visible: false,
        type: 'hidden',
        object: true
    }
];

// Function definition
_oCompanyConfig.addData = function (datatable, rowdata, success, error) {
    rowdata.LogoUrl = null;
    rowdata.Website = getValidUrl(rowdata.Website);
    addCompany(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata.Address,
        function (data, message) {
            rowdata.Id = data;
            sessionStorage.setItem('selectId', data);
            success(rowdata, message);
            changeSelector();
        },
        function (message) {
            error(message);
        });
};

_oCompanyConfig.editData = function (datatable, rowdata, success, error) {
    rowdata.Website = getValidUrl(rowdata.Website);
    editCompany(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, rowdata.Email, rowdata.Telephone, rowdata.Address, function (data, message) {
        _oTable.row(_oTemp.rowId).data(_oTemp);
        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(rowdata, _oTemp.rowId)).show();
        _oTable.config.afterOpenRow();
        success(rowdata, message);
    },
        function (message) {
            error(message);
        });
};

_oCompanyConfig.deleteData = function (datatable, rowdata, success, error) {
    deleteCompany(rowdata.Id, function (data, message) {
        success(message);
    },
        function (message) {
            error(message);
        });
};

// Render inside of row
_oCompanyConfig.renderOpenRow = function (data, indexes) {
    // Set temp object
    _oTemp = data;
    _oTemp.rowId = indexes;

    // Build cases sectors content
    var sCases = '';
    for (var i = 0; i < data.CasesSectors.length; i++) {
        if (i == 0)
            sCases += '<tr><td>Cases:</td><td>Sector ' + data.CasesSectors[i].Sector.Name + '</td><td>' + data.CasesSectors[i].Case.Name + '</td></tr>';
        else
            sCases += '<tr><td></td><td>Sector ' + data.CasesSectors[i].Sector.Name + '</td><td>' + data.CasesSectors[i].Case.Name + '</td></tr>';
    }

    // Build software activities content
    var sSoftware = '<tr><td>Software activities:</td>';
    for (var i = 0; i < data.SoftwareActivities.length; i++) {
        sSoftware += `<td>` + data.SoftwareActivities[i].Software.Name + `</td><td>` + data.SoftwareActivities[i].Activity.Name + `</td>`;
        if (_sRole === "Administrator") {
            sSoftware += `<td><button class="deleteLink"  data-mes="` + data.SoftwareActivities[i].Software.Id + `" 
                                                        data-act="` + data.SoftwareActivities[i].Activity.Id + `" 
                                                        data-index="` + i + `">Delete</button></td>`;
                        
        }
        sSoftware += `</tr><tr><td></td>`;
    }
    if (_sRole === "Administrator") {
        sSoftware += '<td><select id="SoftwareToAdd"></select></td><td><select id="ActivityToAdd"></select></td><td><button id="AddSoftAct">Add</button></td>';
    }
    sSoftware += `</tr>`

    // Build contacts content
    var sContacts = '';
    for (var i = 0; i < data.Contacts.length; i++) {
        if (i == 0)
            sContacts += '<tr><td>Contacts:</td><td>' + data.Contacts[i].UserName + '</td><td>' + data.Contacts[i].Occupation + '</td></tr>';
        else
            sContacts += '<tr><td></td><td>' + data.Contacts[i].UserName + '</td><td>' + data.Contacts[i].Occupation + '</td></tr>';
    }

    // Build logo content
    var sLogo = '';
    if (data.LogoUrl !== undefined && data.LogoUrl !== null && data.LogoUrl !== '')
        sLogo += '<img alt="Logo" src="' + data.LogoUrl + '" height="36px" style="float:left;"></td><td>';
    sLogo += '<input type="file" name="Logo" accept = "image/*" onchange="uploadLogo(this.files);" style="padding:8px 10px;">';

    // Build public content
    var sPublic = '';
    if (_sRole === "Administrator") {
        sPublic += `<tr>
                        <td>Public:</td>
                        <td>
                            <div class="fancy-checkbox">
                                <label for="Public">
                                    <input type="checkbox" id="Public" />
                                        <i class="fa fa-fw fa-eye-slash unchecked"></i>
                                        <i class="fa fa-fw fa-eye checked" ></i>
                                </label>
                            </div>
                        </td>
                    </tr>`;
    }

    // Build total table
    return '<table id="details-table" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Address:</td>' +
        '<td colspan="3">' + data.Address + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Logo:</td>' +
        '<td>' + sLogo + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Description:</td>' +
        '<td colspan="3">' + data.Description + '</td>' +
        '</tr>' +
        sContacts + 
        sSoftware +
        sCases +
        sPublic +
        '</table>';
};

// do stuff after the table is initialised
_oCompanyConfig.afterDrawing = function () {

    if (_sRole === "Administrator") {
        // Get activities list
        getActivities(function (result) {
            _oActivities = result;

            if ($("#ActivityToAdd").length > 0) {
                // Fill in activities list
                for (var i = 0; i < _oActivities.length; i++) {
                    $("#ActivityToAdd").append(`<option value="${_oActivities[i].Id}">${_oActivities[i].Name}</option>`);
                }
            }
        });

        // Get software list
        getSoftwares(function (result) {
            _oSoftwares = result;

            if ($("#SoftwareToAdd").length > 0) {
                // Fill in software list
                for (var i = 0; i < _oSoftwares.length; i++) {
                    $("#SoftwareToAdd").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
                }
            }
        });
    }
}

// do stuff after the row is opened
_oCompanyConfig.afterOpenRow = function () {

    if (_sRole === "Administrator") {
        // Fill in software list
        for (var i = 0; i < _oSoftwares.length; i++) {
            $("#SoftwareToAdd").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
        }

        // Fill in activities list
        for (var i = 0; i < _oActivities.length; i++) {
            $("#ActivityToAdd").append(`<option value="${_oActivities[i].Id}">${_oActivities[i].Name}</option>`);
        }

        // Bind delete buttons to delete links
        $(".deleteLink").each(function (index) {
            $(this).click(function () {
                if (confirm("Are you sure you want to delete this link?")) {
                    // Delete the link
                    var softwareId = parseInt($(this).attr('data-mes'));
                    var activityId = parseInt($(this).attr('data-act'));
                    var index = parseInt($(this).attr('data-index'));
                    var companyId = _oTemp.Id;
                    deleteSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result, message) {
                        // Remove the link from the temp object
                        _oTemp.SoftwareActivities.splice(index, 1);

                        // Render the row again
                        _oTable.row(_oTemp.rowId).data(_oTemp);
                        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                        _oTable.config.afterOpenRow();
                        statusMessage(message, true, '#details-table');
                    }, function (message) {
                        statusMessage(message, false, '#details-table');
                    });
                }
            });
        });

        // Bind add button to add link
        $("#AddSoftAct").click(function () {
            if (confirm("Are you sure you want to add this link?")) {
                // Add the link
                var softwareId = parseInt($("#SoftwareToAdd").val());
                var activityId = parseInt($("#ActivityToAdd").val());
                var companyId = _oTemp.Id;
                addSoftwareCompanyActivityLink(softwareId, companyId, activityId, function (result, message) {
                    // Add the link from the temp object
                    _oTemp.SoftwareActivities.push({
                        LinkId: Math.floor((Math.random() * 100000) + 1),
                        Software: {
                            Id: softwareId,
                            Name: $("#SoftwareToAdd option:selected").text()
                        },
                        Activity: {
                            Id: activityId,
                            Name: $("#ActivityToAdd option:selected").text()
                        }
                    });
                    _oTable.row(_oTemp.rowId).data(_oTemp);
                    _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                    _oTable.config.afterOpenRow();
                    statusMessage(message, true, '#details-table');
                }, function (message) {
                    statusMessage(message, false, '#details-table');
                });
            }
        });

        // Set checkbox value
        $("#Public").prop("checked", JSON.parse(_oTemp.Public));

        // Bind add button to add link
        $("#Public").click(function () {
            // Add the link
            var public = $("#Public").prop("checked");
            var companyId = _oTemp.Id;
            editCompanyVisibility(companyId, public, function (result, message) {
                _oTemp.Public = public;
                _oTable.row(_oTemp.rowId).data(_oTemp);
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
                statusMessage(message, true, '#details-table');
            }, function (message) {
                statusMessage(message, false, '#details-table');
            });
        });
    }
}