﻿
var dsoMC = {};
var dsoRange = {};
var dsoAnswers = {};

function GetQuestions(dsoCategories) {
    //console.log(dsoCategories);

    var Filters = [];
    for (let key in dsoCategories) {
        var c = dsoCategories[key];
        var f = {};
        f.Value = c["percentage"];
        f.CategoryId = c["category"].Id;
        Filters.push(f);
    }

    //console.log(Filters)

    $.ajax({
        type: "POST",
        url: window.location.protocol + "//" + window.location.host + "/Question/GetQuestions",
        dataType: "json",
        data: {
            loFilters: Filters
        },
        success: function (result) {
            GroupQuestions(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}

function GroupQuestions(result) {

    var dso = {};

    for (i = 0; i < result.length; i++) {
        var r = result[i];

        var o = {}
        var list = [];

        if (r.Category.Id in dso) {
            list = dso[r.Category.Id].Questions;
        }

        o.Category = r.Category;
        list.push(r);
        o.Questions = list;

        dso[r.Category.Id] = o;

    }

    //console.log(dso);

    ShowQuestions(dso);
}

function ShowQuestions(result) {

    //btn =  button
    //c = content
    //q = questions

    dsoQuestions = {};

    var sContent = `<br><table class="table table-striped"> <tbody>`;

    var k=0;
    for (let key in result) {
        var r = result[key];

        var style = "";
        if (k % 2 == 1) {
            style = `style="background-color: lightgray;"`;
        }
        k++;

        sContent += `<tr id="Category_` + r.Category.Id + `" ` + style +`>
                        <td scope="row">` + r.Category.Name + ` </td>
                    </tr>`;

        sContent += `<tr id="cCategory_` + r.Category.Id + `"  style="visibility: collapse;">
                        <td scope="row">
                            <div style="padding:0.5rem;">`;

        if (r.Category.Description != null) {
            sContent += `       <p>` + r.Category.Description + `</p>`;
        }
        sContent += `           <div id="qCategory` + r.Category.Id + `">`;

        var questions = r.Questions;

        for (i = 0; i < questions.length; i++) {
            var q = questions[i];

            sContent += `           <div id="question_` + q.Id + `" style="padding:0.5rem;">
                                        <button class="collapsible" id="button_` + q.Id + `">` + q.Name + `</button>
                                        <div class="content" id="content_` + q.Id + `">
                                            <div style="padding: 1rem;">
                                                <p>` + q.Description + `</p>
                                                <div id="choice_` + q.Id + `">`;

            if (q.PossibleAnswers != null) {
                for (j = 0; j < q.PossibleAnswers.length; j++) {
                    var a = q.PossibleAnswers[j]

                    var sName = "radio_" + q.Id + "_" + a.Id;
                    sContent += `                   <input type="radio" id="`+sName+`"> ` + a.Name + ` `;
                    dsoMC[sName] = {
                        Q: q.Id,
                        A: a.Id
                    };
                }
            }
            else {


                var sSelect = "select_" + q.Id;
                var sRange = "Range" + q.Id;

                sContent += `                       <div class="row" id="row_` + q.Id + `">
                                                        <div class="col-md-1" id="column_1_` + q.Id + `">
                                                            <select id="` + sSelect + `">
                                                                <option value="==">=</option>
                                                                <option value="!=">!=</option>
                                                                <option value=">">></option>
                                                                <option value=">=">>=</option>
                                                                <option value="<"><</option>
                                                                <option value="<="><=</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1" id="column_2_` + q.Id + `">
                                                            <p id="txtRange` + q.Id + `"></p>
                                                        </div>
                                                        <div class="col-md-10" id="column_3_` + q.Id + `">
                                                            <input type="range" id="` + sRange + `" name="rangeInput" min="` + q.Minimum + `" max="` + q.Maximum + `">
                                                        </div>
                                                    </div>`;

                dsoRange[sSelect] = q.Id;
                dsoRange[sRange] = q.Id;

            }

            sContent += `                       </div>
                                            </div>
                                        </div>
                                    </div>`;

        }

        sContent += `           </div>
                            </div>
                        </td>
                    </tr>`;

    }

    sContent += `</tbody></table> <br> <button class="btn btn-primary" id="submit" onclick="analyze()">Analyze</button>`;

    $("#Main").append(sContent);
    $("#Main").show();

    SetUp();
}

function SetUp() {

    //categories
    var Categories = $('[id^="Category_"]');
    for (i = 0; i < Categories.length; i++) {
        Categories[i].addEventListener("click", function () {
            var row = document.getElementById('c' + this.id);
            if (row.style.visibility == "collapse") {
                row.style.visibility = "visible";
            }
            else {
                row.style.visibility = "collapse";
            }

        });
    }

    var coll = document.getElementsByClassName("collapsible");
    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var content = this.nextElementSibling;

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            }
            else {
                content.style.maxHeight = content.scrollHeight + "px";
                
            }

        });
    }

    //ranges init values
    //document.getElementById('range16').dispatchEvent(new Event('change'))
    var ranges = document.querySelectorAll('input[type=range]');
    for (i = 0; i < ranges.length; i++) {
        var r = ranges[i];

        ['change','input'].forEach( evt =>
            r.addEventListener(evt, ChangeRange, false)
            );
        
        r.dispatchEvent(new Event('change'));
        r.dispatchEvent(new Event('input'));
    }

    var radios = document.querySelectorAll('input[type=radio]');
    for (i = 0; i < radios.length; i++) {
        var r = radios[i];
        r.addEventListener("click", function () {
            var data = dsoMC[this.id];
            dsoAnswers[data.Q] = data.A;
        });
    }

    var selectors = document.querySelectorAll('select');
    for (i = 0; i < selectors.length; i++) {
        var s = selectors[i];
        s.selectedIndex = -1;
        s.addEventListener("change", function () {
            var q = dsoRange[this.id];
            var data = {};
            if (q in dsoAnswers) {
                data = dsoAnswers[q];
            }
            data.Operator = this.value;
            dsoAnswers[q] = data;
        });
    }
}

function ChangeRange(e) {
    document.getElementById("txt" + this.id).innerHTML = this.value;
    var q = dsoRange[this.id];
    var data = {};
    if (q in dsoAnswers) {
        data = dsoAnswers[q];
    }
    data.Value = this.value;
    dsoAnswers[q] = data;
}

function analyze() {

    console.log(dsoAnswers);
    sessionStorage.setItem('Answers', JSON.stringify(dsoAnswers));
    window.open("/Question/Result");

    //var MC = [];
    //var Range = [];

    //for (let key in dsoAnswers) {
    //    var answer = dsoAnswers[key];
    //    if (Number.isInteger(answer)) {
    //        MC.push({
    //            QuestionId: key,
    //            AnswerId: answer
    //        });
    //    }
    //    else {
    //        if (answer.Operator != null) 
    //            Range.push({
    //                QuestionId: key,
    //                Value: answer.Value,
    //                Operator: answer.Operator
    //            });
    //    }
    //}

    //console.log(MC);
    //console.log(Range);

    //$.ajax({
    //    type: "POST",
    //    url: window.location.protocol + "//" + window.location.host + "/Extra/postTest",
    //    dataType: "json",
    //    data: {
    //        oMC: MC,
    //        oRange: Range
    //    },
    //    success: function (result) {
    //        showAnalyzedData(result);
    //    },
    //    error: function (xhr, textStatus, errorThrown) {
    //        console.log(xhr, textStatus, errorThrown);
    //    }
    //});

}

$(function () {
    var dsoCategories = JSON.parse(sessionStorage.getItem("Overview"));
    GetQuestions(dsoCategories);
});
