﻿// Software config definition
var _oCaseConfig = {}, _oActSel, _oCompSel, _oSectSel, _oSoftSel;

// Column definition
_oCaseConfig.columns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'hidden',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        width: '40%'
    },
    {
        data: 'Website',
        title: 'Website',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.', 
        width: '50%'
    },
    {
        data: 'LogoUrl',
        title: 'Logo',
        visible: false,
        type: 'hidden'
    },
    {
        data: 'CategoryFunctions',
        title: 'CategoryFunctions',
        visible: false,
        type: 'hidden',
        object: true
    },
    {
        data: 'Company',
        title: 'Company',
        visible: false,
        type: 'select',
        addOnly: true,
        object: true
    },
    {
        data: 'Activity',
        title: 'Activity',
        visible: false,
        type: 'hidden',
        object: true
    },
    {
        data: 'Software',
        title: 'Software',
        visible: false,
        type: 'select',
        addOnly: true,
        object: true
    },
    {
        data: 'Sector',
        title: 'Sector',
        visible: false,
        type: 'select',
        addOnly: true,
        object: true
    },
    {
        data: 'Description',
        title: 'Description',
        pattern: '.{2,}',
        errorMsg: 'Please fill in at least 2 characters.',
        type: 'textarea',
        visible: false
    },
    {
        data: 'Public',
        title: 'Show',
        width: '10%',
        type: 'hidden',
        render: function (data, type, row) {
            if (data)
                return '<i class="far fa-eye" style="font-weight:900;"></i>';
            else
                return '<i class="far fa-eye-slash" style="font-weight:900;"></i>';
        }
    }
];

// Function definition
_oCaseConfig.addData = function (datatable, rowdata, success, error) {

    var companyId = _oCompSel.val();
    var sectorId = _oSectSel.val();
    var softwareId = _oSoftSel.val();

    // Check if a software was choosen, maybe the user needs to add a software first..
    if (softwareId == null) {
        //statusMessage("No software was selected, make sure you already have a software before adding a case.");
        error("No software was selected, make sure you already have a software before adding a case.");
        return;
    }

    // Find activity for this company and software
    var software = _oSoftwares.find(function (element) { if (element.Id == softwareId) return element; });
    var activity = software.CompanyActivities.find(function (element) {
        if (element.Company.Id == companyId)
            return element;
    }).Activity;

    var activityId = activity.Id;

    rowdata.Website = getValidUrl(rowdata.Website);

    addCase(rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl, companyId, softwareId, sectorId, activityId,
        function (data, message) {

            rowdata.Id = data;
            rowdata.LogoUrl = null;

            rowdata.Company = {
                Id: companyId,
                Name: _oCompanies.find(function (element) { if (element.Id == companyId) return element; }).Name
            };

            rowdata.Activity = {
                Id: activityId,
                Name: activity.Name
            };

            rowdata.Software = {
                Id: softwareId,
                Name: software.Name
            };

            rowdata.Sector = {
                Id: sectorId,
                Name: _oSectors.find(function (element) { if (element.Id == sectorId) return element; }).Name
            };

            rowdata.CategoryFunctions = [];

            sessionStorage.setItem('selectId', data);
            // this was commented out?
            success(rowdata, message);
            changeSelector();
        },
        function (message) {
            error(message);
        });

};

_oCaseConfig.editData = function (datatable, rowdata, success, error) {

    rowdata.Website = getValidUrl(rowdata.Website);

    editCase(rowdata.Id, rowdata.Name, rowdata.Description, rowdata.Website, rowdata.LogoUrl,
        function (data, message) {
            _oTable.row(_oTemp.rowId).data(_oTemp);
            _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(rowdata, _oTemp.rowId)).show();
            _oTable.config.afterOpenRow();
            success(rowdata, message);
            
        },
        function (message) {
            error(message);
        });


};

_oCaseConfig.deleteData = function (datatable, rowdata, success, error) {
    deleteCase(rowdata.Id, function (data, message) {
            success(message);
        },
        function (message) {
            error(message);
        });
};

// Render inside of row
_oCaseConfig.renderOpenRow = function (data, indexes) {
    // Set temp object
    _oTemp = data; 
    _oTemp.rowId = indexes;

    // Build sector content
    var sSector = '<tr><td>Case sector:</td><td><select id="CaseSector"></select></td><td><button id="EditSector">Edit</button></td></tr>';

    // Build company content
    var sCompany = '';
    if (_sRole === "Administrator") {
        sCompany = '<tr><td>Company:</td><td><select id="CaseCompany"></select></td><td><button id="EditCompany">Edit</button></td></tr>';
    } else {
        sCompany = `<tr><td>Company:</td><td>${_oTemp.Company.Name}</tr>`;
    }

    // Build software content
    var sSoftware = '<tr><td>Software:</td><td><select id="CaseSoftware"></select></td><td><button id="EditSoftware">Edit</button></td></tr>';

    // Build activity content
    var sActivity = `<tr><td>Activity:</td><td>${_oTemp.Activity.Name}</td></tr>`;

    // Build category functions content
    var sCategory = '<tr><td>Category functions:</td>';
    for (var i = 0; i < data.CategoryFunctions.length; i++) {
        sCategory += `<td>` + data.CategoryFunctions[i].Name + `</td>`;
        for (var j = 0; j < data.CategoryFunctions[i].Functions.length; j++) {
            sCategory += `<td>` + data.CategoryFunctions[i].Functions[j].Name + `</td>
                            <td><button class="deleteCatFunc"   data-cat="` + data.CategoryFunctions[i].Id + `"
                                                                data-func="` + data.CategoryFunctions[i].Functions[j].Id + `"
                                                                data-cat-index="` + i + `"
                                                                data-func-index="` + j + `">Delete</button></td >
                            </tr><tr>`;
            if (j < data.CategoryFunctions[i].Functions.length - 1)
                sCategory += `<td></td><td></td>`;
        }
        sCategory += `<td></td>`;
    }
    sCategory += '<td><select id="CategoryToAdd"></select></td><td><select id="FunctionToAdd"></select></td><td><button id="AddCatFunc">Add</button></td></tr>';

    // Build logo content
    var sLogo = '';
    if (data.LogoUrl !== undefined && data.LogoUrl !== null && data.LogoUrl !== '')
        sLogo += '<img alt="Logo" src="' + data.LogoUrl + '" height="36px" style="float:left;"></td><td>';
    sLogo += '<input type="file" name="Logo" accept="image/*" onchange="uploadLogo(this.files);" style="padding:8px 10px;">';

    // Build public content
    var sPublic = '';
    if (_sRole === "Administrator") {
        sPublic += `<tr>
                        <td>Public:</td>
                        <td>
                            <div class="fancy-checkbox">
                                <label for="Public">
                                    <input type="checkbox" id="Public" />
                                        <i class="fa fa-fw fa-eye-slash unchecked"></i>
                                        <i class="fa fa-fw fa-eye checked" ></i>
                                </label>
                            </div>
                        </td>
                    </tr>`;
    }

    // Build total table
    return '<table id="details-table" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Logo:</td>' +
        '<td>' + sLogo + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Description:</td>' +
        '<td colspan="3">' + data.Description + '</td>' +
        '</tr>' +
        sSector + 
        sCompany +
        sSoftware +
        sActivity +
        sCategory +
        sPublic +
        '</table>';
};

// do stuff after the table is initialised
_oCaseConfig.afterDrawing = function () {

    // Get activities list
    getActivities(function (result) {
        _oActivities = result;
    });

    // Get company list
    getConfigCompanies(function (result) {
        _oCompanies = result;

        if ($("#CaseCompany").length > 0) {
            // Fill in company list
            for (var i = 0; i < _oCompanies.length; i++) {
                $("#CaseCompany").append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
            }

            if (_oTemp !== undefined)
                $("#CaseCompany").val(_oTemp.Company.Id);

        }
    });
    
    // Get softwares list
    getConfigSoftwares(function (result) {
        _oSoftwares = result;

        if ($("#CaseSoftware").length > 0) {
            // Fill in software list
            // loop softwares and return the ones that are linked to it
            for (var i = 0; i < _oSoftwares.length; i++) {
                for (var j = 0; j < _oSoftwares[i].CompanyActivities.length; j++) {
                    // Check if software is linked to this company and then add it
                    if (_oSoftwares[i].CompanyActivities[j].Company.Id == _oTemp.Company.Id) {
                        $("#CaseSoftware").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
                        break;
                    }
                }
            }

            if (_oTemp !== undefined)
                $("#CaseSoftware").val(_oTemp.Software.Id);
        }

    });

    // Get sector list
    getSectors(function (result) {
        _oSectors = result;

        if ($("#CaseSector").length > 0) {
            // Fill in sector list
            for (var i = 0; i < _oSectors.length; i++) {
                $("#CaseSector").append(`<option value="${_oSectors[i].Id}">${_oSectors[i].Name}</option>`);
            }

            if (_oTemp !== undefined)
                $("#CaseSector").val(_oTemp.Sector.Id);
        }
    });

    // Get category list
    getCategories(function (result) {
        _oCategories = result;

        if ($("#CategoryToAdd").length > 0) {
            // Fill in category list
            for (var i = 0; i < _oCategories.length; i++) {
                $("#CategoryToAdd").append(`<option value="${_oCategories[i].Id}">${_oCategories[i].Name}</option>`);
            }
        }
    });

    // Get functions list
    getFunctions(function (result) {
        _oFunctions = result;

        if ($("#FunctionToAdd").length > 0) {
            // Fill in functions list
            for (var i = 0; i < _oFunctions.length; i++) {
                $("#FunctionToAdd").append(`<option value="${_oFunctions[i].Id}">${_oFunctions[i].Name}</option>`);
            }
        }
    });
}

// do stuff after the row is opened
_oCaseConfig.afterOpenRow = function () {

    if (_sRole === "Administrator") {
        // Fill in company list
        for (var i = 0; i < _oCompanies.length; i++) {
            $("#CaseCompany").append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
        }
        $("#CaseCompany").val(_oTemp.Company.Id);

        $("#CaseCompany").on('change', function () {
            // Get the selected company id
            var iCompanyId = $("#CaseCompany").val();

            // Clear the previous options
            $("#CaseSoftware").find('option').remove().end();

            // loop softwares and return the ones that are linked to it
            for (var i = 0; i < _oSoftwares.length; i++) {
                for (var j = 0; j < _oSoftwares[i].CompanyActivities.length; j++) {
                    // Check if software is linked to this company and then add it
                    if (_oSoftwares[i].CompanyActivities[j].Company.Id == iCompanyId) {
                        $("#CaseSoftware").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
                        break;
                    }
                }
            }
        });

        // Set checkbox value
        $("#Public").prop("checked", JSON.parse(_oTemp.Public));

        // Bind add button to add link
        $("#Public").click(function () {
            // Add the link
            var public = $("#Public").prop("checked");
            var caseId = _oTemp.Id;
            editCaseVisibility(caseId, public, function (result, message) {
                _oTemp.Public = public;
                _oTable.row(_oTemp.rowId).data(_oTemp);
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
                statusMessage(message, true, '#details-table');
            }, function (message) {
                statusMessage(message, false, '#details-table');
            });
        });
    }

    // Fill in sector list
    for (var i = 0; i < _oSectors.length; i++) {
        $("#CaseSector").append(`<option value="${_oSectors[i].Id}">${_oSectors[i].Name}</option>`);
    }
    $("#CaseSector").val(_oTemp.Sector.Id);

    // Fill in software list
    // loop softwares and return the ones that are linked to it
    for (var i = 0; i < _oSoftwares.length; i++) {
        for (var j = 0; j < _oSoftwares[i].CompanyActivities.length; j++) {
            // Check if software is linked to this company and then add it
            if (_oSoftwares[i].CompanyActivities[j].Company.Id == _oTemp.Company.Id) {
                $("#CaseSoftware").append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
                break;
            }
        }
    }
    $("#CaseSoftware").val(_oTemp.Software.Id);

    // Fill in category list
    for (var i = 0; i < _oCategories.length; i++) {
        $("#CategoryToAdd").append(`<option value="${_oCategories[i].Id}">${_oCategories[i].Name}</option>`);
    }

    // Fill in functions list
    for (var i = 0; i < _oFunctions.length; i++) {
        $("#FunctionToAdd").append(`<option value="${_oFunctions[i].Id}">${_oFunctions[i].Name}</option>`);
    }

    // Bind delete buttons to delete links
    $(".deleteCatFunc").each(function (index) {
        $(this).click(function () {
            if (confirm("Are you sure you want to delete this link?")) {
                // Delete the link
                var categoryId = parseInt($(this).attr('data-cat'));
                var functionId = parseInt($(this).attr('data-func'));
                var categoryObjectId = parseInt($(this).attr('data-cat-index'));
                var functionObjectId = parseInt($(this).attr('data-func-index'));
                var caseId = _oTemp.Id;
                deleteCaseCategoryFunctionLink(caseId, categoryId, functionId, function (result, message) {
                    // Remove the link from the temp object
                    _oTemp.CategoryFunctions[categoryObjectId].Functions.splice(functionObjectId, 1);
                    if (_oTemp.CategoryFunctions[categoryObjectId].Functions.length == 0)
                        _oTemp.CategoryFunctions.splice(categoryObjectId, 1);

                    // Render the open row again
                    _oTable.row(_oTemp.rowId).data(_oTemp);
                    _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                    _oTable.config.afterOpenRow();
                    statusMessage(message, true, '#details-table');
                }, function (message) {
                    statusMessage(message, false, '#details-table');
                });
            }
        });
    });

    // Bind add button to add link
    $("#AddCatFunc").click(function () {
        // Add the link
        var categoryId = parseInt($("#CategoryToAdd").val());
        var functionId = parseInt($("#FunctionToAdd").val());
        var caseId = _oTemp.Id;
        addCaseCategoryFunctionLink(caseId, categoryId, functionId, function (result, message) {
            // Add the link to the temp object
            var i;
            for (i = 0; i < _oTemp.CategoryFunctions.length; i++) {
                if (_oTemp.CategoryFunctions[i].Id == categoryId)
                    break;
            }
            if (i == _oTemp.CategoryFunctions.length) {
                // Add category if it did not exist 
                _oTemp.CategoryFunctions.push({
                    Id: categoryId,
                    Name: $("#CategoryToAdd option:selected").text(),
                    Functions: []
                });
            }

            // Add function
            _oTemp.CategoryFunctions[i].Functions.push({
                Id: functionId,
                Name: $("#FunctionToAdd option:selected").text()
            });

            // Render the open row again
            _oTable.row(_oTemp.rowId).data(_oTemp);
            _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
            _oTable.config.afterOpenRow(); statusMessage(message, true, '#details-table');
        }, function (message) {
            statusMessage(message, false, '#details-table');
        });
    });

    // Edit the case sector
    $("#EditSector").click(function () {
        // Edit the link
        var caseId = _oTemp.Id;
        var sectorId = parseInt($("#CaseSector").val());
        editCaseSector(caseId, sectorId, function (result, message) {
            // Edit the temp object
            _oTemp.Sector.Id = sectorId;
            _oTemp.Sector.Name = $("#CaseSector option:selected").text();

            // Render the open row again
            _oTable.row(_oTemp.rowId).data(_oTemp);
            _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
            _oTable.config.afterOpenRow(); statusMessage(message, true, '#details-table');
        }, function (message) {
            statusMessage(message, false, '#details-table');
        });
    });

    // Edit the case company software
    $("#EditSoftware, #EditCompany").click(function () {
        if (confirm("Are you sure you want to edit the company or software?")) {
            // Edit the link
            var caseId = _oTemp.Id;
            var softwareId = parseInt($("#CaseSoftware").val());
            var companyId = '';
            if (_sRole === "Administrator") {
                companyId = parseInt($("#CaseCompany").val());
            } else {
                companyId = _oTemp.Company.Id;
            }
            editCaseCompanySoftware(caseId, companyId, softwareId, function (iActivity, message) {
                // Edit the temp object
                _oTemp.Activity.Name = [];
                getActivities(
                    function (result) {

                        for (i = 0; i < result.length; i++) {
                            if (result[i].Id == iActivity) {
                                _oTemp.Activity.Id = result[i].Id;
                                _oTemp.Activity.Name = result[i].Name;
                                break;
                            }
                        }

                        _oTemp.Software.Id = softwareId;
                        _oTemp.Software.Name = $("#CaseSoftware option:selected").text();
                        _oTemp.Company.Id = companyId;
                        _oTemp.Company.Name = $("#CaseCompany option:selected").text();

                        //_oTemp.CategoryFunctions = [];

                        // Render the open row again
                        _oTable.row(_oTemp.rowId).data(_oTemp);
                        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                        _oTable.config.afterOpenRow();
                        statusMessage(message, true, '#details-table');

                    },
                    function () { _oTemp.Activity.Name = []; }
                );
                         
            }, function (message) {
                statusMessage(message, false, '#details-table');
            });
        }
    });

}

function filterSoftwares() {

    // Get the selected company id
    var iCompanyId = _oCompSel.val();

    // Clear the previous options
    _oSoftSel.find('option').remove().end();

    // loop softwares and return the ones that are linked to it
    for (var i = 0; i < _oSoftwares.length; i++) {
        for (var j = 0; j < _oSoftwares[i].CompanyActivities.length; j++) {
            // Check if software is linked to this company and then add it
            if (_oSoftwares[i].CompanyActivities[j].Company.Id == iCompanyId) {
                _oSoftSel.append(`<option value="${_oSoftwares[i].Id}">${_oSoftwares[i].Name}</option>`);
                break;
            }
        }
    }

}

// do stuff before the add window is opening
_oCaseConfig.onOpenAddModal = function () {

    _oCompSel = $('.modal-body select[name="Company"]');
    for (var i = 0; i < _oCompanies.length; i++) {
        _oCompSel.append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
    }

    _oSoftSel = $('.modal-body select[name="Software"]');
    filterSoftwares();

    _oCompSel.on('change', function () {
        filterSoftwares();
    });

    _oSectSel = $('.modal-body select[name="Sector"]');
    for (var i = 0; i < _oSectors.length; i++) {
        _oSectSel.append(`<option value="${_oSectors[i].Id}">${_oSectors[i].Name}</option>`);
    }

}