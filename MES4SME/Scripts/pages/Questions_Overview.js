﻿
function showTooltip(evt, target) {
    var tooltip = document.getElementById('description');
    tooltip.setAttributeNS(null, "style", "visibility: visible; display: block;");

    var title = document.getElementById("title");
    title.innerHTML = target.id;

    var text = document.getElementById('text');
    text.innerHTML = target.getAttributeNS(null, "data-tooltip-text");

}
function hideTooltip() {
    var tooltip = document.getElementById('description');
    tooltip.setAttributeNS(null, "style", "visibility: hidden; display: block;");
}

dsoCategories = {};

function clicker(sender) {

    var value = null;
    while (value == null) {
        var value = prompt("Percentage", "50");
        console.log(value);
        if (value == null|| value == "") {//cancel
            //value = null;
            //console.log("null or empty");
            return;
        }
        else {
            var i = parseInt(value) || -1;
            //console.log(i);
            if (value != "0" && (i < 0 || i > 100)) {
                value = null;
            }
        }
    }

    var x = dsoCategories[sender];
    x["percentage"]= value;
    dsoCategories[sender] = x;

    console.log(dsoCategories);
    console.log(sender);

    Show();
}

function Show() {

    $("#Main").html("");

    var sContent = `

        <div class="row">
                <div class="col-9">
                    <svg version="1.1" id="gam" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 445 260" style="enable-background:new 0 0 445 260;" xml:space="preserve"> <defs>`;
                    
    for (let key in dsoCategories) {
        // do something for each key in the object 

        var c = dsoCategories[key];

        var iPercentage = c["percentage"];
        console.log(iPercentage);

        sContent += `
                        <linearGradient id="lg`+ key + `" gradientTransform="rotate(0)">
                            <stop offset="`+ iPercentage + `%" stop-color="#FFD300" />
                            <stop offset="`+ iPercentage + `%" stop-color="white" />
                        </linearGradient>`;
    }

    sContent += `</defs> <g id="Detailed_Scheduling" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Het plannings gedeelte" onclick=clicker('DetailedScheduling')>
                            <ellipse  cx="215.5" cy="75.5" rx="44" ry="19" fill="url('#lgDetailedScheduling')" />
                            <text transform="matrix(1 0 0 1 194.0938 70.9385)">
                                <tspan x="0" y="0" class="st1 st2">Detailed</tspan>
                                <tspan x="-7.1" y="14.4" class="st1 st2">Scheduling</tspan>
                            </text>
                        </g>

                        <g id="Analysis" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Het analyserend gedeemte" onclick=clicker('Analysis')>
                            <ellipse  cx="391.5" cy="151.5" rx="44" ry="19" fill="url('#lgAnalysis')"/>
                            <text transform="matrix(1 0 0 1 370.8916 154.9385)" class="st1 st2">Analysis</text>
                        </g>

                        <g id="Tracking" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Het deel dat instaat voor tracking" onclick=clicker('Tracking')>
                            <ellipse  cx="303.5" cy="113.5" rx="44" ry="19" fill="url('#lgTracking')"/>
                            <text transform="matrix(1 0 0 1 281.7578 116.9385)" class="st1 st2">Tracking</text>
                        </g>

                        <g id="Dispatching" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Verdelen van de taken over de verschillende workstations/machines" onclick=clicker('Dispatching')>
                            <ellipse  cx="215.5" cy="151.5" rx="44" ry="19" fill="url('#lgDispatching')"/>
                            <text transform="matrix(1 0 0 1 185.0103 154.9385)" class="st1 st2">Dispatching</text>
                        </g>

                        <g id="Resource_Management" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="De master data over de resources" onclick=clicker('ResourceManagement')>
                            <ellipse  cx="127.5" cy="113.5" rx="44" ry="19" fill="url('#lgResourceManagement')"/>
                            <text transform="matrix(1 0 0 1 104.3179 108.9385)">
                                <tspan x="0" y="0" class="st1 st2">Resource </tspan>
                                <tspan x="-10.8" y="14.4" class="st1 st2">Management</tspan>
                            </text>
                        </g>

                        <g id="Data_Collection" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Verzamelen van data" onclick=clicker('DataCollection')>
                            <ellipse  cx="303.5" cy="189.5" rx="44" ry="19" fill="url('#lgDataCollection')"/>
                            <text transform="matrix(1 0 0 1 291.418 184.9385)">
                                <tspan x="0" y="0" class="st1 st2">Data </tspan>
                                <tspan x="-13.5" y="14.4" class="st1 st2">Collection</tspan>
                            </text>
                        </g>

                        <g id="Definition_Management" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="De master data over de producten." onclick=clicker('DefinitionManagement')>
                            <ellipse  cx="127.5" cy="189.5" rx="44" ry="19" fill="url('#lgDefinitionManagement')"/>
                            <text transform="matrix(1 0 0 1 102.2959 184.9385)">
                                <tspan x="0" y="0" class="st1 st2">Definition </tspan>
                                <tspan x="-8.7" y="14.4" class="st1 st2">Management</tspan>
                            </text>
                        </g>

                        <g id="Execution_Management" onmousemove="showTooltip(evt,this);" onmouseout="hideTooltip();" data-tooltip-text="Effectief uitvoeren van de nodig stappen (machines/mensen aansturen)." onclick=clicker('ExecutionManagement')>
                            <ellipse  cx="215.5" cy="227.5" rx="44" ry="19" fill="url('#lgExecutionManagement')"/>
                            <text transform="matrix(1 0 0 1 190.4399 222.9385)">
                                <tspan x="0" y="0" class="st1 st2">Execution </tspan>
                                <tspan x="-8.9" y="14.4" class="st1 st2">Management</tspan>
                            </text>
                        </g>

                        <path class="st3" d="M127,132c0,10.5,19.7,19,44,19" />

                        <g>
                            <g>
                                <path class="st0" d="M127,170c0-9.6,16.5-17.6,38-18.8" />
                                <g>
                                    <path d="M171,151c-2.8,1.2-6.3,3.1-8.4,5.1l1.5-4.8l-1.9-4.7C164.5,148.4,168.1,150,171,151z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M127.5,132.5c0,9.4,16.3,17.1,37.5,18.3" />
                                <g>
                                    <path d="M171,151c-2.9,1-6.5,2.6-8.7,4.5l1.9-4.7l-1.6-4.8C164.7,147.9,168.2,149.8,171,151z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M127.5,94.5c0-9.6,16.5-17.6,38-18.8" />
                                <g>
                                    <path d="M171.5,75.5c-2.8,1.2-6.3,3.1-8.4,5.1l1.5-4.8l-1.9-4.7C165,72.9,168.6,74.5,171.5,75.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M303.5,94.5c0-9.6-16.5-17.6-38-18.8" />
                                <g>
                                    <path d="M259.5,75.5c2.9-1,6.5-2.6,8.7-4.4l-1.9,4.7l1.5,4.8C265.8,78.6,262.3,76.7,259.5,75.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M301.5,138.2c-5.6,7.7-22.3,13.3-42,13.3" />
                                <g>
                                    <path d="M303.5,132.5c-2,2.3-5,4.9-7.5,6.2l5.1,0.1l3.8,3.3C303.9,139.5,303.5,135.5,303.5,132.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M301.5,164.8c-5.6-7.7-22.3-13.3-42-13.3" />
                                <g>
                                    <path d="M303.5,170.5c0-3,0.4-7,1.3-9.7l-3.8,3.3l-5.1,0.1C298.5,165.6,301.5,168.2,303.5,170.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M301.5,214.2c-5.6,7.7-22.3,13.3-42,13.3" />
                                <g>
                                    <path d="M303.5,208.5c-2,2.3-5,4.9-7.5,6.2l5.1,0.1l3.8,3.3C303.9,215.5,303.5,211.5,303.5,208.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M127.5,208.5c0,9.6,16.5,17.6,38,18.8" />
                                <g>
                                    <path d="M171.5,227.5c-2.9,1-6.5,2.6-8.7,4.4l1.9-4.7l-1.5-4.8C165.2,224.4,168.7,226.3,171.5,227.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M391.5,132.5c0-9.6-16.5-17.6-38-18.8" />
                                <g>
                                    <path d="M347.5,113.5c2.9-1,6.5-2.6,8.7-4.4l-1.9,4.7l1.5,4.8C353.8,116.6,350.3,114.7,347.5,113.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M389.5,176.2c-5.6,7.7-22.3,13.3-42,13.3" />
                                <g>
                                    <path d="M391.5,170.5c-2,2.3-5,4.9-7.5,6.2l5.1,0.1l3.8,3.3C391.9,177.5,391.5,173.5,391.5,170.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <line class="st0" x1="215.5" y1="94.5" x2="215.5" y2="126.5" />
                                <g>
                                    <path d="M215.5,132.5c-1.1-2.8-2.9-6.4-4.8-8.5l4.8,1.7l4.8-1.7C218.4,126.1,216.6,129.7,215.5,132.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <line class="st0" x1="215.5" y1="170.5" x2="215.5" y2="202.5" />
                                <g>
                                    <path d="M215.5,208.5c-1.1-2.8-2.9-6.4-4.8-8.5l4.8,1.7l4.8-1.7C218.4,202.1,216.6,205.7,215.5,208.5z" />
                                </g>
                            </g>
                        </g>

                        <text transform="matrix(1 0 0 1 12.2021 18.6665)"><tspan x="0" y="0" class="st1 st2">Operations </tspan><tspan x="0.5" y="14.4" class="st1 st2">Definitions</tspan></text>
                        <text transform="matrix(1 0 0 1 100.2021 18.6665)"><tspan x="0" y="0" class="st1 st2">Operations </tspan><tspan x="2.7" y="14.4" class="st1 st2">Capability</tspan></text>
                        <text transform="matrix(1 0 0 1 188.2021 18.6665)"><tspan x="0" y="0" class="st1 st2">Operations </tspan><tspan x="7.5" y="14.4" class="st1 st2">Request</tspan></text>
                        <text transform="matrix(1 0 0 1 275.2021 18.6665)"><tspan x="0" y="0" class="st1 st2">Operations </tspan><tspan x="3.8" y="14.4" class="st1 st2">Response</tspan></text>
                              
                        <g>
                            <g>
                                <line class="st0" x1="215" y1="39" x2="215" y2="51" />
                                <g>
                                    <path d="M215,57c-1.1-2.8-2.9-6.4-4.8-8.5l4.8,1.7l4.8-1.7C217.9,50.6,216.1,54.2,215,57z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <line class="st0" x1="303.5" y1="94.5" x2="303.5" y2="45.5" />
                                <g>
                                    <path d="M303.5,39.5c1.1,2.8,2.9,6.4,4.8,8.5l-4.8-1.7l-4.8,1.7C300.6,45.9,302.4,42.3,303.5,39.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <line class="st0" x1="127.5" y1="94.5" x2="127.5" y2="45.5" />
                                <g>
                                    <path d="M127.5,39.5c1.1,2.8,2.9,6.4,4.8,8.5l-4.8-1.7l-4.8,1.7C124.6,45.9,126.4,42.3,127.5,39.5z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M39.5,45.5c0.8,73.4,17.1,133.1,38.1,142.6" />
                                <g>
                                    <path d="M39.5,39.5c-1,2.9-2.8,6.4-4.6,8.6l4.7-1.8l4.8,1.6C42.4,45.8,40.6,42.3,39.5,39.5z" />
                                </g>
                                <g>
                                    <path d="M83.5,189.5c-3,0.3-6.9,1.2-9.4,2.6l2.8-4.2l-0.5-5C78,185.2,81,187.8,83.5,189.5z" />
                                </g>
                            </g>
                        </g>
                           
            
                    </svg>
                </div>
                <div class="col-3" style="padding:1rem;">

                    <button id="btnSubmit" class="btn btn-primary" onclick=submit()>Submit</button>

                    <div id="description" style="visibility: hidden;">
                        <h3 id="title"></h3>
                        <p id="text"></p>
                    </div>
                </div>
            </div>`;

    $("#Main").append(sContent);
    $("#Main").show();

}

function submit() {
    sessionStorage.setItem('Overview', JSON.stringify(dsoCategories));
    window.open("/Question/Detail");
}

function Setup(data) {
    dsoCategories = {};
    for (i = 0; i < data.length; i++) {
        var c = data[i];
        var d = {};
        d["category"] = c;
        d["percentage"] = 0;
        dsoCategories[c.Name.replace(' ', '')] = d;
    }
}

function GetCategories() {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Question/GetQuestions_Overview",
        success: function (result) {
            Setup(result);
            Show();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}

$(function () {
    GetCategories();
});