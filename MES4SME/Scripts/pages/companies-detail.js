﻿var _iCompanyId = null,
    _oCompany = {},
    _arriOwnerIds = [],
    _sRole = $('#Role').text();

// Display detail 
function displayDetail() {

    var sContent = `<div class="container">

        <div class="section-header d-md-flex align-items-center">
            <div class="mr-auto">
                <h2 class="section-title">Company info</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/"><i class="far fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="/Companies">Companies</a></li>
                    <li class="breadcrumb-item active">${_oCompany.Name}</li>
                </ol>
            </div>

        </div> 
    
        <div class="section-content">

                <section id="company-detail">
                    <i class="editObject far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit info"></i>
                    <div class="row">
                        <div class="col-md-6">

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">`;
    if (_oCompany.LogoUrl !== null) {
        sContent +=                     `<div class="col-4" style="display:flex;align-items:center;">
                                            <img src="${_oCompany.LogoUrl}" class="img-fluid company-logo" />
                                        </div>`;
    }

    sContent +=                         `<div class="col-8">
                                            <h3 class="card-title">${_oCompany.Name}</h3>

                                            <ul class="properties-list">
                                                <li><i class="far fa-map-marker-alt"></i>${_oCompany.Address}</li>
                                                <li><i class="far fa-phone"></i> <a href="tel:${_oCompany.Telephone}">${_oCompany.Telephone}</a></li>
                                                <li><i class="far fa-envelope"></i> <a href="mailto:${_oCompany.Email}">${_oCompany.Email}</a></li>
                                                <li><i class="far fa-link"></i> <a href="${getValidUrl(_oCompany.Website)}" class="target-blank">${_oCompany.Website}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
    
                        <div class="col-md-6 d-flex flex-column justify-content-center">
                            <h3>Description</h3>
                            <p>${_oCompany.Description}</p>
                        </div>
    
                </div> 
            </section>`;

    sContent += `<section id="editor-content">
                    <i class="editContent far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit content"></i>
                    <div id="EditorContainer">`; 
    if (_oCompany.Content !== null) 
        sContent += renderEditorData(JSON.parse(_oCompany.Content));
    sContent += `</div>`;
    sContent += `</section>
                <section id="related-cases">`;

    if (_oCompany.CasesSectors.length > 0) {
        
        sContent += `<h2 class="section-title">Cases</h2>
                    <div class="row">`;
        for (var i = 0; i < _oCompany.CasesSectors.length; i++) {
            _oCompany.CasesSectors[i].Case.Sector = _oCompany.CasesSectors[i].Sector;
            sContent += renderCard(_oCompany.CasesSectors[i].Case, 'loadCase');
        }

        sContent += `</div>`;

    }

    if (_oCompany.SoftwareActivities.length > 0) {

        sContent += `<h2 class="section-title">Software</h2>
                                <div class="row">`;
        for (var i = 0; i < _oCompany.SoftwareActivities.length; i++) {
            _oCompany.SoftwareActivities[i].Software.Activity = _oCompany.SoftwareActivities[i].Activity;
            sContent += renderCard(_oCompany.SoftwareActivities[i].Software, 'loadSoftware');
        }

        sContent += `</div>`;

    }

    if (_oCompany.Contacts.length > 0) {

        sContent += `<h2 class="section-title">Contacts</h2>
                    <div class="row">`;
        for (var i = 0; i < _oCompany.Contacts.length; i++) {
            sContent += renderUserCard(_oCompany.Contacts[i]);
        }

        sContent += `</div>`;

    }

    sContent +=    `</section>
                </div>    
            </div>`;

    $("#the-content").append(sContent);
    $("#the-content").show();

    $('.editContent').on('click', function () { openEditor(); });

    $('.editObject').on('click', function () {
        sessionStorage.setItem('configSelected', 'Companies');
        sessionStorage.setItem('selectId', _oCompany.Id);
        window.location.href = 'Config';
    });

    $('[data-toggle="tooltip"]').tooltip();
}

// Open editor function
function openEditor() {

    $('.editContent').hide();

    buildEditor(JSON.parse(_oCompany.Content), 'EditorContainer',
        function (data) {
            editCompanyContent(_iCompanyId, JSON.stringify(data), function (result) {
                _oCompany.Content = JSON.stringify(data);
                statusMessage('Content updated.');
            });
        },
        function () {
            $('.tooltip').remove();
            $("#the-content").empty();
            displayDetail();
            $(".editContent").show();
        }
    );
}

// Startup function
$(function () {

    _iCompanyId = parseInt(sessionStorage.getItem("companyId"));

    if (isNaN(_iCompanyId)) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        _iCompanyId = urlParams.get('CompanyId');
    }

    if (_iCompanyId == null || _iCompanyId == 0)
        window.location.href = "Companies";
    else {
        switch (_sRole) {
            case "Administrator":
                getCompanyLinkedById(_iCompanyId, function (result) {
                    _oCompany = result;
                    displayDetail();
                    $(".editContent").show();
                    $(".editObject").show();
                }, function () {
                    window.location.href = "Companies";
                });
                break;
            case "Owner":
                getOwnedCompanies(function (result) {
                    _arriOwnerIds = result;
                    getCompanyLinkedById(_iCompanyId, function (result) {
                        _oCompany = result;
                        displayDetail();
                        if (_arriOwnerIds.includes(_iCompanyId)) {
                            $(".editContent").show();
                            $(".editObject").show();
                        }
                    }, function () {
                        window.location.href = "Companies";
                    });
                });
                break;
            default:
                getCompanyLinkedById(_iCompanyId, function (result) {
                    _oCompany = result;
                    displayDetail();
                }, function () {
                    window.location.href = "Companies";
                });
                break;
        }


    }

});