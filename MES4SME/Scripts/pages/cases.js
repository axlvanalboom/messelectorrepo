﻿var _arroCases = [], _arroFilteredObjects = [];


// Display the cases
function displayObjects(page = 0) {
    var iLimit = 9,
        iPages = _arroFilteredObjects.length / iLimit,
        iFrom = page * iLimit,
        iTo = (page + 1) * iLimit,
        sContent = '';

    if (_arroFilteredObjects.length == 0) {
        sContent = '<p>No public cases to display, log in as owner to see your content...</p>';
    }
    else {
        if (iTo > _arroFilteredObjects.length)
            iTo = _arroFilteredObjects.length;

        for (var i = iFrom; i < iTo; i++)
            sContent += renderCard(_arroFilteredObjects[i], 'loadCase');
    }

    // Render pagination
    if (iPages > 1) {
        $(".section-footer .pagination").empty();
        for (var i = 0; i < iPages; i++) {
            $(".section-footer .pagination").append(`<li class="page-item"><a onclick="displayObjects(${i});" class="page-link" >${i + 1}</a></li>`);
        }
        $(".section-footer .pagination li:nth-child(" + (page + 1) + ")").addClass('active');
        $(".section-footer").show();
    }
    else {
        $(".section-footer").hide();
    }

    $('#cases').empty();
    $('#cases').append(sContent);


    for (var i = 0; i < _arroFilteredObjects.length; i++) {
        var object = _arroFilteredObjects[i]
        var id = "#ID_".concat(object.Id);
        $(id).on("auxclick", function (e) {
            if (e.which == 2) {
                window.open("CaseDetail?CaseId=" + e.currentTarget.id.split("_")[1], '_blank');
            }
        });
    }
}

// Show detail function 
function loadCase(caseId) {
    sessionStorage.setItem("caseId", caseId);
    window.location.href = "caseDetail";
}


// Search cases
var sCheckValue = null;
var _myInterval = null;
function searchCases() {
    var sTag = $("#input-search").val().toUpperCase();

    if (sTag.length > 0) {

        _arroFilteredObjects = [];

        for (var i = 0; i < _arroCases.length; i++)
            if ((_arroCases[i].Name !== null && _arroCases[i].Name.toUpperCase().includes(sTag))
                || (_arroCases[i].Description !== null && _arroCases[i].Description.toUpperCase().includes(sTag))
                || (_arroCases[i].Sector.Name !== null && _arroCases[i].Sector.Name.toUpperCase().includes(sTag))
                || (_arroCases[i].Company.Name !== null && _arroCases[i].Company.Name.toUpperCase().includes(sTag))
                || (_arroCases[i].Software.Name !== null && _arroCases[i].Software.Name.toUpperCase().includes(sTag)))
                _arroFilteredObjects.push(_arroCases[i]);

        displayObjects();

        window.clearTimeout(_myInterval);
        _myInterval = setTimeout(function () {
            if (sTag === sCheckValue) {
                console.log(sTag);

                $.ajax({
                    type: "GET",
                    url: window.location.protocol + "//" + window.location.host + "/User/SearchResult",
                    data: {
                        sType: "Search Cases",
                        sParameter: sTag
                    },
                    success: function (result) {

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr, textStatus, errorThrown);
                    }
                });


            }
        },
            1000);
        sCheckValue = sTag;

    } else {
        _arroFilteredObjects = _arroCases;
        displayObjects();
    }
}

// Startup function
$(function () {

    getCasesLinked(function (response) {
        _arroCases = response;
        _arroFilteredObjects = response;
        displayObjects();
    });

    $("#search-form").on('keyup', function () {
        searchCases();
    });

});