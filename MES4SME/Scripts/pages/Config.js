﻿var _oTable = null,
    _oEditor = null,
    _arroData = [],
    _iPage = {},
    _oTemp = {},
    _oActivities = [],
    _oSoftwares = [],
    _oCategories = [],
    _oFunctions = [],
    _oCompanies = [],
    _oSectors = [],
    _oRoles = [],
    _sRole = $('#Role').text(),
    _sSelector = $('#selector').val();

// draw table with editing 
function drawTable(data, config) {

    // Clear current table
    if (_oTable !== null) {
        _oTable.clear().destroy();
        _oTable = null;
        _oTemp = {};
        _oActivities = [];
        _oSoftwares = [];
        _oCategories = [];
        _oFunctions = [];
        _oCompanies = [];
        $('#table').empty();
        $(".site-content").hide();
    }

    // Create table configurations
    var oConfig = {
        sPaginationType: 'full_numbers',
        autoWidth: false,
        data: data,
        columns: config.columns,
        dom: 'Blfrtip',        // Buttons, length, filter,..
        select: 'single',
        order: [1, 'asc'],
        //responsive: true,
        altEditor: true,     // Enable altEditor
        onAddRow: function (datatable, rowdata, success, error) {
            _iPage = _oTable.page();
            _oTable.config.addData(datatable, rowdata, success, error);
        },
        onDeleteRow: function (datatable, rowdata, success, error) {
            _oTable.config.deleteData(datatable, rowdata, success, error);
        },
        onEditRow: function (datatable, rowdata, success, error) {
            _iPage = _oTable.page();
            _oTable.config.editData(datatable, rowdata, success, error);
        },
        onOpenAddModal: function () {
            if (_oTable.config.onOpenAddModal !== undefined)
                _oTable.config.onOpenAddModal();
        }
    };

    oConfig.buttons = [];
    _sSelector = $('#selector').val();
    // Change button activity
    if ((_sRole === "Administrator" && _sSelector !== "Users") ||
        (_sRole === "Owner" && (_sSelector === "Softwares") || (_sSelector === "Cases"))) {
        oConfig.buttons.push(
            {
                name: 'add',        
                text: '',
                tag: 'i',
                className: 'fa fa-plus'
            }
        );
    }
    oConfig.buttons.push(
        {
            extend: 'selected', 
            text: '',
            name: 'edit',        
            tag: 'i',
            className: 'fa fa-edit'
        }
    );
    if (_sRole === "Administrator") {
        oConfig.buttons.push(
            {
                extend: 'selected',
                text: '',
                name: 'delete',
                tag: 'i',
                className: 'fa fa-minus'
            }
        );
    }

    // Draw table
    _oTable = $('#table').DataTable(oConfig);

    // Set config
    _oTable.config = config;

    _oTable.on('select', function (e, dt, type, indexes) {
        if (_oTable.config.renderOpenRow !== undefined) {
            var row = _oTable.row(indexes);
            sessionStorage.setItem('selectId', row.data().Id);
            row.child(_oTable.config.renderOpenRow(row.data(), indexes)).show();
            if (_oTable.config.afterOpenRow !== undefined)
                _oTable.config.afterOpenRow();
        }
    });

    _oTable.on('deselect', function (e, dt, type, indexes) {
        if (_oTable.config.renderOpenRow !== undefined) {
            var row = _oTable.row(indexes);
            row.child.hide();
        }
    });

    if (_oTable.config.afterDrawing !== undefined)
        _oTable.config.afterDrawing();

    // Check if row needs to be selected
    var sSelectId = sessionStorage.getItem('selectId');
    if (sSelectId !== null) {
        var iId = parseInt(sSelectId);
        // Select row based on id
        _oTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
            if (this.data().Id == iId) {
                _oTemp = this.data();
                var iLength = _oTable.page.info().length;
                _oTable.row(':eq(' + rowIdx.toString() + ')').select();
                _oTable.page(parseInt(rowIdx / iLength)).draw('page');
                if (_oTable.config.renderOpenRow !== undefined) {
                    _oTable.config.renderOpenRow(this.data(), rowIdx)
                    if (_oTable.config.afterOpenRow !== undefined)
                        _oTable.config.afterOpenRow();
                }
                return false;
            }
        });
    }
    else if (_iPage > 0) {
        _oTable.page(_iPage).draw('page');
    }

    $(".dt-buttons").appendTo($("#actions"));

    $(".site-content").show();
}

// Change selector
function changeSelector() {

    sessionStorage.setItem('configSelected', $('#selector').val());

    switch ($('#selector').val()) {
        case 'Companies':
            // Get companies
            getConfigCompaniesLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oCompanyConfig);
            });
            break;
        case 'Softwares':
            // Get softwares
            getConfigSoftwaresLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oSoftwareConfig);
            });
            break;
        case 'Cases':
            // Get cases
            getConfigCasesLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oCaseConfig);
            });
            break;
        case 'Users':
            // Get users
            getUsersLinked(function (result) {
                _arroData = result;
                drawTable(_arroData, _oUserConfig);
            });
            break;
        case 'Sectors':
            // Get sectors
            getSectors(function (result) {
                _arroData = result;
                drawTable(_arroData, _oSectorConfig);
            });
            break;
        case 'Activities':
            // Get activities
            getActivities(function (result) {
                _arroData = result;
                drawTable(_arroData, _oActivityConfig);
            });
            break;
        case 'Functions':
            // Get Functions
            getFunctions(function (result) {
                _arroData = result;
                drawTable(_arroData, _oFunctionConfig);
            });
            break;
        case 'Categories':
            // Get categories
            getCategories(function (result) {
                _arroData = result;
                drawTable(_arroData, _oCategoryConfig);
            });
            break;
        default:
            break;
    }
}


// upload logo function (can be used for softwares and cases as well)
function uploadLogo(files) {
    var formData = new FormData();
    var imageFile = files[0];

    if (imageFile.name.endsWith(".png") || imageFile.name.endsWith(".jpeg") || imageFile.name.endsWith(".jpg") || imageFile.name.endsWith(".jfif")) {
        formData.append("image", imageFile);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/Owner/uploadImage", true);
        xhr.addEventListener("load", function (evt) { uploadComplete(evt, xhr); }, false);
        xhr.addEventListener("error", function (evt) { uploadFailed(evt); }, false);
        xhr.send(formData);
    }
    else {
        alert("Wrong file!");
    }
}

// upload complete
function uploadComplete(evt,xhr) {
    console.log(xhr);
    if (evt.target.status == 200 && JSON.parse(xhr.response).success) {
        statusMessage("Logo uploaded successfully.");
        // edit the object
        _oTemp.LogoUrl = JSON.parse(xhr.response).file.url;
        _oTable.config.editData(null, _oTemp, function (data, message) {
            // render the row content again
            _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
            _oTable.config.afterOpenRow();
            statusMessage(message, true, '#details-table');
        }, function (message) {
            statusMessage(message, false, '#details-table');
        });
    }
    else
        statusMessage("Error Uploading File");
}

// upload failed
function uploadFailed(evt) {
    statusMessage("There was an error attempting to upload the file.");
}

// Startup function
$(function() {

    // bind change selection event
    $('#selector').on('change', function () {
        sessionStorage.setItem('selectId', undefined);
        changeSelector();
    });

    // Run change selection on default value
    var sConfigSelected = sessionStorage.getItem('configSelected');

    if (sConfigSelected !== null)
        $('#selector').val(sConfigSelected);

    changeSelector();

});