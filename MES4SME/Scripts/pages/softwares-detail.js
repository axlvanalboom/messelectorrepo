﻿var _iSoftwareId = null,
    _oSoftware = {},
    _arriOwnerIds = [],
    _sRole = $('#Role').text();

// Search function
function searchFunction(cat, fu) {
    sessionStorage.setItem("searchTags", JSON.stringify(
        [
            {
                value: _oSoftware.CategoryFunctions[cat].Name
            },
            {
                value: _oSoftware.CategoryFunctions[cat].Functions[fu].Name
            }
        ]
    ));
    window.location.href = "/";
}

// Search category
function searchCategory(cat) {
    sessionStorage.setItem("searchTags", JSON.stringify([{
        value: _oSoftware.CategoryFunctions[cat].Name
    }]));
    window.location.href = "/";
}

// Display detail 
function displayDetail() {

    var sContent = `<div class="container">
                        <div class="section-header d-md-flex align-items-center">
                            <div class="mr-auto">
                                <h2 class="section-title">Software info</h2>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/"><i class="far fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="/Software">Software</a></li>
                                    <li class="breadcrumb-item active">${_oSoftware.Name}</li>
                                </ol>
                            </div>

                        </div> 
    
                        <div class="section-content">

                                <section id="software-detail">
                                    <i class="editObject far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit info"></i>
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">`;

    if (_oSoftware.LogoUrl !== null) {
        sContent +=                                     `<div class="col-4" style="display:flex;align-items:center;">
                                                            <img src="${_oSoftware.LogoUrl}" class="img-fluid company-logo" />
                                                        </div>`;
    }

    sContent +=                                         `<div class="col-8">
                                                            <h3 class="card-title">${_oSoftware.Name}</h3>`;

    if (_oSoftware.Website !== undefined && _oSoftware.Website !== null) {
        sContent +=                                         `<ul class="properties-list">
                                                                <li><i class="far fa-link"></i> <a href="${getValidUrl(_oSoftware.Website)}" class="target-blank">${_oSoftware.Website.replace(/(^\w+:|^)\/\//, '')}</a></li>
                                                            </ul>`;
    }
    sContent +=                                         `</div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div> 
    
                                        <div class="col-md-6 d-flex flex-column justify-content-center">
                                            <h3>Description</h3>
                                            <p>${_oSoftware.Description}</p>
                                        </div>
                                    </div> 

                                </section>`;

    sContent +=             `<section id="editor-content">
                                <i class="editContent far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit content"></i>
                                <div id="EditorContainer">`; 
    if (_oSoftware.Content !== null)
        sContent += renderEditorData(JSON.parse(_oSoftware.Content));

    sContent +=                 `</div>`;
    sContent +=             `</section>
                             <section id="related-cases">`;

    if (_oSoftware.CategoryFunctions.length > 0) {
        sContent +=
            `<section id="categories-functions">
                <h2 class="section-title">Categories & functions</h2>
                <div class="row">`;
        for (var i = 0; i < _oSoftware.CategoryFunctions.length; i++) {
            sContent +=
                `<div class="col-md-6 col-xl-3">
                        <article class="cat-fu block">
                            <a class="overlay" href="javascript:void(0);" onclick="searchCategory(${i});"></a>
                                <p><i class="far fa-${getIconName(_oSoftware.CategoryFunctions[i].Name)}"></i>${_oSoftware.CategoryFunctions[i].Name}</p>
                                <ol class="">`;
            for (var j = 0; j < _oSoftware.CategoryFunctions[i].Functions.length; j++) {
                sContent += `<li class="inner">
                                        <a href="javascript:void(0);" onclick="searchFunction(${i}, ${j});" class="target-blank">`
                    + _oSoftware.CategoryFunctions[i].Functions[j].Name +
                    `</a>
                                    </li>`;
            }
            sContent +=
                `</ol>
                            
                        </article>
                    </div>`;
        }
        sContent +=
            `</div>
            </section>`;
    }

    if (_oSoftware.CasesSectors.length > 0) {

        sContent +=             `<h2 class="section-title">Cases</h2>
                                <div class="row">`;
        for (var i = 0; i < _oSoftware.CasesSectors.length; i++) {
            _oSoftware.CasesSectors[i].Case.Sector = _oSoftware.CasesSectors[i].Sector;
            sContent += renderCard(_oSoftware.CasesSectors[i].Case, 'loadCase');
        }

        sContent +=             `</div>`;
    }

    if (_oSoftware.CompanyActivities.length > 0) {

        sContent +=             `<h2 class="section-title">Companies</h2>
                                <div class="row">`;

        for (var i = 0; i < _oSoftware.CompanyActivities.length; i++) {
            _oSoftware.CompanyActivities[i].Company.Activity = _oSoftware.CompanyActivities[i].Activity;
            sContent += renderCard(_oSoftware.CompanyActivities[i].Company, 'loadCompany');
        }
        sContent +=             `</div>`;        
    }

    sContent +=             `</section>
                        </div>    
                    </div>`;

    $("#the-content").append(sContent);
    $("#the-content").show();

    $('.editContent').on('click', function () { openEditor(); });

    $('.editObject').on('click', function () {
        sessionStorage.setItem('configSelected', 'Softwares');
        sessionStorage.setItem('selectId', _oSoftware.Id);
        window.location.href = 'Config';
    });

    $('[data-toggle="tooltip"]').tooltip();
}

// Open editor function
function openEditor() {

    $('.editContent').hide();

    buildEditor(JSON.parse(_oSoftware.Content), 'EditorContainer',
        function (data) {
            editSoftwareContent(_iSoftwareId, JSON.stringify(data), function (result) {
                _oSoftware.Content = JSON.stringify(data);
                statusMessage('Content updated.');
            });
        },
        function () {
            $('.tooltip').remove();
            $("#the-content").empty();
            displayDetail();
            $(".editContent").show();
        }
    );
}


// Startup function
$(function () {

    _iSoftwareId = parseInt(sessionStorage.getItem("softwareId"));

    if (isNaN(_iSoftwareId)) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        _iSoftwareId = urlParams.get('SoftwareId');

    }

    if (_iSoftwareId == null || _iSoftwareId == 0)
        window.location.href = "Softwares";
    else {
        switch (_sRole) {
            case "Administrator":
                getSoftwareLinkedById(_iSoftwareId, function (result) {
                    _oSoftware = result;
                    displayDetail();
                    $(".editContent").show();
                    $(".editObject").show();
                }, function () {
                    window.location.href = "Softwares";
                });
                break;
            case "Owner":
                getOwnedSoftwares(function (result) {
                    _arriOwnerIds = result;
                    getSoftwareLinkedById(_iSoftwareId, function (result) {
                        _oSoftware = result;
                        displayDetail();
                        if (_arriOwnerIds.includes(_iSoftwareId)) {
                            $(".editContent").show();
                            $(".editObject").show();
                        }
                    }, function () {
                            window.location.href = "Softwares";
                    });
                });
                break;
            default:
                getSoftwareLinkedById(_iSoftwareId, function (result) {
                    _oSoftware = result;
                    displayDetail();
                }, function () {
                        window.location.href = "Softwares";
                });
                break;
        }
    }       

});