﻿"use strict";

var _oData = [],
    _oSearch,
    _iGroups = 0; // Add choices from result


function addGroup(data, type, id) {
    // Check data
    if (data == undefined || data.length == 0) return; // Check session storage

    var arrsSelected = [];

    if (sessionStorage.getItem("searchTags") !== null) {
        var arroSelected = JSON.parse(sessionStorage.getItem("searchTags"));

        for (var i = 0; i < arroSelected.length; i++) {
            arrsSelected.push(arroSelected[i].value);
        }
    } // Create choices


    var choices = [];

    for (var i = 0; i < data.length; i++) {
        choices.push({
            label: data[i].Name,
            value: data[i].Name,
            selected: arrsSelected.includes(data[i].Name) ? true : false,
            customProperties: {
                type: type,
                id: data[i].Id,
                description: data[i].Description
            },
            groupId: id
        });
    } // Create group


    var group = {
        label: type,
        value: type,
        id: id,
        choices: choices
    }; // Add group to search field

    _oSearch.setChoices([group]); // Check for all groups


    _iGroups++;
    if (_iGroups == 7) getResults();
} // Get the search results from the server


function getResults() {
    // Get data from search field and initiate link object
    var arroItems = _oSearch.getValue(),
        links = {
            Company: [],
            Activity: [],
            Mes: [],
            Sector: [],
            MesCategory: [],
            MesFunction: [],
            CaseCategory: [],
            CaseFunction: [],
            Case: [],
            String: []
        }; // Check number of items


    if (arroItems.length > 0) {
        // Add items to link
        for (var i = 0; i < arroItems.length; i++) {
            switch (arroItems[i].customProperties.type) {
                case "Sectors":
                    links.Sector.push(arroItems[i].customProperties.id);
                    break;

                case "Functions":
                    links.MesFunction.push(arroItems[i].customProperties.id);
                    links.CaseFunction.push(arroItems[i].customProperties.id);
                    break;

                case "Categories":
                    links.MesCategory.push(arroItems[i].customProperties.id);
                    links.CaseCategory.push(arroItems[i].customProperties.id);
                    break;

                case "Activities":
                    links.Activity.push(arroItems[i].customProperties.id);
                    break;

                case "Companies":
                    links.Company.push(arroItems[i].customProperties.id);
                    break;

                case "Softwares":
                    links.Mes.push(arroItems[i].customProperties.id);
                    break;

                case "Cases":
                    links.Case.push(arroItems[i].customProperties.id);
                    break;

                default:
                    break;
            }
        } // Get the results from the server


        getSearchResults(links, function (result) {
            processResults(result);
        });
    } else {
        $('#result').hide();
    } // Save the selected items


    sessionStorage.setItem("searchTags", JSON.stringify(arroItems));
} // Show detail function 


function loadCompany(companyId) {
    sessionStorage.setItem("companyId", companyId);
    window.location.href = "CompanyDetail";
}

function loadSoftware(softwareId) {
    sessionStorage.setItem("softwareId", softwareId);
    window.location.href = "SoftwareDetail";
}

function loadCase(caseId) {
    sessionStorage.setItem("caseId", caseId);
    window.location.href = "CaseDetail";
} // render card


function renderResult(array, clickFunc, title) {
    if (array.length > 0) {
        var sContent = "<h2 class=\"section-title\">".concat(title, "</h2>");

        for (var i = 0; i < array.length; i++) {
            sContent += "<div class=\"search-result\" >\n                        <article class=\"card\">\n                            <a href=\"javascript:void(0);\" onclick=\"".concat(clickFunc, "(").concat(array[i].Id, ");\">\n                                <div class=\"card-body\">\n                                    <h3 class=\"card-title\">").concat(array[i].Name, "<i class=\"far fa-long-arrow-right\"></i></h3>\n                                    <p class=\"card-desc wysiwyg two-lines\">").concat(truncate(array[i].Description, 30), "&hellip;</p>\n                                </div>\n                            </a>\n                        </article>");
            sContent += '</div>';
        }

        return sContent;
    } else return '';
} // Process the search results


function processResults(result) {
    var arroCompanies = result.Company,
        arroSoftwares = result.Mes,
        arroCases = result.Case,
        sContent = '';

    if (arroCompanies.length > 0 || arroSoftwares.length > 0 || arroCases.length > 0) {
        sContent += renderResult(arroCases, 'loadCase', 'Cases');
        sContent += renderResult(arroSoftwares, 'loadSoftware', 'Softwares');
        sContent += renderResult(arroCompanies, 'loadCompany', 'Company');
    } else {
        sContent = '<span class="no-results">No results found.</span>';
    }

    $('#result').html(sContent);
    $('#result').show();
} // Startup function


$(function () {
    _oSearch = new Choices('#input-search', {
        removeItemButton: true,
        choices: _oData,
        addItems: true //searchFields: ['label', 'value', 'customProperties.description'],

    });

    _oSearch.passedElement.element.addEventListener('change', function (event) {
        getResults();
    });

    _oSearch.passedElement.element.addEventListener('showDropdown', function (event) {
        $("#categories-nav").slideUp(300);
    });

    _oSearch.passedElement.element.addEventListener('hideDropdown', function (event) {
        $("#categories-nav").slideDown(300);
    });

    getSectors(function (result) {
        addGroup(result, 'Sectors', 1);
    });
    getFunctions(function (result) {
        addGroup(result, 'Functions', 2);
    });
    getCategories(function (result) {
        addGroup(result, 'Categories', 3);
    });
    getActivities(function (result) {
        addGroup(result, 'Activities', 4);
    });
    getCompanies(function (result) {
        addGroup(result, 'Companies', 5);
    });
    getSoftwares(function (result) {
        addGroup(result, 'Softwares', 6);
    });
    getCases(function (result) {
        addGroup(result, 'Cases', 7);
    }); // Show it

    $('#search-form').show();
});