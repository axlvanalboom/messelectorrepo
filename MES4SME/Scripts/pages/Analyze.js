﻿// Algemeen
var oUserViews = null;

function DrawBarGraph(data, divName) {
    document.getElementById(divName).innerHTML = ""; //delete the current chart
    //document.getElementById(divName).append('<canvas id="myChart"></canvas>');
    $('#'+divName).append('<canvas id="myChart"></canvas>'); 

    var ctx = document.getElementById('myChart').getContext('2d');

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data.Labels,
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    for (i = 0; i < data.DataSets.length; i++) {
        var d = data.DataSets[i];
        myChart.data.datasets.push({
            label: d.Label,
            data: d.Data,
            backgroundColor: d.BackgroundColor,
            borderColor: d.BorderColor,
            borderWidth: d.BorderWidth
        });      
    }

    myChart.update();

}
function clearGraph() {
    document.getElementById('chart').innerHTML = "";
}
function clearOverView() {
    document.getElementById('overview').innerHTML = "";
}

function makeEpochTimeBegin(sTime) {
    if (sTime === "") { return ""; }
    var parts = sTime.match(/(\d{2})\/(\d{2})\/(\d{4})/);
    return Date.UTC(+parts[3], parts[2] - 1, +parts[1])/1000;
}
function makeEpochTimeEnd(sTime) {
    if (sTime === "") { return ""; }
    sTime += " 23:59:59";
    var parts = sTime.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/);
    return Date.UTC(+parts[3], parts[2] - 1, +parts[1], +parts[4], +parts[5], + parts[6])/1000;
}
// # Visitors
function GetAmountOfVisitors() {
    clearOverView();
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getAmountOfVisitors",
        success: function (result) {
            DrawBarGraph(result,'chart')
         },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });  

}
function GetAmountOfVisitorsBetweenTime() {
    clearOverView();
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getAmountOfVisitorsBetweenTime",
        data: {
            sTimeCode: $('#TimeCode').val(),
            sStart: makeEpochTimeBegin($("#StartDate").val()),
            sStop: makeEpochTimeEnd($("#EndDate").val())
        },
        success: function (result) {
            DrawBarGraph(result, 'chart')
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
   
}


// Visitors Views
function DrawVisitorViews(data, divName) {
    //data: List<clsView>
    document.getElementById(divName).innerHTML = "";

    var sContent = `
                    <table style="width:100%">
                        <tr>
                            <th><strong> Time </strong></th>
                            <th><strong> Type </strong></th>
                            <th><strong> Parameter </strong></th>
                        </tr>
                        <tr>`;
    for (i = 0; i < data.length; i++) {
        sContent += `<tr class="`;
        if (i % 2 === 0)
            sContent += 'TableEven';
        else
            sContent += 'TableOdd';
        sContent += `">
                            <td>` + data[i].Time + `</td>
                            <td>` + data[i].Type + `</td>
                            <td>` + data[i].Parameter + `</td>
                        </tr>`;
    }

 


    sContent += '   </table>';


    $('#' + divName).append(sContent);
    $('#' + divName).show();
}
function DrawVisitorsViews(data, divName) {
    document.getElementById(divName).innerHTML = ""; //clear the div

    var sContent = '';

    sContent += `<div id ="visitorViews" class="row no-gutters">
                 
                    <div class="col-left col-lg-2 d-xs-none d-sm-none d-md-none d-lg-block">
                        <label for="User">User: </label>
                        <select id="Users" onchange="changeUserSelector()"> `;

    for (i = 0; i < data.length; i++) {
        sContent += '<option>' + data[i].UserId + '</option>';
    }

    sContent += `       </select>
                    </div>
                    
                    <div class="col-left col-lg-10 d-xs-none d-sm-none d-md-none d-lg-block" id="UserViews">
                        
                    </div> 
       
                </div>   
               `;

    $('#' + divName).append(sContent);
    $('#' + divName).show();

    changeUserSelector();
}

function GetVisitorsViews() {

    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getVisitorsViews",
        success: function (result) {
            oUserViews = result;
            DrawVisitorsViews(result, 'chart');          
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

    

}
function GetVisitorsViewsBetweenTime() {

    //var sStart = makeEpochTimeBegin($("#StartDate").val());
    //var sStop = makeEpochTimeEnd($("#EndDate").val());

    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getVisitorsViewsBetweenTime",
        data: {
            sTimeCode: $('#TimeCode').val(),
            sStart: makeEpochTimeBegin($("#StartDate").val()),
            sStop: makeEpochTimeEnd($("#EndDate").val())
        },
        success: function (result) {
            oUserViews = result;
            DrawVisitorsViews(result, 'overview');  
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

}

function changeUserSelector() {
    if (oUserViews !== null) {
        for (i = 0; i < oUserViews.length; i++) {
            if (oUserViews[i].UserId === $('#Users').val()) {
                DrawVisitorViews(oUserViews[i].Views, 'UserViews');
                break;
            }
        }
    }
}


// # Detail Views
function DrawAmountDetailViews1(divName) {
    document.getElementById(divName).innerHTML = ""; //clear the div

    var sContent = '';

    sContent += `<div id ="ADVs" class="row no-gutters">
                 
                    <div class="col-left col-lg-2 d-xs-none d-sm-none d-md-none d-lg-block">
                        <select id="DetailView" onchange="changeDetailViewSelector()"> 
                            '<option> Company</option>
                            '<option> Software</option>
                            '<option> Case</option>
                        </select>
                    </div>
                    
                    <div class="col-left col-lg-10 d-xs-none d-sm-none d-md-none d-lg-block" id="ADV">
                       
                    </div> 
       
                </div>   
               `;

    $('#' + divName).append(sContent);
    $('#' + divName).show();

    GetAmountDetailViewsBetweenTime('DetailView','ADV');
}
function DrawAmountDetailViews2(data, divName) {
    document.getElementById(divName).innerHTML = ""; //clear the div

    var sContent = '';

    sContent += `<table style = "width:100%" >
                    <tr>
                        <th><strong> Name </strong></th>
                        <th><strong> Amount </strong></th>
                    </tr>
                    <tr>`;

    for (i = 0; i < data.length; i++) {
        sContent += `<tr data-Parameter="` + data[i].Text + `" class="`;
        if (i % 2 === 0 )
            sContent += 'TableEven';
        else
            sContent += 'TableOdd';

       sContent += `">
                    <td>` + data[i].Text + `</td>
                        <td>` + data[i].Number + `</td>
                    </tr>`;
    }
    sContent += '</table>';

    $('#' + divName).append(sContent);

    $('#' + divName + ' tr').on('click', function (e) {
        //console.log(this.dataset.parameter);
        DrawAmountViewsPerDay(this.dataset.parameter);
    });



    $('#' + divName).show();

    changeUserSelector();
}

function DrawAmountViewsPerDay(parameter) {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getAmmountOfViewsOffTypeParameterBetweenTime",
        data: {
            sType: $('#DetailView').val(),
            sParameter: parameter,
            sTimeCode: $('#TimeCode').val(),
            sStart: $("#StartDate").val(),
            sStop: $("#EndDate").val()
        },
        success: function (result) {
            DrawBarGraph(result, 'chart')
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}


function GetAmountDetailViews(selectName,drawDiv) {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getAmountDetailViews",
        data: {
            sType: $('#'+selectName).val()
        },
        success: function (result) {
            DrawAmountDetailViews2(result, drawDiv);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}
function GetAmountDetailViewsBetweenTime(selectName, drawDiv) {
    $.ajax({
        type: "GET",
        url: window.location.protocol + "//" + window.location.host + "/Analyzing/getAmountDetailViewsBetweenTime",
        data: {
            sType: $('#' + selectName).val(),
            sTimeCode: $('#TimeCode').val(),
            sStart: makeEpochTimeBegin($("#StartDate").val()),
            sStop: makeEpochTimeEnd($("#EndDate").val())
        },
        success: function (result) {
            DrawAmountDetailViews2(result, drawDiv);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
}

function changeDetailViewSelector() {
    clearGraph();
    GetAmountDetailViewsBetweenTime('DetailView', 'ADV');
}


//Overal


function changeSeeSelector() {
    clearGraph();
    switch ($('#WhatToSee').val()) {
        case 'Visitor views':
            GetVisitorsViewsBetweenTime(); 
            break;
        case '# Detail views':
            if ($('#DetailView').val() === 'Software' || $('#DetailView').val() === 'Case')
                GetAmountDetailViewsBetweenTime('DetailView', 'ADV');             
            else 
                DrawAmountDetailViews1('overview');
            
            break;
        default: //#Visitors by default
            GetAmountOfVisitorsBetweenTime();
            break;
    }
}

function changeTimeSelector() {

    switch ($('#TimeCode').val()) {
        case 'Other':
            $("#startTime").show();
            $("#endTime").show();
            $("#searchTime").show();
            break;
        default:
            $("#startTime").hide();
            $("#endTime").hide();
            $("#searchTime").hide();
            break;
    }

    if ($('#TimeCode').val() !== "Other") {
        changeSeeSelector();
    }

}


function DisplayAnalyzing() {
    var sContent = '';

    sContent += `
                <div id ="settings" class="row no-gutters">
                    
                    <div class="col-left col-lg-2 d-xs-none d-sm-none d-md-none d-lg-block">
                        <select id="WhatToSee" name="TimeCode" onchange="changeSeeSelector()">
                            <option># Visitors</option>      
                            <option>Visitor views</option>        
                            <option># Detail views</option>        
                        </select>
                    </div>

                    <div class="col-left col-lg-2 d-xs-none d-sm-none d-md-none d-lg-block">
                        <label for="Time">Time</label>
                        <select id="TimeCode" name="TimeCode" onchange="changeTimeSelector()">
                            <option>Last Week</option>      
                            <option>Last Month</option>      
                            <option>Last Year</option>      
                            <option>All Time</option>      
                            <option>Other</option>           
                        </select>
                    </div>

                    <div class="col-left col-lg-3 d-xs-none d-sm-none d-md-none d-lg-block">
                        <p id="startTime">Start Day: <input type="text" id="StartDate"></p>                    
                    </div>

                    <div class="col-left col-lg-3 d-xs-none d-sm-none d-md-none d-lg-block">
                        <p id="endTime">End Day: <input type="text" id="EndDate"></p>             
                    </div>

                    <div class="col-left col-lg-2 d-xs-none d-sm-none d-md-none d-lg-block">
                        <button id="searchTime"> Search </button>         
                    </div>

                </div>
                `;
    sContent += ' <div id="overview"></div>';
    sContent += ' <div id="chart"></div>';

     


    $("#Analyse").append(sContent);
    $("#Analyse").show();

    $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#startTime").hide();
    $("#endTime").hide();
    $("#searchTime").hide();
    $("#searchTime").click(function () {
        changeSeeSelector()
    });
}

$(function () {
    DisplayAnalyzing();
    changeSeeSelector();
});
