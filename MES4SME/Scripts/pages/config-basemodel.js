﻿// Column definition
var _oBaseColumns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly',
        visible: false
    },
    {
        data: 'Name',
        title: 'Name',
        width: '30%'
    },
    {
        data: 'Description',
        title: 'Description',
        width: '70%'
    }
];

// Activity object definition
var _oActivityConfig = {};
_oActivityConfig.columns = _oBaseColumns;
_oActivityConfig.addData = function (datatable, rowdata, success, error) { addActivity(rowdata.Name, rowdata.Description, function (data) { rowdata.Id = data; success(rowdata); }); };
_oActivityConfig.editData = function (datatable, rowdata, success, error) { editActivity(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); }); };
_oActivityConfig.deleteData = function (datatable, rowdata, success, error) { deleteActivity(rowdata.Id, function () { success(rowdata); }); };

// Sector object definition
var _oSectorConfig = {};
_oSectorConfig.columns = _oBaseColumns;
_oSectorConfig.addData = function (datatable, rowdata, success, error) { addSector(rowdata.Name, rowdata.Description, function (data) { rowdata.Id = data; success(rowdata); }); };
_oSectorConfig.editData = function (datatable, rowdata, success, error) { editSector(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); }); };
_oSectorConfig.deleteData = function (datatable, rowdata, success, error) { deleteSector(rowdata.Id, function () { success(rowdata); }); };

// Function object definition
var _oFunctionConfig = {};
_oFunctionConfig.columns = _oBaseColumns;
_oFunctionConfig.addData = function (datatable, rowdata, success, error) { addFunction(rowdata.Name, rowdata.Description, function (data) { rowdata.Id = data; success(rowdata); }); };
_oFunctionConfig.editData = function (datatable, rowdata, success, error) { editFunction(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); }); };
_oFunctionConfig.deleteData = function (datatable, rowdata, success, error) { deleteFunction(rowdata.Id, function () { success(rowdata); }); };

// Category object definition
var _oCategoryConfig = {};
_oCategoryConfig.columns = _oBaseColumns;
_oCategoryConfig.addData = function (datatable, rowdata, success, error) { addCategory(rowdata.Name, rowdata.Description, function (data) { rowdata.Id = data; success(rowdata); }); };
_oCategoryConfig.editData = function (datatable, rowdata, success, error) { editCategory(rowdata.Id, rowdata.Name, rowdata.Description, function () { success(rowdata); }); };
_oCategoryConfig.deleteData = function (datatable, rowdata, success, error) { deleteCategory(rowdata.Id, function () { success(rowdata); }); };