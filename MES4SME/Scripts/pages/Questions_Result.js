﻿function analyze(dsoAnswers) {
    //console.log(dsoAnswers);

    var MC = [];
    var Range = [];

    for (let key in dsoAnswers) {
        var answer = dsoAnswers[key];
        if (Number.isInteger(answer)) {
            MC.push({
                QuestionId: key,
                AnswerId: answer
            });
        }
        else {
            if (answer.Operator != null)
                Range.push({
                    QuestionId: key,
                    Value: answer.Value,
                    Operator: answer.Operator
                });
        }
    }

    //console.log(MC);
    //console.log(Range);

    $.ajax({
        type: "POST",
        url: window.location.protocol + "//" + window.location.host + "/Question/AnalyzeQuestionAnswers",
        dataType: "json",
        data: {
            oMC: MC,
            oRange: Range
        },
        success: function (result) {
            showAnalyzedData(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

}

function showAnalyzedData(data) {
    //console.log(data);
    document.getElementById("Result").innerHTML = "";
    $('#Result').append('<canvas id="myChart"></canvas>');

    var ctx = document.getElementById('myChart').getContext('2d');

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data.Labels,
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    for (i = 0; i < data.DataSets.length; i++) {
        var d = data.DataSets[i];
        myChart.data.datasets.push({
            label: d.Label,
            data: d.Data,
            backgroundColor: d.BackgroundColor,
            borderColor: d.BorderColor,
            borderWidth: d.BorderWidth
        });
    }

    myChart.update();
}
function clearGraph() {
    document.getElementById('chart').innerHTML = "";
}

$(function () {
    var dsoAnswers = JSON.parse(sessionStorage.getItem("Answers"));
    if (dsoAnswers != null)
        analyze(dsoAnswers);
});