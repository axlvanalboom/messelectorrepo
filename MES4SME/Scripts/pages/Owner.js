﻿// Variables
var _oEditor, _btnSave, _txtOutput, _oData;

// Functions
function buildEditor(data) {

    var arroTools = {
        paragraph: {
            class: Paragraph,
            inlineToolbar: true,
        },
        header: {
            class: Header,
            config: {
                placeholder: 'Enter a header'
            },
            inlineToolbar: true
        },
        list: {
            class: List,
            inlineToolbar: true
        },
        table: {
            class: Table,
            inlineToolbar: true,
            config: {
                rows: 2,
                cols: 3,
            },
            inlineToolbar: true
        },
        image: {
            class: ImageTool,
            config: {
                endpoints: {
                    byFile: 'Owner/uploadImage'
                }
            },
            inlineToolbar: true
        },
        attaches: {
            class: AttachesTool,
            config: {
                endpoint: 'Owner/uploadFile'
            },
            inlineToolbar: true
        },
        linkTool: {
            class: LinkTool,
            config: {
                endpoint: 'Owner/uploadUrl'
            },
            inlineToolbar: true
        },
        quote: {
            class: Quote,
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+O',
            config: {
                quotePlaceholder: 'Enter a quote',
                captionPlaceholder: 'Quote\'s author',
            },
            inlineToolbar: true
        },
        marker: {
            class: Marker,
            shortcut: 'CMD+SHIFT+M',
            inlineToolbar: true
        },
        embed: Embed
    };

    if (_oEditor !== undefined && _oEditor !== null) {
        _oEditor.destroy();
        _oEditor = null;
    }

    _oEditor = new EditorJS({
        holder: 'editorjs',
        tools: arroTools,
        data: data
    });

    _btnSave = document.getElementById('save');
    _txtOutput = document.getElementById('output');

    _btnSave.addEventListener('click', () => {
        _oEditor.save().then(data => {
            editCompanyContent(1238, JSON.stringify(data), function (result) {
                console.log('saved');
            });
        });
    });
}

// Startup function
$(function () {

    getCaseContent(1466, function (result) {
        _oData = JSON.parse(result);
        
        buildEditor(JSON.parse(result));

        sHtml = renderEditorData(_oData);

        $("#editorrender").html(sHtml);
    });

});