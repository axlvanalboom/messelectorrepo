﻿var _iCaseId = null,
    _oCase = {},
    _arriOwnerIds = [],
    _sRole = $('#Role').text();

// Search function
function searchFunction(cat, fu) {
    sessionStorage.setItem("searchTags", JSON.stringify(
        [
            {
                value: _oCase.CategoryFunctions[cat].Name
            },
            {
                value: _oCase.CategoryFunctions[cat].Functions[fu].Name
            }
        ]
    ));
    window.location.href = "/";
}

// Search category
function searchCategory(cat) {
    sessionStorage.setItem("searchTags", JSON.stringify([{
        value: _oCase.CategoryFunctions[cat].Name
    }]));
    window.location.href = "/";
}

// Display detail 
function displayDetail() {

    var sContent = `<div class="container">

        <div class="section-header d-md-flex align-items-center">
            <div class="mr-auto">
                <h2 class="section-title">Case info</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/"><i class="far fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="/Cases">Cases</a></li>
                    <li class="breadcrumb-item active">${_oCase.Name}</li>
                </ol>
            </div>

        </div> 
    
        <div class="section-content">

                <section id="case-detail">
                    <i class="editObject far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit info"></i>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">`;
    if (_oCase.LogoUrl !== null) {
        sContent +=                     `<div class="col-4">
                                            <img src="${_oCase.LogoUrl}" class="img-fluid company-logo" />
                                        </div>`;
    }

    sContent +=                         `<div class="col-8">
                                            <h3 class="card-title">${_oCase.Name}</h3>
                                        <ul class="properties-list">
                                             <li><a href="javascript:void(0);" onclick='searchTag("${_oCase.Sector.Name}");'><i class="far fa-industry"></i>${_oCase.Sector.Name}</a></li>`;
                                        
    if (_oCase.Website !== undefined && _oCase.Website !== null) {
                                sContent += `<li><i class="far fa-link"></i> <a href="${getValidUrl(_oCase.Website)}" class="target-blank">${_oCase.Website.replace(/(^\w+:|^)\/\//, '')}</a></li>`;
    }
    sContent +=                         `</ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
    
                    <div class="col-md-6 d-flex flex-column justify-content-center">
                            <h3>Description</h3>
                            <p>${_oCase.Description}</p>
                        </div>
    
                </div> 
            </section>`;

    
    sContent += `<section id="editor-content">
                    <i class="editContent far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit content"></i>
                    <div id="EditorContainer">`;
    if (_oCase.Content !== null)
        sContent += renderEditorData(JSON.parse(_oCase.Content));
    sContent += `</div>`;
    sContent += `</section>`;

    if (_oCase.CategoryFunctions.length > 0) {
        sContent +=
            `<section id="categories-functions">
                <h2 class="section-title">Categories & functions</h2>
                <div class="row">`;
        for (var i = 0; i < _oCase.CategoryFunctions.length; i++) {
            sContent +=
                `<div class="col-md-6 col-xl-3" >
                        <article class="cat-fu block">
                            <a class="overlay" href="javascript:void(0);" onclick="searchCategory(${i});"></a>
                                <p><i class="far fa-${getIconName(_oCase.CategoryFunctions[i].Name)}"></i>${_oCase.CategoryFunctions[i].Name}</p>
                                <ol class="">`;
            for (var j = 0; j < _oCase.CategoryFunctions[i].Functions.length; j++) {
                sContent += `<li class="inner">
                                        <a href="javascript:void(0);" onclick="searchFunction(${i}, ${j});" class="target-blank">`
                    + _oCase.CategoryFunctions[i].Functions[j].Name +
                                        `</a>
                                    </li>`;
            }
            sContent +=
                `</ol>
                            
                        </article>
                    </div>`;
        }
        sContent +=
            `</div>
            </section>`;
    }

    sContent += `<section id="related-cases">`;

    sContent += `<h2 class="section-title">Company</h2>
                <div class="row">`;
    sContent += renderCard(_oCase.Company, 'loadCompany');
    sContent += `</div>`;

    sContent += `<h2 class="section-title">Software</h2>
                <div class="row">`;
    sContent += renderCard(_oCase.Software, 'loadSoftware');
    sContent += `</div></section>`;

    $("#the-content").append(sContent);
    $("#the-content").show();

    $('.editContent').on('click', function () { openEditor(); });

    $('.editObject').on('click', function () {
        sessionStorage.setItem('configSelected', 'Cases');
        sessionStorage.setItem('selectId', _oCase.Id);
        window.location.href = 'Config';
    });

    $('[data-toggle="tooltip"]').tooltip();
}

// Open editor function
function openEditor() {

    $('.editContent').hide();

    buildEditor(JSON.parse(_oCase.Content), 'EditorContainer',
        function (data) {
            editCaseContent(_iCaseId, JSON.stringify(data), function (result) {
                _oCase.Content = JSON.stringify(data);
                statusMessage('Content updated.');
            });
        },
        function () {
            $('.tooltip').remove();
            $("#the-content").empty();
            displayDetail();
            $(".editContent").show();
        }
    );
}


// Startup function
$(function () {

    _iCaseId = parseInt(sessionStorage.getItem("caseId"));

    if (isNaN(_iCaseId)) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        _iCaseId = urlParams.get('CaseId');

    }

    if (_iCaseId == null || _iCaseId == 0)
        window.location.href = "Cases";
    else {
        switch (_sRole) {
            case "Administrator":
                getCaseLinkedById(_iCaseId, function (result) {
                    _oCase = result;
                    displayDetail();
                    $(".editContent").show();
                    $(".editObject").show();
                }, function () {
                        window.location.href = "Cases";
                });
                break;
            case "Owner":
                getOwnedCases(function (result) {
                    _arriOwnerIds = result;
                    getCaseLinkedById(_iCaseId, function (result) {
                        _oCase = result;
                        displayDetail();
                        if (_arriOwnerIds.includes(_iCaseId)) {
                            $(".editContent").show();
                            $(".editObject").show();
                        }
                    }, function () {
                            window.location.href = "Cases";
                    });
                });
                break;
            default:
                getCaseLinkedById(_iCaseId, function (result) {
                    _oCase = result;
                    displayDetail();
                }, function () {
                        window.location.href = "Cases";
                });
                break;
        }
    }

});