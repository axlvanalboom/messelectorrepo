﻿// User config definition
var _oUserConfig = {}, _oActSel, _oCompSel;

// Column definition
_oUserConfig.columns = [
    {
        data: 'Id',
        title: 'Id',
        type: 'readonly',
        visible: false
    },
    {
        data: 'UserName',
        title: 'Name',
        width: '30%'
    },
    {
        data: 'Email',
        title: 'Email',
        visible: false
    },
    {
        data: 'PhoneNumber',
        title: 'Telephone',
        visible: false
    },
    {
        data: 'Occupation',
        title: 'Occupation',
        width: '25%'
    },
    {
        data: 'Company',
        title: 'Company',
        width: '25%',
        type: 'hidden',
        render: function (data, type, row) {
            if (data !== null)
                return data.Name;
            else
                return 'None';
        }
    },
    {
        data: 'Role',
        title: 'Role',
        width: '10%',
        type: 'hidden',
        render: function (data, type, row) {
            if (data !== null)
                return data.Name;
            else
                return 'User';
        }
    },
    {
        data: 'EmailConfirmed',
        title: 'Confirmed',
        width: '10%',
        type: 'hidden',
        render: function (data, type, row) {
            if (data) 
                return '<i class="far fa-check"></i>';
            else
                return '<i class="far fa-times"></i>';
        }
    }
];

// Function definition
_oUserConfig.editData = function (datatable, rowdata, success, error) {
    editUser(rowdata.Id, rowdata.Email, rowdata.PhoneNumber, rowdata.UserName, rowdata.Occupation, function () {
        _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(rowdata, _oTemp.rowId)).show();
        _oTable.config.afterOpenRow();
        if (rowdata.Company.length == 0)
            rowdata.Company = null;
        if (rowdata.Role.length == 0)
            rowdata.Role = null;
        success(rowdata);
    });
};

_oUserConfig.deleteData = function (datatable, rowdata, success, error) {
    deleteUser(rowdata.Id, function () {
        success();
    });
};

// Render inside of row
_oUserConfig.renderOpenRow = function (data, indexes) {
    // Set temp object
    _oTemp = data;
    _oTemp.rowId = indexes;

    // Build company role content
    var sCompanyRole = '<tr><td>Company:</td><td><select id="UserCompany"></select></td><td>Role:</td><td><select id="UserRole"></select></td><td><td><button id="EditCompanyRole">Save</button></td></tr>';

    // Build total table
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" id="details-table">' +
        //'<tr>' +
        //'<td>Id:</td>' +
        //'<td>' + data.Id + '</td>' +
        //'</tr>' +
        '<tr>' +
        '<td>Email:</td>' +
        '<td>' + data.Email + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Telephone:</td>' +
        '<td>' + data.PhoneNumber + '</td>' +
        '</tr>' +
        sCompanyRole +
        '</table>';
};

// do stuff after the table is initialised
_oUserConfig.afterDrawing = function () {

    // Get company list
    getCompanies(function (result) {
        _oCompanies = result;
    });

    // Get roles list
    _oRoles = [
        {
            Id: 1,
            Name: 'User'
        }, {
            Id: 2,
            Name: 'Owner'
        }, {
            Id: 3,
            Name: 'Administrator'
        }
    ];

}

// do stuff after the row is opened
_oUserConfig.afterOpenRow = function () {

    // Fill in company list
    $("#UserCompany").append(`<option value="0">None</option>`);
    for (var i = 0; i < _oCompanies.length; i++) {
        $("#UserCompany").append(`<option value="${_oCompanies[i].Id}">${_oCompanies[i].Name}</option>`);
    }
    if (_oTemp.Company !== undefined && _oTemp.Company !== null)
        $("#UserCompany").val(_oTemp.Company.Id);

    // Fill in role list
    for (var i = 0; i < _oRoles.length; i++) {
        $("#UserRole").append(`<option value="${_oRoles[i].Id}">${_oRoles[i].Name}</option>`);
    }
    if (_oTemp.Role !== undefined && _oTemp.Role !== null)
        $("#UserRole").val(_oTemp.Role.Id);

    // Bind delete buttons to delete links
    $("#EditCompanyRole").click(function () {
        if (confirm("Are you sure you want to edit this link?")) {
            // Add the link
            var roleId = parseInt($("#UserRole").val());
            var companyId = parseInt($("#UserCompany").val());
            editUserComRo(_oTemp.Id, companyId, roleId, function (result) {
                // Edit temp object
                _oTemp.Role = {
                    Id: roleId,
                    Name: $("#UserRole option:selected").text()
                };
                _oTemp.Company = {
                    Id: companyId,
                    Name: $("#UserCompany option:selected").text()
                };

                // Redraw table row
                _oTable.row(_oTemp.rowId).data(_oTemp);
                _oTable.row(_oTemp.rowId).child(_oTable.config.renderOpenRow(_oTemp, _oTemp.rowId)).show();
                _oTable.config.afterOpenRow();
            });
        }
    });
}