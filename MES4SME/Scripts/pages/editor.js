﻿// Variables
var _oEditor, _btnSave, _txtOutput, _oData;

// Functions
function buildEditor(data, container, save, close) {

    $('.tooltip').remove();

    $("#"+ container).empty();

    var arroTools = {
        paragraph: {
            class: Paragraph,
            inlineToolbar: true,
        },
        header: {
            class: Header,
            config: {
                placeholder: 'Enter a header'
            },
            inlineToolbar: true
        },
        list: {
            class: List,
            inlineToolbar: true
        },
        //table: {
        //    class: Table,
        //    inlineToolbar: true,
        //    config: {
        //        rows: 2,
        //        cols: 3,
        //    },
        //    inlineToolbar: true
        //},
        image: {
            class: ImageTool,
            config: {
                endpoints: {
                    byFile: 'Owner/uploadImage',
                    errorMessage: "Wrong input type! Only image types are allowed!"
                }
            },
            inlineToolbar: true
        },
        attaches: {
            class: AttachesTool,
            config: {
                types: 'application/pdf',
                endpoint: 'Owner/uploadFile',
                errorMessage: "Wrong input type or size to big! Only pdf's less than 4Mb are allowed not containing viruses!"
            },
            inlineToolbar: true
        },
        linkTool: {
            class: LinkTool,
            config: {
                endpoint: 'Owner/uploadUrl'
            },
            inlineToolbar: true
        },
        //quote: {
        //    class: Quote,
        //    inlineToolbar: true,
        //    shortcut: 'CMD+SHIFT+O',
        //    config: {
        //        quotePlaceholder: 'Enter a quote',
        //        captionPlaceholder: 'Quote\'s author',
        //    },
        //    inlineToolbar: true
        //},
        marker: {
            class: Marker,
            shortcut: 'CMD+SHIFT+M',
            inlineToolbar: true
        },
        embed: Embed
    };

    if (_oEditor !== undefined && _oEditor !== null) {
        _oEditor.destroy();
        _oEditor = null;
    }

    _oEditor = new EditorJS({
        holder: container,
        tools: arroTools,
        data: data
    });

    var sButtons = `<i id="closeEditor" data-toggle="tooltip" title="Exit editor mode" data-placement="right" class="editContent far fa-portal-exit" />
                    <i id="saveContent" data-toggle="tooltip" title="Save changes" data-placement="left" class="editContent far fa-save" />`;

    $(sButtons).prependTo('#' + container);

    $('#saveContent').on('click', () => {
        _oEditor.save().then(data => {
            save(data);
        });
    });

    $('#closeEditor').on('click', () => {
        _oEditor.destroy();
        _oEditor = null;
        close();
    });

    $('[data-toggle="tooltip"]').tooltip();
}