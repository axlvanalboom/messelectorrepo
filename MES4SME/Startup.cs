﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MES4SME.Startup))]
namespace MES4SME
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    }
}
