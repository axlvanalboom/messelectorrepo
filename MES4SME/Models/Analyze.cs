﻿using BEC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MES4SME.Models
{

    public static class Helper
    {
        public static LogGraphVisualization CreateLogCraphVisualisation(List<StringInt> oData, string sLabel)
        {
            LogGraphVisualization oLogVisual = new LogGraphVisualization();
            oLogVisual.Labels = new string[oData.Count];
            oLogVisual.DataSets = new List<DataSet>();
            oLogVisual.DataSets.Add(new DataSet());
            oLogVisual.DataSets[0].BackgroundColor = "rgba(54, 162, 235, 0.2)";
            oLogVisual.DataSets[0].BorderColor = "rgba(54, 162, 235, 1)";
            oLogVisual.DataSets[0].BorderWidth = 1;
            oLogVisual.DataSets[0].Label = sLabel;
            oLogVisual.DataSets[0].Data = new object[oData.Count];

            for (int i = 0; i < oData.Count; i++)
            {
                oLogVisual.DataSets[0].Data[i] = oData[i].Number;
                oLogVisual.Labels[i] = oData[i].Text;
            }

            return oLogVisual;
        }
    }

    public class LogGraphVisualization
    {
        public string[] Labels { get; set; }
        public List<DataSet> DataSets { get; set; }

    }

    public class DataSet
    {
        public string Label { get; set; }
        public object[] Data { get; set; }

        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public int BorderWidth { get; set; }
    }

    

}