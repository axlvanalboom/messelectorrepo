﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using BEC;
using System.Diagnostics;
using DAL.DB;
using System.Data;
using System.Data.SqlClient;
namespace CodeTester
{
    class clsMain
    {

        public static void Test()
        {
            TestData_Questions();
        }

        private static void WriteReturn(clsReturn oReturn)
        {
            if (oReturn is null)
            {
                Debug.WriteLine("Return is null");
            }
            else
            {
                Debug.WriteLine("Return status: " + oReturn.Status.ToString());
                Debug.WriteLine("Return Message: " + oReturn.Message);
                Debug.WriteLine("Return Data: " + oReturn.Data);
            }
            Debug.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        public static void TestActivity()
        {
            clsReturn oReturn;
            //oReturn = clsBLL.addActivity(null); WriteReturn(oReturn);
            //oReturn = clsBLL.addActivity(new clsActivity(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addActivity(new clsActivity(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addActivity(new clsActivity(0, "TestActivity", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addActivity(new clsActivity(0, null, "TestDescription")); WriteReturn(oReturn);
            //oReturn = clsBLL.addActivity(new clsActivity(0, "TestActivity2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.getActivities(); WriteReturn(oReturn);

            //oReturn = clsBLL.editActivity(null); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1040, null, null)); WriteReturn(oReturn);

            //oReturn = clsBLL.editActivity(new clsActivity(1041, "TestActivity3", "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1041, "", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1041, null, "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1041, "", "TestDescriptionVersion2")); WriteReturn(oReturn);
            //oReturn = clsBLL.editActivity(new clsActivity(1041, "TestActivity2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteActivity(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteActivity(new clsActivity(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteActivity(new clsActivity(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteActivity(new clsActivity(2, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteActivity(new clsActivity(1040, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteActivity(new clsActivity(1041, null, null)); WriteReturn(oReturn);

        }
        public static void TestSector()
        {
            clsReturn oReturn;

            //oReturn = clsBLL.addSector(null); WriteReturn(oReturn); 
            //oReturn = clsBLL.addSector(new clsSector(0, null, null)); WriteReturn(oReturn); 
            //oReturn = clsBLL.addSector(new clsSector(1, null, null)); WriteReturn(oReturn); 
            //oReturn = clsBLL.addSector(new clsSector(0, "TestSector", null)); WriteReturn(oReturn); 
            //oReturn = clsBLL.addSector(new clsSector(0, null, "TestDescription")); WriteReturn(oReturn); 
            //oReturn = clsBLL.addSector(new clsSector(0, "TestSector2", "TestDescription")); WriteReturn(oReturn); 

            //oReturn = clsBLL.getSectors(); WriteReturn(oReturn); 

            //oReturn = clsBLL.editSector(null); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1187, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1188, "TestSector3", "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1188, "", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1188, null, "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1188, "", "TestDescriptionVersion2")); WriteReturn(oReturn);
            //oReturn = clsBLL.editSector(new clsSector(1188, "TestSector2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteSector(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSector(new clsSector(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSector(new clsSector(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSector(new clsSector(2, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSector(new clsSector(1187, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSector(new clsSector(1188, null, null)); WriteReturn(oReturn);

        }
        public static void TestCategory()
        {
            clsReturn oReturn;

            //oReturn = clsBLL.addCategory(null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCategory(new clsCategory(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCategory(new clsCategory(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCategory(new clsCategory(0, "TestCategory", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCategory(new clsCategory(0, null, "TestDescription")); WriteReturn(oReturn);
            //oReturn = clsBLL.addCategory(new clsCategory(0, "TestCategory2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.getCategories(); WriteReturn(oReturn);

            //oReturn = clsBLL.editCategory(null); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(11111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(11111, "azert", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1062, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1063, "TestCategory3", "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1063, "", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1063, null, "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1063, "", "TestDescriptionVersion2")); WriteReturn(oReturn);
            //oReturn = clsBLL.editCategory(new clsCategory(1063, "TestCategory2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteCategory(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCategory(new clsCategory(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCategory(new clsCategory(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCategory(new clsCategory(2, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCategory(new clsCategory(1062, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCategory(new clsCategory(1063, null, null)); WriteReturn(oReturn);

        }
        public static void TestFunction()
        {
            clsReturn oReturn;

            //oReturn = clsBLL.addFunction(null); WriteReturn(oReturn);
            //oReturn = clsBLL.addFunction(new clsFunction(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addFunction(new clsFunction(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addFunction(new clsFunction(0, "TestFunction", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addFunction(new clsFunction(0, null, "TestDescription")); WriteReturn(oReturn);
            //oReturn = clsBLL.addFunction(new clsFunction(0, "TestFunction2", "TestDescription")); WriteReturn(oReturn);

            //oReturn = clsBLL.getFunctions(); WriteReturn(oReturn);

            //oReturn = clsBLL.editFunction(null); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(11111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(11111, "azerty", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1112, "TestFunction3", "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1112, "", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1112, null, "")); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1112, "", "TestDescriptionVersion2")); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1112, "TestFunction2", "TestDescription")); WriteReturn(oReturn);
            //oReturn = clsBLL.editFunction(new clsFunction(1111, "", "")); WriteReturn(oReturn); //niets aanpassen

            //oReturn = clsBLL.deleteFunction(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteFunction(new clsFunction(0, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteFunction(new clsFunction(1, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteFunction(new clsFunction(2, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteFunction(new clsFunction(1111, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteFunction(new clsFunction(1112, null, null)); WriteReturn(oReturn);

        }
        public static void TestCompany()
        {
            clsReturn oReturn;
            clsCompany o;
            ////--> werkt
            //oReturn = clsBLL.addCompany(null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(0, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(0, "", null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany", null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany2", "TestDescription", null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany3", null, "TestWebsite", null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany4", null, null, "TestEmail", null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany5", null, null, null, "TestTelefoon", null)); WriteReturn(oReturn);
            //oReturn = clsBLL.addCompany(new clsCompany(1, "TestCompany6", null, null, null, null, "TestAdress")); WriteReturn(oReturn);
            //var o = new clsCompany(1, "TestCompany7", null, null, null, null, null); o.LogoUrl = "TestLogo";
            //oReturn = clsBLL.addCompany(o); WriteReturn(oReturn);

            //oReturn = clsBLL.editCompany(null); WriteReturn(oReturn); //null meegeven
            //oReturn = clsBLL.editCompany(new clsCompany(0, null, null, null, null, null, null)); WriteReturn(oReturn);  //id null
            //oReturn = clsBLL.editCompany(new clsCompany(1, null, null, null, null, null, null)); WriteReturn(oReturn); // id bestaat niet
            //oReturn = clsBLL.editCompany(new clsCompany(1, "", null, null, null, null, null)); WriteReturn(oReturn); // id bestaat niet
            //oReturn = clsBLL.editCompany(new clsCompany(1230, null, null, null, null, null, null)); WriteReturn(oReturn); // name null

            //var o1 = new clsCompany(1230, "", "", "", "", "", ""); o1.LogoUrl = ""; //niets aanpassen
            //oReturn = clsBLL.editCompany(o1); WriteReturn(oReturn); 

            //// 1 testen --> werkt
            // o = new clsCompany(1246, "TestCompany", "", "", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription2", "", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "TestWebsite2", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "TestEmail2", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "", "TestTelefoon2", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "", "", "TestAdress2"); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "", "", ""); o.LogoUrl = "TestLogo2";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);

            //o = new clsCompany(1246, "TestCompany", "TestDescription", "", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "TestCompany_", "", "TestWebsite", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "TestCompany", "", "", "TestEmail", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "TestCompany_", "", "", "", "TestTelefoon", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "TestCompany", "", "", "", "", "TestAdress"); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "TestCompany_", "", "", "", "", ""); o.LogoUrl = "TestLogo";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription2", "TestWebsite2", "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription", "", "TestEmail2", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription2", "", "", "TestTelephone", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription", "", "", "", "TestAdress"); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "TestDescription2", "", "", "", ""); o.LogoUrl = "TestLogo2";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "TestWebsite", "TestEmail2", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "TestWebsite2", "", "TestTelefoon", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "TestWebsite", "", "", "TestAdress"); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "TestWebsite2", "", "", ""); o.LogoUrl = "TestLogo147";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "TestEmail", "TestTelefoon2", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "TestEmail2", "", "TestAdres2"); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);

            //o = new clsCompany(1246, "", "", "", "TestEmail", "", ""); o.LogoUrl = null; 
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "", "TestTelefoon", null); o.LogoUrl = "";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", "", "", "", "TestTelefoon2", ""); o.LogoUrl = "TestLogo147";
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(1246, "", null, null, null, null, null); o.LogoUrl = null;
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);
            //o = new clsCompany(11111, "", null, null, null, null, null); o.LogoUrl = null;
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);

            //o = new clsCompany(11111, "azerty", null, null, null, null, null); o.LogoUrl = null;
            //oReturn = clsBLL.editCompany(o); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteCompany(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(0, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(2, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1247, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1248, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1249, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1250, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1251, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1252, null, null, null, null, null, null)); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCompany(new clsCompany(1230, null, null, null, null, null, null)); WriteReturn(oReturn);

            //oReturn = clsBLL.getCompanyById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyById(2); WriteReturn(oReturn);

            //oReturn = clsBLL.getCompanyById(1246); WriteReturn(oReturn); 
            //oReturn = clsBLL.getCompanies(); WriteReturn(oReturn);

            //oReturn = clsBLL.getCompaniesLinked(); WriteReturn(oReturn); 
            //oReturn = clsBLL.getCompanyLinkedById(0); WriteReturn(oReturn);
            ////oReturn = clsBLL.getCompanyLinkedById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyLinkedById(2); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyLinkedById(1230); WriteReturn(oReturn);




        }
        public static void TestMes()
        {
            clsReturn oReturn;
            clsSoftware o;

            //oReturn = clsBLL.addSoftware(null, 0, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftware(null, 1, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftware(null, 0, 1, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftware(null, 456, 1031, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftware(null, 1246, 456, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftware(null, 1246, 1031, null); WriteReturn(oReturn);

            //oReturn = clsBLL.addSoftware(new clsSoftware(0, "TestMes1", null, null), 1246, 1031, null); WriteReturn(oReturn);
            //var o1 = new clsSoftware(0, "TestMes2", "TestDescrption", "TestWebsite"); o1.LogoUrl = "TestLogo";
            //oReturn = clsBLL.addSoftware(o1, 1246, 1031, null); WriteReturn(oReturn);

            //clsFunction o5;
            //o5 = new clsFunction(1103, "detailed scheduling", null);
            //var o6 = new clsFunction(1104, "det", null);
            ////o5 = new clsFunction(11111, "detailed scheduling", null);
            ////o5 = new clsFunction(0, "detailed scheduling", null);
            //var o4 = new List<clsFunction>();
            //o4.Add(o5);
            //o4.Add(o6);
            //var o3 = new clsCategoryFunctions();
            ////o3.Name = "production";
            //o3.Functions = o4;
            //o3.Id = 1056;
            //var o2 = new List<clsCategoryFunctions>();
            //o2.Add(o3);
            //oReturn = clsBLL.addSoftware(new clsSoftware(0, "TestMes8", null, null), 1246, 1031, o2); WriteReturn(oReturn);


            //oReturn = clsBLL.editSoftware(null); WriteReturn(oReturn);

            //o = new clsSoftware(0,null,null,null); o.LogoUrl = null;
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(1, null,null,null); o.LogoUrl = null;
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(283, null, null, null); o.LogoUrl = null;
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(1, "","",""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(283, "", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(283, null, "",""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            ////1 waarde aanpassen
            //o = new clsSoftware(283, "Test", "", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "", "TESTDESCRIPTIE", ""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "", "", "TETWEBSITE"); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "", "", ""); o.LogoUrl = "TESTLOGO";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //2waarden aanpassen
            //o = new clsSoftware(283, "Test2",null, ""); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "Test", "", null); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "Test2", "", ""); o.LogoUrl = null;
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //o = new clsSoftware(283, "", "TEstDescR", "TestWeb"); o.LogoUrl = "";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);
            //o = new clsSoftware(283, "", "TEst", ""); o.LogoUrl = "TestingLogige";
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);


            //o = new clsSoftware(283, "", "", null); o.LogoUrl = null;
            //oReturn = clsBLL.editSoftware(o); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteSoftware(0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftware(1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftware(2); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftware(283); WriteReturn(oReturn);

            //oReturn = clsBLL.getSoftwares(); WriteReturn(oReturn);

            //oReturn = clsBLL.getSoftwareById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareById(2); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareById(282); WriteReturn(oReturn);

            //oReturn = clsBLL.getSoftwareLinked(); WriteReturn(oReturn); 
            //oReturn = clsBLL.getSoftwareLinkedById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareLinkedById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareLinkedById(2); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareLinkedById(282); WriteReturn(oReturn);

        }
        public static void TestCase()
        {
            clsReturn oReturn;
            clsCase o;

            //oReturn = clsBLL.addCase(null, 0, 0, 0, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(null, 1246, 0, 0, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(null, 1246, 0, 1169, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(null, 1246, 282, 1169, 0, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(null, 1246, 282, 1169, 1031, null); WriteReturn(oReturn);

            //oReturn = clsBLL.addCase(new clsCase(0, null, null, null), 1246, 282, 1169, 1031, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(new clsCase(0, "Testing", null, null), 1246, 282, 1169, 1031, null); WriteReturn(oReturn);
            //o = new clsCase(0, "Testingcase2", "TestDescription", "TestWeb"); o.LogoUrl = "TestLogo";
            //oReturn = clsBLL.addCase(o, 1246, 282, 1169, 1031, null); WriteReturn(oReturn);
            //o = new clsCase(0, "Testingcase2", "TestDescription", "TestWeb"); o.LogoUrl = "TestLogo";
            //oReturn = clsBLL.addCase(o, 11246, 282, 1169, 1031, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(o, 1246, 11282, 1169, 1031, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(o, 1246, 282, 11169, 1031, null); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(o, 1246, 282, 1169, 11031, null); WriteReturn(oReturn);

            //clsFunction o5;
            //o5 = new clsFunction(1103, "detailed scheduling", null);
            //var o6 = new clsFunction(1104, "det", null);
            //o5 = new clsFunction(11111, "detailed scheduling", null);
            //////o5 = new clsFunction(0, "detailed scheduling", null);
            //var o4 = new List<clsFunction>();
            //o4.Add(o5);
            //o4.Add(o6);
            //var o3 = new clsCategoryFunctions();
            //////o3.Name = "production";
            //o3.Functions = o4;
            //o3.Id = 1056;
            //var o2 = new List<clsCategoryFunctions>();
            //o2.Add(o3);
            ////oReturn = clsBLL.addSoftware(new clsSoftware(0, "TestMes8", null, null), 1246, 1031, o2); WriteReturn(oReturn);
            //oReturn = clsBLL.addCase(new clsCase(0, "TestingCase", null, null), 1246, 282, 1169, 1031, o2); WriteReturn(oReturn);



            //oReturn = clsBLL.getCaseById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseById(2); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseById(1467); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseById(1468); WriteReturn(oReturn);
            //oReturn = clsBLL.getCases(); WriteReturn(oReturn);


            //oReturn = clsBLL.deleteCaseById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseById(2); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteCaseById(1473); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseById(1474); WriteReturn(oReturn);

            //o = null;  oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(0, null, null, null); o.LogoUrl = null; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, null, null, null); o.LogoUrl = null; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "", ""); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "Name2", "", ""); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "Desc2", ""); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "", "Web2"); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "", ""); o.LogoUrl = "Logo2"; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "Name", null, ""); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "Name2", "", null); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "Name", "", ""); o.LogoUrl = null; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "Desc", "Web"); o.LogoUrl = ""; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "Desc2", ""); o.LogoUrl = "Logo"; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);
            //o = new clsCase(1474, "", "", null); o.LogoUrl = null; oReturn = clsBLL.editCase(o); WriteReturn(oReturn);

            //oReturn = clsBLL.editCaseSector(0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.editCaseSector(1474,0); WriteReturn(oReturn);
            //oReturn = clsBLL.editCaseSector(1474,1); WriteReturn(oReturn);
            //oReturn = clsBLL.editCaseSector(1,1169); WriteReturn(oReturn);
            //oReturn = clsBLL.editCaseSector(1474,1169); WriteReturn(oReturn);
            //oReturn = clsBLL.editCaseSector(1474,1170); WriteReturn(oReturn);

            //oReturn = clsBLL.getCasesLinked(); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseLinkedById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseLinkedById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseLinkedById(2); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseLinkedById(1471); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseLinkedById(1472); WriteReturn(oReturn);

        }
        public static void TestUser()
        {
            clsReturn oReturn;
            clsUser o;

            //oReturn = clsBLL.getUsers(); WriteReturn(oReturn);
            //oReturn = clsBLL.getUserById(null); WriteReturn(oReturn);
            //oReturn = clsBLL.getUserById(""); WriteReturn(oReturn);

            //oReturn = clsBLL.getUserById("azerqgdfhshfhgfghfsgnf"); WriteReturn(oReturn);
            //oReturn = clsBLL.getUserById("azerq"); WriteReturn(oReturn);
            //oReturn = clsBLL.getUserById("2ce8a9b9-f0bc-4d13-b404-5521dc239931"); WriteReturn(oReturn);

            //oReturn = clsBLL.getUsersLinked(); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = null; o.Occupation = null; o.PhoneNumber = null;
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = ""; o.Occupation = null; o.PhoneNumber = null;
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = "qsdgqgresgrsdg"; o.Occupation = null; o.PhoneNumber = null;
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = null; o.PhoneNumber = null; o.UserName = null;
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = "TestOcu"; o.PhoneNumber = ""; o.UserName = "";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);
            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = ""; o.PhoneNumber = "TestPhone"; o.UserName = "";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);
            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = ""; o.PhoneNumber = ""; o.UserName = "TestUserName";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);
            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = "TestOcu2"; o.PhoneNumber = "RingRing"; o.UserName = "";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);
            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = "Octopus"; o.PhoneNumber = ""; o.UserName = "tentacle";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);
            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = ""; o.PhoneNumber = "ringrangringrang"; o.UserName = "nokia";
            //oReturn = clsBLL.editUser(o); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteUser(null); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteUser(""); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteUser("etregedgdfgdf"); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteUser("7beea64a-346b-445c-8225-075dde33868e"); WriteReturn(oReturn);

            //o = new clsUser(); o.Id = "7beea64a-346b-445c-8225-075dde33868e"; o.Occupation = ""; o.PhoneNumber = "ringrangringrang"; o.UserName = "nokia";
            //oReturn = clsBLL.editUserComRo(null,0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("",0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("azef",0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("azef",7,156); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("azef",7,1); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("azef",1,1); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("azef",1230,1); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("7beea64a-346b-445c-8225-075dde33868e", 1,1230); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("7beea64a-346b-445c-8225-075dde33868e", 1230, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("8ea0af0b-ba81-4b77-8466-38c45ec7d9cb", 1230, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.editUserComRo("ada2a979-808d-41f5-aba1-8232123e34a7", 1230, 2); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteUser("8ea0af0b-ba81-4b77-8466-38c45ec7d9cb"); WriteReturn(oReturn);

        }
        public static void TestContent()
        {
            clsReturn oReturn;
            string sOwner = "ada2a979-808d-41f5-aba1-8232123e34a7";
            //oReturn = clsBLL.getCompanyContentById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(4); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1254); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(4, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1254, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(0,0,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(4, 0,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1254, 0,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(0,1,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(4, 1,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1254, 1,null); WriteReturn(oReturn);

            //oReturn = clsBLL.getCompanyContentById(1253); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,5); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,2, sOwner); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,2, ""); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,2, "gsfdg"); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,2, null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCompanyContentById(1253,3); WriteReturn(oReturn);

            //oReturn = clsBLL.getSoftwareContentById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(282); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(282,0); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(282,1); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(282,2); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(282,4); WriteReturn(oReturn);

            //oReturn = clsBLL.getSoftwareContentById(284); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,0); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,1); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,2); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,2,null); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,2,"gerger"); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,2,sOwner); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,3,"tetrgrf"); WriteReturn(oReturn);
            //oReturn = clsBLL.getSoftwareContentById(284,3); WriteReturn(oReturn);

            //oReturn = clsBLL.getCaseContentById(0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1471); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1471, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1471, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1471, 2); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1471, 4); WriteReturn(oReturn);

            //oReturn = clsBLL.getCaseContentById(1472); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 2); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 2, null); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 2, "gerger"); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 2, sOwner); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 3, "tetrgrf"); WriteReturn(oReturn);
            //oReturn = clsBLL.getCaseContentById(1472, 3); WriteReturn(oReturn);

            //oReturn = clsBLL.editContent(3, 1471, "a;delete from Activity"); WriteReturn(oReturn); //testje


        }
        public static void TestLinks()
        {
            clsReturn oReturn;

            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(0,0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(0,0,1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(0,1,0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(1,0,0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(1,1,0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(1,1,1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(284,1,1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(284,1056,1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(284,1,1113); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(284,1056,1113); WriteReturn(oReturn); // wordt toegevoegd
            //oReturn = clsBLL.addSoftwareCategoryFunctionLink(284,1056,1113); WriteReturn(oReturn); // bestaat al

            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(0, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(0, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(0, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(1, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(1, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(1, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(284, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(284, 1056, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(284, 1, 1113); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(284, 1056, 1113); WriteReturn(oReturn); // wordt toegevoegd
            //oReturn = clsBLL.deleteSoftwareCategoryFunctionLink(284, 1056, 1113); WriteReturn(oReturn); // is al weg

            //oReturn = clsBLL.addCaseCategoryFunctionLink(0, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(0, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(0, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1476, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1476, 1056, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1476, 1, 1113); WriteReturn(oReturn);
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1476, 1056, 1113); WriteReturn(oReturn); // geen link met een mes
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1476, 1056, 1113); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1472, 1056, 1113); WriteReturn(oReturn); // link met een mes die dit niet heeft
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1472, 1056, 1113); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1471, 1056, 1113); WriteReturn(oReturn); // link met een mes die dit heeft
            //oReturn = clsBLL.addCaseCategoryFunctionLink(1471, 1056, 1113); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(0, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(0, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(0, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1476, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1476, 1056, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1476, 1, 1113); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1471, 1056, 1113); WriteReturn(oReturn); // wordt toegevoegd
            //oReturn = clsBLL.deleteCaseCategoryFunctionLink(1476, 1056, 1113); WriteReturn(oReturn); // is al weg


        }
        public static void TestLinks2()
        {
            clsReturn oReturn;

            //oReturn = clsBLL.addSoftwareCompanyActivityLink(0, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(0, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(0, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(1, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(1, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(1, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(0, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(1, 1, 1); WriteReturn(oReturn);

            //oReturn = clsBLL.addSoftwareCompanyActivityLink(278, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(278, 1246, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(278, 1, 1031); WriteReturn(oReturn);
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(1, 1246, 1031); WriteReturn(oReturn);

            //oReturn = clsBLL.addSoftwareCompanyActivityLink(278, 1246, 1031); WriteReturn(oReturn); // wordt toegevoegd
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(278, 1246, 1031); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.addSoftwareCompanyActivityLink(279, 1246, 1031); WriteReturn(oReturn); // bestaat al

            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(0, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(0, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(0, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(1, 0, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(1, 1, 0); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(1, 0, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(0, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(1, 1, 1); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1246, 1); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1, 1031); WriteReturn(oReturn);
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(1, 1246, 1031); WriteReturn(oReturn);

            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1246, 1031); WriteReturn(oReturn); // wordt toegevoegd
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1246, 1031); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(279, 1246, 1031); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.deleteSoftwareCompanyActivityLink(278, 1237, 1032); WriteReturn(oReturn); // bestaat al

            //oReturn = clsBLL.editCaseCompanySoftware(0,0,0); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1,0,0); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(0,1,0); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(0,0,1); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1,0,1); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1,1,0); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(0,1,1); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1471,1,1); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1,284,1); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1,1,1246); WriteReturn(oReturn); // bestaat al

            //oReturn = clsBLL.editCaseCompanySoftware(1469, 1246,284); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1469, 1237,282); WriteReturn(oReturn); // bestaat al
            //oReturn = clsBLL.editCaseCompanySoftware(1472,282,1237); WriteReturn(oReturn); // bestaat al

        }
        public static void TestFullyLinked()
        {
            //clsReturn oReturn;
            ////oReturn = clsBLL.getFullyLinked(); WriteReturn(oReturn);
            ////oReturn = clsBLL.getFullyLinked(new clsFullLinks()); WriteReturn(oReturn);


            //clsFullLinks a = new clsFullLinks();
            ////a.Company = new List<int>() { 1230,1231};         //werkt
            ////a.Activity = new List<int>() {1032 };        //werkt
            ////a.Mes = new List<int>() { 256,257};               //werkt
            ////a.Sector = new List<int>() {0,1170,1172 };        //werkt
            ////a.Case = new List<int>() { 1,1431};             //werkt
            ////a.MesCategory = new List<int>() { 1056,1057 };    //werkt
            ////a.MesFunction = new List<int>() { 1101,1103 };    //werkt
            ////a.CaseCategory = new List<int>() {0 ,1057};       //werkt
            //a.CaseFunction = new List<int>() { 0 ,1101,1105}; //werkt
            //oReturn = clsBLL.getFullyLinked(a); WriteReturn(oReturn); 




        }

        public static void TestContacts()
        {
            //List<clsContacts> o;
            int o;
            var c = new clsContact();
            ////o = clsDB.AddContact(null);

            ////o = clsDB.AddContact(c);
            //c.Id = 0;
            ////o = clsDB.AddContact(c);
            //c.Verdeling = "";
            ////o = clsDB.AddContact(c);
            //c.Verdeling = "To";
            ////o = clsDB.AddContact(c);
            //c.Email = "stijn.huysen";
            ////o = clsDB.AddContact(c);
            //c.Email = "axl.vanal@ugent.be";
            //o = clsDB.AddContact(c);

            //o = clsDB.EditContact(null);
            //o = clsDB.EditContact(c);

            // c.Id = 0;
            // //o = clsDB.EditContact(c);
            // c.Id = 5;
            // //o = clsDB.EditContact(c);
            // c.Id = 4;
            // ///o = clsDB.EditContact(c);
            // c.Verdeling = "C";
            // //o = clsDB.EditContact(c);
            // c.Verdeling = "Cc";
            // //o = clsDB.EditContact(c);
            // c.Email = "";
            //// o = clsDB.EditContact(c);
            // c.Email = "sgqdgfdg";
            // //o = clsDB.EditContact(c);
            // c.Email = "qdfq@qgfgfd";
            // o = clsDB.EditContact(c);
            // //o = clsDB.EditContact(c);

            //o = clsDB.DeleteContact(null);
            //o = clsDB.DeleteContact(c);
            //c.Id = 0;
            ////o = clsDB.DeleteContact(c);
            //c.Id = 5;
            //o = clsDB.DeleteContact(c);
            //c.Id = 4;
            //o = clsDB.DeleteContact(c);
            //c.Id = 2;
            //o = clsDB.DeleteContact(c);
            //c.Id = 3;
            //o = clsDB.DeleteContact(c);
            //c.Id = 1;
            //o = clsDB.DeleteContact(c);
        }

        public static void TestLogType()
        {
           // clsDB.AddLogType("");
            //clsDB.AddLogType(null);
            //clsDB.AddLogType("Algemene search");
            //clsDB.AddLogType("Algemene search");
            clsDB.AddLogType("Companies");
            clsDB.AddLogType("Company Detail");
            clsDB.AddLogType("Software");
            clsDB.AddLogType("Software Detail");
            clsDB.AddLogType("Cases");
            clsDB.AddLogType("Case Detail");
            clsDB.AddLogType("Company Search");
            clsDB.AddLogType("Software Search");
            clsDB.AddLogType("Case Search");

        }
        public static void TestLogging()
        {
            //for (int i=0; i<100;i++)
            //{
            //    clsBLL.AddLog(clsBLL.GetRandomId(),"Company Detail");
            //}

            //clsBLL.AddLog("a","a","a");
            //clsDB.AddLog("azer","2019","Companies");
            //clsDB.AddLog("azer","2019","Software");
            //clsDB.AddLog("azer","2019","Cases");
            //clsDB.AddLog("azer","2019","Company Detail","Axl");
            //clsDB.AddLog("azer","2019","Company Detail",1);

            //clsDB.AddLog("azer", "2019", 101);
            //clsDB.AddLog("azer", "2019", 1);
            //clsDB.AddLog("azer", "2019", 2);
            //clsDB.AddLog("azer", "2019", 3);
            //clsDB.AddLog("azer", "2019", 4, "Axl");
            //clsDB.AddLog("azer", "2019", 5, 1);

            //clsDB.GetLogsByTime("1578919377", "1578920088");
            //clsBLL.GetLogsBetweenTime("Year","a","a");
            //clsBLL.GetLogsBetweenTime(null,"a","a");
            //clsBLL.GetLogsBetweenTime(null, "1578919377", "1578920088"); //epoch time
            //clsBLL.GetLogsBetweenTime(null, "1578919377", "a");
            //clsBLL.GetLogsBetweenTime(null,"a", "1578920088");
            //clsBLL.GetLogsBetweenTime(null,"2020/1/13 11:1:1", "2020/1/14 11:0:0");
            //clsBLL.GetLogsBetweenTime(null,"2020/1/10 1:1:1", "a");
            //clsBLL.GetLogsBetweenTime(null,"a", "2020/1/12 1:1:1");
            //clsBLL.GetLogsBetweenTime("Last Year","a", "2020/1/12 1:1:1");

            DataTable oDT = new DataTable();
            oDT.Clear();
            oDT.Columns.Add("Id");
            oDT.Columns.Add("Time");

            DataRow oDr;
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "C"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "C"; oDr["Time"] = "1579097444"; oDT.Rows.Add(oDr); //1

            oDr = oDT.NewRow(); oDr["Id"] = "D"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "D"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "D"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "E"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "E"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "F"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "H"; oDr["Time"] = "1579011044"; oDT.Rows.Add(oDr); //1
            
            oDr = oDT.NewRow(); oDr["Id"] = "H"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "H"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "I"; oDr["Time"] = "1578924644"; oDT.Rows.Add(oDr); //1

            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "K"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "K"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "K"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "L"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "J"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578838244"; oDT.Rows.Add(oDr); //1
            
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578751844"; oDT.Rows.Add(oDr); //1
            
            oDr = oDT.NewRow(); oDr["Id"] = "C"; oDr["Time"] = "1578665444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "C"; oDr["Time"] = "1578665444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1578665444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1578665444"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1578665444"; oDT.Rows.Add(oDr); //1
            
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "R"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "A"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "B"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "C"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "D"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "E"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "F"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1
            oDr = oDT.NewRow(); oDr["Id"] = "G"; oDr["Time"] = "1578579044"; oDT.Rows.Add(oDr); //1






            //clsDB.CalculateAmountOfVisitorsPerDay(oDT);    
        
        }

  
        public static void TestQuestion()
        {
            var r = clsBLL.ReturnAnswers();
            r = clsBLL.ReturnQuestionCategories();
            r = clsBLL.ReturnQuestions();
            r = clsBLL.ReturnSoftwareAnswers(new clsSoftware() { Id = 10 });


            /*
            var Ps = clsBLL.GetQuestionAnswers();
            var aPa1 = (clsAnswerMC)Ps[0];
            var aPa2 = (clsAnswerMC)Ps[1];
            var aPa3 = (clsAnswerMC)Ps[2];

            var QCs = clsBLL.GetQuestionCategories();
            var qc = (clsQuestionCategory)QCs[0];

            var Qs = clsBLL.GetQuestions();
            var q1 = (clsQuestionMC)Qs[0];
            var q2 = (clsQuestionRange)Qs[1];

            //var sa1 = new clsSoftwareAnswerMC()
            //{
            //    Name = "sa1",
            //    Question = q1,
            //    Answer = aPa3,
            //};
            //var sa2 = new clsSoftwareAnswerRange()
            //{
            //    Name = "sa2",
            //    Question = q2,
            //    Value = 5,// not right
            //};
            var oS = new clsSoftwareWithAnswers() { Id = 10};
            //var r = clsBLL.AddSoftwareAnswers(oS);

            var SAs = clsBLL.GetSoftwareAnswer(oS);
            var sa1 = (clsSoftwareAnswerMC)SAs[0];
            var sa2 = (clsSoftwareAnswerRange)SAs[1];
            sa1.Name = "sa1 edited 2";
            sa1.Answer = aPa2;
            var r = clsBLL.EditSoftwareAnswer(sa1);
            sa1.Answer = aPa3;
            r = clsBLL.EditSoftwareAnswer(sa1);
            sa2.Name = "sa2 edited 2";
            sa2.Value = 9;
            r = clsBLL.EditSoftwareAnswer(sa2);
            sa2.Value = 5;
            r = clsBLL.EditSoftwareAnswer(sa2);*/
            /*
            #region Questions
            
            var Ps = clsBLL.GetQuestionAnswers();
            var aPa1 = (clsAnswerMC)Ps[0];
            var aPa2 = (clsAnswerMC)Ps[1];
            var aPa3 = (clsAnswerMC)Ps[2];

            var QCs = clsBLL.GetQuestionCategories();
            var qc = (clsQuestionCategory)QCs[0];

            var Qs = clsBLL.GetQuestions();
            var q1 = (clsQuestionMC)Qs[0];
            var q2 = (clsQuestionRange)Qs[1];

            var sa1 = new clsSoftwareAnswerMC()
            {
                Name = "sa1",
                Question = q1,
                Answer = aPa2, 
            };
            var sa2 = new clsSoftwareAnswerRange()
            {
                Name = "sa2",
                Question = q2,
                Value = 9,// not right
            };

            var r = clsBLL.getSoftwareById(10,3);
            var oSoft = (clsSoftware)r.Data;
            r =clsBLL.AddSoftwareAnswers(new clsSoftwareWithAnswers(oSoft) { Answers = new List<clsAbstractSoftwareAnswer>() { sa1, sa2 } });

            var SAs = clsBLL.GetSoftwareAnswer(oSoft);
            sa1 = (clsSoftwareAnswerMC)SAs[0];
            sa2 = (clsSoftwareAnswerRange)SAs[1];

            r = clsBLL.DeleteSoftwareAnswer(sa1.Id);
            r = clsBLL.DeleteSoftwareAnswer(sa2.Id);

            r = clsBLL.DeleteQuestion(q1.Id);
            r = clsBLL.DeleteQuestion(q2.Id);




            //var x = clsDB.Get_Questions();
            #endregion
                 */
        }
        public static void TestQuestion_v1()
        {
            #region Answers
            /*
            var aBlablabla = new clsAnswerMC("blablabla", "cloud connected");
            aBlablabla.Id = clsDB.Add_QuestionAnswer(aBlablabla);
            aBlablabla.Description = "amaranthe";
            clsDB.Edit_QuestionAnswer(aBlablabla);

            var x = clsDB.Get_QuestionAnswer(aBlablabla);
            var y = clsDB.Get_QuestionAnswer(aBlablabla.Id);
            var z = clsDB.Get_QuestionAnswer();

            clsDB.Delete_QuestionAnswer(aBlablabla);
            */
            #endregion

            #region Category
            /*
            var aBlablabla = new clsQuestionCategory("blablabla", "cloud connected");
            aBlablabla.Id = clsDB.Add_QuestionCategory(aBlablabla);
            aBlablabla.Description = "amaranthe";
            clsDB.Edit_QuestionCategory(aBlablabla);

            var x = clsDB.Get_QuestionCategory(aBlablabla);
            var y = clsDB.Get_QuestionCategory(aBlablabla.Id);
            var z = clsDB.Get_QuestionCategories();

            clsDB.Delete_QuestionCategory(aBlablabla);
            */
            #endregion

            #region Questions
            /*
            var aPa1 = new clsAnswerMC("PA1",null); aPa1.Id = clsDB.Add_QuestionAnswer(aPa1);
            var aPa2 = new clsAnswerMC("PA2",null); aPa2.Id = clsDB.Add_QuestionAnswer(aPa2);
            var aPa3 = new clsAnswerMC("PA3",null); aPa3.Id = clsDB.Add_QuestionAnswer(aPa3);
            var qc = clsDB.Get_QuestionCategory(1);

            var q1 = new clsQuestionMC()
            {
                Name = "question 1",
                Category = qc,
                CategoryMaximum = 60,
                CategoryMinimum = 30,
                PossibleAnswers = new List<clsAnswerMC>() { aPa1, aPa2, aPa3}
            };
            q1.Id = clsDB.Add_Question(q1);
            var q2 = new clsQuestionRange()
            {
                Name = "question 2",
                Category = qc,
                CategoryMaximum = 100,
                CategoryMinimum = 0,
                Minimum = 1,
                Maximum = 10
            };
            q2.Id = clsDB.Add_Question(q2);

            q1 = new clsQuestionMC()
            {
                Id = q1.Id,
                Name = "question 1 edited",
                Category = qc,
                CategoryMaximum = 100,
                CategoryMinimum = 0,
                PossibleAnswers = new List<clsAnswerMC>() { aPa1, aPa3 }
            };
            q2 = new clsQuestionRange()
            {
                Id = q2.Id,
                Name = "question 2 edited",
                Category = qc,
                CategoryMaximum = 10,
                CategoryMinimum = 9,
                Minimum = 5,
                Maximum = 6
            };

            clsDB.Edit_Question(q1);
            clsDB.Edit_Question(q2);

            clsDB.Delete_Question(q2);
            clsDB.Delete_Question(q1);
            */
            //var x = clsDB.Get_Questions();
            #endregion

            #region Software answer
            
            var aPa1 = new clsAnswerMC("PA1", null); aPa1.Id = clsDB.Add_QuestionAnswer(aPa1);
            var aPa2 = new clsAnswerMC("PA2", null); aPa2.Id = clsDB.Add_QuestionAnswer(aPa2);
            var aPa3 = new clsAnswerMC("PA3", null); aPa3.Id = clsDB.Add_QuestionAnswer(aPa3);
            var qc = clsDB.Get_QuestionCategory(1);

            var q1 = new clsQuestionMC()
            {
                Name = "question 1",
                Category = qc,
                CategoryMaximum = 60,
                CategoryMinimum = 30,
                PossibleAnswers = new List<clsAnswerMC>() { aPa1, aPa2, aPa3 }
            };
            q1.Id = clsDB.Add_Question(q1);
            var q2 = new clsQuestionRange()
            {
                Name = "question 2",
                Category = qc,
                CategoryMaximum = 100,
                CategoryMinimum = 0,
                Minimum = 1,
                Maximum = 10
            };
            q2.Id = clsDB.Add_Question(q2);

            var sa1 = new clsSoftwareAnswerMC()
            {
                Name = "sa1",
                Question = q1,
                Answer = aPa1,
            };
            var sa2 = new clsSoftwareAnswerRange()
            {
                Name = "sa2",
                Question = q2,
                Value = 6
            };

            var iSoftware = 1000;

            clsDB.Add_SoftwareAnswers(iSoftware, new List<clsAbstractSoftwareAnswer>() { sa1, sa2 });
            var x = clsDB.Get_SoftwareAnswers(iSoftware);

            sa1 = new clsSoftwareAnswerMC()
            {
                Id = x[0].Id,
                Name = "sa1 edited",
                Question = q1,
                Answer = aPa2
            };
            sa2 = new clsSoftwareAnswerRange()
            {
                Id = x[1].Id,
                Name = "sa2 edited",
                Question = q2,
                Value = 9,
            };

            clsDB.Edit_SoftwareAnswer(sa1);
            clsDB.Edit_SoftwareAnswer(sa2);

            #endregion

        }
        public static void TestQuestion_v0()
        {

            //var oQcScheduling = new clsQuestionCategory() { Id = 1, Name = "Detailed Scheduling",Description= "Detailed Scheduling" };
            //var oQC = new clsQuestionCategory() { Id = 9, Name = "X", Description = "Y" };
            //var x = clsDB.Add_QuestionCategory(oQC);
            //var y = clsDB.Delete_QuestionCategory(oQC);

            //var oQcDispatching = new clsQuestionCategory() { Id = 2, Name = "Dispatching" };
            //var oQcExeMgt = new clsQuestionCategory() { Id = 3, Name = "Execution Management" };
            //var oQcResMgt = new clsQuestionCategory() { Id = 4, Name = "Resource Management" };
            //var oQcDefMgt = new clsQuestionCategory() { Id = 5, Name = "Definition Management" };
            //var oQcDC = new clsQuestionCategory() { Id = 6, Name = "Data Collection" };
            //var oQcAnalysis = new clsQuestionCategory() { Id = 7, Name = "Analysis" };
            //var oQcTracking = new clsQuestionCategory() { Id = 8, Name = "Tracking" };

            //clsDB.Add_QuestionCategory(oQcScheduling);    
            //clsDB.Add_QuestionCategory(oQcDispatching);    
            //clsDB.Add_QuestionCategory(oQcExeMgt);    
            //clsDB.Add_QuestionCategory(oQcResMgt);    
            //clsDB.Add_QuestionCategory(oQcDefMgt);    
            //clsDB.Add_QuestionCategory(oQcDC);    
            //clsDB.Add_QuestionCategory(oQcAnalysis);    
            //clsDB.Add_QuestionCategory(oQcTracking);      

            //var oQcScheduling = clsDB.Get_QuestionCategory(1);

            //var aYes = new clsAnswerMC() { Name = "Yes" };
            //var aNo = new clsAnswerMC() { Name = "No" };
            //aYes.Id = clsDB.Add_QuestionAnswer(aYes);
            //aNo.Id = clsDB.Add_QuestionAnswer(aNo);

            //var q_1_1 = new clsQuestionMC() { Name = "1 1", Id = 11, Description = "1 1", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } };
            //var q_1_5 = new clsQuestionRange() { Name = "1 5", Id = 15, Description = "1 5", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 };



            //int i = clsDB.Add_Question(q_1_5);
            var x = clsDB.Get_Question(1);
            //var y = clsDB.Get_Question(2);s
            var y = clsDB.Get_Question(new clsQuestionMC() { Id = 2 });

        }
   
        public static void TestData_Questions()
        {

            clsReturn r = null;
           
            #region Categories

            r=clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Detailed Scheduling" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Dispatching" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Execution Management" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Resource Management" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Definition Management" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Data Collection" }); Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Analysis" }); Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestionCategory(new clsQuestionCategory() { Name = "Tracking" }); Debug.WriteLine(r.Message);
            var Categories = clsBLL.getQuestionCategories();
            var oQcScheduling = Categories.Where(x => x.Name == "Detailed Scheduling").FirstOrDefault();
            var oQcDispatching = Categories.Where(x => x.Name == "Dispatching").FirstOrDefault();
            var oQcExeMgt = Categories.Where(x => x.Name == "Execution Management").FirstOrDefault();
            var oQcResMgt = Categories.Where(x => x.Name == "Resource Management").FirstOrDefault();
            var oQcDefMgt = Categories.Where(x => x.Name == "Definition Management").FirstOrDefault();
            var oQcDC = Categories.Where(x => x.Name == "Data Collection").FirstOrDefault();
            var oQcAnalysis = Categories.Where(x => x.Name == "Analysis").FirstOrDefault();
            var oQcTracking = Categories.Where(x => x.Name == "Tracking").FirstOrDefault();

            #endregion

            #region Answers

            r = clsBLL.AddQuestionAnswer(new clsAnswerMC() { Name = "Yes"}); Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestionAnswer(new clsAnswerMC() { Name = "No"}); Debug.WriteLine(r.Message);
            var lAnswers = clsBLL.getQuestionAnswers();
            var aYes = lAnswers.Where(x => x.Name == "Yes").FirstOrDefault();
            var aNo = lAnswers.Where(x => x.Name == "No").FirstOrDefault();

            #endregion

            #region Questions

            r=clsBLL.AddQuestion(new clsQuestionMC() { Name = "1 1", Description = "1 1", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r =clsBLL.AddQuestion(new clsQuestionMC() { Name = "1 2", Description = "1 2", Category = oQcScheduling, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "1 3", Description = "1 3", Category = oQcScheduling, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "1 4", Description = "1 4", Category = oQcScheduling, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "1 5", Description = "1 5", Category = oQcScheduling, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "1 6", Description = "1 6", Category = oQcScheduling, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "1 7", Description = "1 7", Category = oQcScheduling, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "1 8", Description = "1 8", Category = oQcScheduling, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "2 1",  Description = "2 1", Category = oQcDispatching, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "2 2",  Description = "2 2", Category = oQcDispatching, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "2 3",  Description = "2 3", Category = oQcDispatching, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "2 4",  Description = "2 4", Category = oQcDispatching, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "2 5",  Description = "2 5", Category = oQcDispatching, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "2 6",  Description = "2 6", Category = oQcDispatching, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "2 7",  Description = "2 7", Category = oQcDispatching, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "2 8",  Description = "2 8", Category = oQcDispatching, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "3 1",  Description = "3 1", Category = oQcExeMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "3 2",  Description = "3 2", Category = oQcExeMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "3 3",  Description = "3 3", Category = oQcExeMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "3 4",  Description = "3 4", Category = oQcExeMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "3 5",  Description = "3 5", Category = oQcExeMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "3 6",  Description = "3 6", Category = oQcExeMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "3 7",  Description = "3 7", Category = oQcExeMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "3 8",  Description = "3 8", Category = oQcExeMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "4 1",  Description = "4 1", Category = oQcResMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "4 2",  Description = "4 2", Category = oQcResMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "4 3",  Description = "4 3", Category = oQcResMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "4 4",  Description = "4 4", Category = oQcResMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "4 5",  Description = "4 5", Category = oQcResMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "4 6",  Description = "4 6", Category = oQcResMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "4 7",  Description = "4 7", Category = oQcResMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "4 8",  Description = "4 8", Category = oQcResMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "5 1",  Description = "5 1", Category = oQcDefMgt, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "5 2",  Description = "5 2", Category = oQcDefMgt, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "5 3",  Description = "5 3", Category = oQcDefMgt, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "5 4",  Description = "5 4", Category = oQcDefMgt, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "5 5",  Description = "5 5", Category = oQcDefMgt, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "5 6",  Description = "5 6", Category = oQcDefMgt, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "5 7",  Description = "5 7", Category = oQcDefMgt, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "5 8",  Description = "5 8", Category = oQcDefMgt, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "6 1",  Description = "6 1", Category = oQcDC, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "6 2",  Description = "6 2", Category = oQcDC, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "6 3",  Description = "6 3", Category = oQcDC, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "6 4",  Description = "6 4", Category = oQcDC, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "6 5",  Description = "6 5", Category = oQcDC, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "6 6",  Description = "6 6", Category = oQcDC, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "6 7",  Description = "6 7", Category = oQcDC, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "6 8",  Description = "6 8", Category = oQcDC, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "7 1",  Description = "7 1", Category = oQcAnalysis, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "7 2",  Description = "7 2", Category = oQcAnalysis, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "7 3",  Description = "7 3", Category = oQcAnalysis, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "7 4",  Description = "7 4", Category = oQcAnalysis, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "7 5",  Description = "7 5", Category = oQcAnalysis, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "7 6",  Description = "7 6", Category = oQcAnalysis, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "7 7",  Description = "7 7", Category = oQcAnalysis, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "7 8",  Description = "7 8", Category = oQcAnalysis, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "8 1",  Description = "8 1", Category = oQcTracking, CategoryMinimum = 10, CategoryMaximum = 25, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "8 2",  Description = "8 2", Category = oQcTracking, CategoryMinimum = 25, CategoryMaximum = 50, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "8 3",  Description = "8 3", Category = oQcTracking, CategoryMinimum = 50, CategoryMaximum = 75, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionMC() { Name = "8 4",  Description = "8 4", Category = oQcTracking, CategoryMinimum = 75, CategoryMaximum = 100, PossibleAnswers = new List<clsAnswerMC>() { aYes, aNo } });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "8 5",  Description = "8 5", Category = oQcTracking, CategoryMinimum = 10, CategoryMaximum = 33, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "8 6",  Description = "8 6", Category = oQcTracking, CategoryMinimum = 33, CategoryMaximum = 67, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "8 7",  Description = "8 7", Category = oQcTracking, CategoryMinimum = 67, CategoryMaximum = 100, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);
            r = clsBLL.AddQuestion(new clsQuestionRange() { Name = "8 8",  Description = "8 8", Category = oQcTracking, CategoryMinimum = 40, CategoryMaximum = 80, Minimum = 0, Maximum = 10 });
            Debug.WriteLine(r.Message);

            var lQuestionData = clsBLL.getQuestions();

            var q_1_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "1 1").FirstOrDefault();
            var q_1_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "1 2").FirstOrDefault();
            var q_1_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "1 3").FirstOrDefault();
            var q_1_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "1 4").FirstOrDefault();
            var q_1_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "1 5").FirstOrDefault();
            var q_1_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "1 6").FirstOrDefault();
            var q_1_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "1 7").FirstOrDefault();
            var q_1_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "1 8").FirstOrDefault();
            var q_2_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "2 1").FirstOrDefault();
            var q_2_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "2 2").FirstOrDefault();
            var q_2_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "2 3").FirstOrDefault();
            var q_2_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "2 4").FirstOrDefault();
            var q_2_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "2 5").FirstOrDefault();
            var q_2_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "2 6").FirstOrDefault();
            var q_2_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "2 7").FirstOrDefault();
            var q_2_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "2 8").FirstOrDefault();
            var q_3_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "3 1").FirstOrDefault();
            var q_3_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "3 2").FirstOrDefault();
            var q_3_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "3 3").FirstOrDefault();
            var q_3_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "3 4").FirstOrDefault();
            var q_3_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "3 5").FirstOrDefault();
            var q_3_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "3 6").FirstOrDefault();
            var q_3_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "3 7").FirstOrDefault();
            var q_3_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "3 8").FirstOrDefault();
            var q_4_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "4 1").FirstOrDefault();
            var q_4_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "4 2").FirstOrDefault();
            var q_4_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "4 3").FirstOrDefault();
            var q_4_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "4 4").FirstOrDefault();
            var q_4_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "4 5").FirstOrDefault();
            var q_4_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "4 6").FirstOrDefault();
            var q_4_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "4 7").FirstOrDefault();
            var q_4_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "4 8").FirstOrDefault();
            var q_5_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "5 1").FirstOrDefault();
            var q_5_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "5 2").FirstOrDefault();
            var q_5_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "5 3").FirstOrDefault();
            var q_5_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "5 4").FirstOrDefault();
            var q_5_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "5 5").FirstOrDefault();
            var q_5_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "5 6").FirstOrDefault();
            var q_5_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "5 7").FirstOrDefault();
            var q_5_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "5 8").FirstOrDefault();
            var q_6_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "6 1").FirstOrDefault();
            var q_6_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "6 2").FirstOrDefault();
            var q_6_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "6 3").FirstOrDefault();
            var q_6_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "6 4").FirstOrDefault();
            var q_6_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "6 5").FirstOrDefault();
            var q_6_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "6 6").FirstOrDefault();
            var q_6_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "6 7").FirstOrDefault();
            var q_6_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "6 8").FirstOrDefault();
            var q_7_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "7 1").FirstOrDefault();
            var q_7_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "7 2").FirstOrDefault();
            var q_7_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "7 3").FirstOrDefault();
            var q_7_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "7 4").FirstOrDefault();
            var q_7_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "7 5").FirstOrDefault();
            var q_7_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "7 6").FirstOrDefault();
            var q_7_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "7 7").FirstOrDefault();
            var q_7_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "7 8").FirstOrDefault();
            var q_8_1 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "8 1").FirstOrDefault();
            var q_8_2 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "8 2").FirstOrDefault();
            var q_8_3 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "8 3").FirstOrDefault();
            var q_8_4 = (clsQuestionMC)lQuestionData.Where(x => x.Name == "8 4").FirstOrDefault();
            var q_8_5 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "8 5").FirstOrDefault();
            var q_8_6 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "8 6").FirstOrDefault();
            var q_8_7 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "8 7").FirstOrDefault();
            var q_8_8 = (clsQuestionRange)lQuestionData.Where(x => x.Name == "8 8").FirstOrDefault();

            #endregion

            #region Software

            string sC = "Questions test company";
            r=clsBLL.addCompany(new clsCompany() { Name = sC }); Debug.WriteLine(r.Message);
            var lCs = ((List<clsBaseModel>)clsBLL.getCompanies(3).Data).Cast<clsCompany>().ToList();
            var oC = lCs.Where(x => x.Name == sC).FirstOrDefault();
            string sA = "Questions test activity";
            r=clsBLL.addActivity(new clsActivity() { Name = sA }); Debug.WriteLine(r.Message);
            var lAs = ((List<clsBaseModel>)clsBLL.getActivities().Data).Cast<clsActivity>().ToList();
            var oA = lAs.Where(x => x.Name == sA).FirstOrDefault();

            string sS1 = "Questions test Software 1";
            r=clsBLL.addSoftware(new clsSoftware() { Name = sS1 }, oC.Id, oA.Id,3); Debug.WriteLine(r.Message);
            var oS1A1_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_1 };
            var oS1A1_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_2 };
            var oS1A1_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_3 };
            var oS1A1_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_4 };
            var oS1A1_5 = new clsSoftwareAnswerRange() { Value = 0, Question = q_1_5 };
            var oS1A1_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_1_6 };
            var oS1A1_7 = new clsSoftwareAnswerRange() { Value = 2, Question = q_1_7 };
            var oS1A1_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_1_8 };
            var oS1A2_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_1 };
            var oS1A2_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_2 };
            var oS1A2_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_3 };
            var oS1A2_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_4 };
            var oS1A2_5 = new clsSoftwareAnswerRange() { Value = 4, Question = q_2_5 };
            var oS1A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS1A2_7 = new clsSoftwareAnswerRange() { Value = 6, Question = q_2_7 };
            var oS1A2_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_2_8 };
            var oS1A3_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_1 };
            var oS1A3_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_2 };
            var oS1A3_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_3 };
            var oS1A3_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_4 };
            var oS1A3_5 = new clsSoftwareAnswerRange() { Value = 8, Question = q_3_5 };
            var oS1A3_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_3_6 };
            var oS1A3_7 = new clsSoftwareAnswerRange() { Value = 10, Question = q_3_7 };
            var oS1A3_8 = new clsSoftwareAnswerRange() { Value = 0, Question = q_3_8 };
            var oS1A4_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_1 };
            var oS1A4_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_2 };
            var oS1A4_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_3 };
            var oS1A4_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_4 };
            var oS1A4_5 = new clsSoftwareAnswerRange() { Value = 1, Question = q_4_5 };
            var oS1A4_6 = new clsSoftwareAnswerRange() { Value = 2, Question = q_4_6 };
            var oS1A4_7 = new clsSoftwareAnswerRange() { Value = 3, Question = q_4_7 };
            var oS1A4_8 = new clsSoftwareAnswerRange() { Value = 4, Question = q_4_8 };
            var oS1A5_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_1 };
            var oS1A5_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_2 };
            var oS1A5_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_3 };
            var oS1A5_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_4 };
            var oS1A5_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_5 };
            var oS1A5_6 = new clsSoftwareAnswerRange() { Value = 6, Question = q_5_6 };
            var oS1A5_7 = new clsSoftwareAnswerRange() { Value = 7, Question = q_5_7 };
            var oS1A5_8 = new clsSoftwareAnswerRange() { Value = 8, Question = q_5_8 };
            var oS1A6_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_1 };
            var oS1A6_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_2 };
            var oS1A6_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_3 };
            var oS1A6_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_4 };
            var oS1A6_5 = new clsSoftwareAnswerRange() { Value = 9, Question = q_6_5 };
            var oS1A6_6 = new clsSoftwareAnswerRange() { Value = 10, Question = q_6_6 };
            var oS1A6_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_6_7 };
            var oS1A6_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_6_8 };
            var oS1A7_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_1 };
            var oS1A7_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_2 };
            var oS1A7_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_3 };
            var oS1A7_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_4 };
            var oS1A7_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_7_5 };
            var oS1A7_6 = new clsSoftwareAnswerRange() { Value = 3, Question = q_7_6 };
            var oS1A7_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_7_7 };
            var oS1A7_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_8 };
            var oS1A8_1 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_1 };
            var oS1A8_2 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_2 };
            var oS1A8_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_3 };
            var oS1A8_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_4 };
            var oS1A8_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_8_5 };
            var oS1A8_6 = new clsSoftwareAnswerRange() { Value = 7, Question = q_8_6 };
            var oS1A8_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_8_7 };
            var oS1A8_8 = new clsSoftwareAnswerRange() { Value = 9, Question = q_8_8 };

            string sS2 = "Questions test Software 2";
            r=clsBLL.addSoftware(new clsSoftware() { Name = sS2 }, oC.Id, oA.Id,3); Debug.WriteLine(r.Message);
            var oS2A1_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_1 };
            var oS2A1_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_2 };
            var oS2A1_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_3 };
            var oS2A1_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_4 };
            var oS2A1_5 = new clsSoftwareAnswerRange() { Value = 10, Question = q_1_5 };
            var oS2A1_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_1_6 };
            var oS2A1_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_1_7 };
            var oS2A1_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_1_8 };
            var oS2A2_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_1 };
            var oS2A2_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_2 };
            var oS2A2_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_3 };
            var oS2A2_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_4 };
            var oS2A2_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_2_5 };
            var oS2A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS2A2_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_2_7 };
            var oS2A2_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_2_8 };
            var oS2A3_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_1 };
            var oS2A3_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_2 };
            var oS2A3_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_3 };
            var oS2A3_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_4 };
            var oS2A3_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_3_5 };
            var oS2A3_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_3_6 };
            var oS2A3_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_3_7 };
            var oS2A3_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_3_8 };
            var oS2A4_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_1 };
            var oS2A4_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_2 };
            var oS2A4_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_3 };
            var oS2A4_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_4 };
            var oS2A4_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_4_5 };
            var oS2A4_6 = new clsSoftwareAnswerRange() { Value = 3, Question = q_4_6 };
            var oS2A4_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_4_7 };
            var oS2A4_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_8 };
            var oS2A5_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_1 };
            var oS2A5_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_2 };
            var oS2A5_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_3 };
            var oS2A5_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_4 };
            var oS2A5_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_5_5 };
            var oS2A5_6 = new clsSoftwareAnswerRange() { Value = 7, Question = q_5_6 };
            var oS2A5_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_5_7 };
            var oS2A5_8 = new clsSoftwareAnswerRange() { Value = 9, Question = q_5_8 };
            var oS2A6_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_1 };
            var oS2A6_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_2 };
            var oS2A6_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_3 };
            var oS2A6_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_4 };
            var oS2A6_5 = new clsSoftwareAnswerRange() { Value = 10, Question = q_6_5 };
            var oS2A6_6 = new clsSoftwareAnswerRange() { Value = 9, Question = q_6_6 };
            var oS2A6_7 = new clsSoftwareAnswerRange() { Value = 8, Question = q_6_7 };
            var oS2A6_8 = new clsSoftwareAnswerRange() { Value = 7, Question = q_6_8 };
            var oS2A7_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_1 };
            var oS2A7_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_2 };
            var oS2A7_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_3 };
            var oS2A7_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_4 };
            var oS2A7_5 = new clsSoftwareAnswerRange() { Value = 6, Question = q_7_5 };
            var oS2A7_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_6 };
            var oS2A7_7 = new clsSoftwareAnswerRange() { Value = 4, Question = q_7_7 };
            var oS2A7_8 = new clsSoftwareAnswerRange() { Value = 3, Question = q_7_8 };
            var oS2A8_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_1 };
            var oS2A8_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_2 };
            var oS2A8_3 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_3 };
            var oS2A8_4 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_4 };
            var oS2A8_5 = new clsSoftwareAnswerRange() { Value = 2, Question = q_8_5 };
            var oS2A8_6 = new clsSoftwareAnswerRange() { Value = 1, Question = q_8_6 };
            var oS2A8_7 = new clsSoftwareAnswerRange() { Value = 0, Question = q_8_7 };
            var oS2A8_8 = new clsSoftwareAnswerRange() { Value = 1, Question = q_8_8 };

            string sS3 = "Questions test Software 3";
            r=clsBLL.addSoftware(new clsSoftware() { Name = sS3 }, oC.Id, oA.Id,3); Debug.WriteLine(r.Message);
            var oS3A1_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_1 };
            var oS3A1_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_2 };
            var oS3A1_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_1_3 };
            var oS3A1_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_1_4 };
            var oS3A1_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_5 };
            var oS3A1_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_6 };
            var oS3A1_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_7 };
            var oS3A1_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_1_8 };
            var oS3A2_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_1 };
            var oS3A2_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_2 };
            var oS3A2_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_2_3 };
            var oS3A2_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_2_4 };
            var oS3A2_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_5 };
            var oS3A2_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_6 };
            var oS3A2_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_7 };
            var oS3A2_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_2_8 };
            var oS3A3_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_1 };
            var oS3A3_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_2 };
            var oS3A3_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_3_3 };
            var oS3A3_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_3_4 };
            var oS3A3_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_5 };
            var oS3A3_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_6 };
            var oS3A3_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_7 };
            var oS3A3_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_3_8 };
            var oS3A4_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_1 };
            var oS3A4_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_2 };
            var oS3A4_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_4_3 };
            var oS3A4_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_4_4 };
            var oS3A4_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_5 };
            var oS3A4_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_6 };
            var oS3A4_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_7 };
            var oS3A4_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_4_8 };
            var oS3A5_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_1 };
            var oS3A5_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_2 };
            var oS3A5_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_5_3 };
            var oS3A5_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_5_4 };
            var oS3A5_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_5 };
            var oS3A5_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_6 };
            var oS3A5_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_7 };
            var oS3A5_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_5_8 };
            var oS3A6_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_1 };
            var oS3A6_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_2 };
            var oS3A6_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_6_3 };
            var oS3A6_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_6_4 };
            var oS3A6_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_5 };
            var oS3A6_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_6 };
            var oS3A6_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_7 };
            var oS3A6_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_6_8 };
            var oS3A7_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_1 };
            var oS3A7_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_2 };
            var oS3A7_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_7_3 };
            var oS3A7_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_7_4 };
            var oS3A7_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_5 };
            var oS3A7_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_6 };
            var oS3A7_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_7 };
            var oS3A7_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_7_8 };
            var oS3A8_1 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_1 };
            var oS3A8_2 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_2 };
            var oS3A8_3 = new clsSoftwareAnswerMC() { Answer = aYes, Question = q_8_3 };
            var oS3A8_4 = new clsSoftwareAnswerMC() { Answer = aNo, Question = q_8_4 };
            var oS3A8_5 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_5 };
            var oS3A8_6 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_6 };
            var oS3A8_7 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_7 };
            var oS3A8_8 = new clsSoftwareAnswerRange() { Value = 5, Question = q_8_8 };

            var lSs = ((List<clsBaseModel>)clsBLL.getSoftwares(3).Data).Cast<clsSoftware>().ToList();
            var oS1 = lSs.Where(x => x.Name == sS1).FirstOrDefault();
            var oS2 = lSs.Where(x => x.Name == sS2).FirstOrDefault();
            var oS3 = lSs.Where(x => x.Name == sS3).FirstOrDefault();

            var oSA1 = new clsSoftwareWithAnswers(oS1)
            {
                Answers = new List<clsAbstractSoftwareAnswer>(){
                    oS1A1_1,
                    oS1A1_2,
                    oS1A1_3,
                    oS1A1_4,
                    oS1A1_5,
                    oS1A1_6,
                    oS1A1_7,
                    oS1A1_8,
                    oS1A2_1,
                    oS1A2_2,
                    oS1A2_3,
                    oS1A2_4,
                    oS1A2_5,
                    oS1A2_6,
                    oS1A2_7,
                    oS1A2_8,
                    oS1A3_1,
                    oS1A3_2,
                    oS1A3_3,
                    oS1A3_4,
                    oS1A3_5,
                    oS1A3_6,
                    oS1A3_7,
                    oS1A3_8,
                    oS1A4_1,
                    oS1A4_2,
                    oS1A4_3,
                    oS1A4_4,
                    oS1A4_5,
                    oS1A4_6,
                    oS1A4_7,
                    oS1A4_8,
                    oS1A5_1,
                    oS1A5_2,
                    oS1A5_3,
                    oS1A5_4,
                    oS1A5_5,
                    oS1A5_6,
                    oS1A5_7,
                    oS1A5_8,
                    oS1A6_1,
                    oS1A6_2,
                    oS1A6_3,
                    oS1A6_4,
                    oS1A6_5,
                    oS1A6_6,
                    oS1A6_7,
                    oS1A6_8,
                    oS1A7_1,
                    oS1A7_2,
                    oS1A7_3,
                    oS1A7_4,
                    oS1A7_5,
                    oS1A7_6,
                    oS1A7_7,
                    oS1A7_8,
                    oS1A8_1,
                    oS1A8_2,
                    oS1A8_3,
                    oS1A8_4,
                    oS1A8_5,
                    oS1A8_6,
                    oS1A8_7,
                    oS1A8_8
                }
            };
            r = clsBLL.AddSoftwareAnswers(oSA1); Debug.WriteLine(r.Message);
            var oSA2 = new clsSoftwareWithAnswers(oS2)
            {
                Answers = new List<clsAbstractSoftwareAnswer>(){
                    oS2A1_1,
                    oS2A1_2,
                    oS2A1_3,
                    oS2A1_4,
                    oS2A1_5,
                    oS2A1_6,
                    oS2A1_7,
                    oS2A1_8,
                    oS2A2_1,
                    oS2A2_2,
                    oS2A2_3,
                    oS2A2_4,
                    oS2A2_5,
                    oS2A2_6,
                    oS2A2_7,
                    oS2A2_8,
                    oS2A3_1,
                    oS2A3_2,
                    oS2A3_3,
                    oS2A3_4,
                    oS2A3_5,
                    oS2A3_6,
                    oS2A3_7,
                    oS2A3_8,
                    oS2A4_1,
                    oS2A4_2,
                    oS2A4_3,
                    oS2A4_4,
                    oS2A4_5,
                    oS2A4_6,
                    oS2A4_7,
                    oS2A4_8,
                    oS2A5_1,
                    oS2A5_2,
                    oS2A5_3,
                    oS2A5_4,
                    oS2A5_5,
                    oS2A5_6,
                    oS2A5_7,
                    oS2A5_8,
                    oS2A6_1,
                    oS2A6_2,
                    oS2A6_3,
                    oS2A6_4,
                    oS2A6_5,
                    oS2A6_6,
                    oS2A6_7,
                    oS2A6_8,
                    oS2A7_1,
                    oS2A7_2,
                    oS2A7_3,
                    oS2A7_4,
                    oS2A7_5,
                    oS2A7_6,
                    oS2A7_7,
                    oS2A7_8,
                    oS2A8_1,
                    oS2A8_2,
                    oS2A8_3,
                    oS2A8_4,
                    oS2A8_5,
                    oS2A8_6,
                    oS2A8_7,
                    oS2A8_8
                }           
            };
            r = clsBLL.AddSoftwareAnswers(oSA2); Debug.WriteLine(r.Message);
            var oSA3 = new clsSoftwareWithAnswers(oS3)
            {
                Answers = new List<clsAbstractSoftwareAnswer>(){
                    oS3A1_1,
                    oS3A1_2,
                    oS3A1_3,
                    oS3A1_4,
                    oS3A1_5,
                    oS3A1_6,
                    oS3A1_7,
                    oS3A1_8,
                    oS3A2_1,
                    oS3A2_2,
                    oS3A2_3,
                    oS3A2_4,
                    oS3A2_5,
                    oS3A2_6,
                    oS3A2_7,
                    oS3A2_8,
                    oS3A3_1,
                    oS3A3_2,
                    oS3A3_3,
                    oS3A3_4,
                    oS3A3_5,
                    oS3A3_6,
                    oS3A3_7,
                    oS3A3_8,
                    oS3A4_1,
                    oS3A4_2,
                    oS3A4_3,
                    oS3A4_4,
                    oS3A4_5,
                    oS3A4_6,
                    oS3A4_7,
                    oS3A4_8,
                    oS3A5_1,
                    oS3A5_2,
                    oS3A5_3,
                    oS3A5_4,
                    oS3A5_5,
                    oS3A5_6,
                    oS3A5_7,
                    oS3A5_8,
                    oS3A6_1,
                    oS3A6_2,
                    oS3A6_3,
                    oS3A6_4,
                    oS3A6_5,
                    oS3A6_6,
                    oS3A6_7,
                    oS3A6_8,
                    oS3A7_1,
                    oS3A7_2,
                    oS3A7_3,
                    oS3A7_4,
                    oS3A7_5,
                    oS3A7_6,
                    oS3A7_7,
                    oS3A7_8,
                    oS3A8_1,
                    oS3A8_2,
                    oS3A8_3,
                    oS3A8_4,
                    oS3A8_5,
                    oS3A8_6,
                    oS3A8_7,
                    oS3A8_8
                }
            };
            r = clsBLL.AddSoftwareAnswers(oSA3); Debug.WriteLine(r.Message);
            #endregion

        }
    
    }
}
