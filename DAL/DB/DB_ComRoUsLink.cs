﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iCompany">company Id, if their is no company to link, give null to this parameter</param>
        /// <param name="sRole"></param>
        /// <param name="sUser"></param>
        /// <returns></returns>
        public static int Add_ComRoUsLink(int? iCompany, int iRole, string sUser)
        {
            Check_UserId(sUser); Check_RoleId(iRole);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                //if (!Check_CompanyRoleUser_Exist(iCompany, sRole, sUser)) { return 0; }
                //if (!Check_CompanyRoleUser_NotLinked(iCompany, sRole, sUser)) { return 0; }

                oCnn.Open();

                string sSQl = @"insert into AspNetUserRoles (crulCompany,UserId,RoleId)
                values(@Company, @User, @Role);
                select crulID from AspNetUserRoles where UserId = @User and RoleId = @Role";
                if (iCompany != null) { sSQl += " and crulCompany = @Company"; }
                sSQl += " order by crulID desc; ";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);             
                oCmd.Parameters.Add(new SqlParameter("@Company", (iCompany != null) ? iCompany : (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@User", sUser));
                oCmd.Parameters.Add(new SqlParameter("@Role", iRole.ToString()));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="iCompany"></param>
        /// <param name="sRole"></param>
        /// <param name="sUser"></param>
        /// <returns></returns>
        public static int Edit_ComRoUsLink(int iId, int? iCompany, int iRole, string sUser)
        {
            Check_ID(iId); Check_RoleUser_Null(iRole, sUser);Check_ComRoUs(iCompany, iRole, sUser);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {  
                //if (!Check_CompanyRoleUser_Exist2(iCompany, sRole, sUser)) { return false; }         
                //if (!Check_CompanyRoleUser_Id(iId)) { return false; }

                oCnn.Open();
                string sSQl = "update AspNetUserRoles set ";

                object oC= 0;
                switch(iCompany)
                {
                    case null:
                        oC = DBNull.Value;
                        sSQl += " crulCompany = @Company ";
                        break;
                    case 0:
                        break;
                    default:
                        oC = iCompany;
                        sSQl += " crulCompany = @Company ";
                        break;
                }

                if ((iCompany != 0) && ((iRole != 0) || (sUser != null && sUser != ""))) { sSQl += ","; }

                object oR = 0;
                switch (iRole)
                {
                    case 0:
                        break;
                    default:
                        oR = iRole;
                        sSQl += " RoleId = @Role";
                        break;
                }
                if (iRole != 0 && (sUser != null && sUser != "")) { sSQl += ","; }

                object oU = "";
                switch (sUser)
                {
                    case null:
                        // de User kan niet dbnull zijn!
                        break;
                    case "":
                        break;
                    default:
                        oU = sUser;
                        sSQl += " UserId = @User";
                        break;
                }

                sSQl +=  " where crulId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@Company", oC));
                oCmd.Parameters.Add(new SqlParameter("@User", oU));
                oCmd.Parameters.Add(new SqlParameter("@Role", oR.ToString()));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_ComRoUsLink(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CompanyRoleUser_Id(iId)) { return false; }

                oCnn.Open();

                string sSQl = @"delete from AspNetUserRoles where crulId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// oLinks = null --> de volledige tabel terug krijgen |
        /// Per lijst heb je verschillende mogelijkheden, 
        /// ofwel is de lijst null --> er wordt geen rekening gehouden met deze variabelen,
        /// ofwel bevat de lijst 0 or "" --> deze parameter moet DBNull zijn,
        /// ofwel bevat de lijst een aantal  ints/strings --> de parameter moet tot een van deze waarden horen
        /// </summary>
        /// <param name="oLinks"></param>
        /// <returns></returns>
        public static List<clsLink> Get_ComRoUsLink(clsLinks oLinks = null)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "SELECT crulID,crulCompany,RoleId,UserId FROM AspNetUserRoles";

                // opstellen where clausule
                if (oLinks != null)
                {
                    List<int> oCompany = oLinks.Company;
                    List<int> oRoles = oLinks.Role;
                    List<string> oUsers = oLinks.User;

                    if ((oCompany != null && oCompany.Count > 0) || (oRoles != null && oRoles.Count > 0) || (oUsers != null && oUsers.Count > 0)) { sSQL += " where "; }
                
                    if (oCompany != null && oCompany.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oCompany.Contains(0))
                        {
                            // een Company kan DBNULL zijn!
                            sSQL += " (crulCompany is null ";
                            oCompany.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oCompany.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " crulCompany  in (";
                            for (int i = 0; i < oCompany.Count; i++)
                            {
                                if (i != oCompany.Count - 1) { sSQL += "@CompanyPara" + i + ","; }
                                else { sSQL += "@CompanyPara" + i; }
                            }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                        if ((oRoles != null && oRoles.Count > 0) || (oUsers != null && oUsers.Count > 0)) { sSQL += " and "; }
                    }

                    if (oRoles != null && oRoles.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oRoles.Contains(0)) { throw new Exception("Input parameter is wrong! Role Id can't be DBNULL!"); } // een role kan niet DBNULL zijn!                     
                        sSQL += " RoleId  in (";

                        for (int i = 0; i < oRoles.Count; i++)
                        {
                            if (i != oRoles.Count - 1) { sSQL += "@RolePara" + i + ","; }
                            else { sSQL += "@RolePara" + i; }
                        }

                        sSQL += ") ";

                        if (oUsers != null && oUsers.Count > 0) { sSQL += " and "; }
                    }

                    if (oUsers != null && oUsers.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oUsers.Contains("") || oUsers.Contains(null)) { throw new Exception("Input parameter is wrong! User Id can't be DBNULL!"); } // een User kan niet DBNULL zijn!                     
                        sSQL += " UserId  in (";

                        for (int i = 0; i < oUsers.Count; i++)
                        {
                            if (i != oUsers.Count - 1) { sSQL += "@UserPara" + i + ","; }
                            else { sSQL += "@UserPara" + i; }
                        }

                        sSQL += ") ";

                    }

                }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                if (oLinks != null)
                {
                    List<int> oCompany = oLinks.Company;
                    List<int> oRoles = oLinks.Role;
                    List<string> oUsers = oLinks.User;

                    if (oCompany != null)
                    {
                        for (int i = 0; i < oCompany.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@CompanyPara" + i, oCompany[i])); }
                    }

                    if (oRoles != null)
                    {
                        for (int i = 0; i < oRoles.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@RolePara" + i, oRoles[i])); }
                    }

                    if (oUsers != null)
                    {
                        for (int i = 0; i < oUsers.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@UserPara" + i, oUsers[i])); }
                    }

                }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0) { return new List<clsLink>(); }

                var oList = new List<clsLink>();

                foreach (DataRow r in oDT.Rows)
                {
                    var oLink = new clsLink();
                    if (r[0] != DBNull.Value) { oLink.Id = (int)r[0]; }
                    if (r[1] != DBNull.Value) { oLink.Company = (int)r[1]; }
                    if (r[2] != DBNull.Value) { int i = 0;  int.TryParse(r[2].ToString(), out i); oLink.Role = i; }
                    if (r[3] != DBNull.Value) { oLink.User= (string)r[3]; }
                    oList.Add(oLink);
                }
                return oList;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }

        private static Boolean Check_CompanyRoleUser_Exist(int iCompany, string sRole, string sUser)
        {
            if (sRole == "" || sRole == null || sUser == "" || sUser == null) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select Id from AspNetUsers where Id = @User;
                                select Id from AspNetRoles where Id = @Role;";

                if (iCompany != 0) { sSQl += " select comId from Company where comId = @Company;";; }

                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", (iCompany != 0) ? iCompany : (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@Role", sRole));
                oCmd.Parameters.Add(new SqlParameter("@User", sUser));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyRoleUser_Exist2(int? iCompany, string sRole, string sUser)
        {
            if (sRole == null || sUser == null) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "";
                    
                if (sUser != "") { sSQl += "select Id from AspNetUsers where Id = @User;"; }  
                if (sRole != "") { sSQl += "select Id from AspNetRoles where Id = @Role;"; }   
                if (iCompany != 0 && iCompany != null) { sSQl += " select comId from Company where comId = @Company;"; ; }

                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                if (iCompany != 0 && iCompany!= null) oCmd.Parameters.Add(new SqlParameter("@Company", iCompany));
                oCmd.Parameters.Add(new SqlParameter("@Role", sRole));
                oCmd.Parameters.Add(new SqlParameter("@User", sUser));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyRoleUser_NotLinked(int iCompany, string sRole, string sUser)
        {
            if (sRole == "" || sRole == null || sUser == "" || sUser == null) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select crulId from AspNetUserRoles
                    where RoleId = @Role and UserId = @User";
                if (iCompany != 0) {sSQl += " and crulCompany = @Company"; }
                    
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", (iCompany != 0) ? iCompany : (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@Role", sRole));
                oCmd.Parameters.Add(new SqlParameter("@User", sUser));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return true; }

                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyRoleUser_Id(int iId)
        {
            if (iId == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select crulId from AspnetUserRoles where crulId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static void Check_RoleUser_Null(int iRole, string sUser)
        {
            if (iRole is 0) { throw new Exception("Input parameter is wrong! Role Id is 0!"); }
            if (sUser is null) { throw new Exception("Input parameter is wrong! User Id is null!"); }
        }
        private static void Check_ComRoUs(int? iCompany, int iRole, string sUser)
        {
            if (iCompany == 0 && iRole == 0 && sUser == "") { throw new Exception("Input parameters are wrong! Nothing to change!"); } // er moet niets aangepast worden
        }
    }
}
