﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        public static void CreateFullDb()
        {
            CreateCompany();
            CreateActivity();
            CreateCategory();
            CreateSector();
            CreateFunction();
            CreateMesSoftware();
            CreateCase();
            CreateMeCatFuLink();
            CreateCasCatFuLink();
            CreateComMeCasSeAcLink();
        }


        public static void CreateCompany()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"USE[MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Company]    Script Date: 29/10/2019 11:02:12 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE[dbo].[Company](

                                    [comId][int] IDENTITY(1, 1) NOT NULL,

                                    [comName] [varchar] (max) NOT NULL,
	                                [comDescription]
                                        [varchar]
                                        (max) NULL,

                                    [comWebsite] [varchar]
                                        (max) NULL,

                                    [comAdress] [varchar]
                                        (max) NULL,

                                    [comContactEmail] [varchar]
                                        (max) NULL,

                                    [comContactTelefoon] [varchar]
                                        (max) NULL,

                                    [comContent] [nvarchar]
                                        (max) NULL,

                                    [comPublic] [int] NULL,
	                                [comLogoUrl]
                                        [varchar]
                                        (max) NULL,
                                 CONSTRAINT[PK__Company__9052B5562F6D9AD6] PRIMARY KEY CLUSTERED
                                (
                                   [comId] ASC
                                )WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
                                ) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateActivity()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Activity]    Script Date: 29/10/2019 11:12:06 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[Activity](
	                                [acId] [int] IDENTITY(1,1) NOT NULL,
	                                [acName] [varchar](max) NOT NULL,
	                                [acDescription] [varchar](max) NULL,
                                PRIMARY KEY CLUSTERED 
                                (
	                                [acId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateCategory()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Category]    Script Date: 29/10/2019 11:13:21 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[Category](
	                                [catId] [int] IDENTITY(1,1) NOT NULL,
	                                [catName] [varchar](max) NOT NULL,
	                                [catDescription] [varchar](max) NULL,
                                 CONSTRAINT [PK__Category__17B6DD0600B23649] PRIMARY KEY CLUSTERED 
                                (
	                                [catId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateSector()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Sector]    Script Date: 29/10/2019 11:16:08 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[Sector](
	                                [seId] [int] IDENTITY(1,1) NOT NULL,
	                                [seName] [varchar](max) NOT NULL,
	                                [seDescription] [varchar](max) NULL,
                                 CONSTRAINT [PK__Sector__2C89FEAF81344FA6] PRIMARY KEY CLUSTERED 
                                (
	                                [seId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateFunction()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Functions]    Script Date: 29/10/2019 11:14:12 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[Functions](
	                                [fuId] [int] IDENTITY(1,1) NOT NULL,
	                                [fuName] [varchar](max) NOT NULL,
	                                [fuDescription] [varchar](max) NULL,
                                PRIMARY KEY CLUSTERED 
                                (
	                                [fuId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateMesSoftware()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[MesSoftware]    Script Date: 29/10/2019 11:15:33 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[MesSoftware](
	                                [meId] [int] IDENTITY(1,1) NOT NULL,
	                                [meName] [varchar](max) NOT NULL,
	                                [meDescription] [varchar](max) NULL,
	                                [meWebsite] [varchar](max) NULL,
	                                [meContent] [nvarchar](max) NULL,
	                                [mePublic] [int] NULL,
	                                [meLogoUrl] [varchar](max) NULL,
                                 CONSTRAINT [PK__MesSoftw__7D0E9F4B1C96D05B] PRIMARY KEY CLUSTERED 
                                (
	                                [meId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateCase()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[Cases]    Script Date: 29/10/2019 11:12:58 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[Cases](
	                                [casId] [int] IDENTITY(1,1) NOT NULL,
	                                [casName] [varchar](max) NOT NULL,
	                                [casDescription] [varchar](max) NULL,
	                                [casWebsite] [varchar](max) NULL,
	                                [casContent] [nvarchar](max) NULL,
	                                [casPublic] [int] NULL,
	                                [casLogoUrl] [varchar](max) NULL,
                                PRIMARY KEY CLUSTERED 
                                (
	                                [casId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateMeCatFuLink()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[MeCatFuLink]    Script Date: 29/10/2019 11:14:41 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[MeCatFuLink](
	                                [mcflId] [int] IDENTITY(1,1) NOT NULL,
	                                [mcflMes] [int] NOT NULL,
	                                [mcflCategory] [int] NOT NULL,
	                                [mcflFunction] [int] NOT NULL,
                                PRIMARY KEY CLUSTERED 
                                (
	                                [mcflId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY]
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_Category] FOREIGN KEY([mcflCategory])
                                REFERENCES [dbo].[Category] ([catId])
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_Category]
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_Functions] FOREIGN KEY([mcflFunction])
                                REFERENCES [dbo].[Functions] ([fuId])
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_Functions]
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_MeCatFuLink_MesSoftware] FOREIGN KEY([mcflMes])
                                REFERENCES [dbo].[MesSoftware] ([meId])
                                GO

                                ALTER TABLE [dbo].[MeCatFuLink] CHECK CONSTRAINT [FK_MeCatFuLink_MesSoftware]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateCasCatFuLink()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[CasCatFuLink]    Script Date: 29/10/2019 11:12:34 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[CasCatFuLink](
	                                [ccflId] [int] IDENTITY(1,1) NOT NULL,
	                                [ccflCase] [int] NOT NULL,
	                                [ccflCategory] [int] NOT NULL,
	                                [ccflFunction] [int] NOT NULL,
                                PRIMARY KEY CLUSTERED 
                                (
	                                [ccflId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY]
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Cases] FOREIGN KEY([ccflCase])
                                REFERENCES [dbo].[Cases] ([casId])
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Cases]
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Category] FOREIGN KEY([ccflCategory])
                                REFERENCES [dbo].[Category] ([catId])
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Category]
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink]  WITH CHECK ADD  CONSTRAINT [FK_CasCatFuLink_Functions] FOREIGN KEY([ccflFunction])
                                REFERENCES [dbo].[Functions] ([fuId])
                                GO

                                ALTER TABLE [dbo].[CasCatFuLink] CHECK CONSTRAINT [FK_CasCatFuLink_Functions]
                                GO

                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void CreateComMeCasSeAcLink()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                USE [MES4SME_Selector]
                                GO

                                /****** Object:  Table [dbo].[ComMeCasSeAcLink]    Script Date: 29/10/2019 11:13:42 ******/
                                SET ANSI_NULLS ON
                                GO

                                SET QUOTED_IDENTIFIER ON
                                GO

                                CREATE TABLE [dbo].[ComMeCasSeAcLink](
	                                [cmcsalId] [int] IDENTITY(1,1) NOT NULL,
	                                [cmcsalCompany] [int] NOT NULL,
	                                [cmcsalActivity] [int] NOT NULL,
	                                [cmcsalMesSoftware] [int] NOT NULL,
	                                [cmcsalSector] [int] NULL,
	                                [cmcsalCase] [int] NULL,
                                 CONSTRAINT [PK__ComMeCas__A0DFFED7A2EE75B8] PRIMARY KEY CLUSTERED 
                                (
	                                [cmcsalId] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY]
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Activity] FOREIGN KEY([cmcsalActivity])
                                REFERENCES [dbo].[Activity] ([acId])
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Activity]
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Cases] FOREIGN KEY([cmcsalCase])
                                REFERENCES [dbo].[Cases] ([casId])
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Cases]
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Company] FOREIGN KEY([cmcsalCompany])
                                REFERENCES [dbo].[Company] ([comId])
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Company]
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_MesSoftware] FOREIGN KEY([cmcsalMesSoftware])
                                REFERENCES [dbo].[MesSoftware] ([meId])
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_MesSoftware]
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink]  WITH CHECK ADD  CONSTRAINT [FK_ComMeCasSeAcLink_Sector] FOREIGN KEY([cmcsalSector])
                                REFERENCES [dbo].[Sector] ([seId])
                                GO

                                ALTER TABLE [dbo].[ComMeCasSeAcLink] CHECK CONSTRAINT [FK_ComMeCasSeAcLink_Sector]
                                GO



                                ";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }


    }
}
