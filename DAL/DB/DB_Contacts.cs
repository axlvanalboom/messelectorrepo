﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        public static clsContacts GetContacts()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "select * from Contacts;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsContact> oTo = new List<clsContact>();
                List<clsContact> oCc = new List<clsContact>();
                List<clsContact> oBcc = new List<clsContact>();
                foreach (DataRow r in oDT.Rows)
                {
                    clsContact oContact = new clsContact();
                    oContact.Id = (int)r[0];
                    oContact.Email = (string)r[1];
                    string sVerdeling = (string)r[2];
                    oContact.Verdeling = sVerdeling;
                    switch (sVerdeling)
                    {
                        case "To":
                            oTo.Add(oContact);
                            break;
                        case "Cc":
                            oCc.Add(oContact);
                            break;
                        case "Bcc":
                            oBcc.Add(oContact);
                            break;
                        default:
                            throw new Exception();
                    }
                }
                clsContacts oContacts = new clsContacts();
                oContacts.To = oTo;
                oContacts.Cc = oCc;
                oContacts.Bcc = oBcc;

                return oContacts;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }


        }
        public static int AddContact(clsContact oContact)
        {
            if (oContact == null) { throw new Exception("The contact can't be null!!!"); }
            int iID = oContact.Id; string sEmail = oContact.Email; string sVerdeling = oContact.Verdeling;
            Check_ContactsVerdeling(sVerdeling); Check_ContactsEmail(sEmail);

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "insert into Contacts (contactEmail,contactVerdeling) values (@Email,@Verdeling); " +
                    "select top(1) contactId from Contacts where contactEmail = @Email and contactVerdeling = @Verdeling order by contactId desc;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                oCmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                oCmd.Parameters.Add(new SqlParameter("@Verdeling", sVerdeling));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }
        public static int EditContact(clsContact oContact)
        {
            if (oContact == null) { throw new Exception("The contact can't be null!!!"); }
            int iID = oContact.Id; string sEmail = oContact.Email; string sVerdeling = oContact.Verdeling;
            Check_ID(iID); Check_ContactsVerdeling(sVerdeling); Check_ContactsEmail(sEmail);

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Contacts set ";
                sSQL += "  contactEmail = @Email, ";
                sSQL += "  contactVerdeling = @Verdeling ";
                sSQL += "  where contactId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                oCmd.Parameters.Add(new SqlParameter("@Verdeling", sVerdeling));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }
        public static int DeleteContact(clsContact oContact)
        {
            if (oContact == null) { throw new Exception("The contact can't be null!!!"); }
            int iID = oContact.Id; Check_ID(iID);

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select contactVerdeling from Contacts where contactId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                string sVerdeling = (string)oCmd.ExecuteScalar();

                if (sVerdeling == "To")
                {
                    clsContacts oContacts = GetContacts();
                    if (oContacts.To.Count <=1) { throw new Exception("Onmogelijk om de laatste contact met de verdeling 'To' te verwijderen!"); }

                }

                sSQL = "delete from Contacts where contactId = @ID;";
                oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static void Check_ContactsVerdeling(string sVerdeling)
        {
            var o = new List<string>() { "To", "Cc", "Bcc" };
            if (!o.Contains(sVerdeling)) { throw new Exception("De verdeling is mis! Dit kan enkel ingevuld worden op To, Cc of Bcc!!!"); } 
        }
        private static void Check_ContactsEmail(string sEmail)
        {
            if (sEmail ==null || sEmail == "") { throw new Exception("De email is mis! Dit kan niet leeg zijn!!!"); }
            if (!sEmail.Contains("@")) { throw new Exception("De email is mis!"); }
        }

    }
}
