﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {        

        //updated 11/08 --> to be tested

        public static int Add_Sector(clsBaseModel x)
        {
            return Add<clsSector>(x, addSector);
        }
        public static int Add_Sector(string sSector, string sDescription = null)
        {
            return Add<clsSector>(sSector, sDescription, addSector);
        }
        private static int addSector(SqlConnection oCnn, string sName, string sDescription = null)
        {
            string sSQl = @"insert into Sector (seName,seDescription)
            values(@seName, @seDescription);
            select seID from Sector where seName = @seName;";
            SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@seName", sName));
            oCmd.Parameters.Add(new SqlParameter("@seDescription", sDescription ?? (object)DBNull.Value));

            int i = (int)oCmd.ExecuteScalar();

            return i;
        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="oSector"></param>
        /// <returns></returns>
        public static int Edit_Sector(clsBaseModel oSector)
        {
            Check_Class(oSector);
            if (oSector.GetType() == typeof(clsSector))
            {
                clsSector oS = (clsSector)oSector;
                return Edit_Sector(oS.Id, oS.Name, oS.Description);
            }
            else { WrongModelType(); return 0; }           
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <returns></returns>
        public static int Edit_Sector(int iID, string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName, sDescription);

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "update Sector set ";
                object oN = "";
                if (sName != "")
                {
                    sSQL += " seName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " seDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where seId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }


 
        }
   
        public static clsBaseModel Get_Sector(clsBaseModel x)
        {
            return Get<clsBaseModel>(x, getSector);
        }
        public static clsBaseModel Get_Sector(int iID)
        {
            return Get<clsBaseModel>(iID, getSector);
        }
        private static clsBaseModel getSector(SqlConnection oCnn, int iID)
        {
            string sSQl = @"select seId,seName,seDescription
                            from Sector where seId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
                return Sector(oDT.Rows[0]);          
            else { return null; }
        }

        public static List<clsBaseModel> Get_Sectors()
        {
            return Get(getSectors);
        }
        private static List<clsBaseModel> getSectors(SqlConnection oCnn)
        {
            string sSQl = @"select seId,seName,seDescription
                            from Sector order by seName asc;";
            SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            List<clsBaseModel> loResult = new List<clsBaseModel>();

            if (oDT.Rows.Count > 0)
                foreach (DataRow oRow in oDT.Rows)
                    loResult.Add(Sector(oRow));
            
            return loResult;
        }

        private static clsSector Sector(DataRow oRow)
        {
            var ID = (int)oRow[0];
            string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
            string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
            return new clsSector(ID, sName, sDescription);
        }

        public static int Delete_Sector(clsBaseModel x)
        {
            return Delete<clsSector>(x, deleteSector);
        }
        public static int Delete_Sector(int iID)
        {
            return Delete(iID, deleteSector);
        }
        private static int deleteSector(SqlConnection oCnn, int iID)
        {
            string sSQl = @"delete from Sector where seId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            return oCmd.ExecuteNonQuery();
        }

    }
}