﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        
        public static int Add_Category(clsBaseModel oCategory)
        {
            Check_Class(oCategory);
            if (oCategory.GetType() == typeof(clsCategory))
            {
                clsCategory oC = (clsCategory)oCategory;
                return Add_Category(oC.Name, oC.Description);
            }
            else { WrongModelType(); return 0; }        
        }
        public static int Add_Category(string sCategory, string sDescription = null)
        {
            Check_Name(sCategory);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"insert into Category (catName,catDescription)
                values(@catName, @catDescription);
                select catID from Category where catName = @catName;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@catName", sCategory));
                oCmd.Parameters.Add(new SqlParameter("@catDescription", sDescription ?? (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Edit_Category(clsBaseModel oCategory)
        {
            Check_Class(oCategory);
            if (oCategory.GetType() == typeof(clsCategory))
            {
                clsCategory oC = (clsCategory)oCategory;
                return Edit_Category(oC.Id, oC.Name, oC.Description);
            }
            else { WrongModelType(); return 0; }
            }
        public static int Edit_Category(int iID, string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName,sDescription);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "update Category set ";

                object oN = "";
                if (sName != "")
                {
                    sSQL += " catName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " catDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where catId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static clsBaseModel Get_Category(clsBaseModel oCategory)
        {
            Check_Class(oCategory);
            if (oCategory.GetType() == typeof(clsCategory))
            {
                clsCategory oC = (clsCategory)oCategory;
                return Get_Category(oC.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_Category(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select catId,catName,catDescription
                                from Category where catId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsCategory oCategory;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    oCategory = new clsCategory(ID, sName, sDescription);
                    return oCategory;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static List<clsBaseModel> Get_Categories()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select catId,catName,catDescription
                                from Category order by catName asc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsCategory oCategory;
                List<clsBaseModel> oCategories;
                oCategories = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];

                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        oCategory = new clsCategory(ID, sName, sDescription);

                        oCategories.Add(oCategory);
                    }

                    return oCategories;
                }
                else { return new List<clsBaseModel>(); ; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Delete_Category(clsBaseModel oCategory)
        {
            Check_Class(oCategory);
            if (oCategory.GetType() == typeof(clsCategory))
            {
                clsCategory oC = (clsCategory)oCategory;
                return Delete_Category(oC.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Category(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"delete from Category where catId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
    }
}