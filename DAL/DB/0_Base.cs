﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        private static int Add<T>(clsBaseModel X, Func<SqlConnection, string,string, int> f) where T : clsBaseModel
        {
            Check_Class(X);
            if (X.GetType() == typeof(T))
                return Add<T>(X.Name, X.Description, f);      
            else { WrongModelType(); return 0; }
        }
        private static int Add<T>(clsBaseModel X, Func<SqlConnection, T, int> f, bool xCheckName=true) where T : clsBaseModel
        { 
            Check_Class(X);
            if (X.GetType() == typeof(T))
            {
                if (xCheckName)
                    Check_Name(X.Name);
                SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
                try
                {
                    oCnn.Open();
                    var Y = (T)X;
                    return f(oCnn, Y);
                }
                catch (Exception ex) { throw ex; }
                finally { oCnn.Close(); }
            }
            else { WrongModelType(); return 0; }
        }
        private static int Add<T>(string sName, string sDescription, Func<SqlConnection, string, string, int> f) where T : clsBaseModel
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                return f(oCnn, sName, sDescription);
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static T Get<T>(clsBaseModel X, Func<SqlConnection, int, T> f) where T : clsBaseModel
        {
            Check_Class(X);
            if (X.GetType() == typeof(T))
                return Get<T>(X.Id, f);       
            else { WrongModelType(); return null; }
        }
        private static T Get<T>(int id, Func<SqlConnection, int, T> f)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                return f(oCnn, id);
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static List<clsBaseModel> Get(Func<SqlConnection, List<clsBaseModel>> f)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                return f(oCnn);
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static int Edit<T>(clsBaseModel X, Func<int,string,string, int> f) where T : clsBaseModel
        {
            Check_Class(X);
            if (X.GetType() == typeof(T))
                return f(X.Id, X.Name, X.Description);  
            else { WrongModelType(); return 0; }
        }

        //private static int Delete<T>(clsBaseModel X, Func<int, int> f)
        //{
        //    Check_Class(X);
        //    if (X.GetType() == typeof(T))
        //        return f(X.Id);
           
        //    else { WrongModelType(); return 0; }
        //}
        private static int Delete<T>(clsBaseModel X, Func<SqlConnection, int, int> f)
        {
            Check_Class(X);
            if (X.GetType() == typeof(T))
            {
                return Delete(X.Id, f);
            }          
            else { WrongModelType(); return 0; }
        }
        private static int Delete(int id, Func<SqlConnection, int, int> f)
        {
            Check_ID(id);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                return f(oCnn, id);
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

    }
}
