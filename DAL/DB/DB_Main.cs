﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        //todo: algemene functies voor de base models maken, eventueel ook voor de advanced models --> naam van tabel meegeven, prefix meegeven!

        private static void Check_ID(int iId)
        {
            if (iId == 0) { throw new Exception("Input parameter is wrong! Id is 0!"); }
        }
        private static void Check_Name(string sName)
        {
            if (sName is null) { throw new Exception("Input parameter is wrong! Name is null!"); }
            if (sName == "") { throw new Exception("Input parameter is wrong! Name is empty!"); }
        }
        private static void Check_Class(object oClass)
        {
            if (oClass is null) { throw new Exception("Input parameter is wrong! The class is null!"); }
        }
        private static void Check_CategoryFunction(int iCategory, int iFunction)
        {
            if (iCategory == 0) { throw new Exception("Input parameter is wrong! Category is 0!"); }
            if (iFunction == 0) { throw new Exception("Input parameter is wrong! Function is 0!"); }
        }
        private static void Check_Mes(int iMes)
        {
            if (iMes == 0) { throw new Exception("Input parameter is wrong! Mes is 0!"); }
        }
        private static void Check_Case(int iCase)
        {
            if (iCase == 0) { throw new Exception("Input parameter is wrong! Case is 0!"); }
        }

        private static void Check_UserId(string sUserId)
        {
            if (sUserId is null) { throw new Exception("Input parameter is wrong! User Id is null!"); }
            if (sUserId == "") { throw new Exception("Input parameter is wrong! User Id is empty!"); }
        }
        private static void Check_RoleId(int iRoleId)
        {
            if (iRoleId ==0) { throw new Exception("Input parameter is wrong! Role Id is 0!"); }
        }

        private static void WrongModelType()
        {
            throw new Exception("Wrong model type!");
        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        private static void Check_BaseModel_Edit(string sName, string sDescription)
        {
            if (sName == "" && sDescription == "") { throw new Exception("Input parameters are wrong! Nothing to change!"); }
            if (sName == null) { throw new Exception("Input parameter is wrong! Name can't be DBNULL!"); }
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        private static void Check_AdvancedBaseModel_Edit(string sName, string sDescription = null, string sWebsite = null, string sLogoUrl = null)
        {
            if (sName == "" && sDescription == "" && sWebsite == ""  && sLogoUrl == "") { throw new Exception("Input parameters are wrong! Nothing to change!"); }
            if (sName == null) { throw new Exception("Input parameter is wrong! Name can't be DBNULL!"); }
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <param name="sWebsite"></param>
        /// <param name="sAdress"></param>
        /// <param name="sContactEmail"></param>
        /// <param name="sContactTelephone"></param>
        /// <param name="sLogoUrl"></param>
        private static void Check_Company_Edit(string sName, string sDescription, string sWebsite , string sAdress , string sContactEmail , string sContactTelephone, string sLogoUrl)       
        {
            if (sName == "" && sDescription == "" && sWebsite =="" && sAdress =="" && sContactEmail =="" && sContactTelephone =="" && sLogoUrl =="")
                throw new Exception("Input parameters are wrong! Nothing to change!"); 
            if (sName == null) { throw new Exception("Input parameter is wrong! Name can't be DBNULL!"); }
        }

    }
}
