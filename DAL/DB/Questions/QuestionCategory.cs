﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        public static int Add_QuestionCategory(clsBaseModel x)
        {
            return Add<clsQuestionCategory>(x, addQuestionCategory);
        }
        public static int Add_QuestionCategory(string sName, string sDescription = null)
        {
            return Add<clsQuestionCategory>(sName, sDescription, addQuestionCategory);
        }
        private static int addQuestionCategory(SqlConnection oCnn, string sName, string sDescription = null)
        {
            string sSQL = @"insert into QuestionCategory (qcName,qcDescription)
                values(@xName, @xDescription);
                select qcID from QuestionCategory where qcName = @xName;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@xName", sName));
            oCmd.Parameters.Add(new SqlParameter("@xDescription", sDescription ?? (object)DBNull.Value));

            return (int)oCmd.ExecuteScalar();
        }
        
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="iMes"></param>
        /// <param name="iCategory"></param>
        /// <param name="iFunction"></param>
        /// <returns></returns>
        public static int Edit_QuestionCategory(clsBaseModel x)
        {
            return Edit<clsQuestionCategory>(x, Edit_QuestionCategory);
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <returns></returns>
        public static int Edit_QuestionCategory(int iID, string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName, sDescription);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update QuestionCategory set ";

                object oN = "";
                if (sName != "")
                {
                    sSQL += " qcName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " qcDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where qcId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }


        public static clsQuestionCategory Get_QuestionCategory(clsBaseModel x)
        {
            return Get<clsQuestionCategory>(x, getQuestionCategory);
        }
        public static clsQuestionCategory Get_QuestionCategory(int iID)
        {
            return Get<clsQuestionCategory>(iID, getQuestionCategory);
        }
        private static clsQuestionCategory getQuestionCategory(SqlConnection oCnn, int iID)
        {
            string sSQL = @"select qcId,qcName,qcDescription
                                from QuestionCategory where qcId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
            {
                var oRow = oDT.Rows[0];
                var ID = (int)oRow[0];

                string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                return new clsQuestionCategory(ID, sName, sDescription);
            }
            else { return null; }
        }
        
        public static List<clsBaseModel> Get_QuestionCategories()
        {
            return Get(getQuestionCategories);
        }
        private static List<clsBaseModel> getQuestionCategories(SqlConnection oCnn)
        {
            string sSQL = @"select qcId,qcName,qcDescription
                            from QuestionCategory order by qcId asc;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            List<clsBaseModel> loResult = new List<clsBaseModel>();

            if (oDT.Rows.Count > 0)
            {
                foreach (DataRow oRow in oDT.Rows)
                {
                    var ID = (int)oRow[0];
                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    loResult.Add(new clsQuestionCategory(ID, sName, sDescription));
                }
            }
            return loResult;
        }

        public static int Delete_QuestionCategory(clsBaseModel x)
        {
            return Delete<clsQuestionCategory>(x, deleteQuestionCategory);
        }
        public static int Delete_QuestionCategory(int iID)
        {
            return Delete(iID, deleteQuestionCategory);
        }
        private static int deleteQuestionCategory(SqlConnection oCnn, int iID)
        {
            string sSQL = @"delete from QuestionCategory where qcId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            return oCmd.ExecuteNonQuery();
        }
 
    }
}
