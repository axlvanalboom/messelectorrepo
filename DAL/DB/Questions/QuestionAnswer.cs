﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        public static int Add_QuestionAnswer(clsBaseModel x)
        {
            return Add<clsAnswerMC>(x, addQuestionAnswer);
        }
        public static int Add_QuestionAnswer(string sName, string sDescription = null)
        {
            return Add<clsAnswerMC>(sName, sDescription, addQuestionAnswer);
        }
        private static int addQuestionAnswer(SqlConnection oCnn, string sName, string sDescription = null)
        {
            string sSQL = @"insert into QuestionAnswer (qaName,qaDescription)
                values(@xName, @xDescription);
                select qaID from QuestionAnswer where qaName = @xName;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@xName", sName));
            oCmd.Parameters.Add(new SqlParameter("@xDescription", sDescription ?? (object)DBNull.Value));

            return (int)oCmd.ExecuteScalar();
        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="iMes"></param>
        /// <param name="iAnswer"></param>
        /// <param name="iFunction"></param>
        /// <returns></returns>
        public static int Edit_QuestionAnswer(clsBaseModel x)
        {
            return Edit<clsAnswerMC>(x, Edit_QuestionAnswer);
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <returns></returns>
        public static int Edit_QuestionAnswer(int iID, string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName, sDescription);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update QuestionAnswer set ";

                object oN = "";
                if (sName != "")
                {
                    sSQL += " qaName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " qaDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where qaId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }


        public static clsAnswerMC Get_QuestionAnswer(clsBaseModel x)
        {
            return Get<clsAnswerMC>(x, getQuestionAnswer);
        }
        public static clsAnswerMC Get_QuestionAnswer(int iID)
        {
            return Get<clsAnswerMC>(iID, getQuestionAnswer);
        }
        private static clsAnswerMC getQuestionAnswer(SqlConnection oCnn, int iID)
        {
            string sSQL = @"select qaId,qaName,qaDescription
                                from QuestionAnswer where qaId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
            {
                var oRow = oDT.Rows[0];
                var ID = (int)oRow[0];

                string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                return new clsAnswerMC(ID, sName, sDescription);
            }
            else { return null; }
        }

        public static List<clsBaseModel> Get_QuestionAnswers()
        {
            return Get(getQuestionAnswers);
        }
        private static List<clsBaseModel> getQuestionAnswers(SqlConnection oCnn)
        {
            string sSQL = @"select qaId,qaName,qaDescription
                            from QuestionAnswer order by qaId asc;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            List<clsBaseModel> loResult =  new List<clsBaseModel>();

            if (oDT.Rows.Count > 0)
            {
                foreach (DataRow oRow in oDT.Rows)
                {
                    var ID = (int)oRow[0];
                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    loResult.Add(new clsAnswerMC(ID, sName, sDescription));
                }
            }
            return loResult;

        }
        
        public static int Delete_QuestionAnswer(clsBaseModel x)
        {
            return Delete<clsAnswerMC>(x, deleteQuestionAnswer);
        }
        public static int Delete_QuestionAnswer(int iID)
        {
            return Delete(iID, deleteQuestionAnswer);
        }
        private static int deleteQuestionAnswer(SqlConnection oCnn,int iID)
        {
            string sSQL = @"delete from QuestionAnswer where qaId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            return oCmd.ExecuteNonQuery();
        }
    
    }
}
