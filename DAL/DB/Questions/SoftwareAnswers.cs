﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{

    public partial class clsDB
    {

        public static int Add_SoftwareAnswers(clsSoftware s, List<clsAbstractSoftwareAnswer> answers)
        {
            return Add_SoftwareAnswers(s.Id, answers);
        }
        public static int Add_SoftwareAnswers(int iSoftware, List<clsAbstractSoftwareAnswer> answers)
        {
            Check_ID(iSoftware);
            if (answers == null || answers.Count == 0) return 0;
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                foreach (var a in answers)
                {
                    if (a is clsSoftwareAnswerMC)
                        addSoftwareAnswer(oCnn, iSoftware, (clsSoftwareAnswerMC)a);
                    else if (a is clsSoftwareAnswerRange)
                        addSoftwareAnswer(oCnn, iSoftware, (clsSoftwareAnswerRange)a);
                }
                return iSoftware;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Add_SoftwareAnswers(clsSoftwareWithAnswers x)
        {
            return Add<clsSoftwareWithAnswers>(x, addSoftwareAnswers,false);
        }
        private static int addSoftwareAnswers(SqlConnection oCnn, clsSoftwareWithAnswers x)
        {
            foreach (var a in x.Answers)
            {
                if (a is clsSoftwareAnswerMC)
                    addSoftwareAnswer(oCnn, x.Id, (clsSoftwareAnswerMC)a);
                else if (a is clsSoftwareAnswerRange)
                    addSoftwareAnswer(oCnn, x.Id, (clsSoftwareAnswerRange)a);
            }
            return x.Id;
        }

        private static void addSoftwareAnswer(SqlConnection oCnn, int iSoftware, clsSoftwareAnswerMC a)
        {
            string sSQL = @"insert into QuestionSoftwareAnswer (Name,Description,Question, Answer) values (@N, @D, @Q, @A);
                            insert into SSAlink (ssaSoftware, ssaAnswer) (select @S, ID from QuestionSoftwareAnswer where Name=@N and Question=@Q and Answer=@A);";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@S", iSoftware));
            oCmd.Parameters.Add(new SqlParameter("@N", a.Name == null || a.Name== "" ? Guid.NewGuid().ToString(): a.Name));
            oCmd.Parameters.Add(new SqlParameter("@D", a.Description ?? (object)DBNull.Value));
            oCmd.Parameters.Add(new SqlParameter("@Q", a.Question.Id));
            oCmd.Parameters.Add(new SqlParameter("@A", a.Answer.Id));
            oCmd.ExecuteNonQuery();
        }
        private static void addSoftwareAnswer(SqlConnection oCnn, int iSoftware, clsSoftwareAnswerRange a)
        {
            string sSQL = @"insert into QuestionSoftwareAnswer (Name,Description,Question, Value) values (@N, @D, @Q, @V);
                            insert into SSAlink (ssaSoftware, ssaAnswer) (select @S, ID from QuestionSoftwareAnswer where Name=@N and Question=@Q and Value=@V);";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@S", iSoftware));
            oCmd.Parameters.Add(new SqlParameter("@N", a.Name == null || a.Name == "" ? Guid.NewGuid().ToString() : a.Name));
            oCmd.Parameters.Add(new SqlParameter("@D", a.Description ?? (object)DBNull.Value));
            oCmd.Parameters.Add(new SqlParameter("@Q", a.Question.Id));
            oCmd.Parameters.Add(new SqlParameter("@V", a.Value));
            oCmd.ExecuteNonQuery();
        }

        public static int Edit_SoftwareAnswer(clsAbstractSoftwareAnswer x)
        {
            Check_Class(x);
            if (x.GetType() == typeof(clsSoftwareAnswerMC))
            {
                var qmc = (clsSoftwareAnswerMC)x;
                return Edit_SoftwareAnswerMC(x.Id, qmc.Name, qmc.Description, qmc.Answer.Id);
            }
            else if (x.GetType() == typeof(clsSoftwareAnswerRange))
            {
                var qr = (clsSoftwareAnswerRange)x;
                return Edit_SoftwareAnswerRange(x.Id, qr.Name, qr.Description, qr.Value);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_SoftwareAnswerMC(int id, string sName, string sDescription, int iAnswer)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update QuestionSoftwareAnswer set Name = @N, Description = @D,  Answer = @A where Id = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", id));
                oCmd.Parameters.Add(new SqlParameter("@N", sName == null || sName == "" ? Guid.NewGuid().ToString() : sName));
                oCmd.Parameters.Add(new SqlParameter("@D", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@A", iAnswer));
                oCmd.ExecuteScalar();

                return id;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Edit_SoftwareAnswerRange(int id, string sName, string sDescription,  int iValue)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update QuestionSoftwareAnswer set Name = @N, Description = @D, Value = @V where Id = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", id));
                oCmd.Parameters.Add(new SqlParameter("@N", sName == null || sName == "" ? Guid.NewGuid().ToString() : sName));
                oCmd.Parameters.Add(new SqlParameter("@D", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@V", iValue));
                oCmd.ExecuteScalar();

                return id;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsAbstractSoftwareAnswer> Get_SoftwareAnswers(clsSoftware x)
        {
            return Get<List<clsAbstractSoftwareAnswer>>(x.Id, getSoftwareAnswers);
        }
        public static List<clsAbstractSoftwareAnswer> Get_SoftwareAnswers(int iSoftwareId)
        {
            return Get<List<clsAbstractSoftwareAnswer>>(iSoftwareId, getSoftwareAnswers);
        }
        private static List<clsAbstractSoftwareAnswer> getSoftwareAnswers(SqlConnection oCnn, int id)
        {
            string sSQL = @"select Id, Name, Description, Question, Answer, Value 
                            from QuestionSoftwareAnswer 
                            join SSAlink on ssaAnswer = Id
                            where ssaSoftware = @ID";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", id));
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
            {
                var loResult = new List<clsAbstractSoftwareAnswer>();
                foreach (DataRow oRow in oDT.Rows)
                    loResult.Add(getSoftwareAnswer(oRow));
                return loResult;
            }
            else { return null; }
        }
        private static clsAbstractSoftwareAnswer getSoftwareAnswer(DataRow oRow)
        {
            var ID = (int)oRow[0];
            string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
            string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
            var oQuestion = Get_Question((int)oRow[3]);
            object iAnswer = (oRow[4] == DBNull.Value) ? null : oRow[4];
            object iValue = (oRow[5] == DBNull.Value) ? null : oRow[5];

            if (iAnswer != null)
                return new clsSoftwareAnswerMC(ID, sName, sDescription) { Question = (clsQuestionMC)oQuestion, Answer = Get_QuestionAnswer((int)iAnswer) };
            else
                return new clsSoftwareAnswerRange(ID, sName, sDescription) { Question = (clsQuestionRange)oQuestion, Value = (int)iValue };
        }

        public static List<clsBaseModel> Get_SoftwareAnswers()
        {
            return Get(getSoftwareAnswers);
        }
        private static List<clsBaseModel> getSoftwareAnswers(SqlConnection oCnn)
        {
            string sSQL = @"select Id, Name, Description, Question, Answer, Value 
                            from QuestionSoftwareAnswer";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
            {
                var loResult = new List<clsBaseModel>();
                foreach (DataRow oRow in oDT.Rows)
                    loResult.Add(getSoftwareAnswer(oRow));
                return loResult;
            }
            else { return null; }
        }

        public static int Delete_SoftwareAnswer(clsSoftwareAnswerMC x)
        {
            return Delete<clsSoftwareAnswerMC>(x, deleteSoftwareAnswer);
        }
        public static int Delete_SoftwareAnswer(clsSoftwareAnswerRange x)
        {
            return Delete<clsSoftwareAnswerRange>(x, deleteSoftwareAnswer);
        }
        public static int Delete_SoftwareAnswer(int iID)
        {
            return Delete(iID, deleteSoftwareAnswer);
        }
        private static int deleteSoftwareAnswer(SqlConnection oCnn, int iID)
        {
            string sSQL = @"delete from QuestionSoftwareAnswer where Id = @ID;
                            delete from SSAlink where ssaAnswer = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            return oCmd.ExecuteNonQuery(); 
        }

    }

}
