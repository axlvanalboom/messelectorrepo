﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        public static int Add_Question(clsBaseModel x)
        {
            Check_Class(x);
            if (x.GetType() == typeof(clsQuestionMC))
            {
                var qmc = (clsQuestionMC)x;
                return Add_QuestionMC(qmc.Name, qmc.Description, qmc.Category.Id,qmc.CategoryMinimum, qmc.CategoryMaximum, qmc.PossibleAnswers.Select(a => a.Id).ToList());
            }
            else if (x.GetType() == typeof(clsQuestionRange))
            {
                var qr = (clsQuestionRange)x;
                return Add_QuestionRange(qr.Name, qr.Description, qr.Category.Id, qr.CategoryMinimum, qr.CategoryMaximum, qr.Minimum, qr.Maximum);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Add_QuestionMC(string sName, string sDescription,int iCategory, int iCategoryMin, int iCategoryMax, List<int> liAnswers)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"insert into Question (qName,qDescription,qCategory, qCategoryMinimum, qCategoryMaximum) 
                values (@Name, @Description, @Category, @cMin, @cMax);
                select qId from Question where qName = @Name;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Name", sName));
                oCmd.Parameters.Add(new SqlParameter("@Description", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@cMin", iCategoryMin));
                oCmd.Parameters.Add(new SqlParameter("@cMax", iCategoryMax));

                int id = (int)oCmd.ExecuteScalar();

                addQuestionPossibleAnswers(oCnn, id, liAnswers);

                return id;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Add_QuestionRange(string sName, string sDescription, int iCategory, int iCategoryMin, int iCategoryMax, int iMin, int iMax)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"insert into Question (qName,qDescription,qCategory, qCategoryMinimum, qCategoryMaximum, qMinimum, qMaximum) 
                values (@Name, @Description, @Category, @cMin, @cMax, @min, @max);
                select qId from Question where qName = @Name;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Name", sName));
                oCmd.Parameters.Add(new SqlParameter("@Description", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@cMin", iCategoryMin));
                oCmd.Parameters.Add(new SqlParameter("@cMax", iCategoryMax));
                oCmd.Parameters.Add(new SqlParameter("@min", iMin));
                oCmd.Parameters.Add(new SqlParameter("@max", iMax));

                return (int)oCmd.ExecuteScalar();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static void addQuestionPossibleAnswers(SqlConnection oCnn, int question, List<int> liAnswers)
        {
            foreach (var a in liAnswers)
            {
                var sSQL = @"insert into QuestionPossibleAnswers (Question,Answer) values(@Q, @A)";
                var oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Q", question));
                oCmd.Parameters.Add(new SqlParameter("@A", a));
                oCmd.ExecuteNonQuery();
            }
        }

        public static int Edit_Question(clsBaseModel x)
        {
            Check_Class(x);
            if (x.GetType() == typeof(clsQuestionMC))
            {
                var qmc = (clsQuestionMC)x;
                return Edit_QuestionMC(x.Id,qmc.Name, qmc.Description, qmc.Category.Id, qmc.CategoryMinimum, qmc.CategoryMaximum, qmc.PossibleAnswers.Select(a => a.Id).ToList());
            }
            else if (x.GetType() == typeof(clsQuestionRange))
            {
                var qr = (clsQuestionRange)x;
                return Edit_QuestionRange(x.Id, qr.Name, qr.Description, qr.Category.Id, qr.CategoryMinimum, qr.CategoryMaximum, qr.Minimum, qr.Maximum);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_QuestionMC(int id,string sName, string sDescription, int iCategory, int iCategoryMin, int iCategoryMax, List<int> liAnswers)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Question set ";
                if (sName != "")
                    sSQL += " qName = @Name, ";
                
                object oD = "";
                if (sDescription != "")
                    sSQL += " qDescription = @Description, ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                
                object oW = "";
                if (iCategory > 0)
                    sSQL += " qCategory = @Category, ";

                sSQL += " qCategoryMinimum = @CatMin, qCategoryMaximum = @CatMax ";
                sSQL += " where qId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", id));
                oCmd.Parameters.Add(new SqlParameter("@Name", sName));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@CatMin", iCategoryMin));
                oCmd.Parameters.Add(new SqlParameter("@CatMax", iCategoryMax));
                oCmd.ExecuteScalar();

                sSQL = @"delete from QuestionPossibleAnswers where Question = @ID;";
                oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", id));
                oCmd.ExecuteNonQuery();

                addQuestionPossibleAnswers(oCnn, id, liAnswers);

                return id;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Edit_QuestionRange(int id,string sName, string sDescription, int iCategory, int iCategoryMin, int iCategoryMax, int iMin, int iMax)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Question set ";
                if (sName != "")
                    sSQL += " qName = @Name, ";

                object oD = "";
                if (sDescription != "")
                    sSQL += " qDescription = @Description, ";
                if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }

                object oW = "";
                if (iCategory > 0)
                    sSQL += " qCategory = @Category, ";

                sSQL += " qCategoryMinimum = @CatMin, qCategoryMaximum = @CatMax, ";
                sSQL += " qMinimum = @Min, qMaximum = @Max ";
                sSQL += " where qId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", id));
                oCmd.Parameters.Add(new SqlParameter("@Name", sName));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@CatMin", iCategoryMin));
                oCmd.Parameters.Add(new SqlParameter("@CatMax", iCategoryMax));
                oCmd.Parameters.Add(new SqlParameter("@Min", iMin));
                oCmd.Parameters.Add(new SqlParameter("@Max", iMax));
                oCmd.ExecuteScalar();

                return id;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static clsAbstractQuestions Get_Question(clsBaseModel x)
        {
            if (x.GetType() == typeof(clsQuestionMC))
                return Get_Question((clsQuestionMC)x);
            else if (x.GetType() == typeof(clsQuestionRange))
                return Get_Question((clsQuestionRange)x);
            else return null;
        }
        public static clsAbstractQuestions Get_Question(clsQuestionMC x)
        {
            return Get<clsAbstractQuestions>((clsAbstractQuestions)x, getQuestion);
        }
        public static clsAbstractQuestions Get_Question(clsQuestionRange x)
        {
            return Get<clsAbstractQuestions>((clsQuestionRange)x, getQuestion);
        }
        public static clsAbstractQuestions Get_Question(int iID)
        {
            return Get<clsAbstractQuestions>(iID, getQuestion);
        }
        private static clsAbstractQuestions getQuestion(SqlConnection oCnn, int iID)
        {
            string sSQL = @"select qId,qName,qDescription, qCategory, qCategoryMinimum, qCategoryMaximum, qMinimum, qMaximum
                            from Question where qId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
            DataTable oDT = new DataTable();
            oDa.Fill(oDT);

            if (oDT.Rows.Count > 0)
            {
                var oRow = oDT.Rows[0];
                var ID = (int)oRow[0];

                string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                var oCategory = Get_QuestionCategory((int)oRow[3]);
                int iCatMin = (int)oRow[4];
                int iCatMax = (int)oRow[5];
                object iMin = (oRow[6] == DBNull.Value) ? null : oRow[6];
                object iMax = (oRow[7] == DBNull.Value) ? null : oRow[7];

                if (iMin == null || iMax == null)
                {
                    sSQL = @"select Answer from QuestionPossibleAnswers where Question = @ID;";
                    oCmd = new SqlCommand(sSQL, oCnn);
                    oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                    oDa = new SqlDataAdapter(oCmd);
                    oDT = new DataTable();
                    oDa.Fill(oDT);
                    List<clsAnswerMC> loAnswers = new List<clsAnswerMC>();
                    foreach (DataRow r in oDT.Rows)
                        loAnswers.Add(Get_QuestionAnswer((int)r[0]));

                    return new clsQuestionMC(iID, sName, sDescription, oCategory, iCatMin, iCatMax, loAnswers);
                }
                else
                    return new clsQuestionRange(ID, sName, sDescription, oCategory, iCatMin, iCatMax, (int)iMin, (int)iMax);


            }
            else { return null; }
        }

        public static List<clsBaseModel> Get_Questions()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"select qId from Question;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsBaseModel> loResult =  new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                    foreach (DataRow oRow in oDT.Rows)
                        loResult.Add(Get_Question((int)oRow[0]));
                 return loResult;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
                       
        public static int Delete_Question(clsQuestionMC x)
        {
            return Delete<clsQuestionMC>(x, deleteQuestion);
        }
        public static int Delete_Question(clsQuestionRange x)
        {
            return Delete<clsQuestionRange>(x, deleteQuestion);
        }
        public static int Delete_Question(int iID)
        {
            return Delete(iID, deleteQuestion);
        }
        private static int deleteQuestion(SqlConnection oCnn, int iID)
        {
            string sSQL = @"delete from Question where qId = @ID;";
            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            var iReturn = oCmd.ExecuteNonQuery();

            sSQL = @"delete from QuestionPossibleAnswers where Question = @ID;";
            oCmd = new SqlCommand(sSQL, oCnn);
            oCmd.Parameters.Add(new SqlParameter("@ID", iID));
            iReturn += oCmd.ExecuteNonQuery();

            return iReturn;
        }

    }
}
