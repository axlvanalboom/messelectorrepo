﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        public static int Add_Activity(clsBaseModel oActivity)
        {
            Check_Class(oActivity);

            if (oActivity.GetType() == typeof(clsActivity))
            {
                clsActivity oA = (clsActivity)oActivity;
                return Add_Activity(oA.Name, oA.Description);
            }
            else { WrongModelType(); return 0; }         
        }
        public static int Add_Activity(string sActivity, string sDescription = null)
        {
            Check_Name(sActivity);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"insert into Activity (acName,acDescription)
                values(@acName, @acDescription);
                select acID from Activity where acName = @acName;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@acName", sActivity));
                oCmd.Parameters.Add(new SqlParameter("@acDescription", sDescription ?? (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="iMes"></param>
        /// <param name="iCategory"></param>
        /// <param name="iFunction"></param>
        /// <returns></returns>
        public static int Edit_Activity(clsBaseModel oActivity)
        {
            Check_Class(oActivity);
            if (oActivity.GetType() == typeof(clsActivity))
            {
                clsActivity oA = (clsActivity)oActivity;
                return Edit_Activity(oA.Id, oA.Name, oA.Description);
            }
            else { WrongModelType(); return 0; }
            
        }
        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="sName"></param>
        /// <param name="sDescription"></param>
        /// <returns></returns>
        public static int Edit_Activity(int iID,string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName, sDescription);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Activity set ";

                object oN= "";
                if (sName != "")
                {
                    sSQL += " acName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " acDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where acId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static clsBaseModel Get_Activity(clsBaseModel oActivity)
        {
            Check_Class(oActivity);
            if (oActivity.GetType() == typeof(clsActivity))
            {
                clsActivity oA = (clsActivity)oActivity;
                return Get_Activity(oA.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_Activity(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"select acId,acName,acDescription
                                from Activity where acId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsActivity oActivity;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    oActivity = new clsActivity(ID, sName, sDescription);
                    return oActivity;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static List<clsBaseModel> Get_Activities()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"select acId,acName,acDescription
                                from Activity order by acName asc;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsActivity oActivity;
                List<clsBaseModel> oActivities;
                oActivities = new List<clsBaseModel>();
                
                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    { 
                        var ID = (int)oRow[0];

                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        oActivity = new clsActivity(ID, sName, sDescription);

                        oActivities.Add(oActivity);
                    }

                    return oActivities;
                }
                else { return new List<clsBaseModel>(); }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_Activity(clsBaseModel oActivity)
        {
            Check_Class(oActivity);
            if (oActivity.GetType() == typeof(clsActivity))
            {
                clsActivity oA = (clsActivity)oActivity;
                return Delete_Activity(oA.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Activity(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"delete from Activity where acId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
    }
}
