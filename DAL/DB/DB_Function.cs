﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        
        public static int Add_Function(clsBaseModel oFunction)
        {
            Check_Class(oFunction);
            if (oFunction.GetType() == typeof(clsFunction))
            {
                clsFunction oF = (clsFunction)oFunction;
                return Add_Function(oF.Name, oF.Description);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Add_Function(string sFunction, string sDescription = null)
        {
            Check_Name(sFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"insert into Functions (fuName,fuDescription)
                values(@fuName, @fuDescription);
                select fuID from Functions where fuName = @fuName;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@fuName", sFunction));
                oCmd.Parameters.Add(new SqlParameter("@fuDescription", sDescription ?? (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Edit_Function(clsBaseModel oFunction)
        {
            Check_Class(oFunction);
            if (oFunction.GetType() == typeof(clsFunction))
            {
                clsFunction oF = (clsFunction)oFunction;
                return Edit_Function(oF.Id, oF.Name, oF.Description);
            }
            else { WrongModelType(); return 0; }
            }
        public static int Edit_Function(int iID, string sName, string sDescription = null)
        {
            Check_ID(iID); Check_BaseModel_Edit(sName, sDescription);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "update Functions set ";
                object oN = "";
                if (sName != "")
                {
                    sSQL += " fuName = @Name ";
                    oN = sName;
                    if (sDescription != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " fuDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                }

                sSQL += "  where fuId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                return oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static clsBaseModel Get_Function(clsBaseModel oFunction)
        {
            Check_Class(oFunction);
            if (oFunction.GetType() == typeof(clsFunction))
            {
                clsFunction oF = (clsFunction)oFunction;
                return Get_Function(oF.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_Function(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select fuId,fuName,fuDescription
                                from Functions where fuId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsFunction oFunction;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    oFunction = new clsFunction(ID, sName, sDescription);
                    return oFunction;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static List<clsBaseModel> Get_Functions()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select fuId,fuName,fuDescription
                                from Functions order by fuName asc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsFunction oFunction;
                List<clsBaseModel> oFunctions;
                oFunctions = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];

                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        oFunction = new clsFunction(ID, sName, sDescription);

                        oFunctions.Add(oFunction);
                    }

                    return oFunctions;
                }
                else { return new List<clsBaseModel>(); ; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Delete_Function(clsBaseModel oFunction)
        {
            Check_Class(oFunction);
            if (oFunction.GetType() == typeof(clsFunction))
            {
                clsFunction oF = (clsFunction)oFunction;
                return Delete_Function(oF.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Function(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"delete from Functions where fuId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
    }
}
