﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;

namespace DAL.DB
{

    public partial class clsDB//_Adding
    {

        public static int AddLogType(string sLogType)
        {
            if (sLogType == null || sLogType == "") { throw new Exception("The new logtype can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into LogType (ltType) values (@Type); 
                select ltPk from LogType where ltType = @Type order by ltPk desc;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Type", sLogType));
                var i = (int)oCmd.ExecuteScalar();
                return i;
                //return (int)oCmd.ExecuteScalar();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static void AddLog(string sUserId, string sTimeStamp, string sLogType)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The new logtype can't be null or empty!"); }
            if (sLogType == null || sLogType == "") { throw new Exception("The new logtype can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into Logging (logID, logTime,logType) 
                                values (@Id, @Time,(select ltPk from LogType where ltType = @Type));";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", sLogType));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void AddLog(string sUserId, string sTimeStamp, string sLogType, string sParameter)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The timestamp can't be null or empty!"); }
            if (sLogType == null || sLogType == "") { throw new Exception("The type can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into dbo.Logging (logID, logTime,logType, logParamS) 
                                values (@Id, @Time,(select ltPk from dbo.LogType where ltType = @Type),@Param);";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", sLogType));
                oCmd.Parameters.Add(new SqlParameter("@Param", sParameter ?? (object)DBNull.Value));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void AddLog(string sUserId, string sTimeStamp, string sLogType, int iParameter)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The new logtype can't be null or empty!"); }
            if (sLogType == null || sLogType == "") { throw new Exception("The new logtype can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into Logging (logID, logTime,logType, logParamI) 
                                values (@Id, @Time,(select ltPk from LogType where ltType = @Type),@Param);";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", sLogType));
                oCmd.Parameters.Add(new SqlParameter("@Param", iParameter));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static void AddLog(string sUserId, string sTimeStamp, int iLogType)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The new logtype can't be null or empty!"); }
            if (iLogType == 0) { throw new Exception("The new logtype can't be zero!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into Logging (logID, logTime,logType) 
                                values (@Id, @Time,@Type);";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", iLogType));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void AddLog(string sUserId, string sTimeStamp, int iLogType, string sParameter)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The new logtype can't be null or empty!"); }
            if (iLogType == 0) { throw new Exception("The new logtype can't be zero!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into Logging (logID, logTime,logType, logParamS) 
                                values (@Id, @Time,@Type,@Param);";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", iLogType));
                oCmd.Parameters.Add(new SqlParameter("@Param", sParameter));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static void AddLog(string sUserId, string sTimeStamp, int iLogType, int iParameter)
        {
            if (sUserId == null || sUserId == "") { throw new Exception("The userid can't be null or empty!"); }
            if (sTimeStamp == null || sTimeStamp == "") { throw new Exception("The new logtype can't be null or empty!"); }
            if (iLogType == 0) { throw new Exception("The new logtype can't be zero!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"insert into Logging (logID, logTime,logType, logParamI) 
                                values (@Id, @Time, @Type,@Param);";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@Time", sTimeStamp));
                oCmd.Parameters.Add(new SqlParameter("@Type", iLogType));
                oCmd.Parameters.Add(new SqlParameter("@Param", iParameter));

                oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
    }

    public partial class clsDB//_Analyse
    {
        public static List<clsLog> GetAllLogs()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select ID, Time,Type,Parameter  from LogView order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsLog> GetLogsByTime(string sStart, string sStop)
        {
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"select ID, Time,Type,Parameter  from LogView 
                                where Time between @Start and @Stop 
                                order by Time desc;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsLog> GetLogsById(string sId)
        {
            if (sId == null || sId == "") { throw new Exception("The id can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"select ID, Time,Type,Parameter  from LogView 
                                where ID = @ID
                                order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", sId));

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<clsLog> GetLogsByIdAndTime(string sId, string sStart, string sStop)
        {
            if (sId == null || sId == "") { throw new Exception("The id can't be null or empty!"); }
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"select ID, Time,Type,Parameter  from LogView 
                                where ID = @ID and Time between @Start and @Stop       
                                order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", sId));
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsLog> GetLogsByType(string sType)
        {
            if (sType == null || sType == "") { throw new Exception("The Type can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"select ID, Time,Type,Parameter  from LogView 
                                where Type = @Type
                                order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<clsLog> GetLogsByTypeAndTime(string sType, string sStart, string sStop)
        {       
            if (sType == null || sType == "") { throw new Exception("The Type can't be null or empty!"); }
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"select ID, Time,Type,Parameter  from LogView 
                                where Type = @Type and Time between @Start and @Stop
                                order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];
                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    oLogs.Add(oLog);
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        //todo: get TypeAndParameter!!! --> don't use logView

        public static List<clsDayAmountVisitors> GetAmmountOfVisitors()
        {
 
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select logID, logTime from Logging order by logTime asc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                return CalculateAmountOfVisitorsPerDay(oDT);

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<clsDayAmountVisitors> GetAmmountOfVisitorsBetweenTime(string sStart, string sStop)
        {
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select logID, logTime from Logging where logTime between @Start and @Stop order by logTime asc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                return CalculateAmountOfVisitorsPerDay(oDT);

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static List<clsDayAmountVisitors> CalculateAmountOfVisitorsPerDay(DataTable oDT)
        {
            List<clsDayAmountVisitors> oResult = new List<clsDayAmountVisitors>();

            if (oDT.Columns.Count != 2) { throw new Exception("Wrong Data structure!"); }

            foreach (DataRow oRow in oDT.Rows)
            {
                long lDate = 0;
                long.TryParse((string)oRow[1], out lDate); //time
                DateTimeOffset oDate = DateTimeOffset.FromUnixTimeSeconds(lDate);

                string sDate = oDate.Year + "/" + oDate.Month + "/" + oDate.Day;
                var oDayAmount = oResult.Find(x => x.Day == sDate);
                if (oDayAmount == null)
                {
                    var o = new clsDayAmountVisitors() { Day = sDate, Amount = 1, Users = new List<string>() };
                    o.Users.Add((string)oRow[0]);
                    oResult.Add(o);
                }
                else
                {
                    string sUser = (string)oRow[0];
                    if (!(oDayAmount.Users.Contains(sUser)))
                        oResult.Find(x => x.Day == sDate).Amount += 1;
                    oResult.Find(x => x.Day == sDate).Users.Add(sUser);
                }

            }
            return oResult;
        }

        public static List<StringInt> GetAmmountOfViewsOffTypeParameter(string sType, string sParameter)
        {
            if (sType == null || sType == "") { throw new Exception("The type can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select Time from logview where Parameter = @Param and Type = @Type order by Time asc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Param", sParameter));
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                return CalculateAmmountPerDay(oDT);

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<StringInt> GetAmmountOfViewsOffTypeParameterBetweenTime(string sType, string sParameter, string sStart, string sStop)
        {
            if (sType == null || sType == "") { throw new Exception("The type can't be null or empty!"); }
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select Time from logview where Parameter = @Param and Type = @Type and Time between @Start and @Stop order by Time asc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Param", sParameter));
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                return CalculateAmmountPerDay(oDT);

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static List<StringInt> CalculateAmmountPerDay(DataTable oDT)
        {
            if (oDT.Columns.Count != 1) { throw new Exception("Wrong Data structure!"); }

            List<StringInt> oResult = new List<StringInt>();

            foreach (DataRow oRow in oDT.Rows)
            {
                long lDate = 0;
                long.TryParse((string)oRow[0], out lDate); //time
                DateTimeOffset oDate = DateTimeOffset.FromUnixTimeSeconds(lDate);

                string sDate = oDate.Year + "/" + oDate.Month + "/" + oDate.Day;

                bool xPresent = false;
                for (int i = 0; i < oResult.Count; i++)
                {
                    if (oResult[i].Text == sDate)
                    {
                        oResult[i].Number += 1;
                        xPresent = true;
                        break;
                    }
                }
                if (!xPresent)
                    oResult.Add(new StringInt() { Text = sDate, Number = 1 });

            }
            return oResult;

        }

        public static List<StringInt> GetAmountDetailViews(string sType)
        {
            if (sType == null || sType == "") { throw new Exception("The type can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                select Parameter, Count(Parameter) as Count from LogView
                                where Type = @Type
                                group by Parameter
                                order by Count desc;
                                ";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<StringInt> oResult = new List<StringInt>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    oResult.Add(new StringInt() { Text = (string)oRow[0], Number = (int)oRow[1] });

                }

                return oResult;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<StringInt> GetAmountDetailViewsBetweenTime(string sType, string sStart, string sStop)
        {
            if (sType == null || sType == "") { throw new Exception("The type can't be null or empty!"); }
            if (sStart == null || sStart == "") { throw new Exception("The start time can't be null or empty!"); }
            if (sStop == null || sStop == "") { throw new Exception("The stop time can't be null or empty!"); }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = @"
                                WITH tableToUse (Type, Parameter)
                                AS
                                (
	                                select Type,Parameter from logView 
	                                where Time between @Start and @Stop
                                )

                                select Parameter, Count(Parameter) as Count from tableToUse
                                where Type = @Type
                                group by Parameter
                                order by Count desc;
                                ";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Type", sType));
                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<StringInt> oResult = new List<StringInt>();

                foreach (DataRow oRow in oDT.Rows)
                {
                    oResult.Add(new StringInt() { Text = (string)oRow[0], Number = (int)oRow[1] });

                }

                return oResult;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsLog> GetUserViews()
        {

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select ID, Time,Type,Parameter  from LogView order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                int i = 0;
                foreach (DataRow oRow in oDT.Rows)
                {
                    if (i == 585)
                    {
                        var o = true;
                    }
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];

                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();

                    if (oLog.Type == "MainSearch" && oLog.Parameter != null && oLog.Parameter != "")
                    {
                        oLog.Parameter = GetMainSearchParameter(oLog.Parameter, oCnn);
                    }

                    oLogs.Add(oLog);
                    i += 1;
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<clsLog> GetUserViewsByTime(string sStart, string sStop)
        {

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQL = "select ID, Time,Type,Parameter  from LogView  where Time between @Start and @Stop order by Time desc";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                oCmd.Parameters.Add(new SqlParameter("@Start", sStart));
                oCmd.Parameters.Add(new SqlParameter("@Stop", sStop));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsLog> oLogs = new List<clsLog>();

                int i = 0;
                foreach (DataRow oRow in oDT.Rows)
                {
                    if (i == 585)
                    {
                        var o = true;
                    }
                    clsLog oLog = new clsLog();
                    oLog.Id = (string)oRow[0];
                    oLog.Time = (string)oRow[1];

                    oLog.Type = (string)oRow[2];
                    oLog.Parameter = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();

                    if (oLog.Type == "MainSearch" && oLog.Parameter != null && oLog.Parameter != "")
                    {
                        oLog.Parameter = GetMainSearchParameter(oLog.Parameter, oCnn);
                    }

                    oLogs.Add(oLog);
                    i += 1;
                }

                return oLogs;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static string GetMainSearchParameter(string sParameter, SqlConnection oCnn)
        {
            if (sParameter == null || sParameter  == "") { return null; }
            string sSQL = "";

            string sResult = "";
            string[] oParameters = sParameter.Split(' ');
            
            for (int i=0; i<oParameters.Length;i++)
            {
                if (oParameters[i] != "") 
                { 
                    sSQL += "declare @sql" + i + " nvarchar(max);";
                    sSQL += "set @sql" + i + " = ";
                    sSQL += "'select [' + replace(@ColumnName" + i + ", '''', '''''') + '] ";
                    sSQL += "from [' + replace(@Table"+i+", '''', '''''') + '] ";
                    sSQL += "where [' + replace(@ColumnId"+i+", '''', '''''') + ']= ' +replace(@ID"+i+", '''', '''''') +';';";
                    sSQL += "exec sp_executesql @sql" + i + ";";
                }
            }

            SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

            for (int i = 0; i < oParameters.Length; i++)
            {
                if (oParameters[i] != "")
                {
                    string sPrefix = oParameters[i].Substring(0, 3);
                    int iID = 0;
                    int.TryParse(oParameters[i].Substring(3, oParameters[i].Length - 3), out iID);

                    string sColumnId = "";
                    string sColumnName = "";
                    string sTable = "";

                    switch (sPrefix)
                    {
                        case "cas":
                            sColumnId = "casId";
                            sColumnName = "casName";
                            sTable = "Cases";
                            break;
                        case "com":
                            sColumnId = "comId";
                            sColumnName = "comName";
                            sTable = "Company";
                            break;
                        case "mes":
                            sColumnId = "meId";
                            sColumnName = "meName";
                            sTable = "MesSoftware";
                            break;
                        case "act":
                            sColumnId = "acId";
                            sColumnName = "acName";
                            sTable = "Activity";
                            break;
                        case "sec":
                            sColumnId = "seId";
                            sColumnName = "seName";
                            sTable = "Sector";
                            break;
                        case "cat":
                            sColumnId = "catId";
                            sColumnName = "catName";
                            sTable = "Category";
                            break;
                        case "fun":
                            sColumnId = "fuId";
                            sColumnName = "fuName";
                            sTable = "Functions";
                            break;
                    }
                    oCmd.Parameters.Add(new SqlParameter("@ColumnName" + i, sColumnName));
                    oCmd.Parameters.Add(new SqlParameter("@ColumnId" + i, sColumnId));
                    oCmd.Parameters.Add(new SqlParameter("@Table" + i, sTable));
                    oCmd.Parameters.Add(new SqlParameter("@ID" + i, iID));
                }
            }

            SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
            DataSet oDS = new DataSet();
            oDA.Fill(oDS);

            for (int i= 0;i<oDS.Tables.Count;i++)
            {
                sResult += oDS.Tables[i].Rows[0][0] ;
                if (i != oDS.Tables.Count - 1)
                    sResult += ", ";
            }

            return sResult;
        }
    
    
    }
}
