﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        
        public static int Add_Company(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType()== typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Add_Company(oCom.Name, oCom.Description, oCom.Website,oCom.Address,oCom.Email, oCom.Telephone,oCom.LogoUrl);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Add_Company(string sName, string sDescription = null, string sWebsite = null,string sAdress = null,string sContactEmail = null,string sContactTelephone =null,string sLogoUrl = null)
        {
            Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"insert into Company (comName,comDescription,comWebsite,comContactEmail,comContactTelefoon,comAdress,comLogoUrl)
                values(@comName, @comDescription, @comWebsite, @comContactEmail, @comContactTelefoon,@comAdress,@comLogoUrl);
                select top(1) comID from Company where comName = @comName";
                if (sDescription != null) { sSQl += " and comDescription = @comDescription"; }
                if (sWebsite != null) { sSQl += " and comWebsite = @comWebsite"; }
                if (sContactEmail != null) { sSQl += " and comContactEmail = @comContactEmail"; }
                if (sContactTelephone != null) { sSQl += " and comContactTelefoon = @comContactTelefoon"; }
                if (sAdress != null) { sSQl += " and comAdress = @comAdress"; }
                if (sLogoUrl != null) { sSQl += " and comLogoUrl = @comLogoUrl"; }
                sSQl += " order by comId desc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@comName", sName));
                oCmd.Parameters.Add(new SqlParameter("@comDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comWebsite", sWebsite ?? (object) DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comContactEmail", sContactEmail ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comContactTelefoon", sContactTelephone ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comAdress", sAdress ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comLogoUrl", sLogoUrl ?? (object)DBNull.Value));
                
                int i  = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch(Exception ex){ throw ex;}
            finally { oCnn.Close(); }
        }
        
        public static clsBaseModel Get_Company(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Get_Company(oCom.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_Company(int iID)
        {
            Check_ID(iID); 
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select comId,comName,comDescription, comWebsite,comContactEmail, comContactTelefoon, comAdress, comLogoUrl,comPublic
                                from Company where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsCompany oCompany;

                if (oDT.Rows.Count>0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null: oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null: oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sEmail = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                    string sTelephone = (oRow[5] == DBNull.Value) ? null : oRow[5].ToString();
                    string sAdress = (oRow[6] == DBNull.Value) ? null : oRow[6].ToString();
                    string sLogoUrl = (oRow[7] == DBNull.Value) ? null : oRow[7].ToString();
                    oCompany = new clsCompany(ID,sName , sDescription,sWebsite, sLogoUrl, sEmail,sTelephone,sAdress);
                    if (oRow[8] == DBNull.Value || (int)oRow[8] == 0) { oCompany.Public = false; } else { oCompany.Public = true; }

                    return oCompany;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static List<clsBaseModel> Get_Companies()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select comId, comName,comDescription,comWebsite, comContactEmail, comContactTelefoon,comAdress,comLogoUrl,comPublic from Company order by comName asc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsBaseModel> oCompanies = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];
                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                        string sEmail = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                        string sTelephone = (oRow[5] == DBNull.Value) ? null : oRow[5].ToString();

                        string sAdress = (oRow[6] == DBNull.Value) ? null : oRow[6].ToString();
                        string sLogoUrl = (oRow[7] == DBNull.Value) ? null : oRow[7].ToString();
                        var oCompany = new clsCompany(ID, sName, sDescription, sWebsite, sLogoUrl, sEmail, sTelephone, sAdress);
                        if (oRow[8] == DBNull.Value || (int)oRow[8] == 0) { oCompany.Public = false; } else { oCompany.Public = true; }

                        oCompanies.Add(oCompany);
                    }

                    return oCompanies;
                }
                else { return new List<clsBaseModel>(); ; }
          
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static clsBaseModel Get_CompanyContent(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Get_CompanyContent(oCom.Id);
            }
            else { WrongModelType(); return null; }            
        }
        public static clsBaseModel Get_CompanyContent(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select comId,comName,comDescription, comWebsite,comContactEmail, comContactTelefoon,comContent,comPublic,comAdress, comLogoUrl
                                from Company where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsCompany oCompany;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sEmail = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                    string sTelephone = (oRow[5] == DBNull.Value) ? null : oRow[5].ToString();
                    string sAdress = (oRow[8] == DBNull.Value) ? null : oRow[8].ToString();
                    string sLogoUrl = (oRow[9] == DBNull.Value) ? null : oRow[9].ToString();
                    oCompany = new clsCompany(ID, sName, sDescription, sWebsite, sLogoUrl, sEmail, sTelephone, sAdress);
                    oCompany.Content = (oRow[6] == DBNull.Value) ? null : oRow[6].ToString();

                    if (oRow[7] == DBNull.Value || (int)oRow[7] == 0) { oCompany.Public = false; } else { oCompany.Public = true; }
                    return oCompany;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
      
        public static int Edit_Company(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Edit_Company(oCom.Id, oCom.Name, oCom.Description, oCom.Website, oCom.Address,oCom.Email, oCom.Telephone,oCom.LogoUrl);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_Company(int iID, string sName, string sDescription = null, string sWebsite = null, string sAdress = null,string sContactEmail = null, string sContactTelephone = null,string sLogoUrl = null)
        {
            Check_ID(iID); Check_Company_Edit(sName,sDescription,sWebsite,sAdress,sContactEmail,sContactTelephone,sLogoUrl);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Company set ";
                object oN = "";
                if (sName != "")
                {
                    sSQL += " comName = @Name ";
                    oN = sName;
                    if (sDescription != "" || sWebsite != "" || sAdress != "" || sContactEmail != "" || sContactTelephone != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " comDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                    if (sWebsite != "" || sAdress != "" || sContactEmail != "" || sContactTelephone != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oW = "";
                if (sWebsite != "")
                {
                    sSQL += " comWebsite = @Website ";
                    if (sWebsite == null) { oW = DBNull.Value; ; } else { oW = sWebsite ; }
                    if (sAdress!= "" || sContactEmail != "" || sContactTelephone != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oA = "";
                if (sAdress != "")
                {
                    sSQL += " comAdress = @Adress ";
                    if (sAdress == null) { oA = DBNull.Value; ; } else { oA = sAdress; }
                    if (sContactEmail != "" || sContactTelephone != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oE = "";
                if (sContactEmail != "")
                {
                    sSQL += " comContactEmail = @Email ";
                    if (sContactEmail == null) { oE = DBNull.Value; ; } else { oE = sContactEmail; }
                    if (sContactTelephone != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oT = "";
                if (sContactTelephone != "")
                {
                    sSQL += " comContactTelefoon = @Telefoon ";
                    if (sContactTelephone == null ) { oT = DBNull.Value; ; } else { oT = sContactTelephone; }
                    if (sLogoUrl != "") { sSQL += ","; }
                }
                object oL = "";
                if (sLogoUrl != "")
                {
                    sSQL += " comLogoUrl = @LogoUrl ";
                    if (sLogoUrl == null) { oL = DBNull.Value; ; } else { oL = sLogoUrl; }

                }

                sSQL += "  where comId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                oCmd.Parameters.Add(new SqlParameter("@Website", oW));
                oCmd.Parameters.Add(new SqlParameter("@Adress", oA));
                oCmd.Parameters.Add(new SqlParameter("@Email", oE));
                oCmd.Parameters.Add(new SqlParameter("@Telefoon", oT));
                oCmd.Parameters.Add(new SqlParameter("@LogoUrl", oL));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        //todo: aanpassen naar de juiste vorm van een edit, hier wordt alles aangepast ook de onnodige dingen!
        public static int Edit_CompanyFull(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Edit_CompanyFull(oCom.Id, oCom.Name, oCom.Description, oCom.Website, oCom.Address,oCom.Email, oCom.Telephone,oCom.LogoUrl, oCom.Content, oCom.Public);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_CompanyFull(int iID, string sName, string sDescription = null, string sWebsite = null, string sAdress = null,string sContactEmail = null, string sContactTelephone = null, string sLogoUrl = null,string sContent = null, Boolean xPublic= false)
        {
            Check_ID(iID); Check_Name(sName);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                int iPublic; 
                if (xPublic){ iPublic = 1; } else { iPublic = 0; }
                string sSQl = @"update Company set comName = @comName, comDescription = @comDescription, 
                                comWebsite = @comWebsite, comContactEmail = @comContactEmail, comContactTelefoon = @comContactTelephone, comContent = @comContent, comPublic = @comPublic,
                                comAdress = @comAdress, comLogoUrl = @comLogoUrl
                                where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@comName", sName));
                oCmd.Parameters.Add(new SqlParameter("@comDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comWebsite", sWebsite ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comContactEmail", sContactEmail ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comContactTelephone", sContactTelephone ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comContent", sContent ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comPublic", iPublic));
                oCmd.Parameters.Add(new SqlParameter("@comAdress", sAdress ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@comLogoUrl", sLogoUrl ?? (object)DBNull.Value));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_CompanyContent(int iID, string sContent)
        {
            Check_ID(iID);Check_Name(sContent);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"update Company set 
                                comContent = @comContent
                                 where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@comContent", sContent ?? (object)DBNull.Value));

                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_CompanyVisibility(int iId, bool xPublic)
        {
            Check_ID(iId);

            int iPublic = 0;
            if (xPublic)
                iPublic = 1;

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Company set comPublic = @comPublic where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@comPublic", iPublic));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Show_CompanyContent(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Show_CompanyContent(oCom.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Show_CompanyContent(int iId)
        {
            Check_ID(iId); 
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Company set comPublic = 1
                                where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Hide_CompanyContent(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Hide_CompanyContent(oCom.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Hide_CompanyContent(int iId)
        {
            Check_ID(iId); 
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Company set comPublic = 0
                                where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_Company(clsBaseModel oCompany)
        {
            Check_Class(oCompany);
            if (oCompany.GetType() == typeof(clsCompany))
            {
                clsCompany oCom = (clsCompany)oCompany;
                return Delete_Company(oCom.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Company(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"delete from Company where comId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

    }
}