﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {
        
        public static int Add_Mes(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Add_Mes(oM.Name, oM.Description, oM.Website,oM.LogoUrl);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Add_Mes(string sMes, string sDescription = null, string sWebsite = null,string sLogoUrl = null)
        {
            Check_Name(sMes);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"insert into MesSoftware (meName,meDescription,meWebsite,meLogoUrl)
                values(@meName, @meDescription, @meWebsite,@meLogoUrl);
                select top(1) meID from MesSoftware where meName = @meName";
                if (sDescription != null) { sSQl += " and meDescription = @meDescription"; }
                if (sWebsite != null) { sSQl += " and meWebsite = @meWebsite"; }
                if (sLogoUrl != null) { sSQl += " and meLogoUrl = @meLogoUrl"; }
                sSQl += " order by meId desc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@meName", sMes));
                oCmd.Parameters.Add(new SqlParameter("@meDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@meWebsite", sWebsite ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@melogoUrl", sLogoUrl ?? (object)DBNull.Value));


                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
      
        public static clsBaseModel Get_Mes(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Get_Mes(oM.Id);
            }
            else { WrongModelType(); return null; }        
        }
        public static clsBaseModel Get_Mes(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select meId,meName,meDescription, meWebsite, meLogoUrl,mePublic
                                from MesSoftware where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsSoftware oMes;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sLogoUrl = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                    oMes = new clsSoftware(ID, sName, sDescription, sWebsite, sLogoUrl);
                    
                    if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oMes.Public = false; } else { oMes.Public = true; }
                    return oMes;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }      

        public static List<clsBaseModel> Get_MesAll()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select meId, meName,meDescription, meWebsite,meLogoUrl,mePublic from MesSoftware order by meName asc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsBaseModel> oMesList = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];
                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                        string sLogoUrl = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                        var oMes = new clsSoftware(ID, sName, sDescription, sWebsite, sLogoUrl);
                        if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oMes.Public = false; } else { oMes.Public = true; }

                        oMesList.Add(oMes);
                    }

                    return oMesList;
                }
                else { return new List<clsBaseModel>(); ; }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static clsBaseModel Get_MesContent(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Get_MesContent(oM.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_MesContent(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select meId,meName,meDescription, meWebsite,meContent,mePublic,meLogoUrl
                                from MesSoftware where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sLogoUrl = (oRow[6] == DBNull.Value) ? null : oRow[6].ToString();
                    var oMes = new clsSoftware(ID, sName, sDescription, sWebsite, sLogoUrl);
                    oMes.Content = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                    

                    if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oMes.Public = false; } else { oMes.Public = true; }
                    return oMes;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_Mes(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Edit_Mes(oM.Id, oM.Name, oM.Description, oM.Website,oM.LogoUrl);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_Mes(int iID, string sName, string sDescription = null, string sWebsite = null,string sLogoUrl = null)
        {
            Check_ID(iID); Check_AdvancedBaseModel_Edit(sName,sDescription,sWebsite,sLogoUrl);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update MesSoftware set ";
                object oN = "";
                if (sName != "")
                {
                    sSQL += " meName = @Name ";
                    oN = sName;
                    if (sDescription != "" || sWebsite != ""  || sLogoUrl != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " meDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                    if (sWebsite != ""  || sLogoUrl != "") { sSQL += ","; }
                }
                object oW = "";
                if (sWebsite != "")
                {
                    sSQL += " meWebsite = @Website ";
                    if (sWebsite == null) { oW = DBNull.Value; ; } else { oW = sWebsite; }
                    if ( sLogoUrl != "") { sSQL += ","; }
                }
                object oL = "";
                if (sLogoUrl != "")
                {
                    sSQL += " meLogoUrl = @LogoUrl ";
                    if (sLogoUrl == null) { oL = DBNull.Value; ; } else { oL = sLogoUrl; }
                }

                sSQL += "  where meId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                oCmd.Parameters.Add(new SqlParameter("@Website", oW));
                oCmd.Parameters.Add(new SqlParameter("@LogoUrl", oL));
                return oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        //todo: aanpassen naar de algemene vorm van de edit, hier wordt telkens alles aangepast
        public static int Edit_MesFull(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Edit_MesFull(oM.Id, oM.Name, oM.Description, oM.Website, oM.LogoUrl,oM.Content, oM.Public);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Edit_MesFull(int iID, string sMes, string sDescription = null, string sWebsite = null, string sLogoUrl = null,string sContent = null, Boolean xPublic = false)
        {
            Check_ID(iID);Check_Name(sMes);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                int iPublic;
                if (xPublic) { iPublic = 1; } else { iPublic = 0; }
                string sSQl = @"update MesSoftware set meName = @meName, meDescription = @meDescription, 
                                meWebsite = @meWebsite, meContent = @meContent, mePublic = @mePublic,meLogoUrl = @meLogoUrl
                                where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@meName", sMes));
                oCmd.Parameters.Add(new SqlParameter("@meDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@meWebsite", sWebsite ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@meContent", sContent ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@meLogoUrl", sLogoUrl ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@mePublic", iPublic));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_MesContent(int iID, string sContent)
        {
            Check_ID(iID); Check_Name(sContent);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"update MesSoftware set meContent = @meContent  where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@meContent", sContent ?? (object)DBNull.Value));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_MesVisibility(int iId, bool xPublic)
        {
            Check_ID(iId);

            int iPublic = 0;
            if (xPublic)
                iPublic = 1;

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update MesSoftware set mePublic = @mePublic where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@mePublic", iPublic));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Show_MesContent(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Show_MesContent(oM.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Show_MesContent(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update MesSoftware set mePublic = 1
                                where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Hide_MesContent(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Hide_MesContent(oM.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Hide_MesContent(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update MesSoftware set mePublic = 0
                                where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.ExecuteNonQuery();
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_Mes(clsBaseModel oMes)
        {
            Check_Class(oMes);
            if (oMes.GetType() == typeof(clsSoftware))
            {
                clsSoftware oM = (clsSoftware)oMes;
                return Delete_Mes(oM.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Mes(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"delete from MesSoftware where meId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
    }

}
