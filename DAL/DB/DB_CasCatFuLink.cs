﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        public static int Add_CasCatFuLink(int iCase, int iCategory, int iFunction)
        {
            Check_Case(iCase); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                //if (!Check_CaseCategoryFunction_Exist(iCase, iCategory, iFunction)) { return 0; }
                //if (!Check_CaseCategoryFunction_NotLinked(iCase, iCategory, iFunction)) { return 0; }

                oCnn.Open();

                string sSQl = @"insert into CasCatFuLink (ccflCase,ccflCategory,ccflFunction)
                values(@ccflCase,@ccflCategory,@ccflFunction);
                select ccflID from CasCatFuLink where ccflCase = @ccflCase and ccflCategory = @ccflCategory and ccflFunction = @ccflFunction
                order by ccflID desc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ccflCase", iCase));
                oCmd.Parameters.Add(new SqlParameter("@ccflCategory", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@ccflFunction", iFunction));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Edit_CasCatFuLink(int iId, int iCase, int iCategory, int iFunction)
        {
            Check_ID(iId);  Check_Case(iCase); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CaseCategoryFunction_Exist(iCase, iCategory, iFunction)) { return false; }
                //if (!Check_CaseCategoryFunction_Id(iId)) { return false; }

                oCnn.Open();
                string sSQl = @"
                    update CasCatFuLink
                    set ccflCase = @Case, ccflCategory = @Category, ccflFunction = @Function
                    where ccflId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@Case", iCase));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@Function", iFunction));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Delete_CasCatFuLink_WithId(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CaseCategoryFunction_Id(iId)) { return false; }

                oCnn.Open();

                string sSQl = @"delete from CasCatFuLink where ccflId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Delete_CasCatFuLink_WithCase(int iCase)
        {
            Check_Case(iCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CaseCategoryFunction_Case(iCase)) { return false; }

                oCnn.Open();

                string sSQl = @"delete from CasCatFuLink where ccflCase = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iCase));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// oLinks = null --> de volledige tabel terug krijgen |
        /// Per lijst heb je verschillende mogelijkheden, 
        /// ofwel is de lijst null --> er wordt geen rekening gehouden met deze variabelen,
        /// ofwel bevat de lijst 0 --> deze parameter moet DBNull zijn,
        /// ofwel bevat de lijst een aantal  ints --> de parameter moet tot een van deze waarden horen
        /// </summary>
        /// <param name="oLinks"></param>
        /// <returns></returns>
        public static List<clsLink> Get_CasCatFuLink(clsLinks oLinks = null)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "SELECT * FROM CasCatFuLink";

                // opstellen where clausule
                if (oLinks != null)
                {
                    List<int> oCase = oLinks.Case;
                    List<int> oCategory = oLinks.Category;
                    List<int> oFunction = oLinks.Function;

                    if ((oCase != null && oCase.Count > 0) || (oCategory != null && oCategory.Count > 0) || (oFunction != null && oFunction.Count > 0)) { sSQL += " where "; }

                    if (oCase != null && oCase.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oCase.Contains(0)) { throw new Exception("Input parameter is wrong! Case can't be DBNULL!"); } // een case kan niet DBNULL zijn!                     
                        sSQL += " ccflCase  in (";

                        for (int i = 0; i < oCase.Count; i++)
                        {
                            if (i != oCase.Count - 1) { sSQL += "@CasePara" + i + ","; }
                            else { sSQL += "@CasePara" + i; }
                        }

                        sSQL += ") ";
                        if ((oCategory != null && oCategory.Count > 0) || (oFunction != null && oFunction.Count > 0)) { sSQL += " and "; }
                    }

                    if (oCategory != null && oCategory.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oCategory.Contains(0)) { throw new Exception("Input parameter is wrong! Category can't be DBNULL!"); } // een category kan niet DBNULL zijn!                     
                        sSQL += " ccflCategory  in (";

                        for (int i = 0; i < oCategory.Count; i++)
                        {
                            if (i != oCategory.Count - 1) { sSQL += "@CategoryPara" + i + ","; }
                            else { sSQL += "@CategoryPara" + i; }
                        }

                        sSQL += ") ";

                        if ((oFunction != null && oFunction.Count > 0)) { sSQL += " and "; }
                    }

                    if (oFunction != null && oFunction.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oFunction.Contains(0)) { throw new Exception("Input parameter is wrong! Function can't be DBNULL!"); } // een function kan niet DBNULL zijn!                     
                        sSQL += " ccflFunction  in (";

                        for (int i = 0; i < oFunction.Count; i++)
                        {
                            if (i != oFunction.Count - 1) { sSQL += "@FunctionPara" + i + ","; }
                            else { sSQL += "@FunctionPara" + i; }
                        }

                        sSQL += ") ";

                    }

                }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                if (oLinks != null)
                {
                    List<int> oCase = oLinks.Case;
                    List<int> oCategory = oLinks.Category;
                    List<int> oFunction = oLinks.Function;

                    if (oCase != null)
                    {
                        for (int i = 0; i < oCase.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@CasePara" + i, oCase[i])); }
                    }

                    if (oCategory != null)
                    {
                        for (int i = 0; i < oCategory.Count; i++)
                        {
                            if (oCategory[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@CategoryPara" + i, oCategory[i])); }
                        }
                    }

                    if (oFunction != null)
                    {
                        for (int i = 0; i < oFunction.Count; i++)
                        {
                            if (oFunction[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@FunctionPara" + i, oFunction[i])); }
                        }
                    }

                }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0) { return new List<clsLink>(); }

                var oList = new List<clsLink>();

                foreach (DataRow r in oDT.Rows)
                {
                    var oLink = new clsLink();
                    if (r[0] != DBNull.Value) { oLink.Id = (int)r[0]; }
                    if (r[1] != DBNull.Value) { oLink.Case = (int)r[1]; }
                    if (r[2] != DBNull.Value) { oLink.Category = (int)r[2]; }
                    if (r[3] != DBNull.Value) { oLink.Function = (int)r[3]; }
                    oList.Add(oLink);
                }
                return oList;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static Boolean Check_CaseCategoryFunction_Exist(int iCase, int iCategory, int iFunction)
        {
            Check_Case(iCase); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select casId from Cases where casId = @casId;
                select catId from Category where catId = @catId;
                select fuId from Functions where fuId = @fuId;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@casId", iCase));
                oCmd.Parameters.Add(new SqlParameter("@catId", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@fuId", iFunction));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CaseCategoryFunction_NotLinked(int iCase, int iCategory, int iFunction)
        {
            Check_Case(iCase); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select ccflId from CasCatFuLink
                    where ccflCase = @Case and ccflCategory = @Category and ccflFunction = @Function";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Case", iCase));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@Function", iFunction));


                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return true; }

                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CaseCategoryFunction_Id(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select ccflId from CasCatFuLink where ccflId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CaseCategoryFunction_Case(int iCase)
        {
            Check_Case(iCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select ccflId from CasCatFuLink where ccflCase = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iCase));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        
    }
}
