﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        /// <summary>
        /// oLinks = null --> de volledige tabel terug krijgen |
        /// Per lijst heb je verschillende mogelijkheden, 
        /// ofwel is de lijst null --> er wordt geen rekening gehouden met deze variabelen,
        /// ofwel bevat de lijst 0 --> deze parameter moet DBNull zijn,
        /// ofwel bevat de lijst een aantal  ints --> de parameter moet tot een van deze waarden horen
        /// </summary>
        /// <param name="oLinks"></param>
        /// <returns></returns>
        public static List<clsFullLink> Get_FullyLinked(clsFullLinks oLinks = null)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "SELECT * FROM FullyLinked";

                // opstellen where clausule
                if (oLinks != null)
                {

                    List<int> oCompany = oLinks.Company;
                    List<int> oActivity = oLinks.Activity;
                    List<int> oMes = oLinks.Mes;
                    List<int> oSector = oLinks.Sector;
                    List<int> oCase = oLinks.Case;
                    List<int> oMesCategory = oLinks.MesCategory;
                    List<int> oMesFunction = oLinks.MesFunction;
                    List<int> oCaseCategory = oLinks.CaseCategory;
                    List<int> oCaseFunction = oLinks.CaseFunction;

                    if ((oCompany != null && oCompany.Count > 0) || (oActivity != null && oActivity.Count > 0)|| (oMes != null && oMes.Count > 0) 
                        || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)
                        || (oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                        || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                        { sSQL += " where "; }

                    if (oCompany != null && oCompany.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oCompany.Contains(0)) { throw new Exception("Input parameter is wrong! Company can't be DBNULL!"); } // een company kan niet DBNULL zijn!                     
                        
                        sSQL += " Company  in (";
                        for (int i = 0; i < oCompany.Count; i++)
                            if (i != oCompany.Count - 1) { sSQL += "@CompanyPara" + i + ","; }  else { sSQL += "@CompanyPara" + i; }
                        sSQL += ") ";

                        if ((oActivity != null && oActivity.Count > 0) || (oMes != null && oMes.Count > 0) 
                            || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)
                            || (oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                            || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                            { sSQL += " and "; }
                    }
                    if (oActivity != null && oActivity.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oActivity.Contains(0)) { throw new Exception("Input parameter is wrong! Activity can't be DBNULL!"); } // een activity kan niet DBNULL zijn!                     
                        
                        sSQL += " Activity  in (";
                        for (int i = 0; i < oActivity.Count; i++)
                            if (i != oActivity.Count - 1) { sSQL += "@ActivityPara" + i + ","; } else { sSQL += "@ActivityPara" + i; }
                        sSQL += ") ";

                        if ((oMes != null && oMes.Count > 0) || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)
                            || (oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                            || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                            { sSQL += " and "; }
                    }
                    if (oMes != null && oMes.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oMes.Contains(0)) { throw new Exception("Input parameter is wrong! Mes can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     
                        
                        sSQL += " Mes in (";
                        for (int i = 0; i < oMes.Count; i++)
                            if (i != oMes.Count - 1) { sSQL += "@MesPara" + i + ","; } else { sSQL += "@MesPara" + i; }
                        sSQL += ") ";

                        if ((oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)
                            || (oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                            || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                            { sSQL += " and "; }
                    }
                    if (oSector != null && oSector.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oSector.Contains(0))
                        {
                            // een sector kan DBNULL zijn!
                            sSQL += " (Sector is null ";
                            oSector.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oSector.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " Sector  in (";
                            for (int i = 0; i < oSector.Count; i++)
                            {
                                if (i != oSector.Count - 1) { sSQL += "@SectorPara" + i + ","; }
                                else { sSQL += "@SectorPara" + i; }
                            }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                        if ((oCase != null && oCase.Count > 0)
                            || (oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                            || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                            { sSQL += " and "; }
                    }
                    if (oCase != null && oCase.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oCase.Contains(0))
                        {
                            // een sector kan DBNULL zijn!
                            sSQL += " (Cases is null ";
                            oCase.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oCase.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " Cases  in (";
                            for (int i = 0; i < oCase.Count; i++)
                            {
                                if (i != oCase.Count - 1) { sSQL += "@CasePara" + i + ","; }
                                else { sSQL += "@CasePara" + i; }
                            }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                        if ((oMesCategory != null && oMesCategory.Count > 0) || (oMesFunction != null && oMesFunction.Count > 0)
                            || (oCaseCategory != null && oCaseCategory.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                            { sSQL += " and "; }
                    }

                    if ((oMesCategory != null && oMesCategory.Count > 0) && (oCaseCategory != null && oCaseCategory.Count > 0))// er moet rekening gehouden worden met de variabele
                    {
                        if (oMesCategory.Contains(0)) { throw new Exception("Input parameter is wrong! A mes category can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     

                        sSQL += " (MesCategory in (";
                        for (int i = 0; i < oMesCategory.Count; i++)
                            if (i != oMesCategory.Count - 1) { sSQL += "@MesCategoryPara" + i + ","; } else { sSQL += "@MescategoryPara" + i; }
                        sSQL += ") or CaseCategory  in (";

                        for (int i = 0; i < oCaseCategory.Count; i++)
                            if (i != oCaseCategory.Count - 1) { sSQL += "@CaseCategoryPara" + i + ","; } else { sSQL += "@CasecategoryPara" + i; }

                        sSQL += ")) ";

                        if ((oMesFunction != null && oMesFunction.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0)){ sSQL += " and "; }

                    }
                    else if (oMesCategory != null && oMesCategory.Count > 0)
                    {
                        if (oMesCategory.Contains(0)) { throw new Exception("Input parameter is wrong! A mes category can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     

                        sSQL += " MesCategory in (";
                        for (int i = 0; i < oMesCategory.Count; i++)
                            if (i != oMesCategory.Count - 1) { sSQL += "@MesCategoryPara" + i + ","; } else { sSQL += "@MescategoryPara" + i; }
                        sSQL += ") ";

                        if ((oMesFunction != null && oMesFunction.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0))
                        { sSQL += " and "; }
                    }
                    else if (oCaseCategory != null && oCaseCategory.Count > 0) 
                    {
                        var xContainedNull = false;
                        if (oCaseCategory.Contains(0))
                        {
                            sSQL += " (CaseCategory is null ";
                            oCaseCategory.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oCaseCategory.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " CaseCategory  in (";
                            for (int i = 0; i < oCaseCategory.Count; i++)
                                if (i != oCaseCategory.Count - 1) { sSQL += "@CaseCategoryPara" + i + ","; } else { sSQL += "@CasecategoryPara" + i; }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                        if ((oMesFunction != null && oMesFunction.Count > 0) || (oCaseFunction != null && oCaseFunction.Count > 0)){ sSQL += " and "; }

                    }
                    if ((oMesFunction != null && oMesFunction.Count > 0) && (oCaseFunction != null && oCaseFunction.Count > 0)) // er moet rekening gehouden worden met de variabele
                    {
                        if (oMesFunction.Contains(0)) { throw new Exception("Input parameter is wrong! A mes function can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     

                        sSQL += " (MesFunction in (";
                        for (int i = 0; i < oMesFunction.Count; i++)
                            if (i != oMesFunction.Count - 1) { sSQL += "@MesFunctionPara" + i + ","; } else { sSQL += "@MesFunctionPara" + i; }
                        sSQL += ") or CaseFunction in (";
                        for (int i = 0; i < oCaseFunction.Count; i++)
                            if (i != oCaseFunction.Count - 1) { sSQL += "@CaseFunctionPara" + i + ","; } else { sSQL += "@CaseFunctionPara" + i; }
                        sSQL += ")) ";

                    }
                    else if (oMesFunction != null && oMesFunction.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oMesFunction.Contains(0)) { throw new Exception("Input parameter is wrong! A mes function can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     

                        sSQL += " MesFunction in (";
                        for (int i = 0; i < oMesFunction.Count; i++)
                            if (i != oMesFunction.Count - 1) { sSQL += "@MesFunctionPara" + i + ","; } else { sSQL += "@MesFunctionPara" + i; }
                        sSQL += ") ";

                    }
                    else if (oCaseFunction != null && oCaseFunction.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oCaseFunction.Contains(0))
                        {
                            sSQL += " (CaseFunction is null ";
                            oCaseFunction.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oCaseFunction.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " CaseFunction  in (";
                            for (int i = 0; i < oCaseFunction.Count; i++)
                                if (i != oCaseFunction.Count - 1) { sSQL += "@CaseFunctionPara" + i + ","; } else { sSQL += "@CaseFunctionPara" + i; }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                    }
                   
                }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                //instellen van de parameters
                if (oLinks != null)
                {
                    List<int> oCompany = oLinks.Company;
                    List<int> oActivity = oLinks.Activity;
                    List<int> oMes = oLinks.Mes;
                    List<int> oSector = oLinks.Sector;
                    List<int> oCase = oLinks.Case;
                    List<int> oMesCategory = oLinks.MesCategory;
                    List<int> oMesFunction = oLinks.MesFunction;
                    List<int> oCaseCategory = oLinks.CaseCategory;
                    List<int> oCaseFunction = oLinks.CaseFunction;

                    if (oCompany != null) {for (int i = 0; i < oCompany.Count; i++){ oCmd.Parameters.Add(new SqlParameter("@CompanyPara" + i, oCompany[i])); }}
                    if (oActivity != null) { for (int i = 0; i < oActivity.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@ActivityPara" + i, oActivity[i])); }}
                    if (oMes != null) { for (int i = 0; i < oMes.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@MesPara" + i, oMes[i])); }}
                    if (oSector != null) {for (int i = 0; i < oSector.Count; i++) {if (oSector[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@SectorPara" + i, oSector[i])); }}}
                    if (oCase != null){for (int i = 0; i < oCase.Count; i++) { if (oCase[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@CasePara" + i, oCase[i])); }}}
                    if (oMesCategory != null) { for (int i = 0; i < oMesCategory.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@MesCategoryPara" + i, oMesCategory[i])); }}
                    if (oMesFunction != null) { for (int i = 0; i < oMesFunction.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@MesFunctionPara" + i, oMesFunction[i])); }}
                    if (oCaseCategory != null) { for (int i = 0; i < oCaseCategory.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@CaseCategoryPara" + i, oCaseCategory[i])); } }
                    if (oCaseFunction != null) { for (int i = 0; i < oCaseFunction.Count; i++) { oCmd.Parameters.Add(new SqlParameter("@CaseFunctionPara" + i, oCaseFunction[i])); } }
                }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0) { return new List<clsFullLink>(); }

                var oList = new List<clsFullLink>();

                foreach (DataRow r in oDT.Rows)
                {
                    var oLink = new clsFullLink();
                    if (r[0] != DBNull.Value) { oLink.Company = (int)r[0]; }
                    if (r[1] != DBNull.Value) { oLink.Activity = (int)r[1]; }
                    if (r[2] != DBNull.Value) { oLink.Mes = (int)r[2]; }
                    if (r[3] != DBNull.Value) { oLink.Sector = (int)r[3]; }
                    if (r[4] != DBNull.Value) { oLink.Case = (int)r[4]; }
                    if (r[5] != DBNull.Value) { oLink.MesCategory = (int)r[5]; }
                    if (r[6] != DBNull.Value) { oLink.MesFunction = (int)r[6]; }
                    if (r[7] != DBNull.Value) { oLink.CaseCategory = (int)r[7]; }
                    if (r[8] != DBNull.Value) { oLink.CaseFunction = (int)r[8]; }
                    oList.Add(oLink);
                }
                return oList;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

    }
}
