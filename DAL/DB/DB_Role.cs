﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
namespace DAL.DB
{
    public partial class clsDB
    {
        public static List<clsRole> Get_Roles()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select Id, Name, roDescription from AspNetRoles order by Name asc";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);
                 
                List<clsRole> oRoles = new List<clsRole>();

                if (oDT.Rows.Count > 0)
                {
                    List<clsUser> oUsers = new List<clsUser>();

                    foreach (DataRow oRow in oDT.Rows)
                    {
                        clsRole oRole;
                        int iId = 0; int.TryParse(oRow[0].ToString(),out iId);
                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription  = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();

                        oRole = new clsRole(iId,sName,sDescription);
                     
                        oRoles.Add(oRole);
                    }
                    return oRoles;

                }
                else { return new List<clsRole>(); ; }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

    }
}
