﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        public static int Add_Case(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Add_Case(oC.Name, oC.Description, oC.Website,oC.LogoUrl);
            }
            else { WrongModelType(); return 0; }       
        }
        public static int Add_Case(string sCase, string sDescription = null, string sWebsite = null, string sLogoUrl = null)
        {
            Check_Name(sCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"insert into Cases (casName,casDescription,casWebsite,casLogoUrl)
                values(@casName, @casDescription, @casWebsite,@casLogoUrl);
                select top(1) casID from Cases where casName = @casName";
                if (sDescription != null) { sSQl += " and casDescription = @casDescription"; }
                if (sWebsite != null) { sSQl += " and casWebsite = @casWebsite"; }
                if (sLogoUrl != null) { sSQl += " and casLogoUrl = @casLogoUrl"; }
                sSQl += " order by casId desc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@casName", sCase));
                oCmd.Parameters.Add(new SqlParameter("@casDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@casWebsite", sWebsite ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@casLogoUrl", sLogoUrl ?? (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static clsBaseModel Get_Case(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Get_Case(oC.Id);
            }
            else { WrongModelType(); return null; }
            
        }
        public static clsBaseModel Get_Case(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select casId,casName,casDescription, casWebsite, casLogoUrl,casPublic
                                from Cases where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                clsCase oCase;

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sLogoUrl = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                    oCase = new clsCase(ID, sName, sDescription, sWebsite, sLogoUrl);
                    
                    if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oCase.Public = false; } else { oCase.Public = true; }
                    return oCase;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }     
        
        public static List<clsBaseModel> Get_Cases()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select casId, casName,casDescription, casWebsite,casLogoUrl,casPublic from Cases order by casName asc";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsBaseModel> oCases = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];
                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                        string sLogoUrl = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                        var oCase = new clsCase(ID, sName, sDescription, sWebsite, sLogoUrl);
                        if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oCase.Public = false; } else { oCase.Public = true; }

                        oCases.Add(oCase);
                    }

                    return oCases;
                }
                else { return new List<clsBaseModel>(); }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static clsBaseModel Get_CaseContent(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Get_CaseContent(oC.Id);
            }
            else { WrongModelType(); return null; }
        }
        public static clsBaseModel Get_CaseContent(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select casId,casName,casDescription, casWebsite,casContent,casPublic,casLogoUrl
                                from Cases where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];

                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    string sWebsite = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                    string sLogoUrl = (oRow[6] == DBNull.Value) ? null : oRow[6].ToString();
                    var oCase = new clsCase(ID, sName, sDescription, sWebsite, sLogoUrl);
                    oCase.Content = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();

                    if (oRow[5] == DBNull.Value || (int)oRow[5] == 0) { oCase.Public = false; } else { oCase.Public = true; }
                    return oCase;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Edit_Case(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Edit_Case(oC.Id, oC.Name, oC.Description, oC.Website,oC.LogoUrl);
            }
            else { WrongModelType(); return 0; }            
        }
        public static int Edit_Case(int iID, string sName, string sDescription = null, string sWebsite = null,string sLogoUrl = null)
        {
            Check_ID(iID); Check_AdvancedBaseModel_Edit(sName, sDescription, sWebsite, sLogoUrl);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update Cases set ";
                object oN = "";
                if (sName != "")
                {
                    sSQL += " casName = @Name ";
                    oN = sName;
                    if (sDescription != "" || sWebsite != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oD = "";
                if (sDescription != "")
                {
                    sSQL += " casDescription = @Description ";
                    if (sDescription == null) { oD = DBNull.Value; ; } else { oD = sDescription; ; }
                    if (sWebsite != "" || sLogoUrl != "") { sSQL += ","; }
                }
                object oW = "";
                if (sWebsite != "")
                {
                    sSQL += " casWebsite = @Website ";
                    if (sWebsite == null) { oW = DBNull.Value; ; } else { oW = sWebsite; }
                    if (sLogoUrl != "") { sSQL += ","; }
                }
                object oL = "";
                if (sLogoUrl != "")
                {
                    sSQL += " casLogoUrl = @LogoUrl ";
                    if (sLogoUrl == null) { oL = DBNull.Value; ; } else { oL = sLogoUrl; }
                }

                sSQL += "  where casId = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Name", oN));
                oCmd.Parameters.Add(new SqlParameter("@Description", oD));
                oCmd.Parameters.Add(new SqlParameter("@Website", oW));
                oCmd.Parameters.Add(new SqlParameter("@LogoUrl", oL));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_CaseFull(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Edit_CaseFull(oC.Id, oC.Name, oC.Description, oC.Website, oC.LogoUrl,oC.Content, oC.Public);
            }
            else { WrongModelType(); return 0; }       
        }
        public static int Edit_CaseFull(int iID, string sCase, string sDescription = null, string sWebsite = null, string sLogoUrl = null,string sContent = null, bool xPublic = false)
        {
            Check_ID(iID); Check_Name(sCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                int iPublic;
                if (xPublic) { iPublic = 1; } else { iPublic = 0; }
                string sSQl = @"update Cases set casName = @casName, casDescription = @casDescription, 
                                casWebsite = @casWebsite, casContent = @casContent, casPublic = @casPublic, casLogoUrl = @casLogoUrl
                                where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@casName", sCase));
                oCmd.Parameters.Add(new SqlParameter("@casDescription", sDescription ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@casWebsite", sWebsite ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@casContent", sContent ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@caslogoUrl", sLogoUrl ?? (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@casPublic", iPublic));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_CaseContent(int iID, string sContent)
        {
            Check_ID(iID); Check_Name(sContent);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"update Cases set casContent = @casContent where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@casContent", sContent ?? (object)DBNull.Value));

                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_CaseVisibility(int iId, bool xPublic)
        {
            Check_ID(iId);

            int iPublic = 0;
            if (xPublic)
                iPublic = 1;

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Cases set casPublic = @casPublic where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@casPublic", iPublic));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Show_CaseContent(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Show_CaseContent(oC.Id);
            }
            else { WrongModelType(); return 0; }            
        }
        public static int Show_CaseContent(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Cases set casPublic = 1
                                where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
         
        public static int Hide_CaseContent(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Hide_CaseContent(oC.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Hide_CaseContent(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();
                string sSQl = @"update Cases set casPublic = 0
                                where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();                
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        
        public static int Delete_Case(clsBaseModel oCase)
        {
            Check_Class(oCase);
            if (oCase.GetType() == typeof(clsCase))
            {
                clsCase oC = (clsCase)oCase;
                return Delete_Case(oC.Id);
            }
            else { WrongModelType(); return 0; }
        }
        public static int Delete_Case(int iID)
        {
            Check_ID(iID);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"delete from Cases where casId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }
        
    }

}
