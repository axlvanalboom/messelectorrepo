﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        private static int Add_BaseModel(clsBaseModel oBaseModel,string sTable, string sPrefix)
        {
            Check_Class(oBaseModel);
            if (oBaseModel.GetType() == typeof(clsBaseModel))
            {
                clsBaseModel oBase = (clsBaseModel)oBaseModel;
                return Add_BaseModel(oBase.Name, oBase.Description, sTable, sPrefix);
            }
            else { WrongModelType(); return 0; }
        }
        private static int Add_BaseModel(string sName, string sDescription, string sTable, string sPrefix)
        {
            Check_Name(sName); Check_BaseModel_Prefix(sPrefix); Check_BaseModel_Table(sTable);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "";
                sSQL += "insert into " + sTable + "(" + sPrefix + "Name," + sPrefix + "Description)";
                sSQL += "values (@Name,@Description);";
                sSQL += "select "+sPrefix + "ID from " + sTable +" where " + sPrefix + "Name = @Name;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Name", sName));
                oCmd.Parameters.Add(new SqlParameter("@Description", sDescription ?? (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static clsBaseModel Get_BaseModel(clsBaseModel oBaseModel, string sTable, string sPrefix)
        {
            Check_Class(oBaseModel);
            return Get_BaseModel(oBaseModel.Id,sTable,sPrefix);
        }
        private static clsBaseModel Get_BaseModel(int iID, string sTable, string sPrefix)
        {
            Check_ID(iID); Check_BaseModel_Prefix(sPrefix); Check_BaseModel_Table(sTable);

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select " + sPrefix + "Id," + sPrefix + "Name, " + sPrefix + "Description";
                sSQL += "from " + sTable + " where " + sPrefix + "Id = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                if (oDT.Rows.Count > 0)
                {
                    var oRow = oDT.Rows[0];
                    var ID = (int)oRow[0];
                    string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                    string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                    return Make_BaseModel(sTable, sPrefix, ID, sName, sDescription);
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static List<clsBaseModel> Get_BaseModels(string sTable, string sPrefix)
        {
            Check_BaseModel_Prefix(sPrefix); Check_BaseModel_Table(sTable);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select " + sPrefix + "Id," + sPrefix + "Name, " + sPrefix + "Description from " + sTable + ";";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsBaseModel> oBaseModels;
                oBaseModels = new List<clsBaseModel>();

                if (oDT.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oDT.Rows)
                    {
                        var ID = (int)oRow[0];
                        string sName = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        string sDescription = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        var o = Make_BaseModel(sTable, sPrefix, ID, sName, sDescription);
                        if (o == null) { return null; }
                        else { oBaseModels.Add(o); }                       
                    }

                    return oBaseModels;
                }
                else { return null; }
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }


        public static int Delete_BaseModel(clsBaseModel oBaseModel, string sTable, string sPrefix)
        {
            Check_Class(oBaseModel);
            return Delete_BaseModel(oBaseModel.Id,sTable,sPrefix);
        }
        public static int Delete_BaseModel(int iID, string sTable, string sPrefix)
        {
            Check_ID(iID); Check_BaseModel_Prefix(sPrefix); Check_BaseModel_Table(sTable);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "delete from @Table where @preId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iID));
                oCmd.Parameters.Add(new SqlParameter("@Table", sTable));
                oCmd.Parameters.Add(new SqlParameter("@preId",sPrefix + "ID"));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }


        private static void Check_BaseModel_Prefix(string sPrefix)
        {
            if (sPrefix == null) { throw new Exception("Prefix is null!"); }
            if (sPrefix == "") { throw new Exception("Prefix is empty!"); }
            var oList = new List<string>() { "ac", "cat", "fu", "se" };
            if (!oList.Contains(sPrefix)) { throw new Exception("Prefix is wrong!"); }
        }
        private static void Check_BaseModel_Table(string sTable)
        {
            if ( sTable == null) { throw new Exception("Table is null!"); }
            if (sTable == "") { throw new Exception("Table is empty!"); }
            var oList = new List<string>() { "Activity","Category","Functions","Sector"};
            if (!oList.Contains(sTable)) { throw new Exception("Table is wrong!"); }
        }
        private static clsBaseModel Make_BaseModel(string sTable, string sPrefix, int iID, string sName, string sDescription)
        {
            if (sTable == "Activity" && sPrefix =="ac") { return new clsActivity(iID, sName, sDescription); }
            else if(sTable == "Category" && sPrefix == "cat") { return new clsCategory(iID, sName, sDescription); }
            else if(sTable == "Function" && sPrefix == "fu") { return new clsFunction(iID, sName, sDescription); }
            else if(sTable == "Sector" && sPrefix == "se") { return new clsSector(iID, sName, sDescription); }
            return null;
        }
    }
}
