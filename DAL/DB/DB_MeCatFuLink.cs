﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;

namespace DAL.DB
{
    public partial class clsDB
    {        
  
        public static int Add_MeCatFuLink(int iMes, int iCategory, int iFunction)
        {
            Check_Mes(iMes); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
              
                //if (!Check_MesCategoryFunction_Exist(iMes,iCategory,iFunction)) { return 0; }
                //if (!Check_MesCategoryFunction_NotLinked(iMes,iCategory,iFunction)) { return 0; }

                oCnn.Open();

                string sSQl = @"insert into MeCatFuLink (mcflMes,mcflCategory,mcflFunction)
                values(@mcflMes,@mcflCategory,@mcflFunction);
                select mcflID from MeCatFuLink where mcflMes = @mcflMes and mcflCategory = @mcflCategory and mcflFunction = @mcflFunction
                order by mcflID desc;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@mcflMes", iMes));
                oCmd.Parameters.Add(new SqlParameter("@mcflCategory", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@mcflFunction", iFunction));
                
                int i = (int)oCmd.ExecuteScalar();

                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// parameter = 0 || "" --> don't change
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="iMes"></param>
        /// <param name="iCategory"></param>
        /// <param name="iFunction"></param>
        /// <returns></returns>
        public static int Edit_MeCatFuLink(int iId, int iMes, int iCategory, int iFunction)
        {
            Check_ID(iId); Check_Mes(iMes); Check_CategoryFunction(iCategory, iFunction);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_MesCategoryFunction_Exist(iMes, iCategory, iFunction)) { return false; }
                //if (!Check_MesCategoryFunction_Id(iId)) { return false; }

                oCnn.Open();
                string sSQl = @"
                    update MeCatFuLink
                    set mcflMes = @Mes, mcflCategory = @Category, mcflFunction = @Function
                    where mcflId = @ID";  
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@Function", iFunction));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_MeCatFuLink_WithId(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_MesCategoryFunction_Id(iId)) { return false; }

                oCnn.Open();

                string sSQl = @"delete from MeCatFuLink where mcflId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static int Delete_MeCatFuLink_WithMes(int iMes)
        {
            Check_Mes(iMes);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_MesCategoryFunction_Mes(iMes)) { return false; }

                oCnn.Open();

                string sSQl = @"delete from MeCatFuLink where mcflMes = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iMes));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// oLinks = null --> de volledige tabel terug krijgen |
        /// Per lijst heb je verschillende mogelijkheden, 
        /// ofwel is de lijst null --> er wordt geen rekening gehouden met deze variabelen,
        /// ofwel bevat de lijst 0 --> deze parameter moet DBNull zijn,
        /// ofwel bevat de lijst een aantal  ints --> de parameter moet tot een van deze waarden horen
        /// </summary>
        /// <param name="oLinks"></param>
        /// <returns></returns>
        public static List<clsLink> Get_MeCatFuLink(clsLinks oLinks = null)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "SELECT * FROM MeCatFuLink";

                // opstellen where clausule
                if (oLinks != null)
                {
                    List<int> oMes = oLinks.Mes;
                    List<int> oCategory = oLinks.Category;
                    List<int> oFunction = oLinks.Function;

                    if ((oMes != null && oMes.Count > 0) || (oCategory != null && oCategory.Count > 0) || (oFunction != null && oFunction.Count > 0)) { sSQL += " where "; }

                    if (oMes != null && oMes.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oMes.Contains(0)) { throw new Exception("Input parameter is wrong! Mes can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     
                        sSQL += " mcflMes  in (";

                        for (int i = 0; i < oMes.Count; i++)
                        {
                            if (i != oMes.Count - 1) { sSQL += "@MesPara" + i + ","; }
                            else { sSQL += "@MesPara" + i; }
                        }

                        sSQL += ") ";
                        if ((oCategory != null && oCategory.Count > 0) || (oFunction != null && oFunction.Count > 0)) { sSQL += " and "; }
                    }

                    if (oCategory != null && oCategory.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oCategory.Contains(0)) { throw new Exception("Input parameter is wrong! Category can't be DBNULL!"); } // een category kan niet DBNULL zijn!                     
                        sSQL += " mcflCategory  in (";

                        for (int i = 0; i < oCategory.Count; i++)
                        {
                            if (i != oCategory.Count - 1) { sSQL += "@CategoryPara" + i + ","; }
                            else { sSQL += "@CategoryPara" + i; }
                        }

                        sSQL += ") ";

                        if ((oFunction != null && oFunction.Count > 0)) { sSQL += " and "; }
                    }

                    if (oFunction != null && oFunction.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oFunction.Contains(0)) { throw new Exception("Input parameter is wrong! Function can't be DBNULL!"); } // een function kan niet DBNULL zijn!                     
                        sSQL += " mcflFunction  in (";

                        for (int i = 0; i < oFunction.Count; i++)
                        {
                            if (i != oFunction.Count - 1) { sSQL += "@FunctionPara" + i + ","; }
                            else { sSQL += "@FunctionPara" + i; }
                        }

                        sSQL += ") ";

                    }

                }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                if (oLinks != null)
                {
                    List<int> oMes = oLinks.Mes;
                    List<int> oCategory = oLinks.Category;
                    List<int> oFunction = oLinks.Function;

                    if (oMes != null)
                    {
                        for (int i = 0; i < oMes.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@MesPara" + i, oMes[i])); }
                    }

                    if (oCategory != null)
                    {
                        for (int i = 0; i < oCategory.Count; i++)
                        {
                            if (oCategory[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@CategoryPara" + i, oCategory[i])); }
                        }
                    }

                    if (oFunction != null)
                    {
                        for (int i = 0; i < oFunction.Count; i++)
                        {
                            if (oFunction[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@FunctionPara" + i, oFunction[i])); }
                        }
                    }

                }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0) { return new List<clsLink>(); }

                var oList = new List<clsLink>();

                foreach (DataRow r in oDT.Rows)
                {
                    var oLink = new clsLink();
                    if (r[0] != DBNull.Value) { oLink.Id = (int)r[0]; }
                    if (r[1] != DBNull.Value) { oLink.Mes = (int)r[1]; }
                    if (r[2] != DBNull.Value) { oLink.Category = (int)r[2]; }
                    if (r[3] != DBNull.Value) { oLink.Function = (int)r[3]; }
                    oList.Add(oLink);
                }
                return oList;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }

        private static Boolean Check_MesCategoryFunction_Exist(int iMes, int iCategory, int iFunction)
        {
            if (iMes == 0 || iCategory == 0 || iFunction == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select meId from MesSoftware where meId = @meId;
                select catId from Category where catId = @catId;
                select fuId from Functions where fuId = @fuId;";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@meId", iMes));
                oCmd.Parameters.Add(new SqlParameter("@catId", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@fuId", iFunction));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_MesCategoryFunction_NotLinked(int iMes, int iCategory, int iFunction)
        {
            if (iMes == 0 || iCategory == 0 || iFunction == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select mcflId from MeCatFuLink
                    where mcflMes = @Mes and mcflCategory = @Category and mcflFunction = @Function";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                oCmd.Parameters.Add(new SqlParameter("@Category", iCategory));
                oCmd.Parameters.Add(new SqlParameter("@Function", iFunction));


                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                 if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return true; }
                
                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_MesCategoryFunction_Id(int iId)
        {
            if (iId == 0 ) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select mcflId from MeCatFuLink where mcflId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_MesCategoryFunction_Mes(int iMes)
        {
            if (iMes == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = @"select mcflMes from MeCatFuLink where mcflMes = @ID";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iMes));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
           

        
    }
}
