﻿using BEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public partial class clsDB
    {

        public static int Add_ComAcMeSeCasLink(int iCompany, int iActivity, int iMes, int iSector, int iCase)
        {
            Check_ComAcMeSeCas_ComAcMes(iCompany, iActivity, iMes);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                //if (!Check_CompanyActivityMesSectorCase_Exist(iCompany, iActivity, iMes, iSector, iCase)) { return 0; }
                //if (!Check_CompanyActivityMesSectorCase_NotLinked(iCompany, iActivity, iMes, iSector, iCase)) { return 0; }
                //if (iCase != 0 && !Check_Case_NotLinked(iCase)) { return 0; }

                oCnn.Open();

                string sSQL = @"insert into ComMeCasSeAcLink (cmcsalCompany,cmcsalActivity,cmcsalMesSoftware,cmcsalSector,cmcsalCase)
                values(@Company, @Activity, @Mes,@Sector,@Case);
                select cmcsalID from ComMeCasSeAcLink where cmcsalCompany = @Company and cmcsalActivity = @Activity and cmcsalMesSoftware = @Mes";
                if (iSector != 0) { sSQL += " and cmcsalSector = @Sector"; }
                if (iCase != 0) { sSQL += " and cmcsalCase = @Case"; }
                sSQL += " order by cmcsalID desc; ";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", iCompany));
                oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity));
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                oCmd.Parameters.Add(new SqlParameter("@Sector", (iSector != 0) ? iSector : (object)DBNull.Value));
                oCmd.Parameters.Add(new SqlParameter("@Case", (iCase != 0) ? iCase : (object)DBNull.Value));

                int i = (int)oCmd.ExecuteScalar();
                return i;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// parameter = 0 --> don't change,
        /// parameter = null --> change to DBnull
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="iCompany"></param>
        /// <param name="iActivity"></param>
        /// <param name="iMes"></param>
        /// <param name="iSector"></param>
        /// <param name="iCase"></param>
        /// <returns></returns>
        public static int Edit_ComAcMeSeCasLink(int iId, int iCompany, int iActivity, int iMes, int? iSector, int? iCase)
        {
            Check_ID(iId);
            Check_ComAcMeSeCas_AllParameters(iCompany, iActivity, iMes, iSector, iCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CompanyActivityMesSectorCase_Exist2(iCompany, iActivity, iMes, iSector, iCase)) { return false; }
                //if (!Check_CompanyActivityMesSectorCase_Id(iId)) { return false; }

                oCnn.Open();
                string sSQL = "update ComMeCasSeAcLink set ";

                object oCo = 0;
                if (iCompany != 0)
                {
                    sSQL += " cmcsalCompany = @Company ";
                    oCo = iCompany;
                    if (iActivity != 0 || iMes != 0 || (iSector != 0) || (iCase != 0)) { sSQL += ","; }
                }
                object oA = "";
                if (iActivity != 0)
                {
                    sSQL += " cmcsalActivity = @Activity ";
                    oA = iActivity;
                    if (iMes != 0 || (iSector != 0) || (iCase != 0)) { sSQL += ","; }
                }
                object oM = "";
                if (iMes != 0)
                {
                    sSQL += " cmcsalMesSoftware = @Mes ";
                    oM = iMes;
                    if (iSector != 0 || (iCase != 0)) { sSQL += ","; }
                }
                object oS = "";
                if (iSector != 0)
                {
                    sSQL += " cmcsalSector = @Sector ";
                    if (iSector == null) { oS = DBNull.Value; ; } else { oS = iSector; ; }
                    if (iCase != 0) { sSQL += ","; }
                }
                object oCa = "";
                if (iCase != 0)
                {
                    sSQL += " cmcsalCase = @Case ";
                    if (iCase == null) { oCa = DBNull.Value; ; } else { oCa = iCase; ; }
                }

                sSQL += " where cmcsalId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                oCmd.Parameters.Add(new SqlParameter("@Company", oCo));
                oCmd.Parameters.Add(new SqlParameter("@Activity", oA));
                oCmd.Parameters.Add(new SqlParameter("@Mes", oM));
                oCmd.Parameters.Add(new SqlParameter("@Sector", oS));
                oCmd.Parameters.Add(new SqlParameter("@Case", oCa));

                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Delete_ComAcMeSeCasLink_WithId(int iId)
        {
            Check_ID(iId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CompanyActivityMesSectorCase_Id(iId)) { return false; }

                oCnn.Open();

                string sSQL = "delete from ComMeCasSeAcLink where cmcsalId = @ID;";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// parameter = 0 --> doet er niet toe in de zoekfunctie |
        /// parameter != 0 --> wordt gebruikt om te bepalen welke rijen weg mogen
        /// </summary>
        /// <param name="iCompany"></param>
        /// <param name="iActivity"></param>
        /// <param name="iMes"></param>
        /// <param name="iSector"></param>
        /// <param name="iCase"></param>
        /// <returns></returns>
        public static int Delete_ComAcMeSeCasLink(int iCompany, int iActivity, int iMes, int iSector, int iCase)
        {
            Check_ComAcMeSeCas_AllParameters(iCompany,iActivity,iMes,iSector,iCase);
            Check_ComAcMeSeCas_SecAc(iCompany, iActivity, iMes, iSector, iCase);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                //if (!Check_CompanyActivityMesSectorCase_Exist2(iCompany, iActivity, iMes, iSector, iCase)) { return false; } // bestaan ze wel?
                //if (!Check_CompanyActivityMesSectorCase_Linked(iCompany, iActivity, iMes, iSector, iCase)) { return false; } // bestaan ze wel?

                oCnn.Open();

                string sSQL = "delete from ComMeCasSeAcLink where ";
                if (iCompany != 0)
                {
                    sSQL += " cmcsalCompany = @Company ";
                    if (iActivity != 0 || iMes != 0 || iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iActivity != 0)
                {
                    sSQL += " cmcsalActivity = @Activity ";
                    if (iMes != 0 || iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iMes != 0)
                {
                    sSQL += " cmcsalMesSoftware = @Mes ";
                    if (iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iSector != 0)
                {
                    sSQL += " cmcsalSector = @Sector ";
                    if (iCase != 0) { sSQL += "and"; }
                }
                if (iCase != 0) { sSQL += " cmcsalCase = @Case "; }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                if (iCompany != 0) { oCmd.Parameters.Add(new SqlParameter("@Company", iCompany)); }
                if (iActivity != 0) { oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity)); }
                if (iMes != 0) { oCmd.Parameters.Add(new SqlParameter("@Mes", iMes)); }
                if (iSector != 0) { oCmd.Parameters.Add(new SqlParameter("@Sector", iSector)); }
                if (iCase != 0) { oCmd.Parameters.Add(new SqlParameter("@Case", iCase)); }
                return oCmd.ExecuteNonQuery();
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        /// <summary>
        /// oLinks = null --> de volledige tabel terug krijgen |
        /// Per lijst heb je verschillende mogelijkheden, 
        /// ofwel is de lijst null --> er wordt geen rekening gehouden met deze variabelen,
        /// ofwel bevat de lijst 0 --> deze parameter moet DBNull zijn,
        /// ofwel bevat de lijst een aantal  ints --> de parameter moet tot een van deze waarden horen
        /// </summary>
        /// <param name="oLinks"></param>
        /// <returns></returns>
        public static List<clsLink> Get_ComAcMeSeCasLink(clsLinks oLinks = null)
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {

                oCnn.Open();

                string sSQL = "SELECT * FROM ComMeCasSeAcLink";

                // opstellen where clausule
                if (oLinks != null)
                {

                    List<int> oCompany = oLinks.Company;
                    List<int> oActivity = oLinks.Activity;
                    List<int> oMes = oLinks.Mes;
                    List<int> oSector = oLinks.Sector;
                    List<int> oCase = oLinks.Case;

                    if ((oCompany != null && oCompany.Count > 0) || (oActivity != null && oActivity.Count > 0)
                        || (oMes != null && oMes.Count > 0) || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)) { sSQL += " where "; }

                    if (oCompany != null && oCompany.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oCompany.Contains(0)) { throw new Exception("Input parameter is wrong! Company can't be DBNULL!"); } // een company kan niet DBNULL zijn!                     
                        sSQL += " cmcsalCompany  in (";

                        for (int i = 0; i < oCompany.Count; i++)
                        {
                            if (i != oCompany.Count - 1) { sSQL += "@CompanyPara" + i + ","; }
                            else { sSQL += "@CompanyPara" + i; }

                        }

                        sSQL += ") ";

                        if ((oActivity != null && oActivity.Count > 0) || (oMes != null && oMes.Count > 0) || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)) { sSQL += " and "; }
                    }

                    if (oActivity != null && oActivity.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oActivity.Contains(0)) { throw new Exception("Input parameter is wrong! Activity can't be DBNULL!"); } // een activity kan niet DBNULL zijn!                     
                        sSQL += " cmcsalActivity  in (";

                        for (int i = 0; i < oActivity.Count; i++)
                        {
                            if (i != oActivity.Count - 1) { sSQL += "@ActivityPara" + i + ","; }
                            else { sSQL += "@ActivityPara" + i; }

                        }

                        sSQL += ") ";
                        if ((oMes != null && oMes.Count > 0) || (oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)) { sSQL += " and "; }

                    }

                    if (oMes != null && oMes.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        if (oMes.Contains(0)) { throw new Exception("Input parameter is wrong! Mes can't be DBNULL!"); } // een mes kan niet DBNULL zijn!                     
                        sSQL += " cmcsalMesSoftware  in (";

                        for (int i = 0; i < oMes.Count; i++)
                        {
                            if (i != oMes.Count - 1) { sSQL += "@MesPara" + i + ","; }
                            else { sSQL += "@MesPara" + i; }
                        }

                        sSQL += ") ";
                        if ((oSector != null && oSector.Count > 0) || (oCase != null && oCase.Count > 0)) { sSQL += " and "; }
                    }

                    if (oSector != null && oSector.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oSector.Contains(0))
                        {
                            // een sector kan DBNULL zijn!
                            sSQL += " (cmcsalSector is null ";
                            oSector.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oSector.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " cmcsalSector  in (";
                            for (int i = 0; i < oSector.Count; i++)
                            {
                                if (i != oSector.Count - 1) { sSQL += "@SectorPara" + i + ","; }
                                else { sSQL += "@SectorPara" + i; }
                            }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                        if ((oCase != null && oCase.Count > 0)) { sSQL += " and "; }
                    }

                    if (oCase != null && oCase.Count > 0) // er moet rekening gehouden worden met de variabele
                    {
                        var xContainedNull = false;
                        if (oCase.Contains(0))
                        {
                            // een sector kan DBNULL zijn!
                            sSQL += " (cmcsalCase is null ";
                            oCase.RemoveAll(i => i == 0);
                            xContainedNull = true;
                        }

                        if (oCase.Count > 0)
                        {
                            if (xContainedNull) { sSQL += " or "; }
                            sSQL += " cmcsalCase  in (";
                            for (int i = 0; i < oCase.Count; i++)
                            {
                                if (i != oCase.Count - 1) { sSQL += "@CasePara" + i + ","; }
                                else { sSQL += "@CasePara" + i; }
                            }

                            sSQL += ") ";
                        }

                        if (xContainedNull) { sSQL += ") "; }

                    }

                }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                if (oLinks != null)
                {
                    List<int> oCompany = oLinks.Company;
                    List<int> oActivity = oLinks.Activity;
                    List<int> oMes = oLinks.Mes;
                    List<int> oSector = oLinks.Sector;
                    List<int> oCase = oLinks.Case;

                    if (oCompany != null)
                    {
                        for (int i = 0; i < oCompany.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@CompanyPara" + i, oCompany[i])); }
                    }

                    if (oActivity != null)
                    {
                        for (int i = 0; i < oActivity.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@ActivityPara" + i, oActivity[i])); }
                    }

                    if (oMes != null)
                    {
                        for (int i = 0; i < oMes.Count; i++)
                        { oCmd.Parameters.Add(new SqlParameter("@MesPara" + i, oMes[i])); }
                    }

                    if (oSector != null)
                    {
                        for (int i = 0; i < oSector.Count; i++)
                        {
                            if (oSector[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@SectorPara" + i, oSector[i])); }
                        }
                    }

                    if (oCase != null)
                    {
                        for (int i = 0; i < oCase.Count; i++)
                        {
                            if (oCase[i] != 0) { oCmd.Parameters.Add(new SqlParameter("@CasePara" + i, oCase[i])); }
                        }
                    }

                }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0) { return new List<clsLink>(); }

                var oList = new List<clsLink>();

                foreach (DataRow r in oDT.Rows)
                {
                    var oLink = new clsLink();
                    if (r[0] != DBNull.Value) { oLink.Id = (int)r[0]; }
                    if (r[1] != DBNull.Value) { oLink.Company = (int)r[1]; }
                    if (r[2] != DBNull.Value) { oLink.Activity = (int)r[2]; }
                    if (r[3] != DBNull.Value) { oLink.Mes = (int)r[3]; }
                    if (r[4] != DBNull.Value) { oLink.Sector = (int)r[4]; }
                    if (r[5] != DBNull.Value) { oLink.Case = (int)r[5]; }
                    oList.Add(oLink);
                }
                return oList;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static Boolean Check_CompanyActivityMesSectorCase_Exist(int iCompany, int iActivity, int iMes, int iSector, int iCase)
        {
            if (iCompany == 0 || iActivity == 0 || iMes == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"select comId from Company where comId = @Company;
                select acId from Activity where acId = @Activity;
                select meId from MesSoftware where meId = @Mes;";
                if (iSector != 0) { sSQL += "select seId from Sector where seId = @Sector;"; }
                if (iCase != 0) { sSQL += "select casId from Cases where casId = @Case;"; }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", iCompany));
                oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity));
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                oCmd.Parameters.Add(new SqlParameter("@Sector", iSector));
                oCmd.Parameters.Add(new SqlParameter("@Case", iCase));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyActivityMesSectorCase_Exist2(int iCompany, int iActivity, int iMes, int? iSector, int? iCase)
        {
            if (iCompany == 0 && iActivity == 0 && iMes == 0 && iSector == 0 && iCase == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "";
                if (iCompany != 0) { sSQL += "select comId from Company where comId = @Company;"; }
                if (iActivity != 0) { sSQL += "select acId from Activity where acId = @Activity;"; }
                if (iMes != 0) { sSQL += "select meId from MesSoftware where meId = @Mes;"; }
                if (iSector != 0 && iSector != null) { sSQL += "select seId from Sector where seId = @Sector;"; }
                if (iCase != 0 && iCase != null) { sSQL += "select casId from Cases where casId = @Case;"; }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", iCompany));
                oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity));
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                if (iSector != 0 && iSector != null) { oCmd.Parameters.Add(new SqlParameter("@Sector", iSector)); }
                if (iCase != 0 && iCase != null) { oCmd.Parameters.Add(new SqlParameter("@Case", iCase)); }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataSet oDS = new DataSet();
                oDA.Fill(oDS);
                foreach (DataTable dt in oDS.Tables)
                {
                    if (dt.Rows.Count == 0 || dt.Rows[0][0] == DBNull.Value) { return false; }
                }
                return true;

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyActivityMesSectorCase_NotLinked(int iCompany, int iActivity, int iMes, int iSector, int iCase)
        {
            if (iCompany == 0 || iActivity == 0 || iMes == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select cmcsalID from ComMeCasSeAcLink where cmcsalCompany = @Company and cmcsalActivity = @Activity and cmcsalMesSoftware = @Mes";
                if (iSector != 0) { sSQL += " and cmcsalSector = @Sector"; }
                if (iCase != 0) { sSQL += " and cmcsalCase = @Case"; }
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Company", iCompany));
                oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity));
                oCmd.Parameters.Add(new SqlParameter("@Mes", iMes));
                if (iSector != 0) { oCmd.Parameters.Add(new SqlParameter("@Sector", iSector)); }
                if (iCase != 0) { oCmd.Parameters.Add(new SqlParameter("@Case", iCase)); }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return true; }

                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }
        private static Boolean Check_Case_NotLinked(int iCase)
        {
            if (iCase == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select cmcsalID from ComMeCasSeAcLink where cmcsalCase = @Case";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                { oCmd.Parameters.Add(new SqlParameter("@Case", iCase)); }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return true; }

                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        private static Boolean Check_CompanyActivityMesSectorCase_Id(int iId)
        {
            if (iId == 0) { return false; }

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = @"select cmcsalId from ComMeCasSeAcLink where cmcsalId = @ID";
                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", iId));

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);
                if (oDT.Rows.Count == 0 || oDT.Rows[0][0] == DBNull.Value) { return false; }
                return true;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        private static Boolean Check_CompanyActivityMesSectorCase_Linked(int iCompany, int iActivity, int iMes, int iSector, int iCase)
        {
            if (iCompany == 0 && iActivity == 0 && iMes == 0 && iSector == 0 && iCase == 0) { return false; }
            if ((iSector != 0 || iActivity != 0) && (iCompany == 0 && iMes == 0 && iCase == 0)) { return false; } // er moet een company, mes of case meegegeven worden

            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "select cmcsalID from ComMeCasSeAcLink where";
                if (iCompany != 0)
                {
                    sSQL += " cmcsalCompany = @Company ";
                    if (iActivity != 0 || iMes != 0 || iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iActivity != 0)
                {
                    sSQL += " cmcsalActivity = @Activity ";
                    if (iMes != 0 || iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iMes != 0)
                {
                    sSQL += " cmcsalMesSoftware = @Mes ";
                    if (iSector != 0 || iCase != 0) { sSQL += "and"; }
                }
                if (iSector != 0)
                {
                    sSQL += " cmcsalSector = @Sector ";
                    if (iCase != 0) { sSQL += "and"; }
                }
                if (iCase != 0) { sSQL += " cmcsalCase = @Case "; }

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);

                if (iCompany != 0) { oCmd.Parameters.Add(new SqlParameter("@Company", iCompany)); }
                if (iActivity != 0) { oCmd.Parameters.Add(new SqlParameter("@Activity", iActivity)); }
                if (iMes != 0) { oCmd.Parameters.Add(new SqlParameter("@Mes", iMes)); }
                if (iSector != 0) { oCmd.Parameters.Add(new SqlParameter("@Sector", iSector)); }
                if (iCase != 0) { oCmd.Parameters.Add(new SqlParameter("@Case", iCase)); }

                SqlDataAdapter oDA = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDA.Fill(oDT);

                if (oDT.Rows.Count > 0 && oDT.Rows[0][0] != DBNull.Value) { return true; }

                return false;
            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }


        }

        private static void Check_ComAcMeSeCas_ComAcMes(int iCompany, int iActivity, int iMes)
        {
            if (iCompany == 0 ) { throw new Exception("Input parameters are wrong! Company is 0!"); }
            if (iActivity == 0 ) { throw new Exception("Input parameters are wrong! Activity is 0!"); }
            if (iMes == 0) { throw new Exception("Input parameters are wrong! Mes is 0!"); }
        }

        private static void Check_ComAcMeSeCas_AllParameters(int iCompany, int iActivity, int iMes, int? iSector, int? iCase)
        {
            if (iCompany == 0 && iActivity == 0 && iMes == 0 && iSector == 0 && iCase == 0)
            { throw new Exception("Input parameters are wrong! Nothing to change!"); }
        }
        private static void Check_ComAcMeSeCas_SecAc(int iCompany, int iActivity, int iMes, int? iSector, int? iCase)
        {
            if ((iSector != 0 || iActivity != 0) && (iCompany == 0 && iMes == 0 && iCase == 0))
            { throw new Exception("Input parameters are wrong! Can't only give sector and activity that are different from 0!"); }
        }
    }
}
