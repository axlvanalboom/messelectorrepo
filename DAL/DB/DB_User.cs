﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
namespace DAL.DB
{
    public partial class clsDB
    {
        
        public static clsUser Get_User(string sUserId)
        {
            Check_UserId(sUserId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select Id, Email,PhoneNumber, usUserName, usOccupation,EmailConfirmed from AspNetUsers where Id = @Id";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);
                
                if (oDT.Rows.Count > 0)
                {
                    clsUser oUser;
                    oUser = new clsUser();
                    oUser.Id = (string)oDT.Rows[0][0];
                    oUser.Email = (oDT.Rows[0][1] == DBNull.Value) ? null : oDT.Rows[0][1].ToString();
                    oUser.PhoneNumber = (oDT.Rows[0][2] == DBNull.Value) ? null : oDT.Rows[0][2].ToString();
                    oUser.UserName = (oDT.Rows[0][3] == DBNull.Value) ? null : oDT.Rows[0][3].ToString();
                    oUser.Occupation = (oDT.Rows[0][4] == DBNull.Value) ? null : oDT.Rows[0][4].ToString();
                    oUser.EmailConfirmed = (oDT.Rows[0][5] == DBNull.Value) ? false : (bool)oDT.Rows[0][5];
                    return oUser;
                }
                else { return null; }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }
        public static clsUser Get_UserByEmail(string sUserEmail)
        {
            Check_UserId(sUserEmail);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select Id, Email,PhoneNumber, usUserName, usOccupation,EmailConfirmed from AspNetUsers where Email = @Email";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Email", sUserEmail));
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                if (oDT.Rows.Count > 0)
                {
                    clsUser oUser;
                    oUser = new clsUser();
                    oUser.Id = (string)oDT.Rows[0][0];
                    oUser.Email = (oDT.Rows[0][1] == DBNull.Value) ? null : oDT.Rows[0][1].ToString();
                    oUser.PhoneNumber = (oDT.Rows[0][2] == DBNull.Value) ? null : oDT.Rows[0][2].ToString();
                    oUser.UserName = (oDT.Rows[0][3] == DBNull.Value) ? null : oDT.Rows[0][3].ToString();
                    oUser.Occupation = (oDT.Rows[0][4] == DBNull.Value) ? null : oDT.Rows[0][4].ToString();
                    oUser.EmailConfirmed = (oDT.Rows[0][5] == DBNull.Value) ? false : (bool)oDT.Rows[0][5];
                    return oUser;
                }
                else { return null; }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static List<clsUser> Get_Users()
        {
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "select Id, Email,PhoneNumber, usUserName,usOccupation,EmailConfirmed  from AspNetUsers order by usUserName asc";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                SqlDataAdapter oDa = new SqlDataAdapter(oCmd);
                DataTable oDT = new DataTable();
                oDa.Fill(oDT);

                List<clsCase> oCases = new List<clsCase>();

                if (oDT.Rows.Count > 0)
                {
                    List<clsUser> oUsers = new List<clsUser>();

                    foreach (DataRow oRow in oDT.Rows)
                    {
                        clsUser oUser;
                        oUser = new clsUser();
                        oUser.Id = (string)oRow[0];
                        oUser.Email = (oRow[1] == DBNull.Value) ? null : oRow[1].ToString();
                        oUser.PhoneNumber = (oRow[2] == DBNull.Value) ? null : oRow[2].ToString();
                        oUser.UserName = (oRow[3] == DBNull.Value) ? null : oRow[3].ToString();
                        oUser.Occupation = (oRow[4] == DBNull.Value) ? null : oRow[4].ToString();
                        oUser.EmailConfirmed = (oDT.Rows[0][5] == DBNull.Value) ? false : (bool)oDT.Rows[0][5];

                        oUsers.Add(oUser);
                    }
                    return oUsers;

                }
                else { return new List<clsUser>(); ; }

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

        public static int Edit_User(clsUser oUser)
        {
            Check_Class(oUser); return Edit_User(oUser.Id, oUser.PhoneNumber, oUser.UserName, oUser.Occupation);
        }
        public static int Edit_User(string sUserId, string sPhoneNumber=null, string sUserName=null, string sOccupation=null)
        {
            Check_UserId(sUserId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQL = "update AspNetUsers set  ";

                object oP = "";
                if (sPhoneNumber != "")
                {
                    sSQL += " PhoneNumber = @PhoneNumber ";
                    if (sPhoneNumber == null) { oP = DBNull.Value; ; } else { oP = sPhoneNumber; }
                    if (sUserName != "" || sOccupation != "") { sSQL += ","; }
                }
                object oU = "";
                if (sUserName != "")
                {
                    sSQL += " usUserName =  @UserName ";
                    if (sUserName == null) { oU = DBNull.Value; ; } else { oU = sUserName; }
                    if (sOccupation != "") { sSQL += ","; }
                }
                object oO = "";
                if (sOccupation != "")
                {
                    sSQL += " usOccupation = @Occupation ";
                    if (sOccupation == null) { oO = DBNull.Value; ; } else { oO = sOccupation; }
                }

                sSQL += "  where Id = @ID;";

                SqlCommand oCmd = new SqlCommand(sSQL, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@ID", sUserId));
                oCmd.Parameters.Add(new SqlParameter("@PhoneNumber", oP));
                oCmd.Parameters.Add(new SqlParameter("@UserName", oU));
                oCmd.Parameters.Add(new SqlParameter("@Occupation", oO));
                return oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }

        }

        public static int Delete_User(string sUserId)
        {
            Check_UserId(sUserId);
            SqlConnection oCnn = new SqlConnection(Properties.Settings.Default.Connectionstring);
            try
            {
                oCnn.Open();

                string sSQl = "delete from AspNetUsers where Id = @Id";
                SqlCommand oCmd = new SqlCommand(sSQl, oCnn);
                oCmd.Parameters.Add(new SqlParameter("@Id", sUserId));
                return oCmd.ExecuteNonQuery();

            }
            catch (Exception ex) { throw ex; }
            finally { oCnn.Close(); }
        }

    }
    
}
